using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfCamController : Singleton<WolfCamController>
{
    [SerializeField] GameObject holder;
    [SerializeField] CameraController3D cameraController;
    bool wolfCam = false;
    float currentTime = 0;

    public void OnWolfCam(float time)
	{
        currentTime = time;

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(currentTime > 0)
		{
            currentTime -= Time.deltaTime;
            wolfCam = true;

        }
		else
		{
            wolfCam = false;

        }

        holder.SetActive(wolfCam);
        if(wolfCam)
		{
            CamSetWolf();

        }
    }
    void CamSetWolf()
    {

        for (int i = 0; i < GameManager.instance.GetPlayerSize(); i++)
        {
            eTeamType team = GameManager.instance.GetTeamFromPlayerIndex(i);
            if (team == eTeamType.Wolf)
            {

                cameraController.target = GameManager.instance.GetPlayer(i).transform;

            }
        }
    }
}
