using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using Sirenix.OdinInspector;

public class PlayerUIController : MonoBehaviour
{
    [SerializeField] GameObject chattingPannel;
    [SerializeField] Image chatBackground;
    [SerializeField] Text chattingText;
    [SerializeField] float width = 10f;
    [SerializeField] float height = 10f;
    [SerializeField] float minWidth = 200f;
    [SerializeField] float minHeight = 100f;
    [SerializeField] float chatY = 3f;
    float chatCool = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        chattingPannel.SetActive(chatCool > 0);
        if (chatCool > 0)
        {
            chattingPannel.transform.position = Camera.main.WorldToScreenPoint(transform.position + Vector3.up * chatY);
            chatCool -= Time.deltaTime;
        }
    }
    [Button("ShowChatting")]
    public void ShowChatting(string message)
    {
        chattingPannel.SetActive(true);
        chattingText.text = (message);
        float fwidth = Mathf.Max(minWidth, chattingText.preferredWidth + width);
        float fheight = Mathf.Max(minHeight, chattingText.preferredHeight + height);
        chatBackground.rectTransform.sizeDelta = new Vector2(fwidth, fheight);
        chatCool = 5f;
    }
}
