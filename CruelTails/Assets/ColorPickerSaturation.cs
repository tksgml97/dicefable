using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(ColorPickerSlider))]
public class ColorPickerSaturation : MonoBehaviour
{
    public ColorPickerSlider colorSlider;
    public Image satBackgroundImage;
    ColorPickerSlider satSlider;
    Material mat;
    
    // Start is called before the first frame update
    void Start()
    {
        satSlider = GetComponent<ColorPickerSlider>();
        mat = satBackgroundImage.material;
        OnColorChange();
    }

    // Update is called once per frame
    public void OnColorChange()
    {
        Color color = colorSlider.GetColor();
        satSlider.SetKey(1, color);
        mat.SetColor("_Color", color);
    }
}
