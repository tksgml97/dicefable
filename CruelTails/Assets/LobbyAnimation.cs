using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public enum LobbyAnimationState
{
	Main,
	Closet,
}

[RequireComponent(typeof(Animator))]
public class LobbyAnimation : Singleton<LobbyAnimation>
{
	int aniStateHash = Animator.StringToHash("State");
	
	Animator animator;
	LobbyAnimationState currentState;

	private void Start()
	{
		animator = GetComponent<Animator>();
	}

	[Button("SetState")]
	public void GoToState(LobbyAnimationState state)
	{
		currentState = state;
		animator.SetInteger(aniStateHash, (int)currentState);
	}
}
