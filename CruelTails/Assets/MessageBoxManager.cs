using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBoxManager : Singleton<MessageBoxManager>
{


	[SerializeField]
    private GameObject m_debugUiView;

    [SerializeField]
    private GameObject m_messageBoxPrefab;

    public delegate void messageBoxCallbackArg();


    public void ShowMessageBox(string message, messageBoxCallbackArg callback = null)
	{
        GameObject boxObject = Instantiate(m_messageBoxPrefab, m_debugUiView.transform);
        MessageBox messageBox = boxObject.GetComponent<MessageBox>();
        messageBox.SetText(message);
        if (callback != null) messageBox.SetCallback(callback);
    }
}
