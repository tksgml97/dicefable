using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MessageBox : MonoBehaviour
{
    [SerializeField]
    Text m_messageText;
    MessageBoxManager.messageBoxCallbackArg m_callback = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnClick()
	{
        m_callback?.Invoke();

        Destroy(gameObject);
    }

    public void SetText(string text)
	{
        m_messageText.text = text;
    }

    public void SetCallback(MessageBoxManager.messageBoxCallbackArg callback)
	{
        m_callback = callback;
    }
}
