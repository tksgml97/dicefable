using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;



public class Round3UI : MonoBehaviour
{
    [SerializeField] GameObject workText;
    public void UpdateWorkText(int playCount)
	{
        workText.SetActive(playCount > 0);
	}
}
