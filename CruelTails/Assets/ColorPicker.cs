using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{
	[SerializeField] ColorPickerSlider brightSlider;
	[SerializeField] ColorPickerSlider satSlider;
	[SerializeField] Image colorSampleImage;

	Color currentColor;
	// Start is called before the first frame update
	void Start()
	{
		OnChange();
	}

	public Color GetColor()
	{
		return currentColor;
	}

	public void OnChange()
	{
		currentColor = satSlider.GetColor() * brightSlider.GetColor();
		colorSampleImage.color = currentColor;
	}

}
