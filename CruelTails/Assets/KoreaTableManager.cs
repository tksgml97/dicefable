using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KoreaTableManager : MonoBehaviour
{
    [SerializeField] KoreaGameTable[] tabls;
    // Start is called before the first frame update
    void Start()
    {
		for (int i = 0; i < tabls.Length; i++)
		{
            KoreaGameManager.instance.AddTable(tabls[i]);
        }
    }
}
