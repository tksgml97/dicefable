using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class SkinColorSlot : MonoBehaviour
{
    public UnityEvent<Color> colorSelectEvent;
    [SerializeField] Color color;
    [SerializeField] Image buttonImage;
    // Start is called before the first frame update
    void Start()
    {
        ApplyImageColor();
    }
    [Button("Reload")]
    void ApplyImageColor()
	{
        buttonImage.color = color;
    }

    public void SelectColor()
	{
        colorSelectEvent?.Invoke(color);
    }

}
