using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageIconController : MonoBehaviour
{
    [SerializeField] Sprite on;
    [SerializeField] Sprite off;
    [SerializeField] Image[] slots;
    [SerializeField] float iconDistance = 50f;
    void Start()
    {
        
    }

    public void ShowPageIcon(int index, int max)
	{
		for (int i = 0; i < slots.Length; i++)
		{
            slots[i].gameObject.SetActive(i < max);
            slots[i].sprite = i == index ? on : off;

            float localX = (-(max-1) * iconDistance * 0.5f) + (i * iconDistance );
            slots[i].rectTransform.localPosition = new Vector3(localX, 0, 0);

		}
	}

}
