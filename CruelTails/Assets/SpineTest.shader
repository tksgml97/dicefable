Shader "MyoMyo/PCSpine" {
	Properties{

		[HDR] _MaskColor("Mask Color", Color) = (1,1,0,1)
		_Cutoff("Shadow alpha cutoff", Range(0,1)) = 0.1
		[NoScaleOffset] _MainTex("Main Texture", 2D) = "black" {}
		[Toggle(_STRAIGHT_ALPHA_INPUT)] _StraightAlphaInput("Straight Alpha Texture", Int) = 0
		[HideInInspector] _StencilRef("Stencil Reference", Float) = 1.0
		[HideInInspector][Enum(UnityEngine.Rendering.CompareFunction)] _StencilComp("Stencil Comparison", Float) = 8 // Set to Always as default

		// Outline properties are drawn via custom editor.
		[HideInInspector] _OutlineWidth("Outline Width", Range(0,8)) = 3.0
		[HideInInspector] _OutlineColor("Outline Color", Color) = (1,1,0,1)
		[HideInInspector] _OutlineReferenceTexWidth("Reference Texture Width", Int) = 1024
		[HideInInspector] _ThresholdEnd("Outline Threshold", Range(0,1)) = 0.25
		[HideInInspector] _OutlineSmoothness("Outline Smoothness", Range(0,1)) = 1.0
		[HideInInspector][MaterialToggle(_USE8NEIGHBOURHOOD_ON)] _Use8Neighbourhood("Sample 8 Neighbours", Float) = 1
		[HideInInspector] _OutlineMipLevel("Outline Mip Level", Range(0,3)) = 0
	}

		SubShader{

			//Cull Back
			//Tags { "RenderType" = "Opaque" }





			Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "PreviewType" = "Plane" }
			Fog { Mode Off }
			Cull Off
			ZWrite Off
			Blend One OneMinusSrcAlpha
			Lighting Off


			Pass
			{
				Stencil {
				Ref 1
				Comp always
				Pass Replace
				ZFail Keep
			}
				Name "Normal"

				CGPROGRAM
				#pragma shader_feature _ _STRAIGHT_ALPHA_INPUT
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				#include "CGIncludes/Spine-Common.cginc"
				sampler2D _MainTex;

				struct VertexInput {
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float4 vertexColor : COLOR;
					float4 customColor : Tangent;
				};

				struct VertexOutput {
					float4 pos : SV_POSITION;
					float2 uv : TEXCOORD0;
					float4 vertexColor : COLOR;
				};

				VertexOutput vert(VertexInput v) {
					VertexOutput o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					float3 vRGB = v.vertexColor.rgb * (1+v.customColor.r);
					o.vertexColor = float4(vRGB, v.vertexColor.a);	
					return o;
				}
				float _test;

				float4 frag(VertexOutput i) : SV_Target {
					float4 texColor = tex2D(_MainTex, i.uv);

					#if defined(_STRAIGHT_ALPHA_INPUT)
					texColor.rgb *= texColor.a;
					#endif
					float4 col = (texColor * i.vertexColor);

					return col;
					//return float4((col*_test).rgb , min((col * _test).a, 1));

					//return lerp(col, float4(1,1,1,1) * texColor.a, _test-1);
				}
				ENDCG
			}


			ZWrite Off ZTest Always
			Pass
			{

				Stencil{
					Ref 0
					Comp equal
				}

				Name "Back"
				Tags { "LightMode" = "UniversalForward" }

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				fixed4 _MaskColor;



				fixed4 frag(v2f i) : SV_Target
				{

					float4 texColor = tex2D(_MainTex, i.uv);

					return lerp(texColor, _MaskColor, 0.95f) * texColor.a;
				}
				ENDCG
			}

			Pass {
				Name "Caster"
				Tags { "LightMode" = "ShadowCaster" }
				Offset 1, 1
				ZWrite On
				ZTest LEqual

				Fog { Mode Off }
				Cull Off
				Lighting Off

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_shadowcaster
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"
				sampler2D _MainTex;
				fixed _Cutoff;

				struct VertexOutput {
					V2F_SHADOW_CASTER;
					float4 uvAndAlpha : TEXCOORD1;
				};

				VertexOutput vert(appdata_base v, float4 vertexColor : COLOR, float4 customColor : Tangent) {
					VertexOutput o;
					o.uvAndAlpha = v.texcoord;

					o.uvAndAlpha.a = (int)vertexColor.a;
					if (customColor.g == 1)
					{
						o.uvAndAlpha.a = 0;
					}
					TRANSFER_SHADOW_CASTER(o)
					return o;
				}

				float4 frag(VertexOutput i) : SV_Target {
					fixed4 texcol = tex2D(_MainTex, i.uvAndAlpha.xy);
					clip(texcol.a* i.uvAndAlpha.a - _Cutoff);
					SHADOW_CASTER_FRAGMENT(i)
				}
				ENDCG
			}
		}
			CustomEditor "SpineShaderWithOutlineGUI"
}
