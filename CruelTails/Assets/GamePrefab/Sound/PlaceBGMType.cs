using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlaceType
{
    NORMAL, DEAD
}

public class PlaceBGMType : MonoBehaviour
{

    public PlaceType placeType;

    private void Awake()
    {
       
        this.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (placeType == PlaceType.DEAD && other.gameObject.layer == 3)
        {

            StopCoroutine(SoundFadeOut());
            StartCoroutine(SoundFadeIn()); 
            //SoundInstance.Instance.rtpc_Place.SetValue(SoundInstance.Instance.gameObject, 0);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (placeType == PlaceType.DEAD && other.gameObject.layer == 3)
        {
            StopCoroutine(SoundFadeIn());
            StartCoroutine(SoundFadeOut());
            //SoundInstance.Instance.rtpc_Place.SetValue(SoundInstance.Instance.gameObject, 1);
        }
    }


    // sound fade in/out
    IEnumerator SoundFadeIn()
    {
        for (float f = 1f; f > 0; f -= 0.1f)
        {
            SoundInstance.Instance.rtpc_Place.SetValue(SoundInstance.Instance.gameObject, f);
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }


    IEnumerator SoundFadeOut()
    {
        for (float f = 0f; f < 1f; f += 0.1f)
        {
            SoundInstance.Instance.rtpc_Place.SetValue(SoundInstance.Instance.gameObject, f);
            yield return new WaitForSeconds(0.05f);
        }
        yield return null;
    }
}
