using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinColorSelector : MonoBehaviour
{
	Color color;
    public void ChangeColor(Color color)
	{
		this.color = color;
	}

	public Color GetColor()
	{
		return color;
	}
}
