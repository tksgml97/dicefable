using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyUIController : MonoBehaviour
{
    [SerializeField] ReadyUserSlotUI[] userSlots;
    // Start is called before the first frame update
    void Start()
    {

    }
	private void Update()
	{
	}
	public void SetReady(int index, bool state)
    {
        userSlots[index].SetReady(state);
    }
    public void SetDeath(int index, bool state)
    {
        userSlots[index].SetDeath(state);
    }

    public void SetName(int index, string playerName)
	{
        userSlots[index].SetName(playerName);
    }
    public void SetSkin(int index, PlayerSpineSkinData skinData)
    {
        userSlots[index].SetSkin(skinData);
        Debug.Log("Set Skin");
    }

}
