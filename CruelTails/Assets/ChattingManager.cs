using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChattingManager : MonoBehaviour
{
    [SerializeField] InputField inputField;
    bool onChattingInput = false;
    // Start is called before the first frame update
    void Start()
    {

        OffChattingInput();
    }

    // Update is called once per frame
    void Update()
    {
        if(onChattingInput)
		{
            if(inputField.isFocused == false)
			{
                OffChattingInput();

            }
		}
		else
		{
            if(Input.GetKeyDown(KeyCode.Return))
			{
                OnChattingInput();
			}
		}
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OffChattingInput();
        }
    }
    void OnChattingInput()
    {
        onChattingInput = true;
        inputField.gameObject.SetActive(true);
        inputField.text = "";
        inputField.Select();
        inputField.ActivateInputField();

    }
    void OffChattingInput()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if(inputField.text != "")
			{
                NetworkConnect.instance.SendMessage(MessageType.chat, inputField.text);
			}
        }
        onChattingInput = false;
        inputField.gameObject.SetActive(false);
    }

}
