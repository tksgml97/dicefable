using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaSkill_SackAttack : MonoBehaviour
{
    [SerializeField] Santa santa;
    SpriteRenderer attackRange;
    BoxCollider attackCollider;

    //범위표시 ->(attackDelay초 대기)-> 애니메이션 시작-> (attackAnimationTime초 대기) -> 데미지 적용
    public float attackDelay = 0.7f; // 공격 선딜레이(내리칠 준비하는 애니메이션)

    [SerializeField] float attackTime = 0.5f; // 공격 지속 시간
    [SerializeField] float attackAnimationTime = 0.5f; // 공격 지속 시간

    string skillanimName = "All_Attack_Sack";
    [field: SerializeField] public int sackDamage { get; set; } = 30;

    [SerializeField] float cameraShakeTime = 1.0f;
    [SerializeField] float cameraShakePower = 1f;
    private void Awake()
    {
        attackRange = GetComponent<SpriteRenderer>();
        attackCollider = GetComponent<BoxCollider>();
        attackRange.enabled = false;
        attackCollider.enabled = false;
    }
    public void AttackActive()
    {
        Debug.Log("SackAttack");
        attackRange.enabled = true;
        // 애니메이션
        Invoke(nameof(playAni), attackDelay );
    }

    void playAni()
    {
        Debug.Log($"play santa ani is {skillanimName}");

        santa.SantaSkillAnimation(skillanimName);
        Invoke(nameof(BoxOn), attackAnimationTime);
    }

    void BoxOn()
    {
        attackCollider.enabled = true;
        //카메라 쉐이크
        CameraController3D.instance.shakeTIme = cameraShakeTime;
        CameraController3D.instance.shakePower = cameraShakePower;
        Invoke(nameof(BoxOff), attackTime);
    }

    void BoxOff()
    {
        attackRange.enabled = false;
        attackCollider.enabled = false;
        //Debug.Log("End: SackAttack ");
    }


    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.layer == 3)
        {
            //santa.Text(true);
            //if (ScrollMapManager.Instance.canDamage)
            {
                //Debug.Assert(false, "Sack Damage");
                other.GetComponentInChildren<PlayerScrollMap>().Damaged(sackDamage);
                //other.GetComponentInChildren<PlayerScrollMap>().canDamaged = false;
                //other.GetComponentInChildren<PlayerScrollMap>().origin_layer = other.gameObject.layer;
            }
                
        }
        else if (other.gameObject.layer == 6)
        {
            RemotePlayer r = other.GetComponent<RemotePlayer>();
            if (r && !r.skillHit)
            {
                //r.gameObject.layer = 2; // ignore collider
                r.SkillHit();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
    }
}
