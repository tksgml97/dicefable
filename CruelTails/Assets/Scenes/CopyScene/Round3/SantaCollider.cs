using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaCollider : MonoBehaviour
{
    [SerializeField] Santa santa;
    [SerializeField] float Damage = 10.0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 3 && santa.isStart)
        {
            //if (ScrollMapManager.Instance.canDamage)
            {
                PlayerController pc = other.GetComponent<PlayerController>();
                PlayerScrollMap p = other.GetComponentInChildren<PlayerScrollMap>();
                //if (p.canDamaged)
                //{
                    pc.Pushed(p.noDamageTime);
                    //p.origin_layer = other.gameObject.layer;

                    p.pushed = true;
                    p.Damaged(Damage);
                    //p.canDamaged = false;
                //}
            }
        }
        else if (other.gameObject.layer == 6 && santa.isStart) // remote player
        {
            RemotePlayer r = other.GetComponent<RemotePlayer>();
            //PlayerScrollMap p = other.GetComponentInChildren<PlayerScrollMap>();
            if (r && !r.skillHit)
            {
                r.pushed = true;
                //p.origin_layer = other.gameObject.layer;
                r.PushedCool();
                //r.gameObject.layer = 2; // ignore collider
                r.SkillHit();
            }
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 3)
        {
            //if (ScrollMapManager.Instance.canDamage)
            {
                PlayerController pc = other.GetComponent<PlayerController>();
                PlayerScrollMap p = other.GetComponentInChildren<PlayerScrollMap>();
                //if (p.canDamaged)
                //{
                    p.Damaged(Damage);
                    pc.Pushed(p.noDamageTime);
                    //p.origin_layer = other.gameObject.layer;

                    p.pushed = true;
                    
                    //p.canDamaged = false;
                //}
            }
        }
        else if (other.gameObject.layer == 6) // remote player
        {
            RemotePlayer r = other.GetComponent<RemotePlayer>();
            //PlayerScrollMap p = other.GetComponentInChildren<PlayerScrollMap>();
            if (r && !r.skillHit)
            {
                r.pushed = true;
                //p.origin_layer = other.gameObject.layer;
                r.PushedCool();
                //r.gameObject.layer = 2; // ignore collider
                r.SkillHit();
            }
        }
    }
}
