using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum GiftType
{
    small, big
}

public class ScrollMapObject : MonoBehaviour
{
    [SerializeField] AK.Wwise.Event sound_boxAttack = null;
    //[SerializeField] GiftType giftType = GiftType.small;
    [SerializeField] GiftDestroy gift;
    public eParticleType hitParticleType = eParticleType.HitStar;
    public int id { get; set; } = -1;
    [SerializeField] int attackCount = 1;
    BoxCollider box;
    [SerializeField] SpriteRenderer spriter;
    //[SerializeField] SkeletonAnimation skeletonAnim;
    private void Awake()
    {
        box = GetComponent<BoxCollider>();
        
    }
    public void Attacked()
    {
        //Debug.Log("gift attacked, 남은 카운트 : " + (attackCount - 1));
        //gift.Hit();

        if (--attackCount <= 0) // 장애물 파괴
        {
            Debug.Log(name + " Destroyed id : " + id);
            gift.Destroy();
            sound_boxAttack?.Post(gameObject);
            if (ScrollMapObjManager.Instance)
            {
                //if(ScrollMapObjManager.Instance.skillObjects[id] == null)
                //{
                //    Debug.Log(id + " skillObject is Null");
                //}
                //else
                //{
                    // 참조 해제
                    ScrollMapObjManager.Instance.skillObjects[id] = null;
                //}
            }
            id = -1;

            box.enabled = false;
            spriter.enabled = false;
            //Destroy(gameObject);

        }
        else
        {
            Debug.Log("gift attacked, 남은 카운트 : " + (attackCount));
            gift.Hit();
        }
    }
}
