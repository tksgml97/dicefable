using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallGift : GiftDestroy
{
    private void Awake()
    {
        if (!ScrollMapObjManager.Instance.showSmallGiftTutorial)
        {
            if(tutorial_mouse)
                tutorial_mouse.SetActive(false);
            
        }  
        else
        {
            ScrollMapObjManager.Instance.showSmallGiftTutorial = false;
        }
    }
    public override void Hit()
    {
            skeletonAnim.AnimationState.AddAnimation(0, hitAniName, false, 0);

    }
    public override void Destroy()
    {
        skeletonAnim.AnimationState.AddAnimation(0, destAniName, false, 0);
    }
}
