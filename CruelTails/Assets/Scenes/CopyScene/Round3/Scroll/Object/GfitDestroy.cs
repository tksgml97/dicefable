using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class GiftDestroy : MonoBehaviour
{
    [SerializeField] protected GameObject tutorial_mouse;
    //ScrollMapObject scrollMapObj;
    [SerializeField] protected SkeletonAnimation skeletonAnim;
    [SpineAnimation] public string destAniName;
    [SpineAnimation] public string hitAniName;
    public virtual void Destroy() { }
    public virtual void Hit() { }


    // Update is called once per frame
    void Update()
    {
        
    }

}
