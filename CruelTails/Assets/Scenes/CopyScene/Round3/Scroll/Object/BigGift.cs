using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigGift : GiftDestroy
{
    [SerializeField] ParticleSystem rocket;
    [SerializeField] float rocketDelay = 0.5f;
    [SerializeField] BoxCollider box;
    private void Awake()
    {
        if (!ScrollMapObjManager.Instance.showBigGiftTutorial)
            tutorial_mouse.SetActive(false);
        else
            ScrollMapObjManager.Instance.showBigGiftTutorial = false;
    }

    public override void Hit()
    {
        skeletonAnim.AnimationState.SetAnimation(0, hitAniName, false);
        ParticleManager.instance.playEffect(transform.position, Quaternion.identity, eParticleType.HitStar);
        Debug.Log("biggift hit");
    }
    public override void Destroy()
    {
        Debug.Log("big Destroy");
        skeletonAnim.AnimationState.SetAnimation(0, destAniName, false);
        Invoke(nameof(PlayParticle), rocketDelay);
    }

    void PlayParticle()
    {
        //foreach (var r in rocket)
        //    r.Play();
        rocket.Play();
        
    }
}
