using SanNetwork;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Types;
using UnityEngine.UI;

public enum DiceActionState {
    Disable,
    Active
}

public class DiceAction : MonoBehaviour
{
    [SerializeField] GameObject dice_prefab;
    [HideInInspector] public int prefab_number;
    [SerializeField] float ActSpeed = 1.5f;
    [SerializeField] float ActEndTime = 0.5f;
    [SerializeField] Transform WaitObj;
    [SerializeField] AK.Wwise.Event sound_diceShaker = null;
    public int score = 0;
    public int dice_number = 0;
    public DiceActionState state = DiceActionState.Active;

    void Start() {
        GameManager.playerController.spaceHoldEvent += MissionEvent;
    }
    static DiceAction da = null;
    //[SerializeField]SpriteRenderer sprite;
    void MissionEvent() {
        if (state == DiceActionState.Disable) return;
        if (GameManager.playerController.state == PlayerState.Hit) return;
        
        Vector3 vec = WaitObj.position - GameManager.playerController.transform.position;
        vec.y = 0;
        if (da is null && (vec.magnitude < 1f)) {
            //if(sprite) sprite.color = Color.red;
            da = this;
            if (R4GameManager.instance.IsLocalPlay) {
                StartAct(GameManager.playerController.GetComponent<Transform>(), true);
            }
            if(NetworkConnect.instance)
                NetworkConnect.instance.SendTakeDice(dice_number);
            
        }
    }

    public void StartAct(Transform target, bool is_local = false) {
        state = DiceActionState.Disable;
        StartCoroutine(dice_act_run(target, score, is_local));

        R4GameManager.instance.scores[is_local ? 0 : 1].Push(this);
        sound_diceShaker?.Post(gameObject);

        if (NetworkConnect.instance is null) R4GameManager.instance.dice_stack.Push(dice_number);
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    IEnumerator dice_act_run(Transform target, int score, bool is_local = false) {
        var dice = Instantiate(dice_prefab, target);
        dice.transform.localPosition = Vector3.up * R4GameManager.instance.diceYoffset;
        yield return new WaitForSeconds(ActSpeed);
        dice.transform.rotation = Quaternion.Euler(40, 0, 0);
        yield return new WaitForSeconds(ActEndTime * 1.3f);
        dice.GetComponentInChildren<Text>().text = score.ToString();
        yield return new WaitForSeconds(ActEndTime);
        R4GameManager.instance.UpdateScore();
        Destroy(dice);
        if(is_local) da = null;
    }
}
