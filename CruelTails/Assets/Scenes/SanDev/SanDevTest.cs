using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

public class SanDevTest : MonoBehaviour
{
    NetworkModule module;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void StartNetwork()
	{
        Debug.Log("Start NetworkModule");
        module = new NetworkModule();
        module.Start(51235);



        module.BindLogCallback(OnLog);
        module.BindMessageCallback(OnRecv);

        module.ConnectToServer("127.0.0.1");

        Debug.Log("End NetworkModule");
    }

    public static int OnLog(int value1, string text1)
    {
        Debug.Log(value1 + ":" + text1);
        return value1;
    }

    public static void OnRecv(IntPtr buf, int len, int sessionId)
    {
        Debug.Log("proc packet" + len);

        byte[] managedArray = new byte[len];
        Marshal.Copy(buf, managedArray, 0, len);

        ByteBuffer buffer = new ByteBuffer();

        buffer.WriteBytes(managedArray);



        return;
    }
}
