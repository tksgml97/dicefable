using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
public class LoadingUI : MonoBehaviour
{
    [SerializeField] Text titleText;
    [SerializeField] Text chapterText;
    [SerializeField] Text descriptionText;
    [SerializeField] Image LoadingBar;
    [SerializeField] ReadyUIController readyUI;
    [SerializeField] GameObject view;
    [SerializeField] GameObject dummyClear;
    [SerializeField] PageIconController iconController;
    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] VideoClip[] videos;
    [SerializeField] GameObject R3_Sample_Img;
    // Start is called before the first frame update

    int currentPage;
    List<int> currentRoundPages = new List<int>();

    public bool canReady { get; set; } = false;

    void Start()
    {
        SetRound(eGameRound.Round1);
        SceneLoader.instance.loadingUpdateEvent += UpdateLoadingUI;
    }

	private void Update()
	{
		//if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return) || 
        if(Input.GetKeyDown(KeyCode.Space))
		{
            if(canReady && LoadingBar.fillAmount >= 1)
            {
                NetworkConnect.instance.SendReady();
                canReady = false;
            }
            
        }
	}
    public void ButtonReady()
	{

        NetworkConnect.instance.SendReady();
    }

    public void SetData(List<PlayerSpineSkinData> skins, List<string> names)
	{
		for (int i = 0; i < skins.Count; i++)
		{
            readyUI.SetSkin(i, skins[i]);
        }
        for (int i = 0; i < names.Count; i++)
        {
            readyUI.SetName(i, names[i]);
        }
    }


    public void Ready(int index)
    {
        //canReady = false;
        readyUI.SetReady(index, true);
        // SetLoadingView(false);
    }

    public void SetLoadingView(bool state)
	{
        
        view.SetActive(state);
        if(state == true)
		{
            for (int i = 0; i < NetworkGameManager.roomMaxSize; i++)
            {
                readyUI.SetReady(i, false);
            }
            for (int i = 0; i < NetworkGameManager.roomMaxSize; i++)
            {
                var info = NetworkGameManager.instance.GetPlayerInfo(i);
                readyUI.SetDeath(i, info.isDIe);
            }
        }
        canReady = state;
        Debug.Log("isready: " + canReady);
    }

    void UpdateLoadingUI(float progress)
	{
        //Debug.Log("progress : "+progress);
        LoadingBar.fillAmount = progress;

    }

	public void SetRound(eGameRound round)
	{
        R3_Sample_Img?.SetActive(false);
        string title = "";
        string chapter = "";
        string description = "";
        currentRoundPages.Clear();
        currentPage = 0;
        dummyClear.SetActive(false);
        switch (round)
        {
            case eGameRound.Round1:
                title = GameDataLoader.instance.GetLoadingRule(101).m_roundTItle;
                chapter = "Chapter 1";
                description = GameDataLoader.instance.GetLoadingRule(101).m_ruleScript;
                currentRoundPages.Add(101);
                currentRoundPages.Add(102);
                currentRoundPages.Add(103);
                break;
            case eGameRound.Round2:
                title = GameDataLoader.instance.GetLoadingRule(201).m_roundTItle;
                chapter = "Chapter 2";
                description = GameDataLoader.instance.GetLoadingRule(201).m_ruleScript;
                currentRoundPages.Add(201);
                currentRoundPages.Add(202);
                currentRoundPages.Add(203);
                
                break;
            case eGameRound.Round3:
                title = GameDataLoader.instance.GetLoadingRule(301).m_roundTItle;
                chapter = "Chapter 3";
                description = GameDataLoader.instance.GetLoadingRule(301).m_ruleScript;
                currentRoundPages.Add(301);
                currentRoundPages.Add(302);
                currentRoundPages.Add(303);
                break;

            case eGameRound.Round4:
            
                    R3_Sample_Img?.SetActive(true);
                title = "ũ����";
                chapter = "Chapter 4";
                description = "";
                break;
        }


		titleText.text = title;
        chapterText.text = chapter;
        descriptionText.text = description;
        ShowPage();
    }
    public void PrevPageAndShow()
    {
        currentPage--;
        if (currentPage == -1)
            currentPage = currentRoundPages.Count-1;
        ShowPage();
    }
    public void NextPageAndShow()
	{
        currentPage++;
        if (currentPage == currentRoundPages.Count)
            currentPage = 0;
        ShowPage();
	}
    void ShowPage()
	{
        if (currentRoundPages.Count <= currentPage) return;
        int pageNum = currentRoundPages[currentPage];
        LoadingRule loadingRule = GameDataLoader.instance.GetLoadingRule(pageNum);

        string title = loadingRule.m_roundTItle;
        string description = loadingRule.m_ruleScript;

        titleText.text = title;
        descriptionText.text = description.Replace("<br>", "\n");
        iconController.ShowPageIcon(currentPage, currentRoundPages.Count);

        int videoIndex = 0;
        switch (pageNum)
        {
            case 101:
                videoIndex = 0;
                break;

            case 102:
                videoIndex = 1;
                break;

            case 103:
                videoIndex = 2;
                break;

            case 201:
                videoIndex = 3;
                break;

            case 202:
                videoIndex = 4;
                break;

            case 203:
                videoIndex = 5;
                break;

            case 301:
                videoIndex = 6;
                break;

            case 302:
                videoIndex = 7;
                break;

            case 303:
                videoIndex = 8;
                break;
        }
        videoPlayer.clip = videos[videoIndex];
    }
    
}
