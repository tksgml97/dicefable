using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class LobbyUIManager : Singleton<LobbyUIManager>
{
    bool isMatching = false;
    public Text startButton;
    [HideInInspector] public string savedName;
    public InputField nameInputField;

    public PlayerSpineSkinUpdator[] matchPlayers;

    public int personality;
    
    float cooldown = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

	private void Update()
	{
        if (cooldown > 0) cooldown -= Time.deltaTime;

    }

	public void GameStart()
    {
        if (cooldown > 0) return;
        cooldown = 0.5f;
        if (isMatching)
        {
            MatchCancel();
        }
        else
        {
            MatchStart();
        }
    }
    public void ClearMatchPlayerList()
	{

        for (int i = 0; i < matchPlayers.Length; i++)
        {
            matchPlayers[i].gameObject.SetActive(false);
        }
    }
    public void MatchCancel()
	{
        if (isMatching)
        {
            startButton.text = "��Ī";
            NetworkConnect.instance.MatchCancel();
            isMatching = false;
            ClearMatchPlayerList();
        }
    }
    public void MatchStart()
    {
        if (!isMatching)
        {
            startButton.text = "��Ī���";
            NetworkConnect.instance.MatchStart();
            isMatching = true;
        }
    }
    public void SetPersonality(int _personality) {
        personality = _personality;
    }
}
