using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScaleCollider : MonoBehaviour
{
    [SerializeField] float addSize = 3f;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Player"))
        {
            return;
        }
        CameraController3D.instance.sizeUp = addSize;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Player"))
        {
            return;
        }
        CameraController3D.instance.sizeUp = 0;
    }
}