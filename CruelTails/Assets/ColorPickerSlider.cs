using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class ColorPickerSlider : MonoBehaviour
{
    Slider slider;
    public Color[] colorKeys;
    // Start is called before the first frame update
    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    public Color GetColor()
	{

        int keyLen = colorKeys.Length-1;
        int i = (int)(slider.value * keyLen);
        i = Mathf.Min(i, keyLen-1);
        Color lp = colorKeys[i];
        Color rp = colorKeys[i + 1];

        Color color = Color.Lerp(lp, rp, slider.value * keyLen - i);
        return color;
    }
    public void SetKey(int index, Color color)
	{
        if (index < 0) return;
        if (index >= colorKeys.Length) return;

        colorKeys[index] = color;
	}
}
