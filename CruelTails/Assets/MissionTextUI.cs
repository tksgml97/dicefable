using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionTextUI : MonoBehaviour
{
    [SerializeField] GameObject claerImage;
	[SerializeField] Text mission;
	public Mission missionInst;
    // Start is called before the first frame update
    public void SetText(string text)
	{
		mission.text = text;
	}
	public void SetColor(Color color)
	{
		mission.color = color;
	}


	public void SetClear(bool set)
	{
		claerImage.SetActive(set);
	}
}
