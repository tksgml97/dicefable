using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Spine.Unity;
using Spine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class DoorScreenController : MonoBehaviour
{
    [SerializeField] SkeletonGraphic doorSpine;
    [SpineAnimation] public string open;
    public UnityEvent startOpen;
    public UnityEvent endOpen;

    [SpineAnimation] public string close;
    public UnityEvent startClose;
    public UnityEvent endClose;

    // Start is called before the first frame update
    void Start()
    {

    }

    [Button("Open")]

    public void Open()
	{
        TrackEntry te = doorSpine.AnimationState.SetAnimation(0, open, false);
        startOpen?.Invoke();
        te.Complete += delegate
        {
            endOpen?.Invoke();
        };
    }
    [Button("Close")]
    public void Close()
    {
        TrackEntry te = doorSpine.AnimationState.SetAnimation(0, close, false);
        startClose?.Invoke();
        te.Complete += delegate
        {
            endClose?.Invoke();
        };
    }

}
