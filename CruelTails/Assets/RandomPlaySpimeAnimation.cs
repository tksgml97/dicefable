using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Sirenix.OdinInspector;

public class RandomPlaySpimeAnimation : MonoBehaviour
{
    [SerializeField] float randomTime = 10;
    [SerializeField] SkeletonAnimation[] spines;
    // Start is called before the first frame update
    void Start()
    {
        SetRandom();
    }

    [Button("Randomize")]
    public void SetRandom()
	{
        foreach (var item in spines)
        {
            if (item == null) continue;
            item.AnimationState.Update(Random.Range(0f, randomTime));
        }
    }
}
