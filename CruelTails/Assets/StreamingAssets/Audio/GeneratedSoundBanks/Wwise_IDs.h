/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID A_REPAIRWORK = 3091344409U;
        static const AkUniqueID A_WORKCOMPLETE = 1162321659U;
        static const AkUniqueID AMB_KOREA = 505429160U;
        static const AkUniqueID AMBCHANGE = 809710687U;
        static const AkUniqueID B_BOXATTACK = 238736995U;
        static const AkUniqueID B_TREEBROKEN = 1215056935U;
        static const AkUniqueID CUT_SCENE_CHANGE = 3227563421U;
        static const AkUniqueID DICESHAKER = 691946944U;
        static const AkUniqueID FOURROUNDAMB = 917134565U;
        static const AkUniqueID INDOORAMB = 323640748U;
        static const AkUniqueID MAIN = 3161908922U;
        static const AkUniqueID MISSION_COMPLETE = 295066189U;
        static const AkUniqueID MISSION_GAUGE = 966641873U;
        static const AkUniqueID ONEROUND_PLAY = 2296566906U;
        static const AkUniqueID ONEROUND_STOP = 3737122436U;
        static const AkUniqueID OUTDOORAMB = 3045138681U;
        static const AkUniqueID PLAY_2ROUND_DEATH = 1765862395U;
        static const AkUniqueID PLAY_3R_2_PLAY = 1081346035U;
        static const AkUniqueID PLAY_3R_2_STOP = 211467993U;
        static const AkUniqueID PLAY_3R_PLAY = 1530679134U;
        static const AkUniqueID PLAY_3R_STOP = 2568675104U;
        static const AkUniqueID PLAY_4DEATH = 638956060U;
        static const AkUniqueID PLAY_4ROUND = 1185057898U;
        static const AkUniqueID PLAY_CRUELTALES_KOREAMAP = 2780148019U;
        static const AkUniqueID PLAY_CRUELTALES_MAIN = 1450347440U;
        static const AkUniqueID SOUND_AMB_KOREASTAGE_BIRD = 2934225628U;
        static const AkUniqueID SOUND_PC_BASIC_ATK = 33960370U;
        static const AkUniqueID SOUND_PC_BASIC_ATKED = 3030213581U;
        static const AkUniqueID SOUND_PC_DIRT_RUN = 3120952712U;
        static const AkUniqueID SOUND_PC_DIRT_WALK = 1652076318U;
        static const AkUniqueID SOUND_PC_INTERACTION_ITEMOFFPLAYER = 315925317U;
        static const AkUniqueID SOUND_PC_INTERACTION_ITEMOFFTABLE = 2344599758U;
        static const AkUniqueID SOUND_PC_INTERACTION_ITEMONPLAYER = 40253707U;
        static const AkUniqueID SOUND_PC_INTERACTION_ITEMONTABLE = 1050909296U;
        static const AkUniqueID SOUND_PC_INTERACTION_PORTAL = 1530796912U;
        static const AkUniqueID SOUND_PC_INTERACTION_REDDEATH = 1650323083U;
        static const AkUniqueID SOUND_PC_STONE_RUN = 3171484990U;
        static const AkUniqueID SOUND_PC_STONE_WALK = 2449100384U;
        static const AkUniqueID SOUND_PC_WOLF_ATK = 969269994U;
        static const AkUniqueID SOUND_PC_WOLF_CHANGE = 3038262284U;
        static const AkUniqueID SOUND_PC_WOLF_FAINT = 3912017786U;
        static const AkUniqueID SOUND_PC_WOOD_RUN = 2678247032U;
        static const AkUniqueID SOUND_PC_WOOD_WALK = 2282887886U;
        static const AkUniqueID SOUND_UI_CHAT = 4124986852U;
        static const AkUniqueID SOUND_UI_GAME_COUNTDOWN = 2423973992U;
        static const AkUniqueID SOUND_UI_GAME_GETOUT = 1477531195U;
        static const AkUniqueID SOUND_UI_GAME_OFFMINIMAP = 3264723155U;
        static const AkUniqueID SOUND_UI_GAME_ONMINIMAP = 2926528253U;
        static const AkUniqueID SOUND_UI_GAME_ROULETTE = 3001137003U;
        static const AkUniqueID SOUND_UI_MAIN_DELETE = 1586525917U;
        static const AkUniqueID SOUND_UI_MAIN_HOVER = 3050023194U;
        static const AkUniqueID SOUND_UI_MAIN_SELECTION = 4291770614U;
        static const AkUniqueID TWOROUND_PLAY = 2405436624U;
        static const AkUniqueID TWOROUND_STOP = 753918578U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace AMB
        {
            static const AkUniqueID GROUP = 1117531639U;

            namespace SWITCH
            {
                static const AkUniqueID SOUND_AMB_KOREASTAGE_BUG = 1781907809U;
                static const AkUniqueID SOUND_AMB_KOREASTAGE_FLOWINGWATER = 751608088U;
                static const AkUniqueID SOUND_AMB_KOREASTAGE_IRONPOT = 1078693128U;
                static const AkUniqueID SOUND_AMB_NONE = 744418432U;
            } // namespace SWITCH
        } // namespace AMB

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BGM_VOLUME = 341651998U;
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID SFX_VOLUME = 1564184899U;
        static const AkUniqueID SPACE = 4164838345U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID TIMER = 3920142940U;
        static const AkUniqueID ZONE = 832057375U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID NEW_SOUNDBANK = 4072029455U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MOTION_FACTORY_BUS = 985987111U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
