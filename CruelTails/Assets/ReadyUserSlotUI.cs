using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ReadyUserSlotUI : MonoBehaviour
{
    [SerializeField] GameObject readyOnObject;
    [SerializeField] GameObject readyOffObject;
    [SerializeField] GameObject deathObject;
    [SerializeField] PlayerSpineSkinUpdator spine;
    [SerializeField] Text playerName;
    bool isDeath;
    bool isReady;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetDeath(bool state)
	{
        isDeath = state;
        deathObject.SetActive(isDeath);
    }

    public void SetReady(bool state)
    {
        isReady = state;
        readyOnObject.SetActive(isReady);
        readyOffObject.SetActive(!isReady);
    }

    public void SetSkin(PlayerSpineSkinData skinData)
	{
        spine.UpdateSpineSkin(skinData);
    }

    public void SetName(string name)
	{
        playerName.text = name;

    }



}
