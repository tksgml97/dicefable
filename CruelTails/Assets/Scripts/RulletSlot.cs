using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RulletSlot : MonoBehaviour
{
    [SerializeField] PlayerSpineSkinUpdator playerSpineSkinUpdator;
    [SerializeField] Text username;
    [SerializeField] GameObject dieImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetSlot(PlayerSpineSkinData playerSpineSkinData, string text, bool isDie, bool currentDie)
	{
        username.text = text;
        if (playerSpineSkinData == null) return;
        playerSpineSkinUpdator.UpdateSpineSkin(playerSpineSkinData);
        dieImage.SetActive(isDie);
    }
}
