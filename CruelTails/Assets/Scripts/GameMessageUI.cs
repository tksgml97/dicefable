using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class GameMessageUI : MonoBehaviour
{
    [SerializeField] float liveTime = 3f;
    [SerializeField] float speed = 5f;
    Text textUI;
    // Start is called before the first frame update
    void Start()
    {
        textUI = GetComponent<Text>();
        StartCoroutine(WaitDestroy(liveTime));
    }


	private void Update()
	{
        transform.position = transform.position + Vector3.up * speed * Time.deltaTime;
        Color c = textUI.color;
        c.a -= Time.deltaTime / liveTime;
        textUI.color = c;

    }

	IEnumerator WaitDestroy(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }
}
