using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MapData
{
    public eGameRound round;
    public SpawnPointReader spawnPoint;
    public GameObject map;
}


public class MapManager : MonoBehaviour
{
    [SerializeField] MapData[] inputRoundData;

    Dictionary<eGameRound, MapData> mapDatas = new Dictionary<eGameRound, MapData>();

    // Start is called before the first frame update
    void Awake()
    {
		for (int i = 0; i < inputRoundData.Length; i++)
		{
            mapDatas.Add(inputRoundData[i].round, inputRoundData[i]);
        }
    }

    public MapData GetMapData(eGameRound round)
	{
        return mapDatas[round];
	}

    public void LoadMap(eGameRound round)
	{
        mapDatas[round].map.SetActive(true);
    }
}
