using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointReader : MonoBehaviour
{
    List<Transform> spawnPoints = new List<Transform>();


	private void Awake()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform spawnPoint = transform.GetChild(i);
			spawnPoints.Add(spawnPoint);
		}
	}

	public Transform GetSpawnPoint(int i)
	{
		i = i % spawnPoints.Count;
		return spawnPoints[i];
	}

}
