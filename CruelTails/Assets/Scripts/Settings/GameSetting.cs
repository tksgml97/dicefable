using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameSetting : MonoBehaviour
{
    [SerializeField] Resolution resolution = null;
    [SerializeField] Text windowMode = null;

    [SerializeField] GameObject PopUp_CheckOK = null;
    [SerializeField] GameObject PopUp_CheckCancel = null;
    public bool PopUpActive { get; set; } = false;

    [SerializeField] SoundSlider SFXSlider, BGMSlider = null;
    

    public SettingInfo savedInfo { get; private set; } = new SettingInfo();
    SettingInfo changedInfo = new SettingInfo();

    void Awake()
    {
        savedInfo.Load();
        SoundLoad();
        savedInfo.Apply();
        Copy(savedInfo, changedInfo);
        ShowData();
    }

    private void Update()
    {
        if(!PopUpActive)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                RollBack();
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Apply();
            }
        }
    }

    void ShowData()
    {
        resolution.Load(savedInfo.resolution_Index);
        //Debug.Log("isFullScreen: " + changedInfo.fullScreen);
        windowMode.text = (!savedInfo.fullScreen) ? "ON" : "OFF";

    }
    void Copy(SettingInfo copy, SettingInfo paste)
    {
        paste.resolution_Index = copy.resolution_Index;
        paste.fullScreen = copy.fullScreen;
        paste.SFX_Volume = copy.SFX_Volume;
        paste.BGM_Volume = copy.BGM_Volume;
    }


    // OnClick() Methods
    public void PopUpSetting()
    {
        savedInfo.Load();
        Copy(savedInfo, changedInfo);
        ShowData();
    }
    
    public void SetResolution()
    {
        changedInfo.resolution_Index = resolution.curIndex;
        //Debug.Log(changedInfo.resolution_Index + "]] saved:  " + savedInfo.resolution_Index);
    }

    public void Apply() // 설정 확인 버튼 OnClick
    {
        PopUp_CheckOK.SetActive(true); PopUpActive = true;

        changedInfo.Apply();

        if (CheckChange())
        {
            // 바뀐 내용이 있으면 카피, 저장
            Copy(changedInfo, savedInfo);
            savedInfo.Save();
            Debug.Log("changed: " + changedInfo.SFX_Volume + "saved: " + savedInfo.SFX_Volume);
        }
    }

    public bool CheckChange()
    {
        if(changedInfo.resolution_Index != savedInfo.resolution_Index
        || changedInfo.fullScreen != savedInfo.fullScreen)
        //|| changedInfo.SFX_Volume != savedInfo.SFX_Volume
        //|| changedInfo.BGM_Volume != savedInfo.BGM_Volume
        {
            //Debug.Log(savedInfo.resolution_Index + " change " + changedInfo.resolution_Index);
            return true;
        }
        Debug.Log("no change");
        return false;
    }

    public void RollBack() // 나가기 버튼 OnClick <<
    {
        if (CheckChange())
        {
            PopUp_CheckCancel.SetActive(true); PopUpActive = true;
        }
            
        // 변경 값 없음
        else
        {
            gameObject.SetActive(false); PopUpActive = false;
        }
    }

    public void ChangeWindowMode() // 창모드 변경 버튼 OnClick
    {
        windowMode.text = (windowMode.text == "ON") ? "OFF" : "ON";
        changedInfo.fullScreen = !changedInfo.fullScreen;
    }

    // OnValueChanged Method
    public void SoundLoad()
    {
        SFXSlider.setValue(savedInfo.SFX_Volume);
        BGMSlider.setValue(savedInfo.BGM_Volume);
    }
    public void SetBGMVolume(float _input)
    {
        changedInfo.BGM_Volume = (byte)_input;
    }
    public void setSFXVolume(float _input)
    {
        changedInfo.SFX_Volume = (byte)_input;
    }
}
