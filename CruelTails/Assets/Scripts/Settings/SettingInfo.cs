using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingInfo
{
    public int[] x = { 1920, 1600, 1280, 960 };
    public int[] y = { 1080, 1200, 720, 480 };

    // Video
    public int resolution_Index = 0;
    public bool fullScreen = true;

    // Audio
    public byte SFX_Volume = 100;
    public byte BGM_Volume = 100;

    public void Save()
    {
        PlayerPrefs.SetInt("resolution_Index", resolution_Index);
        PlayerPrefs.SetInt("SFX_Volume", SFX_Volume);
        PlayerPrefs.SetInt("BGM_Volume", BGM_Volume);
    }
    public void Load()
    {
        // 저장된 값이 없다
        if (!PlayerPrefs.HasKey("resolution_Index")||
        !PlayerPrefs.HasKey("SFX_Volume") || !PlayerPrefs.HasKey("BGM_Volume"))
        {
            Save(); // 초기화 값을 저장한다
        }
        resolution_Index = PlayerPrefs.GetInt("resolution_Index");
        SFX_Volume = (byte)PlayerPrefs.GetInt("SFX_Volume");
        BGM_Volume = (byte)PlayerPrefs.GetInt("BGM_Volume");
        Debug.Log("Volume: " + SFX_Volume + " " + BGM_Volume);
    }
    public void Apply()
    {
        Screen.SetResolution(x[resolution_Index],
            y[resolution_Index],
            fullScreen);
        
    }
}
