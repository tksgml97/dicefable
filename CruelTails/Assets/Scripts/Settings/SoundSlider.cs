using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SOUNDTYPE
{
    SFX,BGM
}
public class SoundSlider : MonoBehaviour
{
    [SerializeField] SOUNDTYPE soundType;

    [SerializeField] GameSetting gameSetting;
    [SerializeField] Slider slider;
    [SerializeField] Text text;
    [SerializeField] AK.Wwise.RTPC volume;

    private void Awake()
    {
        UpdateVolume();
    }


    public void setValue(byte _input)
    {
        slider.value = _input;
        slider.onValueChanged.AddListener(delegate { UpdateVolume(); });

        if (soundType == SOUNDTYPE.SFX)
            slider.onValueChanged.AddListener(delegate { gameSetting.setSFXVolume(slider.value); });
        else if (soundType == SOUNDTYPE.BGM)
            slider.onValueChanged.AddListener(delegate { gameSetting.SetBGMVolume(slider.value); });

    }
    public void UpdateVolume()
    {
        byte value = (byte)slider.value;
        if(text != null)
            text.text = value.ToString();
        volume.SetGlobalValue(value);
    }
}
