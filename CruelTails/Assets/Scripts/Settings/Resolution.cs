using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resolution : MonoBehaviour
{
    [SerializeField] GameSetting gameSetting;
    [SerializeField]Text resolution_text;
    string[] resolutions = {"1920x1080", "1600x1200", "1280x720", "960x480" };

    public int curIndex { get; private set; } = 0;

    // 설정 창에 저장된 데이터 표시
    public void Load(int i)
    {
        resolution_text.text = resolutions[i];
    }

    public void changeResolution(bool input)
    {
        //>
        if (input)
            curIndex++;
        // <
        else
            curIndex--;

        if(curIndex < 0 )
        {
            Debug.Log(curIndex + "Changed To"+ (resolutions.Length - 1));
            curIndex = resolutions.Length - 1;
        }
        else if(resolutions.Length <= curIndex)
            curIndex = 0;

        resolution_text.text = resolutions[curIndex];

        gameSetting.SetResolution();
    }


}
