using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;


public class KoreaItemObject : NetworkItem
{

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    public void Init()
	{
        if(spriteRenderer == null)
		{
            spriteRenderer = GetComponent<SpriteRenderer>();

        }
        Sprite sprite;
        string name = spriteRenderer.sprite.ToString();
        string spriteName = name.Split()[0];
        type = (eItemType)Enum.Parse(typeof(eItemType), spriteName);
        gameObject.layer = LayerMask.NameToLayer("Item");
        SetType(type);
    }
    public override void Eat(GameObject player)
	{

    }
}
