﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class NetworkUIControll : MonoBehaviour {

	[Header("UI Object")]
	public GameObject FailConnectionUI;
	public GameObject ConnectLoadingUI;
	public GameObject FailText;


	//public Text matchText;




	private void Awake()
	{
	}

	// Use this for initialization
	void Start () {
        //string text = "유저" + Random.RandomRange(0, 100).ToString();
        //LobbyUIManager.Instance.savedName = text;
        //LobbyUIManager.Instance.nameInputField.text = text;
    }
	
	public void UpdateMatchList(List<PlayerSpineSkinData> skins)
	{
		if (LobbyUIManager.Instance == null) return;
		//LobbyUIManager.Instance.ClearMatchPlayerList();
		for (int i = 0; i < LobbyUIManager.Instance.matchPlayers.Length; i++)
		{
			if (LobbyUIManager.Instance.matchPlayers[i] == null) return;
			LobbyUIManager.Instance.matchPlayers[i].gameObject.SetActive(false);
		}
		for (int i = 0; i < skins.Count; i++)
		{

			LobbyUIManager.Instance.matchPlayers[i].gameObject.SetActive(true);
			LobbyUIManager.Instance.matchPlayers[i].UpdateSpineSkin(skins[i]);
		}
	}
	
	public void NameApplyButton()
	{
		//NetworkConnect.instance.NameSet(LobbyUIManager.Instance.nameInputField.text);
		NetworkConnect.instance.NameSet(LobbyUIManager.Instance.savedName);
	}
}
