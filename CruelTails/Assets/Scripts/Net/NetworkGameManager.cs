﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
public enum eAttackType
{
	Normal,
	Fatal,
	KnockNormal
}

public class NetworkPlayerInfo
{
	public bool isDIe = false;
	public PlayerSpineSkinData skinData;
	public string playerName;
}
public class NetworkGameManager : MonoBehaviour {

	static public bool isPVP = false;
	static public bool isCamera = false;

	static public NetworkGameManager instance;
	
	[SerializeField] GameObject gameUICanvas;
	[SerializeField] GameObject networkPlayerPref;

	public UIGameOverView gameOverUI;
	public LoadingUI loadingUI;
	public GameObject[] networkPlayer;
	public bool[] onPlayer;

	//public eGameRound curStage = 0;
	static public int memberNumber = -1;
	public bool isInit = false;

	public int maxPlayer = -1;
	
	float noHitModeTimer = 0f;
	float noHitModeTime = 0.35f;

	public const int roomMaxSize = 5;
	public bool DEBUG_CAN_HIT = false;
	NetworkPlayerInfo[] networkPlayerInfos;
	// Use this for initialization
	private void Awake()
	{
		networkPlayerInfos = new NetworkPlayerInfo[roomMaxSize];
		InitData();

		if(instance == null)
		{
			instance = this;
		}

		
	}

	public void InitData()
	{

		for (int i = 0; i < roomMaxSize; i++)
		{
			networkPlayerInfos[i] = new NetworkPlayerInfo();
		}
	}
	void Start() {
		StartCoroutine(ITR());
	}

	IEnumerator ITR()
	{
		yield return new WaitForEndOfFrame();
		if(NetworkConnect.isConnected)
		NetworkConnect.instance.GetGameInitData();
	}

	public bool isDie()
	{
		return networkPlayerInfos[memberNumber].isDIe;
	}
	public NetworkPlayerInfo GetPlayerInfo(int networkMemberNumber)
	{
		return networkPlayerInfos[networkMemberNumber];
	}
	public GameObject GetPlayerObject(int networkMemberNumber)
	{
		if (networkMemberNumber != memberNumber)
			return networkPlayer[networkMemberNumber];
		else
		{
			return GameManager.playerController.gameObject;
		}
	}


	public void Init()
	{
		networkPlayer = new GameObject[maxPlayer];
		for (int i = 0; i < maxPlayer; i++)
		{
			if (onPlayer[i] != true)
				networkPlayer[i] = null;
			else if (i != memberNumber)
			{
				networkPlayer[i] = Instantiate(networkPlayerPref);
				networkPlayer[i].GetComponent<RemotePlayer>().id = i;
			}
		}
	}

	
	// Update is called once per frame
	void Update () {
		noHitModeTimer -= Time.deltaTime;
	}

	public void GameDataHandler(ByteBuffer readBuffer)
	{
		ServerGamePackets packetType = (ServerGamePackets)readBuffer.ReadInteger();
		Debug.Log(packetType.ToString());
		if (isInit == false && packetType != ServerGamePackets.SGameInit) return;

		ServerPacketBase packet;
		switch(packetType)
		{
			case ServerGamePackets.SAniState:
				packet = new SAniStatePacket();
				break;
			case ServerGamePackets.SEntityEatStart:
				packet = new SEntityEatStartPacket();
				break;
			case ServerGamePackets.SExitClient:
				packet = new SExitClientPacket();
				break;
			case ServerGamePackets.SFailTake:
				packet = new SFailTakePacket();
				break;
			case ServerGamePackets.SGameInit:
				packet = new SGameInitPacketPacket();
				break;
			case ServerGamePackets.SGameStart:
				packet = new SGameStartPacket();
				break;
			case ServerGamePackets.SHitPlayer:
				packet = new SHitPlayerPacket();
				break;
			case ServerGamePackets.SKillPlayer:
				packet = new SKillPlayerPacket();
				break;
			case ServerGamePackets.SPlayEffect:
				packet = new SPlayEffectPacket();
				break;
			case ServerGamePackets.SPlayerState:
				packet = new SPlayerStatePacket();
				break;
			case ServerGamePackets.SPosition:
				packet = new SPositionPacket();
				break;
			case ServerGamePackets.SRemoteCharSet:
				packet = new SRemoteCharSetPacket();
				break;
			case ServerGamePackets.SSetPlayerItem:
				packet = new SSetPlayerItemPacket();
				break;
			case ServerGamePackets.SSpawnEntity:
				packet = new SSpawnEntitytPacket();
				break;
			case ServerGamePackets.STableSet:
				packet = new STableSetPacket();
				break;
			case ServerGamePackets.STakeTable:
				packet = new STakeTablePacket();
				break;
			case ServerGamePackets.SMissionStart:
				packet = new SMissionStartPacket();
				break;
			case ServerGamePackets.SPlayerInfo:
				packet = new SPlayerInfoPacket();
				break;
			case ServerGamePackets.SReady:
				packet = new SReadyPacket();
				break;
			case ServerGamePackets.SMessage:
				packet = new SMessagePacket();
				break;
			case ServerGamePackets.SInsideTeleport:
				packet = new SInsidePacket();
				break;
			case ServerGamePackets.SHitSkillObject:
				packet = new SHitSkillObject();
				break;
			case ServerGamePackets.STakeDice:
                packet = new STakeDicePacket();
                break;
            case ServerGamePackets.SSpawnDice:
                packet = new SSpawnDicePacket();
                break;
			case ServerGamePackets.SShot:
				packet = new SShotPacket();
				break;
			case ServerGamePackets.SMobTarget:
				packet = new SMobTargetPacket();
				break;
			case ServerGamePackets.SMobAttack:
				packet = new SMobAttackPacket();
				break;
			default:
				Debug.LogError("패킷 깨짐");
				return;
		}

		packet.Deserialize(readBuffer);
		packet.Excute(this, 0, memberNumber);

	}
	public void DestroyRemotePlayer(int remotePlayerNumber)
	{
		StartCoroutine(DestroyRemotePlayerEndFrame(remotePlayerNumber));
	}
	IEnumerator DestroyRemotePlayerEndFrame(int remotePlayerNumber)
	{
		yield return new WaitForEndOfFrame();
		if (networkPlayer[remotePlayerNumber] != null)
		{
			Destroy(networkPlayer[remotePlayerNumber]);
			networkPlayer[remotePlayerNumber] = null;
		}
	}

}
