﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

public class NetworkHandler : MonoBehaviour {

	public static NetworkHandler Instance;
	public Queue<byte[]> networkMessageQueue;
	private void Awake()
	{
		networkMessageQueue = new Queue<byte[]>();
		if (Instance == null)
		{
			Instance = this;
		}
	}
	void Start () {
		//Handdling();
	}

	public static bool inOnHandler = true;
	public void Handdling(ByteBuffer readBuffer)
	{

		ServerPackets packetHeader;
		packetHeader = (ServerPackets)readBuffer.ReadInteger();

		Debug.Log(packetHeader.ToString());
		//Handle 
		switch (packetHeader)
		{
			case ServerPackets.SConnect:
				{
					Debug.Log("연결확인");
					int userNumber = readBuffer.ReadInteger();
					NetworkConnect.instance.clientNumber = userNumber;
					Debug.Log(userNumber + "번 클라이언트");
				}
				break;
			case ServerPackets.SMatchMakeClient:
				{
					GameObject.Find("LobbySoundInstance").GetComponent<LobbySoundInstance>().LobbySoundEnd();
					Debug.Log("매칭완료");
					NetworkConnect.instance.match.text = "매칭완료";
					NetworkGameManager.memberNumber = readBuffer.ReadInteger();

					eGameRound startStage = (eGameRound)readBuffer.ReadInteger();
					NetworkConnect.isStart = true;

					NetworkGameManager.instance.loadingUI.SetLoadingView(true);
					//NetworkGameManager.instance.curStage = startStage;
					StartCoroutine(StartLoading(0f, startStage));
				}
				break;
			case ServerPackets.SPlayGameData:
				{

					if (NetworkGameManager.instance == null)
					{
						Debug.Log("ERROR");
					}
					else
					{
						NetworkGameManager.instance.GameDataHandler(readBuffer);
					}
					break;
				}
				break;
			case ServerPackets.SMatchInfo:
				{
					int size = readBuffer.ReadInteger();

					List<PlayerSpineSkinData> spineSkinDatas = new List<PlayerSpineSkinData>(size);
					List<string> names = new List<string>();

					// 성격 저장용 리스트
					List<int> personalitys = new List<int>();

                    for (int i = 0; i < size; i++)
					{

						string playerName = readBuffer.ReadString();

						PlayerSpineSkinData skinData = new PlayerSpineSkinData();

						int nSkinSlot = (int)SpineSkinSlot.MAX;
						for (int j = 0; j < nSkinSlot; j++)
						{
							int slot = readBuffer.ReadInteger();
							skinData.slots.Add((SpineSkinSlot)j, SpineSkinManager.instance.GetSkin((SpineSkinSlot)j, slot));
						}


						Color hairColor = Color.white;
						hairColor.r = readBuffer.ReadFloat();
						hairColor.g = readBuffer.ReadFloat();
						hairColor.b = readBuffer.ReadFloat();
						Color skinColor = Color.white;
						skinColor.r = readBuffer.ReadFloat();
						skinColor.g = readBuffer.ReadFloat();
						skinColor.b = readBuffer.ReadFloat();
						skinData.hair = hairColor;
						skinData.skin = skinColor;

						spineSkinDatas.Add(skinData);
						names.Add(playerName);
                        personalitys.Add(readBuffer.ReadInteger());
                    }
					NetworkConnect.instance.personalitys = personalitys;
                    NetworkConnect.instance.UpdateMatchList(spineSkinDatas, names);
				}
				break;
			default:
				Debug.Log("예외 데이터 " + packetHeader);
				break;
		}

		NetworkConnect.shouldHandleData = false;
		
	}
	/*
	void Handdling()
	{
		if (Input.GetKeyDown(KeyCode.F11)) Debug.Log("Handlling");

		if (NetworkConnect.instance == null)
		{
			Debug.Log(NetworkConnect.instance);
			return;
		}
		if (NetworkConnect.instance.network == null) return;
		if (!NetworkConnect.instance.network. isConnected) return;

		if (inOnHandler == false) return;

		while (networkMessageQueue.Count > 0)
		{

			ServerPackets packetHeader;
			byte[] data;
			lock (networkMessageQueue)
			{
				data = networkMessageQueue.Dequeue();
			}

			ByteBuffer readBuffer = new ByteBuffer();
			readBuffer.WriteBytes(data);

               packetHeader = (ServerPackets)readBuffer.ReadInteger();

			Debug.Log(packetHeader.ToString());
			//Handle 
			switch (packetHeader)
			{
				case ServerPackets.SConnect:
					{
						Debug.Log("연결확인");
						int userNumber = readBuffer.ReadInteger();
						NetworkConnect.instance.clientNumber = userNumber;
						NetworkConnect.instance.network.clientNumber = userNumber;
						Debug.Log(userNumber + "번 클라이언트");
					}
					break;
				case ServerPackets.SMatchMakeClient:
					{
						GameObject.Find("LobbySoundInstance").GetComponent<LobbySoundInstance>().LobbySoundEnd();
						Debug.Log("매칭완료");
						NetworkConnect.instance.match.text = "매칭완료";
						NetworkGameManager.memberNumber = readBuffer.ReadInteger();

						eGameRound startStage = (eGameRound)readBuffer.ReadInteger();
						NetworkConnect.isStart = true;
						NetworkGameManager.instance.loadingUI.SetLoadingView(true);
						//NetworkGameManager.instance.curStage = startStage;
						StartCoroutine(StartLoading(0f, startStage));
					}
					break;
				case ServerPackets.SPlayGameData:
					{

						if (NetworkGameManager.instance == null)
						{
							Debug.Log("ERROR");
						}
						else
						{
							NetworkGameManager.instance.GameDataHandler(readBuffer);
						}
						break;
					}
					break;
				case ServerPackets.SMatchInfo:
					{
						int size = readBuffer.ReadInteger();

						List<PlayerSpineSkinData> spineSkinDatas = new List<PlayerSpineSkinData>(size);
						List<string> names = new List<string>();
						for (int i = 0; i < size; i++)
						{

							string playerName = readBuffer.ReadString();

							PlayerSpineSkinData skinData = new PlayerSpineSkinData();

							int nSkinSlot = (int)SpineSkinSlot.MAX;
							for (int j = 0; j < nSkinSlot; j++)
							{
								int slot = readBuffer.ReadInteger();
								skinData.slots.Add((SpineSkinSlot)j, SpineSkinManager.instance.GetSkin((SpineSkinSlot)j, slot));
							}


							Color hairColor = Color.white;
							hairColor.r = readBuffer.ReadFloat();
							hairColor.g = readBuffer.ReadFloat();
							hairColor.b = readBuffer.ReadFloat();
							Color skinColor = Color.white;
							skinColor.r = readBuffer.ReadFloat();
							skinColor.g = readBuffer.ReadFloat();
							skinColor.b = readBuffer.ReadFloat();
							skinData.hair = hairColor;
							skinData.skin = skinColor;

							spineSkinDatas.Add(skinData);
							names.Add(playerName);
						}

						NetworkConnect.instance.UpdateMatchList(spineSkinDatas, names);
					}
					break;
				default:
					Debug.Log("예외 데이터 " + packetHeader);
					break;
			}

            NetworkConnect.shouldHandleData = false;
		}
	}
	*/
	// Update is called once per frame
	void FixedUpdate () {
		//Handdling();
		if (NetworkConnect.instance.isModuleStart == false) return;
		NetworkConnect.instance.module.Update();

	}
	public IEnumerator StartLoading(float waitTime, eGameRound stage)
	{
		yield return new WaitForSeconds(waitTime);
		
		eSceneType loadScene = eSceneType.Round1;
		switch (stage)
		{
			case eGameRound.Round1:
				loadScene = eSceneType.Round1;
				break;

			case eGameRound.Round2:
				loadScene = eSceneType.Round2;
				break;

			case eGameRound.Round3:
				loadScene = eSceneType.Round3;
				break;
			case eGameRound.Round4:
				loadScene = eSceneType.Round4;
				break;
		}

		StartCoroutine(SceneLoader.instance.LoadScene(loadScene, NetworkConnect.instance.GetGameInitData));
	}
}
