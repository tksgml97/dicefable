﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public static class Native
{
    public static T Invoke<T, T2>(IntPtr library, params object[] pars)
    {
        if (library == IntPtr.Zero)
        {
            Debug.Log("library is nullptr");
            return default(T);
        }
        IntPtr funcPtr = GetProcAddress(library, typeof(T2).Name);
        if (funcPtr == IntPtr.Zero)
        {
            Debug.Log("not found <" + typeof(T2).Name + ">function");
            return default(T);
        }

        var func = Marshal.GetDelegateForFunctionPointer(GetProcAddress(library, typeof(T2).Name), typeof(T2));
        return (T)func.DynamicInvoke(pars);
    }

    public static void Invoke<T>(IntPtr library, params object[] pars)
    {
        if (library == IntPtr.Zero)
        {
            Debug.Log("library is nullptr");
            return;
        }
        IntPtr funcPtr = GetProcAddress(library, typeof(T).Name);
        if (funcPtr == IntPtr.Zero)
        {
            Debug.Log("not found <" + typeof(T).Name + ">function");
            return;
        }

        var func = Marshal.GetDelegateForFunctionPointer(funcPtr, typeof(T));
        func.DynamicInvoke(pars);
    }

    [DllImport("kernel32", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool FreeLibrary(IntPtr hModule);


    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public extern static IntPtr LoadLibrary(string librayName);


    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)]
    public extern static IntPtr LoadLibraryA(string librayName);

    [DllImport("kernel32")]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);


    [DllImport("kernel32.dll", EntryPoint = "GetLastError")]
    public static extern int GetLastError();
}