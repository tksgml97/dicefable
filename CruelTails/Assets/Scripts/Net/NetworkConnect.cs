﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.Runtime.InteropServices;
using Spine;

public class NetworkConnect : MonoBehaviour
{

	public static NetworkConnect instance;

	public static bool canCharChange;

	NetworkUIControll networkUI;
	//public MainSceneUIControll mainSceneUI;
	//Thread udpListenThread;


	[Header("Network Settings")]
	public string m_serverIP = "127.0.0.1";
	public int ServerPort = 51235;
	[HideInInspector]
	static public bool isConnected = false;
	public bool isLocal = true;
	[HideInInspector] public int clientNumber;
	[HideInInspector]
	static public byte[] asyncBuff;
	[HideInInspector]
	static public bool shouldHandleData = false;
	static private byte[] myBytes;
	[HideInInspector]
	static public bool isStart = false;

	static public bool canExit = true;

	public List<int> personalitys = new List<int>();


    //public SanNetwork.Network network;

    public NetworkModule module;
	public bool isModuleStart = false;
	private void Awake()
	{
		if (instance == null)
		{
			DontDestroyOnLoad(this);
			instance = this;
		} else
		{
			gameObject.SetActive(false);
		}
		Debug.Log(instance);
	}

	// Use this for initialization
	void Start()
	{
		canExit = true;
		networkUI = GameObject.Find("NetworkUI").GetComponent<NetworkUIControll>();
		Initalize();
	}

	public void UpdateMatchList(List<PlayerSpineSkinData> skins, List<string> names)
	{
		networkUI.UpdateMatchList(skins);
		NetworkGameManager.instance.loadingUI.SetData(skins, names);
		
	}

	public void Initalize()
	{
		if (isLocal == true)
		{
			m_serverIP = "127.0.0.1";
		}
		//networkModule.Start(port);
		//network = new SanNetwork.Network(ref NetworkHandler.Instance.networkMessageQueue);
		//Debug.Assert(network != null);
		//network.FailConnection += OnFailConnection;
		//network.ConnectionSuccess += OnConnectionSuccess;
	}

	public bool Connect(string IP, int port)
	{
		Debug.Log("connect "+IP);
		return StartNetwork(IP);
	}

	void OnFailConnection(string errorCode)
	{
		Debug.Log(errorCode);
		networkUI.FailText.GetComponent<Text>().text = "서버와의 연결이 끊어졌습니다" + errorCode.ToString();
	}


	public bool StartNetwork(string ip)
	{
		Debug.Log("Start NetworkModule");
		module = new NetworkModule();
		module.Start(51235);

		module.BindLogCallback(OnLog);
		module.BindMessageCallback(OnRecv);
		isModuleStart = true;

		bool result = module.ConnectToServer(ip);

		Debug.Log("End NetworkModule");

		return result;
	}

	public static int OnLog(int value1, string text1)
	{
		Debug.Log(value1 + ":" + text1);
		return value1;
	}

	public static void OnRecv(IntPtr buf, int len, int sessionId)
	{
		Debug.Log("proc packet" + len);

		byte[] managedArray = new byte[len];
		Marshal.Copy(buf, managedArray, 0, len);

		ByteBuffer buffer = new ByteBuffer();

		buffer.WriteBytes(managedArray);

		NetworkHandler.Instance.Handdling(buffer);



		return;
	}

	void OnConnectionSuccess()
	{
		Debug.Log("연결완료");
		isConnected = true;
	}

	public Text match;

	private void Update()
	{

		//test
		if (Input.GetKeyDown(KeyCode.X))
		{

			Vector3 p = new Vector3(UnityEngine.Random.RandomRange(-5f, 5f), 0, UnityEngine.Random.RandomRange(-5f, 5f));
			SendSpawnItem((int)eItemType.SpeedUp, GameManager.playerController.transform.position + p, false, Vector3.zero);

		}

		if (Input.GetKey(KeyCode.X) && Input.GetKey(KeyCode.C))
		{
			for (int i = 0; i < 10; i++)
			{
				Vector3 p = new Vector3(UnityEngine.Random.RandomRange(-2f, 2f), 0, UnityEngine.Random.RandomRange(-2f, 2f));
				SendSpawnItem(UnityEngine.Random.Range(0, 2), GameManager.playerController.transform.position + p, false, Vector3.zero);
			}


		}
		if (instance != this) return;


	}

	static public string myName = " ";


    public void PersonalitySet(int prsonality_type) {
        ByteBuffer sendBuffer = new ByteBuffer();
        sendBuffer.WriteInteger((int)ClientPackets.CSetPersonality);
		sendBuffer.WriteInteger(prsonality_type);
		SendDataToServer(sendBuffer.ToArray());
	}


    public void SkinSet(PlayerSpineSkinData skin)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CSetSkin);
		for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
		{
			
			string skinName = skin.slots[(SpineSkinSlot)i];
			skinSlotString slotString;
			slotString.slotType = (SpineSkinSlot)i;
			slotString.name = skinName;
			int id = -1;
			if(skinName != "")
			{
				id = SpineSkinManager.instance.stringToSkinIndex[slotString];
			}
			//Debug.Log("[skinTestLog]"+slotString.slotType.ToString() + slotString.name);
			//Debug.Log("[skinTestLog]" + SpineSkinManager.instance.stringToSkinIndex.ContainsKey(slotString));
			//int 
			sendBuffer.WriteInteger(id);
		}
		sendBuffer.WriteVector3(new Vector3(skin.hair.r, skin.hair.g, skin.hair.b));
		sendBuffer.WriteVector3(new Vector3(skin.skin.r, skin.skin.g, skin.skin.b));

		SendDataToServer(sendBuffer.ToArray());

	}
	public void NameSet(string name)
	{
		if (name == "") name = " ";
		myName = name;
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CSetName);
		sendBuffer.WriteString(myName);

		SendDataToServer(sendBuffer.ToArray());

	}
	public void SendReady()
	{
		Debug.Log("SendReady");
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CReady);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendHeal()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CHeal);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendJoinRoom(int roomNum)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CRoomChange);
		sendBuffer.WriteInteger(roomNum);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendPlayerSkin(PlayerSpineSkinData skinData)
	{

		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CCharSet);

		sendBuffer.WriteInteger((int)SpineSkinSlot.MAX);
		for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
		{
			sendBuffer.WriteString(skinData.slots[(SpineSkinSlot)i]);
		}

		SendDataToServer(sendBuffer.ToArray());
	}
	public void ChangeRoom(int room)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CRoomChange);
		sendBuffer.WriteInteger(room);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void ExitGame()
	{
		isMatching = false;
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CExit);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendShotFire(Vector3 dir, Vector3 pos, float power)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CShot);
		sendBuffer.WriteInteger(0); // ShotType
		sendBuffer.WriteVector3(dir);
		sendBuffer.WriteVector3(pos);
		sendBuffer.WriteFloat(power);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendShotSkill(Vector3 dir, Vector3 pos, float power)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CShot);
		sendBuffer.WriteInteger(4); // ShotType
		sendBuffer.WriteVector3(dir);
		sendBuffer.WriteVector3(pos);
		sendBuffer.WriteFloat(power);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendElevatorEnter(bool enter)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CElevatorEnter);
		sendBuffer.WriteInteger(enter ? 1 : 0);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SendInsideTeleport(bool inside, Vector3 position)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CInsideTeleport);
		sendBuffer.WriteBool(inside);
		sendBuffer.WriteVector3(position);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendDoorOpenTry(int doorNumber)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CDoorOpenTry);
		sendBuffer.WriteInteger(doorNumber); // DOorNumber
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendPos(Vector3 position, Vector3 direction, float scale, float speed)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CPosition);
		sendBuffer.WriteFloat(position.x);
		sendBuffer.WriteFloat(position.y);
		sendBuffer.WriteFloat(position.z);
		sendBuffer.WriteFloat(direction.x);	
		sendBuffer.WriteFloat(direction.y);
		sendBuffer.WriteFloat(direction.z);
		sendBuffer.WriteFloat(scale);
		sendBuffer.WriteFloat(speed);
		//SendDataToServer(sendBuffer.ToArray());
		SendDataToServer(sendBuffer.ToArray(), PacketType.TCP);
	}

	public void SendParticlePlay(eParticleType type, Vector3 pos, Quaternion rot)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CPlayEffect);
		sendBuffer.WriteInteger((int)type);
		sendBuffer.WriteVector3(pos);
		sendBuffer.WriteQuaternion(rot);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SendAni(int aniNum)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CAniState);
		sendBuffer.WriteInteger(aniNum);
		SendDataToServer(sendBuffer.ToArray());
		//Debug.Log("SendAni");
	}
	public void SendBoxBreak(int room, int objectID)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CBoxBreak);
		sendBuffer.WriteInteger(room);
		sendBuffer.WriteInteger(objectID);
		SendDataToServer(sendBuffer.ToArray());
		//Debug.Log("SendAni");
	}
	public void SendSpawnItem(int type, Vector3 pos, bool playAni, Vector3 velocity)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CSpawnItem);
		sendBuffer.WriteInteger(type);
		sendBuffer.WriteVector3(pos);
		sendBuffer.WriteBool(playAni);
		sendBuffer.WriteVector3(velocity);
		SendDataToServer(sendBuffer.ToArray());
		//Debug.Log("SendAni");
	}
	public void SendGetEntity(int entityID, bool EatComplete)
	{
		Debug.Log("GetEntity");
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CGetEntity);
		sendBuffer.WriteInteger(entityID);
		sendBuffer.WriteBool(EatComplete);
		SendDataToServer(sendBuffer.ToArray());
		//Debug.Log("SendAni");
	}
	public void SendMissionStart(MissionState state, int missionID)
	{
		Debug.Log("SendMisssion");
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CMissionStart);
		sendBuffer.WriteInteger((int)state);
		sendBuffer.WriteInteger(missionID);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SendMissionEnd()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CMissionEnd);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void GetStateData()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CStateGet);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void GetGameInitData()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CGameInit);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SetPlayerItem(int type)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CSetPlayerItem);
		sendBuffer.WriteInteger(type);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SendMessage(MessageType type, string message)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CMessage);
		sendBuffer.WriteInteger((int)type);
		sendBuffer.WriteString(message);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void TakeTable(int table)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CTakeTable);
		sendBuffer.WriteInteger(table);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SetPvpMode(bool on)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CPVP);
		sendBuffer.WriteBool(on);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void HostMakeSuccess()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CHostMake);
		SendDataToServer(sendBuffer.ToArray());
	}
	public bool isMatching = false;

	public void MatchStart()
	{
		if (isMatching) return;
		isMatching = true;
        PersonalitySet(LobbyUIManager.Instance.personality);
		SkinSet(ClosetManager.Instance.getClosetSkin());
		NameSet(LobbyUIManager.Instance.nameInputField.text);
		match.text = "매칭중";
		Debug.Log("Match");
		//	SoundManager.Play(SoundType.Loading);
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CMatchStart);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void MatchCancel()
	{
		isMatching = false;
		match.text = "";
		//	SoundManager.Play(SoundType.Loading);
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CMatchCancel);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SingleStart()
	{
		//	SoundManager.Play(SoundType.Loading);
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CSigleMatch);
		SendDataToServer(sendBuffer.ToArray());
	}


	public void SendDataToServer(byte[] data, PacketType packetType = PacketType.TCP)
	{
		//network.SendDataToServer(data,(SanNetwork.PacketType) packetType);
		//network.SendDataToServer(data, (SanNetwork.PacketType)packetType);
		module.SendTo(data, data.Length);
	}



	public void FailConnection(int failNumber)
	{
		networkUI.FailConnectionUI.SetActive(true);
		networkUI.FailText.GetComponent<Text>().text = "서버와의 연결이 끊어졌습니다" + failNumber.ToString();
	}

	public void Guarding()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CGuarding);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void Charging()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CCharging);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void HealMP()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CHealMP);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendLog(IAsyncResult result)
	{
		Debug.Log("Send");
	}
	public void SendSetTable(int type, int table)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CTableSet);
		sendBuffer.WriteInteger(type);
		sendBuffer.WriteInteger(table);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendHitMessage(Vector3 pos, Vector3 direction, int target)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CHitPlayer);
		sendBuffer.WriteVector3(pos);
		sendBuffer.WriteVector3(direction);
		sendBuffer.WriteInteger(target);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendShotPos(Vector3 pos)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CShotPos);
		sendBuffer.WriteVector3(pos);
		SendDataToServer(sendBuffer.ToArray());
	}
	public void SendDieClient()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CDieClient);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendMissionClear()
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CMissionClear);
		SendDataToServer(sendBuffer.ToArray());
	}

	public void SendHitSkillObject(int id)
	{
		ByteBuffer sendBuffer = new ByteBuffer();
		sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
		sendBuffer.WriteInteger((int)ClientGamePackets.CHitSkillObject);
		sendBuffer.WriteInteger(id);
		SendDataToServer(sendBuffer.ToArray());
	}

    public void SendTakeDice(int dice_number) {
        ByteBuffer sendBuffer = new ByteBuffer();
        sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
        sendBuffer.WriteInteger((int)ClientGamePackets.CTakeDice);
        sendBuffer.WriteInteger(dice_number);
        SendDataToServer(sendBuffer.ToArray());
    }

    public void SendDropDice(Vector3 spawn_pos) {
        ByteBuffer sendBuffer = new ByteBuffer();
        sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
        sendBuffer.WriteInteger((int)ClientGamePackets.CDropDice);
        sendBuffer.WriteVector3(spawn_pos);
        SendDataToServer(sendBuffer.ToArray());
    }

    private void OnDestroy()
	{
		if (isConnected)
		{
			//udpListenThread.Abort();
		}
	}
}
