using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RemotePlayer : MonoBehaviour
{
    [SerializeField] ParticleSystem moveDust;
    [SerializeField] Vector3 realTransform;
    [SerializeField] PlayerAnimationController pac;
    [SerializeField] GameObject nameText;
    [SerializeField] GameObject remotePlayerUI;
    [SerializeField] Transform scaleObj;
    CharacterController rig;
    public string playerName;
    public eTeamType team = eTeamType.Red;
    public int id;
    public bool isDie = false;
    // Start is called before the first frame update


    void Start()
    {
        pc = GetComponent<PlayerController>();
        rig = GetComponent<CharacterController>();
        realTransform = transform.position;
        remotePlayerUI.SetActive(true);
        if (GameManager.instance)
        {
            GameManager.instance.uiActiveEvent += SetActiveUI;
        }
        scrollSpeed = GetComponentInChildren<PlayerScrollMap>().scrollMoveSpeed;
    }

    void SetActiveUI(bool state)
	{
        if (remotePlayerUI == null) return;
        remotePlayerUI.SetActive(state);
    }


    public PlayerAnimationController GetPlayerAnimationController()
	{
        return pac;
	}

    public bool pushed { get; set; } = false;
    public bool skillHit { get; set; } = false;

    public void SkillHit()
    {
        skillHit = true;
        Invoke(nameof(HitCool), noDamageTime);
    }
    void HitCool() { skillHit = false; }// gameObject.layer = 6; }

    
    [SerializeField] float noDamageTime = 3.0f;
    public void PushedCool()
    {
        Debug.Log("pushed");
        Invoke(nameof(CoolTime), noDamageTime);
    }
    void CoolTime()
    {
        Debug.Log("pushed end");
        pushed = false;
    }
    [SerializeField] float scrollMoveSpeed = 1.0f;
    [SerializeField] float pushedSpeed = 2.0f;
    float speedScale = 1f;
    PlayerController pc;
    // Update is called once per frame
    void Update()
    {
        nameText.transform.position = Camera.main.WorldToScreenPoint(transform.position + Vector3.up * 2f);
        moveDust.enableEmission = false;
        if (GameManager.instance.currentGame == eGameRound.Round3)
        {
            if (pushed)
            {
                rig.Move(Vector3.right * pushedSpeed * Time.deltaTime);
                return;
            }
        }
        realTransform = transform.position;
        Move();

        knockbackTime += Time.deltaTime;
        float n = GameManager.instance.knockbackCurve.Evaluate(knockbackTime);
        Vector3 deltaVector = knockDirection.normalized * n * Time.deltaTime * GameManager.instance.knockbackScale;
        rig.Move(deltaVector);
        if(pac.currentState == AnimState.PC_B_L_run || pac.currentState == AnimState.PC_F_L_run)
		{
            moveDust.enableEmission = true;
        }

        
    }
    float scrollSpeed = 0;
    float s;

    float speed = 0;
    Vector3 direction = Vector3.zero;
    public void Move()
    {
        if (ScrollMapManager.Instance != null && ScrollMapManager.Instance.is_start && skillHit == false)
        {
            rig.Move(new Vector3(-1 * scrollSpeed, 0, direction.z) * Time.deltaTime);
        }

        //gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, realTransform, speed);
            rig.Move(direction * speed * Time.deltaTime);
        //Debug.Log(speed);

    }
    public void SetRotate(Quaternion _rotation)
    {
    }

    public void SetSkin(PlayerSpineSkinData skin)
    {
        pac.SetSkin(skin);
    }
    public void SetColor(Color hairColor, Color headColor)
    {
        pac.SetColor(hairColor, headColor);
    }

    public void SetType(int b)
    {

    }

    public void SetWolfHide()
	{
        var ani = SpineAniLoader.instance.GetAni(AnimState.PC_wolf_passout);
        float hideTime = ani.Animation.Duration;
        pac.SetHide(hideTime);
    }

    public void SetTeam(int team)
	{
        pac.SetTeam(team);
        this.team = (eTeamType)team;
        nameText.GetComponent<Text>().color = GameManager.instance.GetTeamColor((eTeamType)team);
    }
    public void SetName(string _name)
    {
        playerName = _name;
        Debug.Log(playerName);
        nameText.GetComponent<Text>().text = _name;
    }
    public void SetAnimVel(float _animFB, float _animRL)
    {
        scaleObj.localScale = new Vector3(_animFB, _animRL, 1);
    }
    public void SetAni(int _aniNum)
    {
        bool changed = pac.SetCurrentAnimation((AnimState)_aniNum);


        if (changed && (AnimState)_aniNum == AnimState.PC_wolf_rotation)
        {
            if (team == eTeamType.RedHat)
            {
                var ani = SpineAniLoader.instance.GetAni(AnimState.PC_wolf_passout);
                float hideTime = ani.Animation.Duration;
                pac.SetHide(hideTime);
            }
        }
    }
    public void SetPosition(Vector3 _position, Vector3 direction, float speed)
    {
        // speed = (transform.position - _position).magnitude * Time.deltaTime / (0.05f);
        
        if(!pushed)
        {
            realTransform = _position;
            transform.position = _position;
            this.speed = speed;
            this.direction = direction;
        }
        
    }

    float knockbackTime = 0;
    Vector3 knockDirection;
    public void SetHit(Vector3 _position, Vector3 direction)
	{
        if (pac.currentState <= AnimState.PC_F_pushed5 && pac.currentState >= AnimState.PC_F_pushed1) return;
        this.speed = 0;
        this.direction = Vector3.zero;

        scaleObj.localScale = new Vector3(direction.x  > 0 ? 1 : -1, 1, 1);

        transform.position = _position;
        knockDirection = direction;
        knockbackTime = 0;
        pac.hitColorTime = 0.2f;
        if ((GameManager.playerController.transform.position - transform.position).magnitude < 2)
        {
            GetComponent<PlayerMoveSound>().PlayAtkedSound();
        }
    }
}
