﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
	public class NetworkModule
    {
        static IntPtr nativeLibraryPtr = IntPtr.Zero;

        delegate int MultiplyFloat(float number, float multiplyBy);
        delegate void DoSomething(string words);

        delegate IntPtr CreateNetwork(int userCapacity, int serverPort, bool isServer);
        delegate bool ServerStart(IntPtr instance);
        delegate bool DisposeNetwork(IntPtr instance);


        delegate void StartAccept(IntPtr instance);
        delegate bool Connect(IntPtr instance, [MarshalAs(UnmanagedType.LPStr)] string text1);
        delegate void Worker(IntPtr instance);
        delegate void SendToUser(IntPtr instance, int user, IntPtr buf, int len);
        delegate void SendToServer(IntPtr instance, IntPtr buf, int len);
        delegate void ProcMessage(IntPtr instance);


        static IntPtr net = IntPtr.Zero;


        #region
        public delegate int Win32Callback(int value1, [MarshalAs(UnmanagedType.LPWStr)] string text1);
        public delegate void MessageCallback(IntPtr buf, int len, int sessionId);
        public delegate void NetworkCallback(int sessionId);

        public static Win32Callback log_callback;
        delegate int fnBindLogFunction(Win32Callback callback, int value);

        public static MessageCallback message_callback;
        delegate void fnBindProcPacket(IntPtr network, MessageCallback callback);


        public static NetworkCallback close_callback;
        public static NetworkCallback accept_callback;
        delegate void fnBindAcceptCallback(IntPtr network, NetworkCallback callback);
        delegate void fnBindCloseCallback(IntPtr network, NetworkCallback callback);


    #endregion


        static List<Thread> workers;
        static Thread networkMain = null;

        public NetworkModule()
        {
        
           workers =  new List<Thread>();;
            if (nativeLibraryPtr != IntPtr.Zero) return;

        List<string> dllPaths = new List<string>();
        dllPaths.Add("../");
        dllPaths.Add("./");
        dllPaths.Add("../DLL/");
        dllPaths.Add("./DLL/");


        List<string> dllFiles = new List<string>();
        dllFiles.Add("SanNetwork2_x64_release.dll");
        dllFiles.Add("SanNetwork2_release.dll");
        dllFiles.Add("SanNetwork2_x64_debug.dll");
        dllFiles.Add("SanNetwork2_debug.dll");

        foreach(string filePath in dllPaths)
		{
            foreach (string fileName in dllFiles)
            {
                string loadTarget = filePath + fileName;
                if (nativeLibraryPtr == IntPtr.Zero)
                {
                    nativeLibraryPtr = Native.LoadLibraryA(loadTarget);
                    Debug.Log($"LoadLibraryA {nativeLibraryPtr == IntPtr.Zero} ,  {nativeLibraryPtr} , { Native.GetLastError()}");
                }

                if (nativeLibraryPtr == IntPtr.Zero)
                {
                    nativeLibraryPtr = Native.LoadLibrary(loadTarget);
                    Debug.Log($"LoadLibrary {nativeLibraryPtr == IntPtr.Zero} ,  {nativeLibraryPtr} , { Native.GetLastError()}");
                }
            }
        }

       


        if (nativeLibraryPtr == IntPtr.Zero)
        {
            Debug.Log("Error : Load native library" + Native.GetLastError());

            MessageBoxManager.Instance.ShowMessageBox("Error : Load native library" + Native.GetLastError());
        }
        else
        {
            Debug.Log("Success to load native library");
        }
        
    }

    public void BindMessageCallback(MessageCallback messageCallback)
		{
            message_callback = messageCallback;
            Native.Invoke<fnBindProcPacket>(nativeLibraryPtr, net, message_callback);
        }
        public void BindLogCallback(Win32Callback messageCallback)
        {
            log_callback = messageCallback;
            int result = Native.Invoke<int, fnBindLogFunction>(nativeLibraryPtr, log_callback, 1);
        }

        public void BindAcceptCallback(NetworkCallback callback)
        {
            accept_callback = callback;
            Native.Invoke<fnBindAcceptCallback>(nativeLibraryPtr, net, accept_callback);
        }
        public void BindCloseCallback(NetworkCallback callback)
        {
            close_callback = callback;
            Native.Invoke<fnBindCloseCallback>(nativeLibraryPtr, net, close_callback);
        }
        public void Start(int port)
		{
            net = Native.Invoke<IntPtr, CreateNetwork>(nativeLibraryPtr, 1, port, false);


            
        }

        public bool ConnectToServer(string ip)
	    {
            bool result = Native.Invoke<bool, Connect>(nativeLibraryPtr, net, ip);
            return result;
        }

        public void Update()
        {

                Native.Invoke<ProcMessage>(nativeLibraryPtr, net);
        }


        void WorkerThread()
        {
            Native.Invoke<Worker>(nativeLibraryPtr, net);
        }

        ~NetworkModule()
        {
            if (networkMain != null)
            {
                if (networkMain.IsAlive)
                    networkMain.Abort();
            }
            foreach (var item in workers)
            {
                if (item.IsAlive)
                {
                    item.Abort();
                    Console.WriteLine("Thread Abort");
                    item.Join();
                    Console.WriteLine("Thread Join");
                }
            }
            bool s = Native.Invoke<bool, DisposeNetwork>(nativeLibraryPtr, net);
            if (nativeLibraryPtr == IntPtr.Zero) return;

            Console.WriteLine(Native.FreeLibrary(nativeLibraryPtr)
                          ? "Native library successfully unloaded."
                          : "Native library could not be unloaded.");
        }
     
        public static void ProcPacket(IntPtr buf, int len, int sessionId)
        {
            Console.WriteLine("proc packet" + len);

            byte[] managedArray = new byte[len];
            Marshal.Copy(buf, managedArray, 0, len);

            return;
        }

        public void SendTo(byte[] buf)
        {

            IntPtr unmanagedPointer = Marshal.AllocHGlobal(buf.Length);
            Marshal.Copy(buf, 0, unmanagedPointer, buf.Length);
            // Call unmanaged code

            Native.Invoke<SendToServer>(nativeLibraryPtr, net, unmanagedPointer, buf.Length);

            Marshal.FreeHGlobal(unmanagedPointer);
        }
        public void SendTo(byte[] buf, int len)
        {
            Console.WriteLine(len + " sended");
            IntPtr unmanagedPointer = Marshal.AllocHGlobal(len);
            Marshal.Copy(buf, 0, unmanagedPointer, len);
            // Call unmanaged code

            Native.Invoke<SendToServer>(nativeLibraryPtr, net, unmanagedPointer, len);

            Marshal.FreeHGlobal(unmanagedPointer);
        }
    }
