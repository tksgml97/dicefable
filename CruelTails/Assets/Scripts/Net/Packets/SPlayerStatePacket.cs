﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPlayerStatePacket : ServerPacketBase
{
	int player;
	int score;
	public SPlayerStatePacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SPlayerState;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		player = readBuffer.ReadInteger();
		score = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{


	}
}
