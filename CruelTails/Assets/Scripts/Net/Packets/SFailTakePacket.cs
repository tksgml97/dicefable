﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFailTakePacket : ServerPacketBase
{
	public SFailTakePacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SFailTake;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		Vector3 pos = GameManager.playerController.transform.position;
		MessageUIManager.instance.CreateMessageFromWorldPosition(pos, "뺏을 수 있는 아이템이 없습니다");
	}
}
