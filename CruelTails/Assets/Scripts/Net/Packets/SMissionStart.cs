﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMissionStartPacket : ServerPacketBase
{
	MissionState missionState;
	int missionPlayMember;
	int missionID;

	public SMissionStartPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SMissionStart;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		missionState = (MissionState)readBuffer.ReadInteger();
		missionPlayMember = readBuffer.ReadInteger();	
		missionID = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		ChristmasGameManager.Instance.SetMissionState(missionState, missionID, missionPlayMember);
	}
}
