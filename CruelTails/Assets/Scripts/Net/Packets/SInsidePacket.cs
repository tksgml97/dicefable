using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SInsidePacket : ServerPacketBase
{
	int playerNum;
	bool isInside;
	Vector3 pos;
	public SInsidePacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SInsideTeleport;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		playerNum = readBuffer.ReadInteger();
		pos = readBuffer.ReadVector3();
		isInside = readBuffer.ReadBool();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		//if (game.networkPlayer[playerNum] == null) return;
		GameManager.instance.minimap.SetInside(playerNum, isInside, pos);

	}
}
