﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SKillPlayerPacket : ServerPacketBase
{
	int killPlayer;
	int winTeam;
	public SKillPlayerPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SKillPlayer;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		killPlayer = readBuffer.ReadInteger();
		winTeam = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		GameManager.instance.SetKillPlayer(killPlayer);
		if (GameManager.instance.currentGame == eGameRound.Round3 && ChristmasGameManager.Instance.state == Round3State.secondGame)
		{
			ChristmasGameManager.Instance.EndSecondGame();
			return;
		}
		if(GameManager.instance.currentGame == eGameRound.Round4)
        {
			bool End = R4GameManager.instance.CalcTrueEnd(); // 엔딩 결과
		}
		//GameManager.instance.SetKillPlayer(killPlayer);
		GameManager.playerController.PlayGameEndMotion(winTeam == (int)GameManager.instance.myTeam);
		GameManager.instance.PlayKIllScene();
	}
}
