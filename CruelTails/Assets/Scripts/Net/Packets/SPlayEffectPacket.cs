﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPlayEffectPacket : ServerPacketBase
{
	eParticleType type;
	Vector3 pos;
	Quaternion rot;
	public SPlayEffectPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SPlayEffect;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		type = (eParticleType)readBuffer.ReadInteger();
		pos = readBuffer.ReadVector3();
		rot = readBuffer.ReadQuaternion();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		ParticleManager.instance.playEffect(pos, rot, type);
	}
}
