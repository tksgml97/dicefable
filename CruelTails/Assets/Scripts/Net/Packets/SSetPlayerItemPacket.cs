﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSetPlayerItemPacket : ServerPacketBase
{
	int player;
	int itemType;

	public SSetPlayerItemPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SSetPlayerItem;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		player = readBuffer.ReadInteger();
		itemType = readBuffer.ReadInteger();

	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{

		if (itemType == -1) itemType = (int)eItemType.None;
		if (memberNum == player)
		{//local
			GameManager.playerController.GetItem(itemType);
		}
		else
		{
			PlayerAnimationController pac = game.networkPlayer[player].GetComponent<PlayerAnimationController>();
			pac.SetItem(itemType);
		}

	}
}
