using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSpawnDicePacket : ServerPacketBase {
    
    int DiceNumber;
    int DicePrefabNumber;
    int DiceScore;
    Vector3 SpawnPoint;
    public SSpawnDicePacket() : base() {
        m_protocol = PacketType.TCP;
        m_packetType = ServerPackets.SPlayGameData;
        m_gamePacketType = ServerGamePackets.STakeDice;
    }
    public override void Deserialize(ByteBuffer readBuffer) {
        DiceNumber = readBuffer.ReadInteger();
        DicePrefabNumber = readBuffer.ReadInteger();
        DiceScore = readBuffer.ReadInteger();
        SpawnPoint = readBuffer.ReadVector3();
    }

    public override void Excute(NetworkGameManager game, int clientIndex, int memberNum) {
        R4GameManager.instance.SpawnDice(SpawnPoint, DiceNumber, DicePrefabNumber, DiceScore);
    }
}
