﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPlayerInfoPacket : ServerPacketBase
{
	int user;
	int score;
	public SPlayerInfoPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SPlayerInfo;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		user = readBuffer.ReadInteger();
		score = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		Debug.Assert(GameManager.instance.users.Length > user);

		GameManager.instance.users[user].score = score;
		GameManager.instance.playerStateUpdateEvent?.Invoke();
	}
}
