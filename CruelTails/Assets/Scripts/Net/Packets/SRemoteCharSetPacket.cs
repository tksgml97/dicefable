﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SRemoteCharSetPacket : ServerPacketBase
{
	int remtoteMebmerNum;
	PlayerSpineSkinData skinData;
	public SRemoteCharSetPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SRemoteCharSet;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		remtoteMebmerNum = readBuffer.ReadInteger();
		skinData = new PlayerSpineSkinData();
		for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
		{
			skinData.slots.Add((SpineSkinSlot)i, readBuffer.ReadString());
		}
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNumber)
	{
		if (game.isInit == false) return;

		if (memberNumber == remtoteMebmerNum) return;
		RemotePlayer remotePlayer = game.networkPlayer[remtoteMebmerNum].GetComponent<RemotePlayer>();
		remotePlayer.GetPlayerAnimationController().UpdateSpineSkin(skinData);

	}
}
