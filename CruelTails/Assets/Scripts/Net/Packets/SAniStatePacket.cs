using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SAniStatePacket : ServerPacketBase
{
	int playerNum;
	int aniNum;
	public SAniStatePacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SAniState;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		playerNum = readBuffer.ReadInteger();
		aniNum = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		if (game.networkPlayer[playerNum] == null) return;
		RemotePlayer remotePlayer = game.networkPlayer[playerNum].GetComponent<RemotePlayer>();

		remotePlayer.SetAni(aniNum);
	}
}
