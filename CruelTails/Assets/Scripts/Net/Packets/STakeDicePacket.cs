using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STakeDicePacket : ServerPacketBase {
    // �÷��̾�� �� ���� ���� ���ѳ��� �Ұ� ����...
    int playerNum;
    int dice_num;
    public STakeDicePacket() : base() {
        m_protocol = PacketType.TCP;
        m_packetType = ServerPackets.SPlayGameData;
        m_gamePacketType = ServerGamePackets.STakeDice;
    }
    public override void Deserialize(ByteBuffer readBuffer) {
        playerNum = readBuffer.ReadInteger();
        dice_num = readBuffer.ReadInteger();
    }

    public override void Excute(NetworkGameManager game, int clientIndex, int memberNum) {
        R4GameManager.instance.TakeDice(playerNum, dice_num);
    }
}
