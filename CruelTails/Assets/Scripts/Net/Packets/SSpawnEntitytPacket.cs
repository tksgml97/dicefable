﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSpawnEntitytPacket : ServerPacketBase
{
	int entityNumber;
	int room;
	eItemType typ;
	Vector3 point;
	bool playAni;
	Vector3 velocity;
	public SSpawnEntitytPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SSpawnEntity;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		entityNumber = readBuffer.ReadInteger();
		room = readBuffer.ReadInteger();
		typ = (eItemType)readBuffer.ReadInteger();
		point = readBuffer.ReadVector3();
		playAni = readBuffer.ReadBool();
		velocity = readBuffer.ReadVector3();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		EntityManager.instance.SpawnEntity(entityNumber, point, typ, room, playAni, velocity);
	}
}
