﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STakeTablePacket : ServerPacketBase
{
	int playerNum;
	int team;
	int itemType;
	public STakeTablePacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.STakeTable;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		playerNum = readBuffer.ReadInteger();
		team = readBuffer.ReadInteger();
		itemType = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		KoreaGameManager.instance.TakeTable(team, itemType);
	}
}
