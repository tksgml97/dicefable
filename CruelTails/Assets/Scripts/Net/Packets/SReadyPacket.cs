﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SReadyPacket : ServerPacketBase
{
	int member;
	public SReadyPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SReady;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		member = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		NetworkGameManager.instance.loadingUI.Ready(member);
	}
}
