using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SHitSkillObject : ServerPacketBase
{
	int id; // 타격된 스킬오브젝트 id
	public SHitSkillObject() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SHitSkillObject;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		id = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		Debug.Log("[Receive] attacked obj" + id);
		if (ScrollMapObjManager.Instance)
        {
			if(ScrollMapObjManager.Instance.skillObjects[id] != null)
				ScrollMapObjManager.Instance.skillObjects[id].Attacked();
		}
			
	}
}
