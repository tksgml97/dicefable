using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPositionPacket : ServerPacketBase
{
	Vector3 pos;
	Vector3 dir;
	float scale;
	float speed;
	int playerNum;
	public SPositionPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SPosition;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		playerNum = readBuffer.ReadInteger();

		pos = new Vector3(readBuffer.ReadFloat(), readBuffer.ReadFloat(), readBuffer.ReadFloat());
		dir = new Vector3(readBuffer.ReadFloat(), readBuffer.ReadFloat(), readBuffer.ReadFloat());
		scale = readBuffer.ReadFloat();
		speed = readBuffer.ReadFloat();
	}

	public override void Excute(NetworkGameManager game,int clientIndex, int memberNum)
	{
		if (game.networkPlayer[playerNum] == null) return;
		RemotePlayer remotePlayer = game.networkPlayer[playerNum].GetComponent<RemotePlayer>();
		remotePlayer.SetPosition(pos, dir, speed);
		remotePlayer.SetAnimVel(scale, 1);
	}
}
