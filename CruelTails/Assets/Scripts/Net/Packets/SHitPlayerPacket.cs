﻿using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HitPlayerData {
    public enum EHitType {
        player_hit,
        player_atk,
		player_self
	}

	public EHitType type;
	public int target_player_member_number;
    public PlayerController pc;
    public RemotePlayer remote;
}
public class HitPlayerEvent {
    public static Dictionary<string, Action<HitPlayerData>> OnPlayerHit = new Dictionary<string, Action<HitPlayerData>>();
}

public class SHitPlayerPacket : ServerPacketBase
{
	int attacker;
	Vector3 pos;
	Vector3 dir;
	int target;
	bool isTeamChange;
	public SHitPlayerPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SHitPlayer;
	}	
	public override void Deserialize(ByteBuffer readBuffer)
	{
		attacker = readBuffer.ReadInteger();
		pos = new Vector3(readBuffer.ReadFloat(), readBuffer.ReadFloat(), readBuffer.ReadFloat());
		dir = new Vector3(readBuffer.ReadFloat(), readBuffer.ReadFloat(), readBuffer.ReadFloat());
		target = readBuffer.ReadInteger();
		isTeamChange = readBuffer.ReadBool();
		// DropDiceCount
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNumber)
	{

		Debug.Log(attacker + " ->" + target + "(" + memberNumber);

        HitPlayerEvent.OnPlayerHit.ForEach(x => {
            var hit_data = new HitPlayerData();
			
			bool is_attacker = attacker == memberNumber;
            hit_data.type = is_attacker ? HitPlayerData.EHitType.player_atk : HitPlayerData.EHitType.player_hit;
			hit_data.pc = GameManager.playerController;
			if (attacker == target) hit_data.type = HitPlayerData.EHitType.player_self;
			hit_data.target_player_member_number = target;
			x.Value?.Invoke(hit_data);
			});

        if (isTeamChange)
		{
			if (attacker == memberNumber || target == memberNumber)
			{
				int pn = attacker == memberNumber ? target : attacker;
				RemotePlayer remotePlayer = game.networkPlayer[pn].GetComponent<RemotePlayer>();
				int t = (int)remotePlayer.team;
				remotePlayer.SetTeam((int)GameManager.instance.myTeam);
				//remotePlayer.SetWolfHide();
				GameManager.playerController.SetTeam(t);
				GameManager.playerController.WolfFight();
				GameManager.instance.myTeam = (eTeamType)t;
				SoundInstance.Instance.ChangeRoleSound(GameManager.instance.myTeam);
			}
			else
			{
				int t = (int)game.networkPlayer[target].GetComponent<RemotePlayer>().team;
				game.networkPlayer[target].GetComponent<RemotePlayer>().SetTeam((int)game.networkPlayer[attacker].GetComponent<RemotePlayer>().team);
				game.networkPlayer[attacker].GetComponent<RemotePlayer>().SetTeam(t);
				//game.networkPlayer[attacker].GetComponent<RemotePlayer>().SetWolfHide();
			}


			return;
		}
		if (target == memberNumber)
		{
			Debug.Log("Target Client : " + memberNumber);
			GameManager.playerController.Hit(dir);
		}
		else
		{
			RemotePlayer remotePlayer = game.networkPlayer[target].GetComponent<RemotePlayer>();
			remotePlayer.SetHit(pos, dir);
		}

	}
}
