﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SExitClientPacket : ServerPacketBase
{
	int remotePlayerNumber;
	public SExitClientPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SExitClient;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		remotePlayerNumber = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		game.DestroyRemotePlayer(remotePlayerNumber);
	}
}
