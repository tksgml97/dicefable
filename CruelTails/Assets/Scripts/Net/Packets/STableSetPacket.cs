﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STableSetPacket : ServerPacketBase
{
	int item;
	int table;
	public STableSetPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.STableSet;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		item = readBuffer.ReadInteger();
		table = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		KoreaGameManager.instance.SetTable(table, item);
	}
}
