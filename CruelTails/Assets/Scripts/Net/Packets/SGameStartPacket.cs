﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SGameStartPacket : ServerPacketBase
{
	long currentTick;
	int limitTime;
	public SGameStartPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SGameStart;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		currentTick = readBuffer.ReadLong();
		limitTime = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		GameManager.instance.SetTime(currentTick, limitTime);
		Debug.Log("gamestartpacket");
		if (GameManager.instance.currentGame == eGameRound.Round3 && ChristmasGameManager.Instance.state == Round3State.secondGame)
		{
			ChristmasGameManager.Instance.StartSecondGame();
		}
		else
		{
			if (RankManager.Instance) RankManager.Instance.RankInit();
			NetworkGameManager.instance.loadingUI.SetLoadingView(false);
			GameManager.instance.StartGame();

			Debug.Log("next Scene Load?????");
            
		}

	}
}
