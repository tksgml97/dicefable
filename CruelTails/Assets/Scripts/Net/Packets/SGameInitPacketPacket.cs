﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SGameInitPacketPacket : ServerPacketBase
{
	public SGameInitPacketPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SGameInit;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		NetworkGameManager net = NetworkGameManager.instance;
		GameManager.instance.isInit = true;
		Debug.Log("INIT");
		GameManager.playerController.currentItem = eItemType.None;
		int maxPlayer = readBuffer.ReadInteger();
		net.maxPlayer = maxPlayer;
		NetworkGameManager.memberNumber = readBuffer.ReadInteger();
		int myTeam = readBuffer.ReadInteger();
		GameManager.instance.isDie = readBuffer.ReadBool();
		int nSkinSlot = (int)SpineSkinSlot.MAX;
		{
			PlayerSpineSkinData skinData = new PlayerSpineSkinData();

			for (int i = 0; i < nSkinSlot; i++)
			{
				int slot = readBuffer.ReadInteger();
				skinData.slots.Add((SpineSkinSlot)i, SpineSkinManager.instance.GetSkin((SpineSkinSlot)i, slot));

			}
			Color hairColor = Color.white;
			hairColor.r = readBuffer.ReadFloat();
			hairColor.g = readBuffer.ReadFloat();
			hairColor.b = readBuffer.ReadFloat();
			Color skinColor = Color.white;
			skinColor.r = readBuffer.ReadFloat();
			skinColor.g = readBuffer.ReadFloat();
			skinColor.b = readBuffer.ReadFloat();


			skinData.hair = hairColor;
			skinData.skin = skinColor;

			GameManager.instance.myTeam = (eTeamType)myTeam;
			if (GameManager.instance.isDie == false)
			{
				GameManager.playerController.SetSkin(skinData);
				GameManager.playerController.SetTeam(myTeam, true);
				GameManager.playerController.SetColor(hairColor, skinColor);
			}
			GameManager.instance.minimap.AddPlayer((eTeamType)myTeam, GameManager.playerController.transform, NetworkGameManager.memberNumber);

			GameManager.playerController.transform.position = GameManager.instance.GetSpawnPoint(NetworkGameManager.memberNumber);
			NetworkGameManager.instance.GetPlayerInfo(NetworkGameManager.memberNumber).skinData = skinData;
			NetworkGameManager.instance.GetPlayerInfo(NetworkGameManager.memberNumber).playerName = NetworkConnect.myName;
		}
		net.onPlayer = new bool[maxPlayer];
		for (int i = 0; i < maxPlayer; i++)
		{
			net.onPlayer[i] = readBuffer.ReadBool();
		}
		net.Init();

		for (int i = 0; i < maxPlayer; i++)
		{
			if (net.onPlayer[i] == true)
			if (i != NetworkGameManager.memberNumber)
			{
				string name = readBuffer.ReadString();
				int team = readBuffer.ReadInteger();
				bool isDieRemote = readBuffer.ReadBool();
				PlayerSpineSkinData remoteSkinData = new PlayerSpineSkinData();

				for (int j = 0; j < nSkinSlot; j++)
				{
					int slot = readBuffer.ReadInteger();
					remoteSkinData.slots.Add((SpineSkinSlot)j, SpineSkinManager.instance.GetSkin((SpineSkinSlot)j, slot));

				}

				Color hairColor = Color.white;
				hairColor.r = readBuffer.ReadFloat();
				hairColor.g = readBuffer.ReadFloat();
				hairColor.b = readBuffer.ReadFloat();
				Color skinColor = Color.white;
				skinColor.r = readBuffer.ReadFloat();
				skinColor.g = readBuffer.ReadFloat();
				skinColor.b = readBuffer.ReadFloat();


				remoteSkinData.hair = hairColor;
				remoteSkinData.skin = skinColor;
				RemotePlayer remotePlayer = net.networkPlayer[i].GetComponent<RemotePlayer>();
				remotePlayer.SetName(name);
				if (isDieRemote == false)
				{
					remotePlayer.SetSkin(remoteSkinData);
					remotePlayer.SetTeam(team);
					remotePlayer.SetColor(hairColor, skinColor);
				}
				remotePlayer.isDie = isDieRemote;
					net.networkPlayer[i].transform.position = GameManager.instance.GetSpawnPoint(i);
				GameManager.playerController.wolfArrow.AddTarget(remotePlayer.transform);
				GameManager.instance.minimap.AddPlayer((eTeamType)team, remotePlayer.transform, i); ;

				NetworkGameManager.instance.GetPlayerInfo(i).skinData = remoteSkinData;
				NetworkGameManager.instance.GetPlayerInfo(i).playerName = name;


			}
		}


			//////// stage data load
			int currentStage = readBuffer.ReadInteger();
			long currentTick = readBuffer.ReadLong();
			int limitTime = readBuffer.ReadInteger();

		int feverTime = readBuffer.ReadInteger();

			int seed = readBuffer.ReadInteger();


			KoreaGameManager.feverStartSecond = feverTime;

			GameManager.instance.SetCurrentStage(currentStage);

			GameManager.instance.LoadMap();
			//net.loadingImg.SetActive(false);
			GameManager.instance.SetTime(currentTick, limitTime);


			if (GameManager.instance.currentGame == eGameRound.Round3  && R3SecondGameManager.Instance != null)
			{
				R3SecondGameManager.Instance.RandomSeed(seed);
			} else if(GameManager.instance.currentGame == eGameRound.Round4) {
				R4GameManager.seed = seed;
			}

			net.isInit = true;
			NetworkConnect.instance.GetStateData();
		}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		GameManager.instance.gameInitEvent?.Invoke();
	}
}
