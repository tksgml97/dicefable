using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMobTargetPacket : ServerPacketBase
{
    int mob_number;
    int client_member_number;
    Vector3 mob_pos;
    long timestamp;
    public SMobTargetPacket() : base()
    {
        m_protocol = PacketType.TCP;
        m_packetType = ServerPackets.SPlayGameData;
        m_gamePacketType = ServerGamePackets.SMobTarget;
    }
    public override void Deserialize(ByteBuffer readBuffer)
    {
        mob_number = readBuffer.ReadInteger();
        client_member_number = readBuffer.ReadInteger();	
        mob_pos = readBuffer.ReadVector3();
        timestamp = readBuffer.ReadLong();
    }

    public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
    {
        R4GameManager.instance.SetMobTarget(mob_number, client_member_number, mob_pos, timestamp);
    }
}