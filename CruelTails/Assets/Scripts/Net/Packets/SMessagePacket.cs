﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MessageType
{
	chat,
	emoticon
}
public class SMessagePacket : ServerPacketBase
{
	int member;
	MessageType type;
	string message;

	public SMessagePacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.STakeTable;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		member = readBuffer.ReadInteger();
		type = (MessageType)readBuffer.ReadInteger();
		message = readBuffer.ReadString();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		GameObject playerObj=  GameManager.instance.GetPlayer(member);
		PlayerUIController uiController = playerObj.GetComponent<PlayerUIController>();
		uiController.ShowChatting(message);
	}
}
