﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEntityEatStartPacket : ServerPacketBase
{
	int entityNumber;
	int eatPlayerNumber;
	public SEntityEatStartPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SEntityEatStart;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		entityNumber = readBuffer.ReadInteger();
		eatPlayerNumber = readBuffer.ReadInteger();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		EntityManager.instance.Eat(entityNumber, eatPlayerNumber);
	}
}
