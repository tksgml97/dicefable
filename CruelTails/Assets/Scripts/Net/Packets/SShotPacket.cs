using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SShotPacket : ServerPacketBase
{
	int id;
	float dam;
	public SShotPacket() : base()
	{
		m_protocol = PacketType.TCP;
		m_packetType = ServerPackets.SPlayGameData;
		m_gamePacketType = ServerGamePackets.SAniState;
	}
	public override void Deserialize(ByteBuffer readBuffer)
	{
		id = readBuffer.ReadInteger();
		dam = readBuffer.ReadFloat();
	}

	public override void Excute(NetworkGameManager game, int clientIndex, int memberNum)
	{
		if (id == memberNum) return;

		RemotePlayer remotePlayer = GameManager.instance.GetPlayer(id).GetComponent<RemotePlayer>();
		remotePlayer.GetComponentInChildren<PlayerScrollMap>().Damaged(dam, true);
	}
}
