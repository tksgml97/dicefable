
public abstract class ClinetPacketBase
{
	protected PacketType m_protocol;
	protected ClientPackets m_packetType;
	protected ClientGamePackets m_gamePacketType;
	public abstract void Serialize();
}
public abstract class ServerPacketBase
{
	protected PacketType m_protocol;
	protected ServerPackets m_packetType;
	protected ServerGamePackets m_gamePacketType;

	public abstract void Excute(NetworkGameManager game, int clientIndex, int playerNum);
	public abstract void Deserialize(ByteBuffer byteBuffer);
}