using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class PlayerAnimation : MonoBehaviour
{
    //스파인 애니메이션
    public SkeletonAnimation skeletonAnimation;
    public AnimationReferenceAsset[] AnimClip;

    //현재 애니메이션 처리가 무엇인지에 대한 변수
    private AnimState _AnimState;
    
    //현재 애니메이션이 재생되고 있는지에 대한 변수
    private string CurrentAnimation;

    //무브처리
    private Rigidbody2D rig;
    float h, v;
    float speed;
    public float normalSpeed;
    public float runSpeed;
    bool isMoveBack;


    private void Awake() => rig = GetComponent<Rigidbody2D>();

    private void Update()
    {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");

        //위아래 이동이 0일 때 이전 이동중이던 정보를 가져옴
        if(v != 0)
        {
            if (v > 0)
                isMoveBack = true;
            else
                isMoveBack = false;
        }

        if (h == 0f && v == 0f)
            if(isMoveBack)
                _AnimState = AnimState.PC_B_L_idle;
            else
                _AnimState = AnimState.PC_F_L_idle;
        else
        {
            //달리기
            if (Input.GetKey(KeyCode.LeftShift))
            {
                speed = runSpeed;
                if (isMoveBack)
                    _AnimState = AnimState.PC_B_L_run;
                else
                    _AnimState = AnimState.PC_F_L_run;
            }
            //걷기
            else
            {
                speed = normalSpeed;
                if (isMoveBack)
                    _AnimState = AnimState.PC_B_L_walk;
                else
                    _AnimState = AnimState.PC_F_L_walk;
            }

            if (h != 0f) transform.localScale = new Vector2(h * (-1), 1);
            
        }

        //애니메이션 적용
        SetCurrentAnimation(_AnimState);

    }

    private void FixedUpdate()
    {
        transform.position += new Vector3(h, v, 0) * speed * Time.fixedDeltaTime;
    }

    private void _AsyncAnimation(AnimationReferenceAsset animClip, bool loop, float timeScale)
    {
        //동일한 애니메이션을 재생하려고 한다면 아래 코드 구문 실행 X
        if (animClip.name.Equals(CurrentAnimation))
            return;

        //해당 애니메이션으로 변경한다. 
        skeletonAnimation.state.SetAnimation(0, animClip, loop).TimeScale = timeScale;
        skeletonAnimation.loop = loop;
        skeletonAnimation.timeScale = timeScale;

        CurrentAnimation = animClip.name;
    }

    private void SetCurrentAnimation(AnimState _state)
    {
        switch(_state)
        {
            case AnimState.PC_F_L_idle:
                _AsyncAnimation(AnimClip[(int)AnimState.PC_F_L_idle], true, 1f);
                break;
            case AnimState.PC_F_L_walk:
                _AsyncAnimation(AnimClip[(int)AnimState.PC_F_L_walk], true, 1f);
                break;
            case AnimState.PC_B_L_walk:
                _AsyncAnimation(AnimClip[(int)AnimState.PC_B_L_walk], true, 1f);
                break;
            case AnimState.PC_F_L_run:
                _AsyncAnimation(AnimClip[(int)AnimState.PC_F_L_run], true, 1f);
                break;
            case AnimState.PC_B_L_run:
                _AsyncAnimation(AnimClip[(int)AnimState.PC_B_L_run], true, 1f);
                break;
            case AnimState.PC_B_L_idle:
                _AsyncAnimation(AnimClip[(int)AnimState.PC_B_L_idle], true, 1f);
                break;

        }
    }

}
