using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Text;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public enum LocalizationType
{
    None = 0,
    Normal = 10,
    Teenager = 20,
}

public enum eGameRound
{
    Round1,
    Round2,
    Round3,
    Round4,
    None
}
[Serializable]
public enum eTeamType
{
   Red,
   Blue,
   RedHat,
   Wolf,
   None
}

[Serializable]
public struct UserData
{
    public int score;
    public string name;
}
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public UIMinimapView minimap;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] MapManager mapManager;
    [SerializeField] KillSceneController killSceneController;

    [HideInInspector] public bool isRunning = false;
    [HideInInspector] public bool isInit = false;
    [HideInInspector] public bool isDie = false;

     public eTeamType myTeam = 0;
    [HideInInspector] public UserData[] users = new UserData[MAX_PLAYERS];
    public const int MAX_PLAYERS = 100;

    public Color[] teamColor;
    public float knockbackScale = 1f;

    public AnimationCurve knockbackCurve;
    static PlayerController pc = null;
    eTeamType winTeam;
    public eGameRound currentGame = eGameRound.Round1;

    public bool isStart = true;

    [field: SerializeField]
    public LocalizationType LocalizationType { get; private set; } = LocalizationType.Teenager;

    public UnityEvent<int> timeUpdateEvent;
    public UnityEvent<int> gameEndEvent;

    public UnityEvent gameStartEvent;

    public delegate void uiEventArg(bool active);
    public delegate void defaultEventArg();
    public defaultEventArg gameInitEvent;
    public defaultEventArg playerStateUpdateEvent;
    public uiEventArg uiActiveEvent;
    public int currentKillPlayer { get; private set; }

    [Button("gameEndTest")]
    public void GameEndTest()
    {
        int killPlayer = 0;
        gameEndEvent?.Invoke(killPlayer);
    }
    //????? Index ???
    public int GetKillPlayerIndex()
    {
        return currentKillPlayer;
    }

    public void SetActiveUI(bool state)
    {
        uiActiveEvent?.Invoke(state);
    }

    public Color GetTeamColor(eTeamType teamType)
	{
        Debug.Assert(teamColor.Length > (int)teamType);
        return teamColor[(int)teamType];
    }


    public GameObject GetPlayer(int index)
    {
        if (NetworkGameManager.instance.networkPlayer.Length <= index)
        {
            Debug.Log("R4: Length :" + NetworkGameManager.instance.networkPlayer.Length + " index :" + index);
            return null;
        }
        if (index == NetworkGameManager.memberNumber)
		{
            return GameManager.playerController.gameObject;
		}
        return NetworkGameManager.instance.networkPlayer[index];
    }
    public string GetNameFromPlayerIndex(int index)
    {
        if (index == NetworkGameManager.memberNumber)
        {  // local player
            return NetworkConnect.myName;
        }
        else
        {
            GameObject rp = GetPlayer(index);
            if (rp == null) return "\0";
            //remote player
            return rp.GetComponent<RemotePlayer>().playerName;
        }
    }

    public PlayerSpineSkinData GetSkinFromPlayerIndex(int index)
	{
        if (GetPlayer(index) == null) return null;
        return GetPlayer(index).GetComponent<PlayerAnimationController>().currentSkin;
	}

    public eTeamType GetTeamFromPlayerIndex(int index)
	{
        if(index == NetworkGameManager.memberNumber)
		{ // local player
            return (eTeamType)GameManager.instance.myTeam;
		}
		else{
            GameObject rp = GetPlayer(index);
            if (rp == null) return eTeamType.Blue;
            //remote player
            return rp.GetComponent<RemotePlayer>().team;
        }
	}

    public int GetPlayerSize()
	{
        return NetworkGameManager.instance.networkPlayer.Length;
	}



    public void StartGame()
    {
        gameStartEvent?.Invoke();
        startTime = DateTime.Now;
        StartCoroutine(WaitStartGame(4f));
    }
    IEnumerator WaitStartGame(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        isStart = true;
        if (GameManager.instance.currentGame == eGameRound.Round2) {
            Round2_Tutorial.instance.StartTutorial();
        }
    }
    public void LoadMap()
	{
        mapManager.LoadMap(currentGame);
    }

    public void SetCurrentStage(int stageNum)
	{
		switch (stageNum)
		{
            case 0:
                currentGame = eGameRound.Round1;
                break;
            case 1:
                currentGame = eGameRound.Round2;
                break;
            case 2:
                currentGame = eGameRound.Round3;
                break;
            case 3:
                currentGame = eGameRound.Round4;
                break;
        }

        Debug.Log(currentGame.ToString());

    }
	public void SetKillPlayer(int index)
    {
        currentKillPlayer = index;
        if (currentGame == eGameRound.Round3 && ChristmasGameManager.Instance.state == Round3State.firstGame) return;
        NetworkGameManager.instance.GetPlayerInfo(index).isDIe = true;
    }
    public void SetWinTeam(int team)
    {
        winTeam = (eTeamType)team;
    }
    public static PlayerController playerController
    {
        get
        {
            if (pc == null) pc = instance.playerPrefab.GetComponent<PlayerController>();
            return pc;
        }
    }
    public void PlayKIllScene()
    {
        if (ScrollMapManager.Instance && ChristmasGameManager.Instance.state == Round3State.secondGame)
        {
            return;
        }
        Debug.Log("PlayerKillScene");
        int killPlayer = GameManager.instance.GetKillPlayerIndex();
        gameEndEvent?.Invoke(killPlayer);
    }

    private void Awake()
    {
        Debug.Assert(instance == null);
        instance = this;
        Application.targetFrameRate = 60;

        for (int i = 0; i < MAX_PLAYERS; i++)
        {
            users[i].name = "";
            users[i].score = 0;
        }

        if (mapManager == null)
		{
            GameObject map = GameObject.Find("Maps");
            mapManager = map?.GetComponent<MapManager>();

            Debug.Assert(mapManager != null);

		}
    }
    DateTime startTime = DateTime.Now;
    long startTick = 0;
    public void SetTime(long currentTick, int limitTime)
	{
        startTick = currentTick;
        startTime = new DateTime(currentTick);
        //startTime = DateTime.Now;
        gameLimitTime = limitTime;
        Debug.Log("limit Time " + gameLimitTime);
    }

    
    // Start is called before the first frame update
    void Start()
    {
        if(NetworkConnect.instance == null) // debug
		{
            isStart = true;
        }
		
    }

    public Vector3 GetSpawnPoint(int i)
	{
        MapData mapData = mapManager.GetMapData(currentGame);
        Transform spawnPointTransform = mapData.spawnPoint.GetSpawnPoint(i);
        return spawnPointTransform.position;
    }

    [HideInInspector] public int gameLimitTime = 300;
    StringBuilder sb = new StringBuilder();
    // Update is called once per frame

    public int GetCurrenetTime()
	{
        int s = (int)(DateTime.Now - startTime).TotalSeconds;
        return gameLimitTime - s;
    }
    void Update()
    {
        if (isStart)
        {
            int s = GetCurrenetTime();

            timeUpdateEvent?.Invoke(s);
        }

        // 연출 넘기기용 스피드키
        //if (Input.GetKey(KeyCode.Tab))
        //    Time.timeScale = 5.0f;
        //if (Input.GetKeyUp(KeyCode.Tab))
        //    Time.timeScale = 1.0f;
    }

}
