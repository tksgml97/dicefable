using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Spine.Unity;
using Spine;

public class UIEyeBoneController : MonoBehaviour
{
    [SpineBone]
    public string boneName;
    Spine.Bone bone;

    [SerializeField]
    private Vector3 offset;

    void Awake()
    {
        SkeletonGraphic skeletonAnimation = GetComponent<SkeletonGraphic>();
        bone = skeletonAnimation.Skeleton.FindBone(boneName);
        skeletonAnimation.UpdateWorld += SkeletonAnimation_UpdateWorld;
    }

    void SkeletonAnimation_UpdateWorld(ISkeletonAnimation animated)
    {
        var tagetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        tagetPosition += offset;
        bone.SetLocalPosition(tagetPosition);
    }

}
