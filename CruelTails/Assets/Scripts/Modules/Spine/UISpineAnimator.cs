using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Spine;
using Spine.Unity;
using Sirenix.OdinInspector;

[System.Serializable]
public class UISpineAnimationState
{
    public string clipName;
    public AnimationReferenceAsset referenceAsset;
    public bool isLoop = false;
    public float timeScale = 1f;
    public float mixTime = 0f;

    public UnityEvent startEvent;
    public UnityEvent completeEvent;

    public SerializableDictionary<string, UnityEvent> customEventDic;

    public void HandleCustomEvent(TrackEntry trackEntry, Spine.Event e)
    {
        if(!customEventDic.ContainsKey(e.Data.Name))
            return;

        customEventDic[e.Data.Name]?.Invoke();
    }
}

[RequireComponent(typeof(SkeletonGraphic))]
public class UISpineAnimator : MonoBehaviour
{
    [ShowInInspector]
    [ReadOnly]
    private SkeletonGraphic skeletonGraphic;

    [SerializeField]
    private SerializableDictionary<string, UISpineAnimationState> animationDic = new SerializableDictionary<string, UISpineAnimationState>();

    private void Awake()
    {
        skeletonGraphic = GetComponent<SkeletonGraphic>();
    }

    public void PlayAnimation(string clipName)
    {
        if (!animationDic.ContainsKey(clipName))
            return;

        if(skeletonGraphic == null)
            skeletonGraphic = GetComponent<SkeletonGraphic>();

        var animationState = animationDic[clipName];

        var trackEntry = skeletonGraphic.AnimationState.SetAnimation(0, animationState.referenceAsset, animationState.isLoop);
        trackEntry.TimeScale = animationState.timeScale;
        trackEntry.MixDuration = animationState.mixTime;

        trackEntry.Start += delegate
        {
            animationState.startEvent?.Invoke();
        };

        trackEntry.Complete += delegate
        {
            animationState.completeEvent?.Invoke();
        };

        trackEntry.Event += animationState.HandleCustomEvent;
    }

}
