using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTNearPoint : MonoBehaviour
{
    [SerializeField]
    private BezierCurve bezierCurve;

    [SerializeField]
    private Vector3 nearestPoint;

    private void Update()
    {
        if (bezierCurve == null)
            return;

        nearestPoint = bezierCurve.FindNearestPoint(transform.position);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(nearestPoint, 1.5f);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, nearestPoint);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, (nearestPoint - transform.position).magnitude);
    }

}
