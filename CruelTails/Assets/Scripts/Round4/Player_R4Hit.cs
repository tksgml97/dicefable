using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_R4Hit : MonoBehaviour
{
    public bool isHit { get; set; } = false; // �¾ҳ�
    [SerializeField] float hitAniTime = 2.0f;

    public bool Hit()
    {
        isHit = true;
        Invoke(nameof(HitFalse), hitAniTime);
        return isHit;
    }
    void HitFalse()
    {
        isHit = false;
    }
}
