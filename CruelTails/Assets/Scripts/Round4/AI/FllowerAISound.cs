using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eFllowerType
{
    Attack,
    Explosion
}

public class FllowerAISound : MonoBehaviour
{
    [SerializeField]
    float explosionDelay = 0.5f;
    [SerializeField]
    AK.Wwise.Event sound_explosion;

    public void PlaySound(eFllowerType type)
    {
        switch(type)
        {
            case eFllowerType.Explosion:
                Invoke(nameof(PlayExplosion), explosionDelay);
                break;
        }
    }

    void PlayExplosion()
    {
        sound_explosion?.Post(gameObject);
    }
}
