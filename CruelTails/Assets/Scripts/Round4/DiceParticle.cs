using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceParticle : MonoBehaviour
{
    [SerializeField] ParticleSystem targeted_effect;
    
    public void SetTarget(bool _input)
    {
        if (targeted_effect == null) return;
        if (_input) targeted_effect.Play();
        else targeted_effect.Stop();
       
    }
}
