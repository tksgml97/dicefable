using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MonsterManager : Singleton<MonsterManager>
{
    //public static MonsterManager instance;
    public List<float> spawn_inttervals = new List<float>(); // 남은 시간마다 변하는 스폰 간격
    float spawn_intterval = 0f; // 스폰 간격 (초)
    public List<Vector3> spawnPos_random = new List<Vector3>(); // 스폰 위치
    List<float> speed_random = new List<float>(); // 랜덤 스피드

    [SerializeField] GameObject follower_prefab; FllowerAI fllower_prefab_ai;
    [SerializeField] GameObject spawn_min, spawn_max;

    int cnt = 0; int maxCnt = 100;

    public int[] playerIdx = new int[2];
    int _seed;
    protected override void Awake()
    {
        playerIdx[0] = 0; playerIdx[1] = 0;
        if (NetworkConnect.instance == null)//local
            Init();
    }

    void Update()
    {
        
    }
    public void Init()
    {
            
        //_seed = ;
        Random.InitState(R4GameManager.seed);
        //Debug.Log("monster init");
        // follower data init
        FollowerDataInit(playerIdx[0], playerIdx[1]);

        // spawn interval data = start
        spawn_intterval = spawn_inttervals[0];


        Vector3 min = spawn_min.transform.position; Vector3 max = spawn_max.transform.position;

        // 랜덤 데이터 리스트 추가
        for(int i = 0; i < maxCnt; i++)
        {
            spawnPos_random.Add(new Vector3(Random.Range(min.x, max.x), 0f, Random.Range(min.z, max.z)));
            speed_random.Add(Random.Range(fllower_prefab_ai.speeds[0], fllower_prefab_ai.speeds[1]));
        }

        // spawn
        Invoke(nameof(StartSpawn), spawn_intterval);
        //StartSpawn();
    }

    void StartSpawn()
    {
        if (!GameManager.instance.isStart) return;
        if (cnt >= maxCnt) cnt = 0;

        GameObject f = Instantiate(follower_prefab);
        f.transform.position = spawnPos_random[cnt];
        f.GetComponent<FllowerAI>().speed = speed_random[cnt];

        cnt++;
        Invoke(nameof(StartSpawn), spawn_intterval);
    }

    void FollowerDataInit(int num1, int num2)
    {
        Debug.Log("Player Number : "+ num1+ " " + num2);
        fllower_prefab_ai = follower_prefab.GetComponent<FllowerAI>();
        fllower_prefab_ai.playerIdx[0] = num1;
        fllower_prefab_ai.playerIdx[1] = num2;

        fllower_prefab_ai.speeds[0] = GameDataLoader.instance.follwerData[0].Item1; 
        fllower_prefab_ai.speeds[1] = GameDataLoader.instance.follwerData[0].Item2;
        fllower_prefab_ai.attack = (GameDataLoader.instance.follwerData[1].Item1);
        fllower_prefab_ai.explosion = (GameDataLoader.instance.follwerData[2].Item1);
        fllower_prefab_ai.spawn_num = (int)GameDataLoader.instance.follwerData[3].Item1;

        //start -> first -> end -> last
        for (int i = 0; i < 4; i++)
        {
            spawn_inttervals.Add(GameDataLoader.instance.follwerData[i + 4].Item1);
        }
    }

}
