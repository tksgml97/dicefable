using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ACTIVE
{
    INACTIVE, ACTIVE
}


public class TentacleDetectBox : MonoBehaviour
{

    [SerializeField] TentacleHit tentacleHit;
    public bool isDetect = false;
    public ACTIVE active = ACTIVE.INACTIVE;
    BoxCollider box;
    void Awake()
    {
        box = GetComponent<BoxCollider>();
    }

    private void Start()
    {
        switch (tentacleHit.size)
        {
            case SIZE.SMALL:
                active = ACTIVE.ACTIVE;
                break;
            case SIZE.MIDDLE:
            //    TentacleManager.Instance.M_tentacles.Add(this);
            //    isDetect = true;
            //    break;
            case SIZE.BIG:
                //    TentacleManager.Instance.B_tentacles.Add(this);
                box.enabled = false;
                break;
        }
    }

    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 3 ||
        other.gameObject.layer == 6)
        {
            if (tentacleHit.size == SIZE.SMALL && !isDetect)
            {
                isDetect = true;
                tentacleHit.AttackRangeStart();
            }
        }
    }

    public void Hit()
    {
        tentacleHit.AttackRangeStart();
    }
}
