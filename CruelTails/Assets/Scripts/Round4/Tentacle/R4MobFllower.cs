using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Spine.Unity;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class R4MobFllower : MonoBehaviour
{
    [SerializeField] FllowerAISound followerSound = null;
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SpineAnimation] public string[] run_AniName;
    [SpineAnimation] public string[] explosion_AniName;
    [SpineAnimation] public string[] jump_AniName;
    [SpineAnimation] public string[] idle_AniName;
    [SerializeField, Range(0.1f, 1f)] public float jumpAnimationTime;
    [SerializeField, Range(0.1f, 1f)] public float explosionAnimationTime;
    [SerializeField] private float speed;
    [SerializeField] private float jumpSpeed;

    [SerializeField] public GameObject explosion_range_mesh;
    [SerializeField] public GameObject jump_start_range_mesh;
    public bool IsConnect => NetworkConnect.instance;
    public bool IsLocalPlay => !NetworkConnect.instance;
    public bool Active => gameObject.activeSelf;
    public List<int> targets = new List<int>();
    public Transform current_target => current_target_index != -1 ? players[current_target_index] : null;
    public int current_target_index = -1;
    [SerializeField] public Vector3 lookVector = Vector3.back;
    
    private int ZPosDir => (current_target == null) ? ((lookVector == Vector3.back) ? 0 : 1) 
                                                    : ((current_target.position.z > transform.position.z) ? 0 : 1);
    
    private float _attackRange;
    private float _attackStartRange;
    private Camera _cam;

    public R4GameManager GM;
    public int mobNumber;

    private Dictionary<int, Transform> players = new Dictionary<int, Transform>();

    public Transform MyPlayer => IsLocalPlay ? GameManager.playerController.transform : players[NetworkGameManager.memberNumber];

    public Transform OtherPlayer
    {
        get
        {
            for (int i = 0; i < NetworkGameManager.instance.networkPlayer.Length; i++) 
            {
                if (i != NetworkGameManager.memberNumber)
                {
                    return players[i];
                }
            }
            return null;
        }
    }
    int player1_idx = 0;
    int player2_idx;


    List<int> GetAllPlayerInRoom()
    {
        var result = new List<int>();
        var r = NetworkGameManager.instance.networkPlayer.ToList();
        for (int i =0 ; i < r.Count; i++) result.Add(i);
        result.Add(NetworkGameManager.memberNumber);
        return result;
    }
    Transform GetPlayerInRoom(int idx)
    {
        if(idx == NetworkGameManager.memberNumber) return GameManager.playerController.transform;
        var r = NetworkGameManager.instance.networkPlayer.ToList();
        return r[idx].transform;
    }

    private Text state_text;

    public void StartMob()
    {
        state_text = GetComponentInChildren<Text>();
        state_text.text = "";
        speed = GameDataLoader.instance.follwerData[0].Item1;
        _attackRange = GameDataLoader.instance.follwerData[2].Item1;
        _attackStartRange = GameDataLoader.instance.follwerData[1].Item1;
        
        explosion_range_mesh.transform.localScale = Vector3.one * (_attackRange * 2);
        jump_start_range_mesh.transform.localScale = Vector3.one * (_attackStartRange * 2);
        
        if (IsLocalPlay)
        {
            Debug.Log("R4: IsLocal");
            players[player1_idx] = GameManager.playerController.transform;
        }
        else
        {    
            var net_players = GetAllPlayerInRoom().Where(x => GameManager.instance.GetPlayer(x) != null).ToList();

            player1_idx = net_players[0];
            player2_idx = net_players[1];

            if(GameManager.instance == null) Debug.Log("R4: GameManager.instance is null");

            players[player1_idx] = GetPlayerInRoom(player1_idx);
            players[player2_idx] = GetPlayerInRoom(player2_idx);
        }
        _cam = FindObjectOfType<Camera>();

        
        AnimationChange(idle_AniName[ZPosDir], true);
        StartCoroutine(CheckTarget());
    }

    static bool IsDebugMode;

    private void Update()
    {
        explosion_range_mesh.SetActive(IsDebugMode);
        jump_start_range_mesh.SetActive(IsDebugMode);
        if (IsDebugMode)
        {
            state_text.text = " = State = ";
            state_text.text += "\n" + "cur Target Index : " + current_target_index;
            state_text.text += "\n" + "target Count : " + targets.Count;
            state_text.text += "\n" + "target dist : " + (current_target_index != -1 ? Vector3.Distance(transform.position, current_target.position) : 0);
            state_text.text += "\n" + "animation : " + skeletonAnimation.AnimationName;
        }
        else 
        {
            state_text.text = "";
        }
        if (IsLocalPlay && Input.GetKeyUp(KeyCode.F1))
        {
            IsDebugMode = !IsDebugMode;
        }
    }

    private void FixedUpdate()
    {
        if (current_target == null) return;
        if (bIsAttackStart) return;
        AnimationChange(Math.Abs(speed - jumpSpeed) < 0.001f ? jump_AniName[ZPosDir] : run_AniName[ZPosDir], true);
        skeletonAnimation.transform.rotation = Quaternion.Euler(0, current_target.position.x > transform.position.x ? 180 : 0, 0);
        
        var tpos = new Vector2(current_target.position.x, current_target.position.z);
        var pos = new Vector2(transform.position.x, transform.position.z);
        Vector2 vel = (tpos - pos).normalized * (speed * Time.fixedDeltaTime * GameManager.playerController.normalSpeed);  

        if(Vector3.Distance(transform.position, current_target.position) > 0.01f)
            transform.position += new Vector3(vel.x, 0, vel.y);
    }

    public void SetTarget(int newTargetIdx) {
        if(current_target == null) StartCoroutine(CheckAttack());
        current_target_index = newTargetIdx;
    }

    private bool bIsAttackStart;

    public void StartJumpTo(int TargetPlayerIdx)
    {
        current_target_index = TargetPlayerIdx;
        StartCoroutine(Jump());
    }
    
    IEnumerator Jump()
    {
        speed = jumpSpeed;
        AnimationChange(jump_AniName[ZPosDir], true);
        yield return new WaitForSeconds(jumpAnimationTime);
        bIsAttackStart = true;
        speed = 0f;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<CapsuleCollider>().enabled = false;
        AnimationChange(explosion_AniName[ZPosDir]);
        followerSound?.PlaySound(eFllowerType.Explosion);
        yield return new WaitForSeconds(explosionAnimationTime);
        if (Vector3.Distance(current_target.position, transform.position) < _attackRange)
        {
            if (current_target == MyPlayer)
            {
                if (!IsLocalPlay)
                {
                    var pos = MyPlayer.transform.position;
                    NetworkConnect.instance.SendHitMessage(
                        pos,
                        (pos - transform.position).normalized,
                        NetworkGameManager.memberNumber);
                }
                else
                {
                    try
                    {
                        GameManager.playerController.GetComponent<PlayerController>().Hit((GameManager.playerController.transform.position - transform.position).normalized);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }
    
    bool IsSendAttackMessage;
    
    IEnumerator CheckAttack()
    {
        while (Active && !bIsAttackStart) {
            if (current_target)
            {
                if (Vector3.Distance(transform.position, current_target.position) < _attackStartRange)
                {
                    if(IsLocalPlay) StartCoroutine(Jump());
                    else
                    {
                        IsSendAttackMessage = true;
                        GM.SendAttackStart(mobNumber, current_target_index, transform.position, DateTime.Now.Ticks);
                    }
                }
            }
            if(IsSendAttackMessage) yield return new WaitForSeconds(0.3f);
            else  yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        yield return null;
    }
    
    bool TargetDetect(Vector3 forword_vector, Vector3 target)
    {
        return Vector3.Dot(forword_vector, target - transform.position) > 0;
    }
    
    bool IsTargetCheck(int player_number, int other_player_number)
    {
        var target = players[player_number];
        var other_player = players[other_player_number];
        var pos = transform.position;
        
        var vel = pos - (target.position - other_player.position);
        
        var screenPos = target == MyPlayer ? _cam.WorldToScreenPoint(pos) : _cam.WorldToScreenPoint(pos + vel);
        return screenPos.x < Screen.width && screenPos.y < Screen.height && 
               TargetDetect(Vector3.forward * (ZPosDir == 1 ? -1 : 1), target.position);
    }
    
    IEnumerator CheckTarget()
    {
        while (Active && !bIsAttackStart)
        {
            if(!targets.Contains(player1_idx) && IsTargetCheck(player1_idx,player2_idx)) targets.Add(player1_idx);
            if(!targets.Contains(player2_idx) && IsTargetCheck(player2_idx,player1_idx)) targets.Add(player2_idx);
            
            if(targets.Count > 0)
            {
                var target = targets.OrderBy(x => (transform.position - players[x].position).magnitude).First();
                if (target != current_target_index)
                {
                    if(IsLocalPlay) SetTarget(target);
                    else GM.SendMobTarget(mobNumber, target, transform.position, DateTime.Now.Ticks);
                }
            }
            
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    void AnimationChange(string animName, bool isLoop = false)
    {
        if(skeletonAnimation.AnimationName == animName) return;
        skeletonAnimation.AnimationState.SetAnimation(0, animName, isLoop);
    }
    
}
