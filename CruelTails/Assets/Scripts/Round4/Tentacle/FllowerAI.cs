using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum eMonsterState
{
    IDLE, CHASE, POP
}

public class FllowerAI : MonoBehaviour
{

    [SerializeField] protected SkeletonAnimation skeletonAnim;
    [SpineAnimation] public string run_AniName;
    [SpineAnimation] public string explosion_AniName;
    [SpineAnimation] public string attack_AniName;
    [SpineAnimation] public string jump_AniName;
    BoxCollider box;
    

    eMonsterState s;
    bool isAlive = true;

    // speed * playerspeed;
    //[HideInInspector] 
    [HideInInspector] public float speed, attack, explosion, spawn_start, spawn_first, spawn_second, spawn_last;
    [HideInInspector] public int spawn_num;

    public float[] speeds = new float[2];

    public int[] playerIdx = new int[2];

    Camera cam;

    

    private void Awake()
    {
        
        
        Debug.LogError("예전에 사용됐던 AI 스크립트입니다. 새로운 AI 스크립트를 사용해주세요.");
        Destroy(gameObject);
        
        
        s = eMonsterState.IDLE;
        cam = CameraController3D.instance.GetComponentInChildren<Camera>();
        cosAngle = Mathf.Cos(angle / 2);
    }

    Vector3 lookVector = Vector3.forward; // 바라보는 벡터, 앞:-1, 1:뒤:1
    float angle = 160; // 시야각
    float cosAngle, lookPlayer = 0; // 내적 결과 (앞뒤 판별)
    Vector3 targetVector1, targetVector2;

    private void Update()
    {
        switch (s)
        {
            case eMonsterState.IDLE:
                CalcLook();
                Detect();
                break;

            case eMonsterState.CHASE:
                Trace();
                break;
            //case eMonsterState.JUMP:

            case eMonsterState.POP:
                if(isAlive)
                    Explosion();
                break;

        }
    }

    void CalcLook()// 바라보는 방향을 계산한다
    {
        if(NetworkConnect.instance != null)
        {
            targetVector1 = GameManager.instance.GetPlayer(playerIdx[0]).transform.position;
            targetVector2 = GameManager.instance.GetPlayer(playerIdx[1]).transform.position;

            vec1 = transform.position - GameManager.instance.GetPlayer(playerIdx[0]).transform.position;
            vec2 = transform.position - GameManager.instance.GetPlayer(playerIdx[1]).transform.position;

            lookPlayer = Vector3.Dot(lookVector, vec1.normalized);

            if (vec1.magnitude - vec2.magnitude > 0) // 각 플레이어 - ai 간 거리의 절댓값 비교
            {
                lookPlayer = Vector3.Dot(lookVector, vec2.normalized);
                Debug.Log("Target : " + playerIdx[1] +" " + vec2.magnitude + ", " + vec1.magnitude);
                targetIdx = playerIdx[1];
            }
            else
            {
                Debug.Log("Target : " + playerIdx[0] + " " + vec1.magnitude + " " + vec2.magnitude);
                targetIdx = playerIdx[0];
            }
        }
        else
        {
            targetIdx = playerIdx[0];
            targetVector1 = (transform.position - GameManager.playerController.transform.position);
            lookPlayer = Vector3.Dot(lookVector, targetVector1.normalized);
        }

        if (lookPlayer > cosAngle)
            Debug.Log("Dir : Forward");
        else
            Debug.Log("Dir : Back");
    }

    int targetIdx = 0;
    //Vector3 vec; // 두 명의 실시간 위치 사용해야함
    Vector3 vec1, vec2;
    public void Detect()
    {
        // screen check
        Vector3 screenPos = cam.WorldToScreenPoint(transform.position);
        if (screenPos.x < Screen.width && screenPos.y < Screen.height && lookPlayer > cosAngle)
        {
            // send packet

            Debug.Log("ATTACK State");
            s = eMonsterState.CHASE;
            AnimationChange(run_AniName, true);
        }
        else
        {
            // none
            //Debug.Log("AI out");
        }

        if (NetworkConnect.instance != null)
        {
            
                
            //vec1 = transform.position - GameManager.instance.GetPlayer(playerIdx[0]).transform.position;
            //vec2 = transform.position - GameManager.instance.GetPlayer(playerIdx[1]).transform.position;

            if (vec1.magnitude < attack || vec2.magnitude < attack)
            {
                //s = eMonsterState.ATTACK;
                //skeletonAnim.AnimationState.AddAnimation(0, attack_AniName, false, 0);
            }
            //if(vec1.magnitude < attack)
            //{
            //    targetIdx = playerIdx[0];
            //}
            //else if(vec2.magnitude < attack)
            //{
            //    targetIdx = playerIdx[1];
            //}
        }
        
        else // local
        {
            //vec1 = transform.position - GameManager.playerController.transform.position;


            if (vec1.magnitude < attack)
            {
                //s = eMonsterState.ATTACK;
                //AnimationChange(attack_AniName);
                //skeletonAnim.AnimationState.AddAnimation(0, attack_AniName, false, 0);
            }
        }
    }


    Vector3 targetVec1;
    public void Trace()
    {
        if(NetworkConnect.instance == null)
        {
            vec1 = transform.position - GameManager.playerController.transform.position;

            if (vec1.magnitude < attack) // -> jump
            {
                s = eMonsterState.POP;
                attackVec = GameManager.playerController.transform.position;
                targetIdx = playerIdx[0];
                AnimationChange(jump_AniName);
                Debug.Log("JUMP State");
            }
            else // chase
            {
                
                targetVec1 = GameManager.playerController.transform.position;
                transform.position = Vector3.MoveTowards(transform.position, targetVec1, speed * Time.deltaTime);

            }
        }
        
        else if (NetworkConnect.instance != null)
        {
            Vector3 targetVec1 = GameManager.instance.GetPlayer(playerIdx[0]).transform.position;
            vec1 = transform.position - targetVec1;

            Vector3 targetVec2;

            targetVec2 = GameManager.instance.GetPlayer(playerIdx[1]).transform.position;
            vec2 = transform.position - targetVec2;

            if (vec1.magnitude < explosion)
            {
                s = eMonsterState.POP;
                targetIdx = playerIdx[0];
                AnimationChange(explosion_AniName);


                Debug.Log("JUMP State");
            }
            else if(vec2.magnitude < explosion)
            {
                s = eMonsterState.POP;
                targetIdx = playerIdx[1];
                AnimationChange(explosion_AniName);
                Debug.Log("JUMP State");
            }
            else
            {
                transform.position = (targetIdx == 0) ? Vector3.MoveTowards(transform.position, targetVec1, speed * Time.deltaTime) :
                    Vector3.MoveTowards(transform.position, targetVec2, speed * Time.deltaTime);

            }
        }
    }

    Vector3 attackVec;
    public void Explosion()
    {
        
        if (NetworkConnect.instance != null)
        {
            attackVec = GameManager.instance.GetPlayer(targetIdx).transform.position;

            vec1 = transform.position - attackVec;

            if (vec1.magnitude < 0.5f)
            {

                if (GameManager.instance.GetPlayer(targetIdx).GetComponentInChildren<Player_R4Hit>().Hit())
                {
                    isAlive = false;
                    Debug.Log("User " + targetIdx + "Hit by " + name);

                    R4GameManager.instance.DropDiceByAI(false, targetIdx, GameManager.instance.GetPlayer(targetIdx).transform.position);

                    skeletonAnim.AnimationState.AddAnimation(0, explosion_AniName, false, 0);

                    if(targetIdx == NetworkGameManager.memberNumber)
                    {
                        Debug.Log("Self User " + targetIdx + " Hit");
                        NetworkConnect.instance.SendHitMessage(GameManager.instance.GetPlayer(targetIdx).transform.position, (attackVec - transform.position).normalized, NetworkGameManager.memberNumber);
                    }
                    
                    //GameManager.instance.GetPlayer(targetIdx).GetComponent<PlayerController>().Hit(Vector3.zero);
                    Invoke(nameof(Destroy), 5.0f);

                }

            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, attackVec, speed * 2 * Time.deltaTime);
            }
        }
        else // local
        {
            //attackVec = GameManager.playerController.transform.position;
            vec1 = transform.position - attackVec;

            if (vec1.magnitude < explosion)
            {
                if (GameManager.playerController.GetComponentInChildren<Player_R4Hit>().Hit())
                {
                    isAlive = false;
                    Debug.Log("Local Hit by " + name);

                    R4GameManager.instance.DropDiceByAI(true, 3, GameManager.playerController.transform.position); // 기믹 레이어 판단으로 로컬 임시활용

                    AnimationChange(explosion_AniName);

                    //GameManager.playerController.GetComponent<PlayerController>().Hit(Vector3.zero);
                    GameManager.playerController.GetComponent<PlayerController>().Hit((attackVec - transform.position).normalized);

                    Invoke(nameof(Destroy), 5.0f);

                }
            }
            else
            {
                //Jump
                //transform.Translate(lookVector * speed * Time.deltaTime);
                transform.position = Vector3.MoveTowards(transform.position, attackVec, speed * 3 * Time.deltaTime);
            }
        }
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }


    void AnimationChange(string animName, bool isLoop = false)
    {
        skeletonAnim.AnimationState.ClearTrack(0);
        skeletonAnim.AnimationState.AddAnimation(0, animName, isLoop, 0);
    }
}
