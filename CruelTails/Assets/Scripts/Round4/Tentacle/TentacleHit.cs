using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum DIR
{
    LEFT, RIGHT
}
public enum SIZE
{
    SMALL, MIDDLE, BIG
}


public class TentacleHit : MonoBehaviour
{
    [SerializeField] DIR dir; public SIZE size; 

    [SerializeField] SkeletonAnimation skeletonAni;
    [SerializeField] string hitAniName = "S_attack";
    [SerializeField] string idleAniName = "S_IDLE";

    Spine.Animation IdleAni;
    Spine.Animation HitAni;
    public float hitAniTime = 1f;

    [Header("애니메이션에 차지하는 선딜레이 비율 (0 ~ 1)")]
    [SerializeField] float predelayScale = 0.35f;

    [Header("공격 판정 지속시간")]
    [SerializeField] float attackTime = 0.05f;


    BoxCollider attackBox;
    [SerializeField]MeshRenderer ren;


    
    [SerializeField] TentacleDetectBox detectBox;
    [SerializeField]SpriteRenderer attackRange;
    [Header("공격 범위 표시 시간")]
    [SerializeField] float attackRangeTime = 1.0f;


    private void Awake()
    {
        IdleAni = skeletonAni.skeletonDataAsset.GetSkeletonData(true).FindAnimation(idleAniName);
        HitAni = skeletonAni.skeletonDataAsset.GetSkeletonData(true).FindAnimation(hitAniName);
        hitAniTime = HitAni.Duration;
        attackBox = GetComponent<BoxCollider>();
        attackBox.enabled = false;

        //AttackStart();
    }
    private void Start()
    {
        //switch(size)
        //{
        //    case SIZE.MIDDLE:
        //        TentacleManager.Instance.M_tentacles.Add(this);
        //        break;

        //    case SIZE.BIG:
        //        TentacleManager.Instance.B_tentacles.Add(this);
        //        break;
        //}
        

    }

    public void AttackRangeStart()
    {
        if (attackRange)
        {
            Invoke(nameof(AttackRangeEnd), attackRangeTime);
            attackRange.enabled = true;
        }
        
    }

    void AttackStart() // 1. 공격 애니메이션 실행, 2. 공격 판정 실행
    {
        skeletonAni.AnimationState.SetAnimation(0, HitAni, false);
        Invoke(nameof(Attack), hitAniTime * predelayScale);
    }
    void Attack() // 애니메이션 선딜레이
    {
        if(ren)
            ren.enabled = true;
        if (attackBox) attackBox.enabled = true;
        Invoke(nameof(AttackAniEnd), hitAniTime * (1 - predelayScale));
        Invoke(nameof(BOX), attackTime);
    }
    void AttackAniEnd() // 애니메이션 후딜레이
    {
        if (size == SIZE.SMALL && detectBox) // small tentacle only
            detectBox.isDetect = false;
        skeletonAni.AnimationState.SetAnimation(0, IdleAni, true);
    }
    
    void BOX() // 공격 판정: attackTime동안 지속
    {
        if(ren)
            ren.enabled = false;
        if (attackBox) attackBox.enabled = false;
        //Invoke(nameof(Attack), predelayTime + postdelayTime);
    }

    void AttackRangeEnd()
    {
        attackRange.enabled=false;

        AttackStart();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer==3 || other.gameObject.layer == 6) // Player : 3, Remote : 6
        {
            //if (size != SIZE.SMALL)
                //return;
            bool hit= other.GetComponentInChildren<Player_R4Hit>().Hit();
            if(hit)
            {
                Debug.Log("Hit by " + name);
                R4GameManager.instance.DropDiceByAI(true, other.gameObject.layer, other.transform.position);

                // 플레이어 hit 재생
                if (NetworkConnect.instance && other.gameObject.layer == 3)
                {
                    Debug.Log("Hit Client : " + NetworkGameManager.memberNumber);
                    NetworkConnect.instance.SendHitMessage(other.transform.position, (dir == DIR.LEFT) ? Vector2.right : Vector2.left, NetworkGameManager.memberNumber);
                }
                    
                else
                    other.GetComponent<PlayerController>().Hit((dir == DIR.LEFT) ? Vector2.right : Vector2.left);

                //other.GetComponent<PlayerController>().Hit(Vector3.zero);
            }
        }
    }
}
