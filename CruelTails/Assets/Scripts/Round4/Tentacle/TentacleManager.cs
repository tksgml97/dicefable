using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleManager : Singleton<TentacleManager>
{
    public List<TentacleDetectBox> M_tentacles;
    public List<TentacleDetectBox> B_tentacles;

    float[] m_min = new float[4]; //float[] m_max = new float[4];
    float[] b_min = new float[4]; //float[] b_max = new float[4];

    float m_tentacle_start_interval;
    float b_tentacle_start_interval;

    public List<int> m_tentacle_rand_idx = new List<int>();
    public List<int> b_tentacle_rand_idx = new List<int>();

    int m_cnt = 0; int maxCnt = 100;
    int b_cnt = 0;

    void MTentacleStart()
    {
        //Debug.Log("tentacleM");
        // 게임 종료됨
        //if (!GameManager.instance.isStart) return;
        // 랜덤 카운트 리셋
        if (m_cnt >= maxCnt) m_cnt = 0;

        // 모든 촉수가 사용되었는지
        int full = 0;
        for (int i = 0; i < M_tentacles.Count; i++)
        {
            if (M_tentacles[i].active == ACTIVE.ACTIVE)
            {
                full++;
                //Debug.Log("fullCount " + i);
            }
            else
                break;

        }
        if (full >= M_tentacles.Count) { for (int i = 0; i < M_tentacles.Count; i++) M_tentacles[i].active = ACTIVE.INACTIVE; }

        // 랜덤 인덱스 받아오기
        int random = m_tentacle_rand_idx[m_cnt] % M_tentacles.Count;
        

        if (M_tentacles[random].active == ACTIVE.INACTIVE)
        {
            //Debug.Log(random + "번째 촉수 실행");
            M_tentacles[random].Hit();
            m_cnt++;
            M_tentacles[random].active = ACTIVE.ACTIVE;

            Invoke(nameof(MTentacleStart), m_tentacle_start_interval);
        }

        else
        {
            //Debug.Log(random + "번째는 이미 실행중인 촉수");
            m_cnt++;
            MTentacleStart();
        }
    }

    void BTentacleStart()
    {
        //Debug.Log("tentacleB");
        // 게임 종료됨
        //if (!GameManager.instance.isStart) return;
        // 랜덤 카운트 리셋
        if (b_cnt >= maxCnt) b_cnt = 0;

        // 모든 촉수가 사용되었는지
        int full = 0;
        for (int i = 0; i < B_tentacles.Count; i++)
        {
            if (B_tentacles[i].active == ACTIVE.ACTIVE)
            {
                full++;
                //Debug.Log("fullCount " + i);
            }
            else
                break;

        }
        if (full >= B_tentacles.Count) { for (int i = 0; i < B_tentacles.Count; i++) B_tentacles[i].active = ACTIVE.INACTIVE; }

        // 랜덤 인덱스 받아오기
        int random = b_tentacle_rand_idx[b_cnt] % B_tentacles.Count;


        if (B_tentacles[random].active == ACTIVE.INACTIVE)
        {
            //Debug.Log(random + "번째 촉수 실행");
            B_tentacles[random].Hit();
            b_cnt++;
            B_tentacles[random].active = ACTIVE.ACTIVE;

            Invoke(nameof(BTentacleStart), b_tentacle_start_interval);
        }

        else
        {
            //Debug.Log(random + "번째는 이미 실행중인 촉수");
            b_cnt++;
            BTentacleStart();
        }
    }


    public void Init(int seed)
    {
        Random.InitState(seed);

        for(int i = 0; i < 4; i++)
        {
            m_min[i] = GameDataLoader.instance.TentacleAttack_Mid[i].Item1;
            b_min[i] = GameDataLoader.instance.TentacleAttack_Big[i].Item1;
        }
        //Debug.Log(m_min[0] + " ");// + m_max[0]);
        //Debug.Log(b_min[0]+" ");// b_max[0]);

        m_tentacle_start_interval = m_min[0];
        b_tentacle_start_interval = b_min[0];

        for (int i = 0; i < maxCnt; i++)
        {
            m_tentacle_rand_idx.Add(Random.Range(0, M_tentacles.Count* M_tentacles.Count - 1));
            b_tentacle_rand_idx.Add(Random.Range(0, B_tentacles.Count* B_tentacles.Count - 1));
        }
        Debug.Log("Tentacle init");
        
        Invoke(nameof(MTentacleStart), m_tentacle_start_interval);
        Invoke(nameof(BTentacleStart), b_tentacle_start_interval);
    }

    public void ChangeInterval(int idx)
    {
        if (idx < m_min.Length && idx < b_min.Length)
        {
            m_tentacle_start_interval = m_min[idx];
            b_tentacle_start_interval = b_min[idx];

            Debug.Log(m_tentacle_start_interval + " " + b_tentacle_start_interval);
        }
        else
            Debug.Log("Interval is Out Of Index");
    }
}
