using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPopupUI : MonoBehaviour
{
    //GameObject popup_ui; // 0: 1, 1: 10, 2: 100
    Animator popup_ui_ani;
    [SerializeField] Text popup_text;

    void Awake()
    {
        popup_ui_ani = GetComponent<Animator>();
    }

    public void ShowPopup(int idx)
    {
        this.gameObject.SetActive(true);
        popup_ui_ani.SetTrigger("Trigger"); // fever time ui ani
        popup_text.text = GameDataLoader.instance.GetScript(4000 + idx).m_scriptResource;
    }
}
