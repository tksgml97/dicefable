using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public static ItemManager instance;

    public Dictionary<int,NetworkObject> items = new Dictionary<int, NetworkObject>();

    private void Awake()
	{
        instance = this;
	}
    int currentItemNum = 0;

    public void AddItem(NetworkObject go, int id)
	{
        go.id = id;
        items.Add(id, go);

	}

	public void DestItem(int index)
	{
        if (items[index] != null)
        {
            Destroy(items[index]);
            items[index] = null;
        }
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
