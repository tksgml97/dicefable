using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FranceGameManager : MonoBehaviour
{
    public static FranceGameManager instance;

    List<Mission> missions = new List<Mission>();

    public UnityEvent<float, MissionData> missionUpdateEvent;
    public UnityEvent<List<Mission>> missionListUpdate;

    bool isAllClear = false;
    private void Awake()
    {
        Debug.Assert(instance == null);
        instance = this;
    }

	private void Start()
    {
        UpdateMissionList();
    }

	private void Update()
	{
        if(isAllClear) CamSetWolf();
    }

	public void AddMission(Mission mission)
	{
        missions.Add(mission);
        UpdateMissionList();
    }

    public void Clear(Mission mission)
    {
		for (int i = 0; i < missions.Count; i++)
		{
            if(missions[i].missionState == MissionState.Disable && missions[i].data.m_missionTriggerID == mission.data.m_missionID)
			{
                missions[i].SetMissionActive();
            }

        }
        UpdateMissionList();

        if(CheckAllClear())
		{
            AllClear();
		}
    }


    public void UpdateMissionList()
	{
        missionListUpdate?.Invoke(missions);
    }
    
    public bool CheckAllClear()
	{
		for (int i = 0; i < missions.Count; i++)
		{
            if(missions[i].missionState != MissionState.Claer)
			{
                return false;
			}
        }
        return true;
	}
    public void AllClear()
	{

        MessageUIManager.instance.CreateMessageFromScreenPosition(Vector3.zero, "미션클리어");
        MessageUIManager.instance.CreateMessageFromScreenPosition(new Vector3(Screen.width, Screen.height)/2, "미션클리어");


        CamSetWolf();
        isAllClear = true;

        NetworkConnect.instance.SendMissionClear();
        GameManager.playerController.Remove();

    }

    void CamSetWolf()
	{
        var cam = GameObject.Find("CameraHolder").GetComponent<CameraController3D>();
        cam.target = transform;

        for (int i = 0; i < GameManager.instance.GetPlayerSize(); i++)
        {
            eTeamType team = GameManager.instance.GetTeamFromPlayerIndex(i);
            if (team == eTeamType.Wolf)
            {

                cam.target = GameManager.instance.GetPlayer(i).transform;

            }
        }
    }

}
