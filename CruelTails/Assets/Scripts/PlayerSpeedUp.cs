using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpeedUp : MonoBehaviour
{
    [SerializeField] float portalActiveRange = 3f;
    [SerializeField] GameObject itemUI;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((GameManager.playerController.transform.position - transform.position).magnitude < portalActiveRange && itemUI != null)
        {
            itemUI.SetActive(true);
            if (Input.GetKeyDown("space"))
            {
                SpeedUp();
            }
        }
        else if((GameManager.playerController.transform.position - transform.position).magnitude >= portalActiveRange && itemUI != null)
        {
            itemUI.SetActive(false);
        }
       

    }
    public void SpeedUp()
    {
        GameManager.playerController.runSpeed += 5;
        GameManager.playerController.normalSpeed += 5;
        Destroy(itemUI);
        Debug.Log("speedUp");
        Debug.Log(GameManager.playerController.runSpeed);


    }
}
