using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//update spine skin
//map data request
//game end

namespace DiceFableServer.Network.Packet
{

    public enum PackType
    {
        MessagePacket,
        UpdatePositionRequest,
        UpdatePosition,
        GetItemRequest,
        GetItem,
        SpawnItem,
        SetGameData,
        SetItem,
        SetTable,
        UpdateGameData,
        PlayerConnection,
        GameOver,
        MAX
    }

    public class PacketHeaderReader
    {
        public static PackType ReadHeader(ByteSerializer serializer)
        {
            return (PackType)serializer.ReadInteger();
        }
    };

    public abstract class PacketBase : ISerializable
    {
        public PackType PackType;

        public PacketBase(PackType type)
        {
            PackType = type;
        }

        public void SerializeHeader(ByteSerializer serializer)
        {
            serializer.WriteInteger((int)PackType);
        }
        public void DeserializeHeader(ByteSerializer serializer)
        {
            PackType = (PackType)serializer.ReadInteger();
        }

        public abstract void Serialize(ByteSerializer serializer);
        public abstract void Deserialize(ByteSerializer serializer);
    }

    public class MessagePacket : PacketBase
    {
        public string message;
        public MessagePacket() : base(PackType.MessagePacket) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteString(message);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            message = serializer.ReadString();
        }
    }

    public class UpdatePositionRequestPacket : PacketBase
    {
        public Vector3 position;
        public bool direction;
        public int animationState;
        public UpdatePositionRequestPacket() : base(PackType.UpdatePositionRequest) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteVector3(position);
            serializer.WriteBool(direction);
            serializer.WriteInteger(animationState);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            position = serializer.ReadVector3();
            direction = serializer.ReadBool();
            animationState = serializer.ReadInteger();
        }
    }
    public class GetItemRequestPacket : PacketBase
    {
        public int itemID;
        public GetItemRequestPacket() : base(PackType.GetItemRequest) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(itemID);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            itemID = serializer.ReadInteger();
        }
    }

    public class GetItemPacket : PacketBase
    {
        public int itemID;
        public int playerNumber;
        public GetItemPacket() : base(PackType.GetItem) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(itemID);
            serializer.WriteInteger(playerNumber);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            itemID = serializer.ReadInteger();
            playerNumber = serializer.ReadInteger();
        }
    }
    public class SetGameData : PacketBase
    {
        public int playerNumber;
        public long startTime;
        public SetGameData() : base(PackType.SetGameData) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(playerNumber);
            //serializer.WriteLong(startTime);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            playerNumber = serializer.ReadInteger();
            //startTime = serializer.ReadLong();
        }
    }

    public class SpawnItemPacket : PacketBase
    {
        public Vector3 position;
        public int type;
        public int id;
        public SpawnItemPacket() : base(PackType.SpawnItem) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteVector3(position);
            serializer.WriteInteger(type);
            serializer.WriteInteger(id);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            position = serializer.ReadVector3();
            type = serializer.ReadInteger();
            id = serializer.ReadInteger();
        }
    }
    public class UpdatePositionPacket : PacketBase
    {
        public Vector3 position;
        public bool direction;
        public int playerNumber;
        public int animationState;
        public UpdatePositionPacket() : base(PackType.UpdatePosition) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteVector3(position);
            serializer.WriteBool(direction);
            serializer.WriteInteger(playerNumber);
            serializer.WriteInteger(animationState);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            position = serializer.ReadVector3();
            direction = serializer.ReadBool();
            playerNumber = serializer.ReadInteger();
            animationState = serializer.ReadInteger();
        }
    }

    public class SetItemPacket : PacketBase
    {
        public int playerNumber;
        public int item;
        public SetItemPacket() : base(PackType.SetItem) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(playerNumber);
            serializer.WriteInteger(item);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            playerNumber = serializer.ReadInteger();
            item = serializer.ReadInteger();
        }
    }

    public class SetTablePacket : PacketBase
    {
        public int playerNumber;
        public int item;
        public int tableNum;
        public SetTablePacket() : base(PackType.SetTable) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(playerNumber);
            serializer.WriteInteger(item);
            serializer.WriteInteger(tableNum);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            playerNumber = serializer.ReadInteger();
            item = serializer.ReadInteger();
            tableNum = serializer.ReadInteger();
        }
    }

    public class UpdateGameData : PacketBase
    {
        public int uid;
        public int score;
        public string name;
        public UpdateGameData() : base(PackType.UpdateGameData) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(uid);
            serializer.WriteInteger(score);
            serializer.WriteString(name);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            uid = serializer.ReadInteger();
            score = serializer.ReadInteger();
            name = serializer.ReadString();
        }
    }
    public class PlayerConnectionPacket : PacketBase
    {
        public int uid;
        public bool connect;
        public PlayerConnectionPacket() : base(PackType.PlayerConnection) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(uid);
            serializer.WriteBool(connect);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            uid = serializer.ReadInteger();
            connect = serializer.ReadBool();
        }
    }

    public class GameOverPacket : PacketBase
    {
        public int dieUserId;
        public GameOverPacket() : base(PackType.GameOver) { }
        public override void Serialize(ByteSerializer serializer)
        {
            serializer.WriteInteger(dieUserId);
        }
        public override void Deserialize(ByteSerializer serializer)
        {
            dieUserId = serializer.ReadInteger();
        }
    }
}
