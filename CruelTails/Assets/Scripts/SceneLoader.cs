using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum eSceneType
{
    Lobby,
    Round1,
    Round2,
    Round3,
    Round4,
    AllRound
}
public class SceneLoader : MonoBehaviour
{
    public static SceneLoader instance;
    Dictionary<eSceneType, string> sceneNames;
    [SerializeField] Text m_Text;
    float loadingProgress = 0;
    public LoadingUpdateEvent loadingUpdateEvent;

    public delegate void LoadingUpdateEvent(float progress);
    public delegate void LoadingEndEvent();
    // Start is called before the first frame update

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
        if (sceneNames == null)
        {
            buildSceneNames();
        }
    }

    void buildSceneNames()
    {
        sceneNames = new Dictionary<eSceneType, string>();
        sceneNames[eSceneType.Lobby] = "Lobby_BuildScene";
        sceneNames[eSceneType.Round1] = "Round1_BuildScene";
        sceneNames[eSceneType.Round2] = "Round2_BuildScene";
        sceneNames[eSceneType.Round3] = "Round3_BuildScene";
        sceneNames[eSceneType.Round4] = "Round4_BuildScene";
        sceneNames[eSceneType.AllRound] = "GameScene";
    }

    eGameRound SceneToRound(eSceneType scene)
	{
		switch (scene)
		{
            case eSceneType.Round1:
                return eGameRound.Round1;

            case eSceneType.Round2:
                return eGameRound.Round2;

            case eSceneType.Round3:
                return eGameRound.Round3;

            case eSceneType.Round4:
                return eGameRound.Round4;
        }
        return eGameRound.None;

	}



	public IEnumerator LoadScene(eSceneType scene, LoadingEndEvent loadingEnd)
    {
        Debug.Log("Go Main2");
        if (scene != eSceneType.Lobby)
        {
            if (NetworkGameManager.instance.isDie())
            {
                NetworkGameManager.instance.gameOverUI.SetActiveGameOverView(true);
            }
            else
            {
                NetworkGameManager.instance.loadingUI.SetLoadingView(true);
                NetworkGameManager.instance.loadingUI.SetRound(SceneToRound(scene));
            }
        }
        Debug.Log("LoadScene" + scene.ToString());
        yield return null;

        string sceneName = sceneNames[scene];
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
        asyncOperation.allowSceneActivation = false;
        asyncOperation.completed += (AsyncOperation) =>
        {
            loadingProgress = 1;
            // m_Text.text = "Loading " + Mathf.Round(asyncOperation.progress * 100) + "%";
             m_Text.text = "";
            Debug.Log("LoadComplete");
            loadingUpdateEvent?.Invoke(loadingProgress);
            loadingEnd?.Invoke();
        };
        Debug.Log("Load1");
        while (!asyncOperation.isDone)
        {
            loadingProgress = asyncOperation.progress;
            m_Text.text = "Loading " + Mathf.Round(asyncOperation.progress * 100) + "%";

            loadingUpdateEvent?.Invoke(loadingProgress);
            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
                yield break;
            }
            yield return null;
        }
    }
}
