using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class PlayerAnimationController : MonoBehaviour
{
    [SerializeField] private AnimationCurve hitColorCurve;
    [SerializeField] private SpriteRenderer item;
    [SerializeField] private SkeletonAnimation skeletonAnimation;
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Transform scale;

    [SerializeField] float attackSpeed = 1f;
    private float attackEventTime = 0.2f;
    
    [HideInInspector] public Vector3 attackDirection = Vector3.zero;
    [HideInInspector] public AnimState currentState;
    [HideInInspector] public PlayerSpineSkinData currentSkin = null;
    [SerializeField] private AnimationCurve jumpCurve;
    PlayerMoveSound moveSound;
    public AnimationCurve attackCurve;
    public AnimationCurve wolfAttackCurve;

    private AnimState _AnimState;


    private bool updateSkin = false;
    private float prevAnimationTime = 0;
    public float currentAnimatonTime = 0;
    private string currentAnimation;
    public float hitColorTime = 0;
    public void SetItem(int type)
    {
        if (KoreaItemResourceLoader.Instance == null) return;
        Sprite sp = KoreaItemResourceLoader.Instance.GetSprite((eItemType)type);
        item.sprite = sp;
    }

    public void DeleteItem()
    {
        item.sprite = null;
    }
    Skin skeletonskin;
    void Awake()
    {
        DeleteItem();
        moveSound = GetComponent<PlayerMoveSound>();
    }

    public bool SoundDistance()
    {
        return (GameManager.playerController.transform.position - transform.position).magnitude < 3;
        
    }

    private void Start()
    {
        //??? ?????? ????
        if(NetworkConnect.instance == null)
            InitSkin();
    }

    private float hideTime = 0;

    // Update is called once per frame
    void Update()
    {

        meshRenderer.enabled = hideTime <= 0 || _AnimState != AnimState.PC_wolf_rotation;
        if (hideTime > 0) hideTime -= Time.deltaTime;

        prevAnimationTime = currentAnimatonTime;
        currentAnimatonTime += Time.deltaTime;
        hitColorTime = Mathf.Max(0, hitColorTime - Time.deltaTime);
    }
    void LateUpdate()
    {
        UpdateColor();

        if(currentState == AnimState.PC_wolf_attack_side || currentState == AnimState.PC_wolf_attack_top)
        {
            var jumpAni = SpineAniLoader.instance.GetAni(currentState);
            float aniTime = jumpAni.Animation.Duration;
            float wolfY = jumpCurve.Evaluate(currentAnimatonTime / aniTime);
            scale.localPosition = new Vector3(0, wolfY, 0);

        } else
		{

            scale.localPosition = new Vector3(0, 0, 0);
        }
    }


    void InitSkin()
    {
        if (currentSkin != null) return;
        currentSkin = SpineSkinManager.instance.MakeRandomSkin();
        UpdateSpineSkin(currentSkin);
    }

    public void SetHide(float second)
	{
        hideTime = second;
    }

    public void SetTeam(int team)
    {
        //InitSkin();
        currentSkin.slots[SpineSkinSlot.team] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.team, team);
        UpdateSpineSkin(currentSkin);
    }

    Color hair;
    Color head = new Color(1,1,1,1);

    public void SetSkin(PlayerSpineSkinData skin)
    {
        currentSkin = skin;
    }

    public void SetColor(Color hairColor, Color headColor)
	{
        hair = hairColor;
        hair.a = 1;
        head = headColor;
        head.a = 1;
    }

    public void UpdateSpineSkin(PlayerSpineSkinData skin)
    {
        currentSkin = skin;
        Skeleton skeleton = skeletonAnimation.Skeleton;
        Skin SKIN = new Skin(skin.ToString());

        foreach (var item in skin.slots)
        {
            if(GameManager.instance.currentGame == eGameRound.Round1)
			{
                if (item.Key == SpineSkinSlot.hat) continue;
                if (item.Key == SpineSkinSlot.helmet) continue;
            }
            if (item.Value == "") continue;
            Skin addSkin = skeleton.Data.FindSkin(item.Value);
            if (addSkin == null)
            {
                Debug.LogError(item.Value);
            }
            SKIN.AddSkin(addSkin);
            
        }
        skeleton.SetSkin(SKIN);
        skeleton.SetSlotsToSetupPose();
        skeletonAnimation.AnimationState.Apply(skeleton);   

    }

    public float colorMul = 1;
    void UpdateColor()
	{
        foreach (var item in skeletonAnimation.Skeleton.Slots)
        {
            string slotName = item.ToString();
            if (SpineSkinManager.instance.slotTypes.ContainsKey(slotName) == false) continue;
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.effect)
            {
                item.SetCustomColor(new Color(0, 1, 0, 0));
                continue;
            }
            Color color = item.GetColor();
            float alpha = color.a;

            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.hair)
            {
                color = hair;
            }
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.skin)
            {
                color = head;
            }
            float val = hitColorCurve.Evaluate(hitColorTime);
            color.a = alpha;
            item.SetColor(color);
            item.SetCustomColor(new Color(colorMul * val, 0, 0, 1));
        }
    }
    private bool _AsyncAnimation(AnimationReferenceAsset animClip, bool loop, float timeScale, bool reset = false)
    {
        //?????? ????????? ???????? ???? ??? ??? ???? ???? X
        if (animClip.name.Equals(currentAnimation) && reset == false)
            return false;

        //Debug.Log(animClip.Animation.Duration);
        //??? ??????????? ???????. 
        currentAnimatonTime = 0;
        skeletonAnimation.loop = loop;
        skeletonAnimation.timeScale = timeScale;
        currentAnimation = animClip.name;   
        TrackEntry te = skeletonAnimation.state.SetAnimation(0, animClip, loop);
        te.TimeScale = timeScale;
        te.Loop = loop;

        return true;
    }

    public bool SetCurrentAnimation(AnimState _state, bool reset = false)
    {
        if (SoundDistance() &&
            (_state == AnimState.PC_B_L_run) ||
            (_state == AnimState.PC_F_L_run) ||
            (_state == AnimState.PC_B_L_walk) ||
            (_state == AnimState.PC_F_L_walk))
        {
            moveSound.PlayMoveSound();
		}
		else
		{

            moveSound.StopMoveSound();
        }
        currentState = _state;
        bool changed = false;
        bool loop = true;
        float timeScale = 1f;
        switch (_state)
        {
            case AnimState.PC_F_L_push:
                timeScale = 1f;
                Attack3D();
                loop = false;
                reset = false;
                break;
            case AnimState.PC_wolf_attack_side:
                timeScale = 1f;
                Attack3D();
                loop = false;
                reset = false;
                break;
            case AnimState.PC_wolf_attack_top:
                timeScale = 1f;
                Attack3D();
                loop = false;
                reset = false;
                break;
            case AnimState.PC_F_pushed1:
                loop = false;
                break;

            case AnimState.PC_F_pushed2:
                loop = false;
                break;

            case AnimState.PC_F_pushed3:
                loop = false;
                break;

            case AnimState.PC_F_pushed4:
                loop = false;
                break;

            case AnimState.PC_F_pushed5:
                loop = false;
                break;
            case AnimState.PC_wolf_passout:
                loop = false;
                break;
            case AnimState.PC_live_win:
                loop = false;
                break;
            case AnimState.PC_live_lose:
            case AnimState.PC_calm_lose:
                loop = false;
                break;
            case AnimState.round3_PC_r3_blood:
            case AnimState.round3_PC_exhaustion:
                loop = false;
                break;
        }

        AnimationReferenceAsset ani = SpineAniLoader.instance.GetAni(_state);
        changed = _AsyncAnimation(ani, loop, timeScale, reset);
        if(changed)
		{
            PlaySound(_state);
		}
        _AnimState = _state;
        return changed;
    }

    void PlaySound(AnimState state)
	{
        if (SoundDistance() == false) return;
        switch(state)
        {
            case AnimState.PC_F_L_push:
                moveSound.PlayAtkSound();
                break;
            case AnimState.PC_wolf_passout:
                moveSound.WolfFaint();
                break;
            case AnimState.PC_wolf_rotation:
                moveSound.WolfChange();
                break;
        }
	}

    public bool isLocal = false;

    void Attack3D()
    {
        if (isLocal == false) return;
        if (!(prevAnimationTime < attackEventTime && currentAnimatonTime >= attackEventTime))
            return;

        float range = 0.25f;
        int playerLayerMask = LayerMask.GetMask("RemotePlayer");
        int giftMask = LayerMask.GetMask("Default");


        Collider[] giftObjs = Physics.OverlapBox(transform.position + attackDirection.normalized * range, new Vector3(0.8f, 1, 0.8f), Quaternion.identity, giftMask);
        foreach(Collider giftObj in giftObjs)
		{
            if(giftObj.CompareTag("ScrollObject"))
			{
                ScrollMapObject obj = giftObj.GetComponent<ScrollMapObject>();
                if (null == obj) continue;
                ParticleManager.instance.playEffect(giftObj.transform.position, Quaternion.identity, obj.hitParticleType, true);

                if (NetworkConnect.instance)
                {
                    Debug.Log("[Send] attacked obj" + obj.id);
                    NetworkConnect.instance.SendHitSkillObject(obj.id); // ???? ??? ?????
                }
                else
                    obj.Attacked();
            }
		}

        Collider[] playerCols = Physics.OverlapBox(transform.position + attackDirection.normalized * range, new Vector3(0.8f, 1, 0.8f), Quaternion.identity, playerLayerMask);

        Debug.Log(playerCols.Length);

        if(playerCols.Length == 0)
		{
            return;
		}

       // Collider shortestCollider = playerCols[0];
       // float minDistance = (playerCols[0].transform.position - transform.position).magnitude;
        for (int i = 0; i < playerCols.Length; i++)
        {
            Debug.Log("player hit" + i);
            var playerCol = playerCols[i];
            if (playerCol == null) continue;
            if (playerCol.gameObject == gameObject) continue;
            if (playerCol.gameObject.GetComponent<RemotePlayer>().isDie) continue;
            if (playerCol.gameObject.GetComponent<RemotePlayer>().team == eTeamType.Wolf) continue;

            Vector3 playerToTarget = playerCol.transform.position - transform.position;
            bool isHit = Physics.Raycast(new Ray(transform.position, playerToTarget.normalized), playerToTarget.magnitude, LayerMask.GetMask("Default"));
            if (isHit) return;

            //if (playerCol.gameObject.GetComponent<PlayerAnimationController>().currentState == eTeamType.Wolf) continue;
            Vector3 direction = (playerCol.transform.position - transform.position).normalized;
            int target = playerCol.gameObject.GetComponent<RemotePlayer>().id;
            ParticleManager.instance.playEffect(playerCol.transform.position, Quaternion.identity, eParticleType.Hit, true);
            //playerCol.gameObject.GetComponent<PlayerController>().Hit(direction);
            NetworkConnect.instance.SendHitMessage(playerCol.transform.position, direction, target);
            CameraController3D.instance.shakeTIme = 0.3f;
            break;
        }

    }

}
