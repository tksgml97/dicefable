using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaTrigger : MonoBehaviour
{
    [SerializeField] SpriteRenderer[] sprite;
    [SerializeField] float alpha = 0.5f;
    // Start is called before the first frame update
    int tCount = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
	private void OnTriggerEnter2D(Collider2D collision)
	{
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
		{
            tCount++;
            for (int i = 0; i < sprite.Length; i++)
			{
                Color c  = sprite[i].color;
                c.a = alpha;
                sprite[i].color = c;
			}

        }
	}
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            tCount--;

            if(tCount == 0) {
            for (int i = 0; i < sprite.Length; i++)
            {
                Color c = sprite[i].color;
                c.a = 1;
                sprite[i].color = c;
            }
            }

        }
    }
}
