using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using Spine;

public class PlayerScrollMap : MonoBehaviour
{
    public float noDamageTime = 3.0f;
    [field: SerializeField]public float hp { get; set; } = 100.0f;
    // 1, 2�� ����ġ
    [SerializeField] float rankHP_1, rankHP_2;
    
    bool hpshow = false;
    bool gameEnd = false;

    // ���� �÷��̾�
    PlayerController playerController = null;
    // ��Ʈ��ũ �÷��̾�
    RemotePlayer remotePlayer = null;
    public bool isLocal = false;

    BoxCollider box;
    CharacterController characterController = null;

    [SerializeField] Vector3 offset;

    bool Test = false;
    bool m_pushed = false;

    public bool pushed
	{
        get { return m_pushed; }
        set {
            if (m_pushed != value) {
                m_pushed = value;
                pushedTime = 0;
			}
        }
	}

    float pushedTime = 0;

    public int origin_layer { get; set; } = 6;

    //[field: SerializeField]public bool canDamaged { get; set; } = true;
    public int id { get; set; } = 0;

    private void Awake()
    {
    }
    private void Start()
    {
        if (GameManager.instance.currentGame != eGameRound.Round3)
        { 
            this.enabled = false;
        }
        playerController = GetComponentInParent<PlayerController>();
        origin_layer = playerController.gameObject.layer;
        remotePlayer = GetComponentInParent<RemotePlayer>();
        characterController = GetComponentInParent<CharacterController>();
        box = GetComponent<BoxCollider>();
    }
    void NoDamage()
    {
        
        //canDamaged = true;
        if (pushed)
            pushed = false;

        //Debug.Log("[Collider] Layer " + origin_layer);
        playerController.gameObject.layer = origin_layer; // ���̾� ����
    }
    
    void DamagedSpeed(bool _input)
    {
        if (_input)
            playerController.scrollSpeed *= hitSpeedScale;
        else
            playerController.scrollSpeed *= 2.0f;

        Debug.Log(playerController.scrollSpeed);
    }

    public void Damaged(float damage, bool isRemoteCall = false)
    {
        bool hpUpdated = false;
        if (R3SecondGameManager.Instance.isStart == false) return;
        //if(canDamaged)
        //{
            //canDamaged = false;
            if (isLocal)
            {
                if (null != NetworkConnect.instance)
                {
                    NetworkConnect.instance.SendShotFire(Vector3.zero, Vector3.zero, damage);
                }
            } else
            {
                if(isRemoteCall == false) return;
                else
                {
                    hpUpdated = true;
                    bool isDie = UpdateHp(-damage);
                    if (isDie) return;
                }
            }
            
            //canDamaged = false;
            //Debug.Log("[Collider] Layer " + 2);
            playerController.gameObject.layer = 2;// �÷��̾� ���̾� ���� Ȯ�� 6->2->6 �Ǵ� 3->2->3

            if(false == hpUpdated)
			{
                bool isDie = UpdateHp(-damage);
                if (isDie) return;
			}
            
            if (NetworkGameManager.instance)
            {
                HPManager.Instance.HPChange();
            }
            else
                Debug.Log("HP: "+ hp);
            

            Invoke(nameof(NoDamage), noDamageTime);

            if (playerController != null)
            {
                Debug.Log("hit");

                playerController.Hit(Vector3.zero);
            }
        //}

    }

    //return isDie
    public bool UpdateHp(float deltaHp)
	{
        hp += deltaHp;
        if (hp <= 0.0f)
        {
            pushed = false;


            if (ScrollMapManager.Instance.isLocal)
                ScrollMapManager.Instance.ScrollMapEnd();

            if (NetworkConnect.instance != null && isLocal)
                NetworkConnect.instance.SendDieClient();

            ScrollMapManager.Instance.canDamage = false;
            hp = 0;
            return true;
        }
        return false;
    }

    public float scrollMoveSpeed = 1.5f;
    [SerializeField] AnimationCurve pushedSpeed;
    [SerializeField] float hitSpeedScale = 0.75f;
    [SerializeField] float time_damage_sec = 0.5f;
    private void Update()
    {

        //if(GameManager.instance.currentGame==eGameRound.Round3)
        if (ScrollMapManager.Instance != null && ScrollMapManager.Instance.is_start)
        {
            UpdateHp(-time_damage_sec * Time.deltaTime);
            if (pushed)
            {
                pushedTime += Time.deltaTime;
                characterController.Move(Vector3.right * pushedSpeed.Evaluate(pushedTime) * Time.deltaTime);
            }
            if (NetworkGameManager.instance)
            {
                HPManager.Instance.HPChange();
            }

        }

        // 죽음 치트키
        //if(Input.GetKeyDown(KeyCode.M))
        //{
        //    pushed = false;
        //    hp = 0;


        //    if (ScrollMapManager.Instance.isLocal)
        //        ScrollMapManager.Instance.ScrollMapEnd();

        //    if (NetworkConnect.instance != null && isLocal)
        //        NetworkConnect.instance.SendDieClient();

        //    ScrollMapManager.Instance.canDamage = false;
        //    hp = 0;
        //    return;
        //}
    }
}
