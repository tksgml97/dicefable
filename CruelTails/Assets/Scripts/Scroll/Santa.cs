using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum SKILLTYPE
{
    USE,          // 범위 공격
    OBJECTMAP     // 맵 패턴 공격
}

public class Santa : MonoBehaviour
{
    [SerializeField] byte randMinIndex = 0; [SerializeField] byte randMaxIndex = 7;

    [SerializeField] ScrollMapManager scrollMapManager;
    public bool isStart = false;

    // 산타 스파인
    [SerializeField] public SkeletonAnimation santaSkeletonAnimation;
    [SerializeField] public SkeletonAnimation effectSkeletonAnimation;

    // 일반 스킬들
    public SantaSkill_RandomGift santaRGiftSkill; // 선물 상자 랜덤 드랍
    [SerializeField] SantaSkill_PlayerGift santaPGiftSkill; // 선물 상자 지정 드랍
    [SerializeField] SantaSkill_SackAttack sackAttack; // 선물 주머니 내리치기
    // 선물 주머니 데미지

    // 스킬 쿨타임 변수
    float curTimer = 10.0f;
    [SerializeField] float skill_cooltime = 0.0f;
    [SerializeField] float skill_cooltimeMax = 20.0f;
    [SerializeField] float skill_cooltimeMin = 5.0f;

    float DownPerSecond = 0.1f; // 1초당 감소할 쿨타임 (초)

    bool Test = false;

    public List<byte> randSkillNum { get; set; } = new List<byte>(); // 시드 랜덤
    public List<float> randSkillXNum { get; set; } = new List<float>();
    public List<float> randSkillZNum { get; set; } = new List<float>();
    int randNum_max, curRandNum = 0;

    public void setRandom()
    {
        randSkillNum.Add((byte)Random.Range(randMinIndex, randMaxIndex));
        // 일반스킬 - 랜덤 선물 드랍
        
        //randSkillXNum.Add(Random.Range(santaRGiftSkill.x1, santaRGiftSkill.x2));
        //randSkillZNum.Add(Random.Range(santaRGiftSkill.z1, santaRGiftSkill.z2));

        randSkillXNum.Add(Random.Range(santaRGiftSkill.r1.transform.position.x, santaRGiftSkill.r2.transform.position.x));
        randSkillZNum.Add(Random.Range(santaRGiftSkill.r1.transform.position.z, santaRGiftSkill.r2.transform.position.z));
    }

    // 애니메이션
    [SpineAnimation] public string All_Stop_Anim;
    [SpineAnimation] public string All_Attack_Sack_Anim;
    string runSackanimName = "All_Run_Sack";
    string attackSackanimName = "All_Attack_Sack";
    string stopAnimName = "All_Stop";

    private void Awake()
    {
        // 랜덤 리스트 최대
        randNum_max = scrollMapManager.Num_max;

        killBox = GetComponent<BoxCollider>();
        killBox.enabled = false;
    }

    public void Init(float timer)
    {
        curTimer = timer;
        DownPerSecond = (skill_cooltimeMax - skill_cooltimeMin) / timer;
        //Debug.Log("쿨감률: " + DownPerSecond);
        skill_cooltime = skill_cooltimeMax; // 쿨타임 최대
        Test = scrollMapManager.Test;
    }
    public void SantaRoundStart()
    {
        Debug.Log("start santa");
        isStart = true;
        Invoke(nameof(Skill), skill_cooltime);
        Invoke(nameof(ReduceCoolTime), 1.0f);
    }


    // 이동할 위치
    Vector3 movePosition = Vector3.zero;
    [SerializeField] Vector3 kill_offset = new Vector3(4, 0, 0.5f);
    GameObject killPlayer = null;
    public void SantaRoundEnd()
    {
        isStart = false;
        CancelInvoke(nameof(Skill));
        CancelInvoke(nameof(ReduceCoolTime));

        killPlayer = ScrollMapManager.Instance.players[ScrollMapManager.Instance.killPlayer];

        

        // 플레이어 강제 이동
        //movePosition = killPlayer.transform.position;// + offset;
        // 이동 위치 지정
        //Invoke(nameof(GoToKillPlayer), 3.0f); // 대상 앞으로 감
    }

    bool isMove = false;
    
    void GoToKillPlayer()
    {
        isMove = true;
        santaSkeletonAnimation.AnimationState.SetAnimation(0, stopAnimName, false);
        //skeletonAnimation.AnimationState.SetAnimation(0, runSackanimName, true);
    }

    [SerializeField] float distance = 3.0f;
    [SerializeField]float moveSpeed = 3.0f;
    float waitTime = 2.0f;
    bool isStopAni = false;
    void Update()
    {
        if(isStart)
        {
            if (curTimer > 0)
            {
                curTimer -= Time.deltaTime;
            }
        }

        if(isMove)
        {
            
            if((movePosition - transform.position).magnitude < distance)
            {
                isMove = false;

                if(GameManager.instance.LocalizationType != LocalizationType.Teenager)
                    Invoke(nameof(killBoxActivate), sackAttack.attackDelay + delay);
                // 처형
                
                //santaSkeletonAnimation.AnimationState.SetAnimation(0, attackSackanimName, true);
                
                //ScrollMapManager.Instance.KillAnimation(transform.position + kill_offset); // 플레이어 산타 앞으로 이동
            }
            else
            {
                //transform.position = Vector3.MoveTowards(transform.position, movePosition,
                //    moveSpeed * Time.deltaTime);
            }
        }
    }

    [SerializeField] float delay = 0.5f;
    void killBoxActivate()
    {
        killBox.enabled = true;
    }
    BoxCollider killBox;
    private void OnTriggerEnter(Collider other)
    {
        if (killBox.enabled && other.gameObject.layer == 3)
        {
            killBox.enabled = false;
            //ScrollMapManager.Instance.BloodAni();
        }
            
    }
    void Skill()
    {

        Debug.Log("skill");
        if (isStart)
        {
            int rand = randSkillNum[curRandNum++];
            Debug.Log($"skill number is {rand}");
            if (rand >= 3)
            {
                int i = 0; // 3 ~ :트리, 큰 선물, 작은 선물, 눈사람
                switch (rand)
                {
                    case 4:
                        i = 1;
                        break;
                    case 5:
                        i = 2;
                        break;
                    case 6:
                        i = 3;
                        break;
                }
                scrollMapManager.CreateSkillPattern(i);
            }
            else
            {
                switch (rand)
                {
                    case 0:
                        sackAttack.AttackActive(); // 주머니 공격
                        break;
                    case 1:
                        santaRGiftSkill.Activate(); // 랜덤 드랍
                        break;
                    case 2:
                        santaPGiftSkill.Activate(); // 지정 드랍
                        break;
                }
            }
            //Debug.Log("스킬 사용 : " + "[" + rand + "]" + curTimer);
            Invoke(nameof(Skill), skill_cooltime);

            // 최대 인덱스 초기화
            if (curRandNum >= randNum_max)
            {
                curRandNum = 0;
            }
        }
        
    }

    // 쿨타임 다운
    void ReduceCoolTime()
    {
        if(isStart)
        {
            // 제한 시간 종료
            if (curTimer < 0)
                skill_cooltime = skill_cooltimeMin;
            else
            {
                skill_cooltime -= DownPerSecond;
                Invoke(nameof(ReduceCoolTime), 1.0f);
            }
        }
    }

    // 산타 애니메이션 설정
    public void SantaSkillAnimation(string anim)
    {
        
        santaSkeletonAnimation.AnimationState.SetAnimation(0, anim, false);
        float animTime = santaSkeletonAnimation.skeletonDataAsset.GetSkeletonData(true).FindAnimation(anim).Duration;

        santaSkeletonAnimation.AnimationState.AddAnimation(0, runSackanimName, true, animTime);
    }
    public void SetSkin(int i)
    {
        santaSkeletonAnimation.skeleton.SetSkin("Santa_present"+i.ToString());
    }
}
