using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R3SecondGameManager : MonoBehaviour
{
    private static R3SecondGameManager instance = null;
    public static R3SecondGameManager Instance { get { if (instance) return instance; return null; } }

    public int _seed { get; private set; } = 0;

    public bool isStart { get; set; } = false;

    private void Awake()
    {
        instance = this;
    }
    public void RandomSeed(int param)
    {
        _seed = param;
    }
}
