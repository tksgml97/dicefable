using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaSkill_RandomGift : MonoBehaviour
{
    public float Damage = 10.0f;
    [SerializeField] Santa santa = null;

    [SerializeField] GameObject[] skillObjects;
    [SerializeField] GameObject skillRangePrefab;
    //[SerializeField] int skill_type = 0; // 1 범위공격 2 패턴공격

    // 스폰 위치
    public GameObject r1, r2;
    //public float x1, x2, z1, z2;
    [SerializeField] float spawn_y = 10f; float skillRange_y = 0f;
    [SerializeField] float spawnTime = 0.1f;
    [SerializeField] int giftNum = 10;

    int i = 0; int curRandNum = 0;

    string skillanimName = "All_Attack";

    [SerializeField] float attackDelay = 1.0f; // 공격 모션 선딜레이

    [SerializeField] float cameraShakeTime = 1.0f;
    [SerializeField] float cameraShakeDelay = 0.3f;
    [SerializeField] float cameraShakePower = 1f;

    [SerializeField] float animDelay = 0.25f; // 애니 실행 전 딜레이
    public void Activate()
    {
        StartCoroutine(ShowRange(0));

        Invoke(nameof(Anim), animDelay);
        
    }

    float x, z;
    List<GameObject> skillRanges = new List<GameObject>();
    float showRangeTime; [SerializeField] float rangeTime = 0.5f;
    IEnumerator ShowRange(int ri)
    {
        showRangeTime = animDelay + attackDelay + cameraShakeDelay + rangeTime;
        int rangeNum = curRandNum++;
        if (curRandNum > ScrollMapManager.Instance.Num_max)
            rangeNum = 0;
            
            
        x = santa.randSkillXNum[rangeNum];
        z = santa.randSkillZNum[rangeNum++];

        GameObject skillRange = Instantiate(skillRangePrefab);
        skillRange.transform.position = new Vector3(x, 0.1f, z);
        skillRanges.Add(skillRange);

        yield return new WaitForSeconds(spawnTime);
        if (ri < giftNum)
        {
            StartCoroutine(ShowRange(ri+1));
        }

        StartCoroutine(SpawnObject(curRandNum));
        Invoke(nameof(CameraShake), attackDelay + cameraShakeDelay);
        Invoke(nameof(DeleteRange), showRangeTime);
    }
    void DeleteRange()
    {
        //skillRanges.Clear();
    }

    void Anim()
    {
        santa.SetSkin(Random.Range(1, 5));
        santa.SantaSkillAnimation(skillanimName);
    }

    void CameraShake()
    {
        //카메라 쉐이크
        CameraController3D.instance.shakeTIme = cameraShakeTime;
        CameraController3D.instance.shakePower = cameraShakePower;

    }
    
    public IEnumerator SpawnObject(int randomNum)
    {

        yield return new WaitForSeconds(attackDelay);
        if (santa)
        {
            //if (curRandNum > ScrollMapManager.Instance.Num_max)
            //    curRandNum = 0;
            //float x, z;
            x = santa.randSkillXNum[randomNum];
            z = santa.randSkillZNum[randomNum++];

            //GameObject skillRange = Instantiate(skillRangePrefab);
            //skillRange.transform.position = new Vector3(x, 0.1f, z);

            GameObject skillObject = Instantiate(skillObjects[Random.Range(0, skillObjects.Length)]);
            skillObject.GetComponent<SkillObject>().damage = Damage;
            skillObject.GetComponent<SkillObject>().skillRange = skillRanges[i];
            skillObject.transform.position = new Vector3(x, spawn_y, z);

            
        }

        i++;
    }
}
