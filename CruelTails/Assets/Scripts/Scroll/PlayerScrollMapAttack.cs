using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScrollMapAttack : MonoBehaviour
{
    BoxCollider boxCollider = null;
    bool canAttack = true;
    bool useAttack = false;
    [SerializeField] float attackCool = 3.0f; // 다음 공격이 가능한 쿨타임
    [SerializeField] float attackTime = 1.0f; // 공격이 지속되는 시간
    [SerializeField] ParticleSystem pushedParticle;
    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        boxCollider.enabled = false;
    }
    private void Start()
    {
        if (GameManager.instance.currentGame != eGameRound.Round3)
            Destroy(this);
    }
    [SerializeField] float preAttackDelay = 0.2f;
    private void Update()
    {
        if (!useAttack && Input.GetMouseButtonDown(0))
        {
            useAttack = true;
            Invoke(nameof(Attack), preAttackDelay);
            Invoke(nameof(AttackEnd), attackTime);
            Invoke(nameof(AttackCool), attackCool);
        }
    }
    void Attack()
    {
        boxCollider.enabled = true;
    }
    void AttackEnd()
    {
        //canAttack = true;
        if (boxCollider.enabled)
            boxCollider.enabled = false;
    }
    void AttackCool()
    {
        useAttack = false;
    }

    private void OnTriggerEnter(Collider other)
    {/*
        if (useAttack && other.CompareTag("ScrollObject"))
        {
            //boxCollider.enabled = false;// 다수의 상자 부술 수 있음
            //canAttack = false;
            //Debug.Log("Attack " + other.name);

            ScrollMapObject obj = other.GetComponent<ScrollMapObject>();
            pushedParticle.Play();

            if (NetworkConnect.instance)
            {
                Debug.Log("[Send] attacked obj" + obj.id);
                NetworkConnect.instance.SendHitSkillObject(obj.id); // 장애물 타격 동기화
            }
            obj.Attacked();

        }*/
    }
        
}
