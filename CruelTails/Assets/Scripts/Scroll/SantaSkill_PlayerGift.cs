using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SantaSkill_PlayerGift : MonoBehaviour
{
    public float Damage = 10.0f;
    [SerializeField] Santa santa = null;

    [SerializeField] GameObject[] skillObjects;
    [SerializeField] GameObject skillRangePrefab;

    [SerializeField] float spawnTime = 0.1f; // 스폰 간격

    [SerializeField] float spawn_y = 10f; float skillRange_y = 0f;

    [SerializeField] int giftNum = 5; // 3명 전부에게 5개씩


    int curGiftNum = 0;
    
    string skillanimName = "All_Attack";
    GameObject[] players = new GameObject[5];
    public List<GameObject> m_players = new List<GameObject>();

    [SerializeField] float attackDelay = 1.0f; // 공격 선딜레이

    int playerNum = 0;
    float[] x, z;

    bool UseSkill = false;
    bool isLocal = false;

    [SerializeField] float animDelay = 0.25f; // 애니 실행 전 딜레이
    public void Activate()
    {
        //santa.SetSkin(Random.Range(1, 5));
        //santa.SantaSkillAnimation(skillanimName);
        StartCoroutine(ShowRange(0));
        Invoke(nameof(Anim), animDelay);
    }

    [SerializeField] float cameraShakeTime = 1.0f;
    [SerializeField] float cameraShakeDelay = 0.3f;
    [SerializeField] float cameraShakePower = 1f;
    Dictionary<int, int> rangePlayerIndexMap = new Dictionary<int, int>();
    List<GameObject> skillRanges = new List<GameObject>();
    List<System.Timers.Timer> skillRangeTimers = new List<System.Timers.Timer>();
    float showRangeTime;[SerializeField] float rangeTime = 0.5f;

    IEnumerator ShowRange(int ri)
    {
        UseSkill = true;
        showRangeTime = animDelay + attackDelay + cameraShakeDelay + rangeTime;
        //for(int i = 0; i < playerNum; i++)
        //{
        //    x[i] = players[i].transform.position.x;
        //    z[i] = players[i].transform.position.z;

        for (int i = 0; i < m_players.Count; i++)
        {
            x[i] = m_players[i].transform.position.x;
            z[i] = m_players[i].transform.position.z;

                GameObject skillRange = Instantiate(skillRangePrefab);
            skillRange.transform.position = new Vector3(x[i], 0.1f, z[i]);
            rangePlayerIndexMap.Add(skillRanges.Count, i);
            var timer = new System.Timers.Timer();
            timer.Start();
            skillRangeTimers.Add(timer);
            skillRanges.Add(skillRange);

        }

        yield return new WaitForSeconds(spawnTime);
        if (ri < giftNum)
        {
            StartCoroutine(ShowRange(ri+1));
        }
        Invoke(nameof(DeleteRange), showRangeTime);
    }
    void DeleteRange()
    {
    }

    void Anim()
    {
        santa.SetSkin(Random.Range(1, 5));
        santa.SantaSkillAnimation(skillanimName);
        Invoke(nameof(SpawnObject), attackDelay);
        Invoke(nameof(CameraShake), attackDelay + cameraShakeDelay);
    }


    void SpawnObject()
    {
        UseSkill = false;
        //dUseSkill = true;
        DropObject();
        Invoke(nameof(CameraShake), cameraShakeDelay);
    }
    
    void CameraShake()
    {
        //카메라 쉐이크
        CameraController3D.instance.shakeTIme = cameraShakeTime;
        CameraController3D.instance.shakePower = cameraShakePower;
    }


    void DropObject()
    {
        if (giftNum <= curGiftNum)
        {
            curGiftNum = 0;
            
            return;
        }
        GameObject skillRange, skillObject;
        for (int i = 0; i < playerNum; i++) // 3명의 플레이어
        {
            //skillRange = Instantiate(skillRangePrefab);
            //skillRange.transform.position = new Vector3(x[i], 0.1f, z[i]);

            skillObject = Instantiate(skillObjects[Random.Range(0, skillObjects.Length)]);
            skillObject.GetComponent<SkillObject>().damage = Damage;
            //skillObject.GetComponent<SkillObject>().skillRange = skillRanges[i];
            skillObject.transform.position = new Vector3(x[i], spawn_y, z[i]);
        }
        Invoke(nameof(DropObject), spawnTime);
        curGiftNum++;
    }

    void Start()
    {
        isLocal = ScrollMapManager.Instance.isLocal;

        if (!isLocal)
        {
            playerNum = GameManager.instance.GetPlayerSize();
            for (int i = 0; i < playerNum; i++)
            {
                players[i] = GameManager.instance.GetPlayer(i);
            }

            playerNum = GameManager.instance.GetPlayerSize();
            for (int i = 0; i < playerNum; i++)
            {
                if(GameManager.instance.GetPlayer(i)==null || GameManager.instance.isDie)
                {
                    continue;
                }
                m_players.Add(GameManager.instance.GetPlayer(i));
            }
        }
        else
        {
            playerNum = 1;
            players[0] = GameManager.playerController.gameObject;
            m_players.Add(GameManager.playerController.gameObject);
        }
        x = new float[playerNum];
        z = new float[playerNum];
    }

    void Update()
    {
        if(UseSkill)
        {
            for(int i = 0; i < skillRanges.Count; i++)
            {
                int pid = rangePlayerIndexMap[i];
                //if (players.Length <= i || players[i] == null) continue;
                if (m_players.Count <= i) continue;
                if (skillRangeTimers[i].Interval > attackDelay) continue;
                //ScrollMapManager.Instance.text[i].text = players[i].transform.position.ToString();
                if (!isLocal)
                {
                    //x[pid] = players[pid].transform.position.x;
                    //z[pid] = players[pid].transform.position.z;
                    x[pid] = m_players[pid].transform.position.x;
                    z[pid] = m_players[pid].transform.position.z;
                    skillRanges[pid].transform.position = new Vector3(
                        x[pid], 0.1f, z[pid]);

                }
                else
                {
                    if(ScrollMapManager.Instance.player)
                    {
                        x[pid] = ScrollMapManager.Instance.player.transform.position.x;
                        z[pid] = ScrollMapManager.Instance.player.transform.position.z;
                        skillRanges[i].transform.position = new Vector3(
                        x[pid], 0.1f, z[pid]);
                    }
                    
                }

            }
            
        }
    }
}
