using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternCollision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("ScrollMap"))
        {
            ScrollMapManager.Instance.CreatePattern(true);
        }
    }
}
