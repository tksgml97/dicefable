using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pattern : MonoBehaviour
{
    float destroy_point_x = 0;
    public Transform startpoint, finishpoint;
    public float width { get; private set; } = 0;
    float y, z;

    public bool is_activate = false;

    [field: SerializeField] public bool isDestroyObject { get; set; } = false; // 파괴되는 장애물이 있는가 없는가
    public void Init()
    {
        y = ScrollMapManager.Instance.y;
        z = ScrollMapManager.Instance.z;
        destroy_point_x = ScrollMapManager.Instance.destroy_pattern_vector.position.x;
        width = (finishpoint.position.x - startpoint.position.x) / 2;
    }

    // Update is called once per frame
    void Update()
    {
        ScrollMap();
        //Debug.Log("SP: " + startpoint.position + " FP: " + finishpoint.position);
    }

    protected void ScrollMap()
    {
        if(ScrollMapManager.Instance.is_start)
        {
            transform.position = new Vector3(transform.position.x - (ScrollMapManager.Instance.scroll_speed * Time.deltaTime), 0, z);
            if (transform.position.x < destroy_point_x)
                Destroy(gameObject);
        }
    }
}
