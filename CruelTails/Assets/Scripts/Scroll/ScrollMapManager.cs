using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Spine.Unity;



public class ScrollMapManager : MonoBehaviour
{
    //[SerializeField] Tutorial_RoundPurpose tutorial = null;
    [SerializeField] bool BuildMode = true;
    public bool canDamage { get; set; } = true;
    //GameObject scoreUI, miniMap = null;
    [SerializeField] GameObject DestinationMap;
    private static ScrollMapManager instance = null;
    public static ScrollMapManager Instance { get { if (null != instance) return instance;  return null; } }

    [SerializeField] AnimState killPlayerAni, survivedPlayerAni;

    [SerializeField] GameObject snowEffectPrefab = null;

    // 플레이어 체력 정보 표시
    Vector3 offset = new Vector3(0, 3, 0);
    float[] hp = new float[3];
    public Image[] Hpbars;

    [SerializeField] GameObject textCanvas = null;
    public GameObject[] players { get; set; }
    int maxPlayer = 0;

    [SerializeField] Santa santa=null; [SerializeField] GameObject santaCamTarget = null;
    [SerializeField] ScrollMapObjManager objManager=null;

    [SerializeField] List<GameObject> Patterns;
    [SerializeField] List<GameObject> SkillPatterns;
    Queue<GameObject> queue;
    [HideInInspector] public float y = 0;
    [HideInInspector] public float z = 0;

    // timer
    float round3_part2_timer = 120.0f;

    // infos
    [SerializeField] List<GameObject> InitialPatterns;
    public float scroll_speed = 5.0f;

    public bool isPlayerRun { get; set; } = false; // 플레이어 도망 연출
    float runPlayerSpeed = 5.0f; //도망 속도


    public Transform first_pattern_vector = null;
    public Transform destroy_pattern_vector = null;

    public bool is_start { get; set; } = false;
    Pattern prePattern, curPattern;

    // Players
    [field: SerializeField] public GameObject player { get; set; } = null;

    public List<byte> randMapNum { get; set; } = new List<byte>();
    public int Num_max { get; private set; } = 10_000;
    int curRandNum = 0;

    [SerializeField] GameObject sideMapCollider;
    //[SerializeField] GameObject[] Destinations; // 도착 지점
    [SerializeField] GameObject CameraPoint = null;
    int curDestination = 0;

    public UnityEvent scrollMapEndEvent;

    public void RandomSeed(int _seed)
    {
        Random.InitState(_seed);
        for (int i = 0; i < Num_max; i++)
        {
            //Debug.Log(i);
            randMapNum.Add((byte)Random.Range(0, Patterns.Count));
            santa.setRandom();
        }
    }
    public Vector3[] min_max_areas; // 카메라 범위
    // * 3라운드 제어 함수
    public void ScrollMapStart()
    {
        CameraController3D.instance.sizeUp = 0.5f;
        CameraController3D.instance.minTargetArea = min_max_areas[0];
        CameraController3D.instance.maxTargetArea = min_max_areas[1];

        if (NetworkGameManager.instance)
        {
            for (int i = 0; i < GameManager.instance.GetPlayerSize(); i++)
            {
                if (GameManager.instance.GetPlayer(i) == null) continue;
                if (i == NetworkGameManager.memberNumber && GameManager.instance.isDie) // local die
                {
                    GameManager.instance.GetPlayer(i).GetComponent< PlayerMoveSound >().DeletePlayer();
                    Debug.Assert(false, "제외 유저: " + i);
                    Debug.Log("제외 유저: " + i);
                    continue;
                }
                if (GameManager.instance.GetPlayer(i).GetComponent<RemotePlayer>().isDie) // client die
                {
                    GameManager.instance.GetPlayer(i).GetComponent<PlayerMoveSound>().DeletePlayer();
                    Debug.Assert(false, "제외 유저: " + i);
                    Debug.Log("제외 유저: " + i);
                    continue;
                }
                
            }
            
        }

        ChristmasGameManager.Instance.state = Round3State.secondGame;
        SoundInstance.Instance.AfterCountDown();

        //if (tutorial)
           // tutorial.SetPurpose(eTeamType.Red);

        if (snowEffectPrefab)
            snowEffectPrefab.GetComponent<ParticleSystem>().Play();

        for (int i = 0; i < 3; i++)
        {
            Hpbars[i].gameObject.SetActive(true);
        }

        // 산타 초기화
        santa.Init(round3_part2_timer);

        R3SecondGameManager.Instance.isStart = true;
        is_start = true;
        santa.SantaRoundStart(); // 산타 활성화
    }

    public void ScrollMapEnd()
    {
        Debug.Log("ScrollMapEnd");
        if(R3SecondGameManager.Instance.isStart)
        {
            R3SecondGameManager.Instance.isStart = false;
            SoundInstance.Instance.Round2SoundEnd();
            canDamage = false;
            
            DestinationMap.SetActive(true);
            for (int i = 0; i < 3; i++)
            {
                HPManager.Instance.HpBarHolders[i].gameObject.SetActive(false);
            }
            ChrismasGameEndPoint();
            
            santa.SantaRoundEnd();
        }
        
    }
    
    public int killPlayer { get; set; } = 0;
    SkeletonAnimation killplayerSkeleton = null;

    // 도착 지점
    public void ChrismasGameEndPoint()
    {
        killPlayer = GameManager.instance.GetKillPlayerIndex();
        killplayerSkeleton = players[killPlayer].GetComponent<PlayerAutoAnimation>().skeleton;
        PlayerRun();
        //Invoke(nameof(PlayerRun), waitTime);

        SetKillPlayer(); // 처형 대상 지정, 애니메이션 지정

        //scrollMapEndEvent?.Invoke();

        // next scene
        //StartCoroutine(CinematicDirector_Round4Stair(7.5f));
    }

    IEnumerator CinematicDirector_Round4Stair(float time)
    {
        yield return new WaitForSeconds(time);
        if (NetworkGameManager.memberNumber == killPlayer)
        {
            Debug.Log("[ScrollMapManager] Local Killed : " + killPlayer);
            StartCoroutine(SceneLoader.instance.LoadScene(eSceneType.Round4, NetworkConnect.instance.GetGameInitData));
        }
        else
        {
            scrollMapEndEvent?.Invoke();
            //StartCoroutine(SceneLoader.instance.LoadScene(eSceneType.Round4, NetworkConnect.instance.GetGameInitData));
        }
        
    }

    void PlayerRun() // 플레이어 도망
    {
        sideMapCollider.SetActive(false); // 맵 충돌 처리 x

        //if (isLocal && !isLocalDie)
        //    killPlayer = 0;

        for (int i = 0; i < maxPlayer; i++)
        {
            if (players[i] == null) continue;
            if (i!=killPlayer)
            {
                if (i != NetworkGameManager.memberNumber && GameManager.instance.isDie != false
                    || players[i].GetComponent<RemotePlayer>().isDie == false)
                {
                    players[i].GetComponent<PlayerController>()._AnimState = AnimState.PC_F_L_run;
                }
            }
        }
        for (int i = 0; i < maxPlayer; i++)
        {
            if (players[i] == null) continue;
            if (i == killPlayer) continue;
            if (players[i].GetComponent<RemotePlayer>().isDie) continue;
            AlivedUsers.Add(players[i]);
            //Debug.Log("Alived Users : " + GameManager.instance.GetPlayer(i));

        }
        isAliveRun = true;
        isDeadRun = true;
        //StartCoroutine(ScrollMapStop(10.0f));
    }

    void SetKillPlayer()
    {
        // 네트워크

        // 처형 대상 지정
        float minHp = 100;
        for (int i = 0; i < maxPlayer; i++)
        {
            if (players[i] == null) continue;

            players[i].GetComponent<PlayerController>().state = PlayerState.GameEnd;

            players[i].layer = 2;
        }
        Debug.Log("[처형 대상] " + GameManager.instance.GetNameFromPlayerIndex(killPlayer) + "id " + killPlayer + " HP " + minHp);

        if (santaCamTarget)
            CameraController3D.instance.target = santaCamTarget.transform;
        //CameraController3D.instance.target = santa.transform;
        CameraController3D.instance.sizeUp = 0.25f;
        // 처형 애니
        for (int i = 0; i < maxPlayer; i++)
        {
            //if (i != killPlayer) continue;
            if (players[i] == null) continue;
            players[i].GetComponent<PlayerController>()._AnimState = AnimState.PC_F_L_run;
            players[i].GetComponent<PlayerController>().scaleObj.localScale = new Vector3(-1, 1, 1);
        }
        GameManager.instance.isStart = false;
    }

    public bool Test = false;
    public bool isLocal = false;
    [SerializeField] bool isLocalDie = true;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        if (NetworkGameManager.instance)
            isLocal = false;

        DestinationMap.SetActive(false);
        for (int i = 0; i < 3; i++)
            hp[i] = 100;

        if (Test && player)
            player.gameObject.SetActive(true);

        if (BuildMode)
            textCanvas.SetActive(false);
    }
    private void Start()
    {
        RandomSeed(R3SecondGameManager.Instance._seed);
        // Test
        if (isLocal)
        {
            ScrollMapStart();
        }

        y = first_pattern_vector.position.y; z = first_pattern_vector.position.z;
        queue = new Queue<GameObject>();

        // 초기 스크롤 맵 패턴
        InitPattern();

        maxPlayer = NetworkGameManager.instance == null ? 1 : GameManager.instance.GetPlayerSize();
        players = new GameObject[maxPlayer];

        if (!NetworkGameManager.instance)
        {
            players[0] = GameManager.playerController.gameObject;
            return;
        }
        for(int i = 0; i < maxPlayer; i++)
        {
            players[i] = GameManager.instance.GetPlayer(i);
        }
    }

    void InitPattern()
    {
        for (int i = 0; i < InitialPatterns.Count; i++)
        {
            queue.Enqueue(InitialPatterns[i]);
            //Debug.Log("[Init" + i + "] " + InitialPatterns[i].name);
        }
        // 초기 패턴 1번째
        prePattern = Instantiate(queue.Dequeue().GetComponent<Pattern>());
        prePattern.Init();

            prePattern.transform.position = new Vector3(first_pattern_vector.position.x + prePattern.width, y, first_pattern_vector.position.z);
        Debug.Log(prePattern.transform.position);

        // 2번째 ~
        for (int i = 1; i < InitialPatterns.Count; i++)
        {
            CreatePattern(false);
        }
    }

    public void CreatePattern(bool isRandom)
    {

        if(isRandom) // 랜덤이 아닌 경우 (고정 패턴)
        {
            curRandNum++;
            if (curRandNum >= Num_max)
            {
                curRandNum = 0;
            }
            int idx = randMapNum[curRandNum];
            queue.Enqueue(Patterns[idx]);
        }
        curPattern = Instantiate(queue.Dequeue().GetComponent<Pattern>());
        curPattern.Init();
        if (curPattern.isDestroyObject)
        {
            curPattern.gameObject.GetComponent<SkillPattern>().SkillPatternInit();
        }
            
        curPattern.transform.position = new Vector3(prePattern.finishpoint.position.x + curPattern.width, y, prePattern.finishpoint.position.z);

        //Debug.Log("[Create] " + curPattern.name);

        prePattern = null;
        prePattern = curPattern;
        curPattern = null;
    }

    public void CreateSkillPattern(int skill_idx)
    {

        queue.Enqueue(SkillPatterns[skill_idx]);

        //Debug.Log("[Skill " + skill_idx + "] " + SkillPatterns[skill_idx].name);
    }
    bool isCameraStop = false;
    int curDestinationNum = 0;
    [SerializeField] GameObject DeadPoint;
    [SerializeField] GameObject[] AlivePoints;
    int[] aliveUserIdx = new int[2];
    List<GameObject> AlivedUsers = new List<GameObject>();

    bool isAliveRun = false;
    bool isDeadRun = false;


    IEnumerator ScrollMapStop(float time)
    {
        scroll_speed -= 0.5f;
        yield return new WaitForSeconds(time);
        is_start = false;
    }
    void Ani_Santa_Stop()
    {
        float duration = santa.santaSkeletonAnimation.skeletonDataAsset.GetSkeletonData(true).FindAnimation(santa.All_Stop_Anim).Duration;
        //players[killPlayer].GetComponent<PlayerController>()._AnimState = AnimState.round3_PC_exhaustion;
        santa.santaSkeletonAnimation.AnimationState.AddAnimation(0, santa.All_Stop_Anim, false, 0);
        santa.effectSkeletonAnimation.gameObject.SetActive(false);

        Invoke(nameof(Ani_Santa_Attack), duration + 2.0f);
    }

    void Ani_Santa_Attack()
    {
        string attackAnim = santa.All_Attack_Sack_Anim;
        santa.santaSkeletonAnimation.AnimationState.AddAnimation(0, attackAnim, true, 0);
        //float t = santa.santaSkeletonAnimation.skeletonDataAsset.GetSkeletonData(true).FindAnimation(attackAnim).Duration - 1.4f;
        //Debug.Log("duration : " + t);
        Invoke(nameof(Ani_Blood), 1.0f);
    }

    void Ani_Blood()
    {

        if (GameManager.instance.LocalizationType == LocalizationType.Teenager)
        {
            if(NetworkGameManager.memberNumber == killPlayer)
            {
                Debug.Log("[ScrollMapManager] Local Killed : "+ killPlayer);
                StartCoroutine(SceneLoader.instance.LoadScene(eSceneType.Round4, NetworkConnect.instance.GetGameInitData));
                return;
            }

            scrollMapEndEvent?.Invoke();
            return;
        }
        players[killPlayer].GetComponent<PlayerController>()._AnimState = AnimState.round3_PC_r3_blood;
        StartCoroutine(CinematicDirector_Round4Stair(3.0f));
    }
    bool santaStop = false;
    float deadRunSpeed = 2.5f;
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.K)) // 처형씬 카메라 테스트
        //{
        //    CameraController3D.instance.target = santaCamTarget.transform;
        //}
        //if (Input.GetKeyDown(KeyCode.M)) // 플레이어 사망 테스트
        //{
        //    GameManager.playerController.GetComponentInChildren<PlayerScrollMap>().Damaged(100);
        //}

        if (isDeadRun)
        {
            Vector3 killplayer_distance = DeadPoint.transform.position - players[killPlayer].transform.position;
            players[killPlayer].GetComponent<CharacterController>().Move(killplayer_distance.normalized
                            * deadRunSpeed * Time.deltaTime);
            

            if (killplayer_distance.magnitude < 2.5f && killplayer_distance.magnitude > 1.0f)
            {
                scroll_speed = 2.0f;
                deadRunSpeed = 1.0f;
                killplayerSkeleton.timeScale = 0.5f;

                if (!santaStop)
                {
                    SantaStop(2.0f);
                }
            }
            else if (killplayer_distance.magnitude < 1.0f && killplayer_distance.magnitude > 0.1f)
            {
                deadRunSpeed = 0.5f;
                killplayerSkeleton.timeScale = 0.25f;

                if (!santaStop)
                {
                    SantaStop(0.5f);
                }
            }
            else if (killplayer_distance.magnitude < 0.1f)
            {
                isDeadRun = false;
                isAliveRun = false;

                players[killPlayer].GetComponent<PlayerController>()._AnimState = AnimState.round3_PC_exhausted;
            }
        }

        if(isAliveRun)
        {
            if (AlivedUsers.Count <= 0)
            {
                return;
            }

            float aliveplayer_distance = AlivePoints[0].transform.position.x - AlivedUsers[0].transform.position.x;
            if (aliveplayer_distance > 0.1f)
            {
                for (int i = 0; i < AlivedUsers.Count; i++)
                {
                    AlivedUsers[i].GetComponent<CharacterController>().Move(Vector3.right * runPlayerSpeed * Time.deltaTime);
                }
            }
            else
            {
                isAliveRun = false;
                for (int i = 0; i < AlivedUsers.Count; i++)
                {
                    AlivedUsers[i].GetComponent<PlayerController>()._AnimState = AnimState.PC_F_L_idle;
                }
                
            }
        }

    }
    
    void SantaStop(float time)
    {
        santaStop = true;
        StartCoroutine(ScrollMapStop(time));
        Ani_Santa_Stop();
    }
}