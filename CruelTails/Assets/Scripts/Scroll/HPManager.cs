using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPManager : Singleton<HPManager>
{
    public Image[] HpBarHolders;
    public Image[] HPbars;
    int?[] playerIDs = new int?[3];
    int[] maxHPs = new int[3];
    int count = 0;

    public void InitHpUser(int i)
    {
        //Debug.Log(count);
        playerIDs[count++] = i;
    }

    public void InitMaxHP()
    {
        if(NetworkGameManager.instance)
            for(int i = 0; i < 3; i++){ HpBarHolders[i].gameObject.SetActive(true); HPbars[i].gameObject.SetActive(true); }
        try
        {
            for (int i = 0; i < 3; i++)
            {
                if (playerIDs[i] == null) continue;
                GameManager.instance.GetPlayer(playerIDs[i].Value).GetComponentInChildren<PlayerScrollMap>().hp
                    += RankManager.instance.rankUsers[i].insentive_hp;
                maxHPs[i] = 100 + RankManager.instance.rankUsers[i].insentive_hp;
                Debug.Log("[Insentive] " + GameManager.instance.GetNameFromPlayerIndex(playerIDs[i].Value) + " HP " + RankManager.instance.rankUsers[i].insentive_hp);
                
                HpBarHolders[i].rectTransform.localScale = new Vector3(HpBarHolders[i].rectTransform.localScale.x * maxHPs[i] * 0.01f,
                    HpBarHolders[i].rectTransform.localScale.y, HpBarHolders[i].rectTransform.localScale.z);

            }
        } catch(System.Exception e)
		{
            Debug.Log(e.Message);
		}
    }

    private void Start()
    {
    }

    public void HPChange()
    {
        for(int i = 0; i < 3; i++)
        {
            if (playerIDs[i] == null) continue;
            HPbars[i].fillAmount = GameManager.instance.GetPlayer(playerIDs[i].Value).GetComponentInChildren<PlayerScrollMap>().hp / maxHPs[i];

            //Debug.Log("[HP Change] " + GameManager.instance.GetNameFromPlayerIndex(playerIDs[i].Value) + " HP " + 
                //GameManager.instance.GetPlayer(playerIDs[i].Value).GetComponentInChildren<PlayerScrollMap>().hp);
        }
    }
    Vector3 offset = new Vector3(0, 3, 0);

    Vector3 playerPos = Vector3.zero;
    Vector3 screenPlayerPos = Vector3.zero;
    private void Update()
    {
        if(NetworkGameManager.instance && ChristmasGameManager.instance
            && R3SecondGameManager.Instance.isStart == true)
        {
            for (int i = 0; i < 3; i++)
            {
                if (playerIDs[i] == null) continue;
                playerPos=GameManager.instance.GetPlayer(playerIDs[i].Value).transform.position;
                screenPlayerPos=Camera.main.WorldToScreenPoint(playerPos + offset);
                HpBarHolders[i].gameObject.transform.position = screenPlayerPos;
                //HPbars[i].gameObject.transform.position = screenPlayerPos;
            }
            
        }
        if (NetworkGameManager.instance == null && GameManager.instance.currentGame == eGameRound.Round3)//local
        {
            playerPos = GameManager.playerController.transform.position;
            screenPlayerPos = Camera.main.WorldToScreenPoint(playerPos + offset);
            HpBarHolders[0].gameObject.transform.position = screenPlayerPos;
            
        }
    }

}
