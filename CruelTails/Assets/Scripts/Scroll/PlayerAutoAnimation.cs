using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum eAutoAnimation
{
    Run_B, Run_F, Idle_B
}

public class PlayerAutoAnimation : MonoBehaviour
{
    public SkeletonAnimation skeleton;
    [SpineAnimation] public string Idle_Back_Anim;
    [SpineAnimation] public string Run_Back_Anim;
    [SpineAnimation] public string Run_Front_Anim;

    public void SetAnimation(eAutoAnimation animType)
    {
        switch(animType)
        {
            case eAutoAnimation.Idle_B:
                skeleton.AnimationState.SetAnimation(0, Idle_Back_Anim, true);
                break;
            case eAutoAnimation.Run_B:
                skeleton.AnimationState.SetAnimation(0, Run_Back_Anim, true);
                break;
            case eAutoAnimation.Run_F:
                skeleton.AnimationState.SetAnimation(0, Run_Front_Anim, true);
                break;
        }
        //if(isFront)
        //    skeleton.AnimationState.SetAnimation(0, Run_Front_Anim, true);
        //else
        //    skeleton.AnimationState.SetAnimation(0, Run_Back_Anim, true);
    }

}
