using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class SkillObject : MonoBehaviour
{
    [SerializeField] ParticleSystem popEffect = null;
    [SerializeField] GameObject rend = null;
    [SerializeField] float dropSpeed = 2.5f;
    [SerializeField] float duration = 0.1f; // ���� ��� �������� ���ӽð�

    bool stop = false;

    public GameObject skillRange { get; set; } = null;
    
    public float damage { get; set; } = 0;


    private void OnTriggerEnter(Collider other)
    {
        // ���� ������
        if (other.gameObject.CompareTag("ScrollMap"))
        {
            if (!stop && popEffect)
                popEffect.Play();
            stop = true;
            if(skillRange)
                Destroy(skillRange);
            Invoke(nameof(Remove), duration);// ���� �� �����
            //Debug.Log("Ground");
        }

        if (other.gameObject.layer == 3 || other.gameObject.layer == 6)
        {
            //Debug.Log("Gift Player hit");
            //Debug.Assert(false, "Gift Damage");
            other.GetComponentInChildren<PlayerScrollMap>().Damaged(damage);
            //other.GetComponentInChildren<PlayerScrollMap>().canDamaged = false;
            //other.GetComponentInChildren<PlayerScrollMap>().origin_layer = other.gameObject.layer;

        }
    }
    private void OnTriggerStay(Collider other)
    {
        // �÷��̾ ����
        if (other.gameObject.layer == 3)
        {
            //Debug.Assert(false, "Gift Damage");
            other.GetComponentInChildren<PlayerScrollMap>().Damaged(damage);
                //other.GetComponentInChildren<PlayerScrollMap>().canDamaged = false;
                //other.GetComponentInChildren<PlayerScrollMap>().origin_layer = other.gameObject.layer;
                
        }
        else if(other.gameObject.layer == 6)
        {
            RemotePlayer r = other.GetComponent<RemotePlayer>();
            if (r && !r.skillHit)
            {
                //r.gameObject.layer = 2; // ignore collider
                r.SkillHit();
            }
        }
 
    }

    void Remove()
    {
        
        Destroy(gameObject);
    }

    private void Update()
    {
        if(!stop)//���� ��������
            transform.position = new Vector3(transform.position.x, transform.position.y - (dropSpeed * Time.deltaTime), transform.position.z);
    }

    //void 
}
