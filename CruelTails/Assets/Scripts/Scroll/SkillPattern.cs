using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPattern : Pattern
{
    [SerializeField] ScrollMapObject[] destroy_objects;

    int curObjNum = 0; int objNum_max = 100;

    // 스킬 패턴 장애물에 id 부여
    public void SkillPatternInit()
    {
        
        for(int i = 0; i < destroy_objects.Length; i++ )
        {
            //Debug.Log(destroy_objects[i].name + " Registed id : " + curObjNum);
            //destroy_objects[i].id = curObjNum;
            if(ScrollMapObjManager.Instance)
            {
                ScrollMapObjManager.Instance.AddObjects(destroy_objects[i]);
            }

            //curObjNum++;
            //if (curObjNum >= objNum_max)
                //curObjNum = 0;
        }
    }

    private void Awake()
    {
        if (ScrollMapObjManager.Instance)
            objNum_max = ScrollMapObjManager.maxNum;
    }
    // Update is called once per frame
    void Update()
    {
        ScrollMap();
    }
}
