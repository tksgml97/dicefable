using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUiManager : MonoBehaviour
{
    public static GameUiManager instance;
    [SerializeField] GameObject uiHolder;
    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(this);
            instance = this;
        }
    }
    public void SetDisplay(bool on)
	{
        uiHolder.SetActive(on);
	}

    // Start is called before the first frame update
    void Start()
    {
        SetDisplay(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
