using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WolfHungry : MonoBehaviour
{
    [SerializeField] Image barImage;
    [SerializeField] GameObject bar;
    [SerializeField] Gradient barGradient;

    [SerializeField] float hungryTimeSecond = 15;
    float currentHungry = 0;
    public float wolfSpeed50 = 1.2f;
    public float wolfSpeed90 = 1.5f;
    bool isDie = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (isDie) return;
        if (GameManager.instance.currentGame != eGameRound.Round1 || GameManager.instance.myTeam != eTeamType.Wolf)
        {
            currentHungry = 0;
            bar.SetActive(false);
            return;
		}
        bar.SetActive(true);

        Vector3 playerPos = GameManager.playerController.transform.position;
        Vector3 screenPlayerPos = Camera.main.WorldToScreenPoint(playerPos);

        bar.transform.position = screenPlayerPos;

        GameManager.playerController.SetHungryWolfSpeed(1);
        if (currentHungry / hungryTimeSecond > 0.5f)
		{
            GameManager.playerController.SetHungryWolfSpeed(wolfSpeed50);
		}
        if (currentHungry / hungryTimeSecond > 0.9f)
        {
            GameManager.playerController.SetHungryWolfSpeed(wolfSpeed90);
        }

        if(GameManager.instance.isStart)
            UpdateHungryState(currentHungry+Time.deltaTime);


        if(currentHungry >= hungryTimeSecond)
		{
            isDie = true;
            NetworkConnect.instance.SendDieClient();

        }

    }

    public void UpdateHungryState(float currentHungry)
	{
        this.currentHungry = currentHungry;
        barImage.fillAmount = currentHungry / hungryTimeSecond;
        barImage.color = barGradient.Evaluate(currentHungry / hungryTimeSecond);

    }
}
