using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spine.Unity;
using Spine;
public enum SpineSkinSlot
{
    bott,
    hair,
    eyebrow,
    eye,
    mouth,
    team,
    shoe,
	top,
    tail,
    glassed,
    onepiece,
    hat,
    bag,
    helmet,
    cliping,
    MAX
}
public enum SpineSlotType
{
    skin,
    hair,
    effect,
    costume,
    other
}

public class PlayerSpineSkinData
{
    public Dictionary<SpineSkinSlot, string> slots = new Dictionary<SpineSkinSlot, string>();
    public Color skin, hair;
}

[Serializable]
public struct skinData{
    public SpineSkinSlot slotType;
    [SpineSkin] public string[] name;
}

public struct skinSlotString
{
    public SpineSkinSlot slotType;
    public string name;
}


public class SpineSkinManager : MonoBehaviour
{

    public static SpineSkinManager instance ;
    [SerializeField] SkeletonAnimation skeleton;
    [SerializeField] skinData[] skinDatas;
    [SerializeField] skinData wolfSkin;
    [SerializeField] skinData redHatSkin;

    public Color[] hairColors;
    public Color[] skinColors;

    public Dictionary<string, SpineSlotType> slotTypes = new Dictionary<string, SpineSlotType>();
    public Dictionary<skinSlotString, int> stringToSkinIndex = new Dictionary<skinSlotString, int>();
    public PlayerSpineSkinData MakeRandomSkin()
    {
        PlayerSpineSkinData playerSpineSkinData = new PlayerSpineSkinData();

        for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
		{
            int randomIndex = UnityEngine.Random.Range(0, skinDatas[i].name.Length);
            playerSpineSkinData.slots[(SpineSkinSlot)i] = skinDatas[i].name[randomIndex];
		}
        return playerSpineSkinData;
    }
    [SerializeField] PlayerCloset playerCloset;
    /// <summary>���� ���� ��Ų</summary>
    public void SetRandomSkin()
    {
        //PlayerSpineSkinData playerSpineSkinData = new PlayerSpineSkinData();

        for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
        {
            if ((SpineSkinSlot)i == SpineSkinSlot.team || (SpineSkinSlot)i == SpineSkinSlot.cliping) continue;
            int randomIndex = UnityEngine.Random.Range(0, skinDatas[i].name.Length);
            
            if (playerCloset != null)
                playerCloset.SetParts((SpineSkinSlot)i, randomIndex);
        }
        //return playerSpineSkinData;
    }

    public string GetSkin(SpineSkinSlot slot, int index)
	{
        //Debug.Log(slot.ToString() + index);
        if (index < 0) return "";
        return skinDatas[(int)slot].name[index];
	}

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
		for (int i = 0; i < skinDatas.Length; i++)
		{
			for (int j = 0; j < skinDatas[i].name.Length; j++)
			{

                skinSlotString slotString;
                slotString.slotType = skinDatas[i].slotType;
                slotString.name = skinDatas[i].name[j];
                if(stringToSkinIndex.ContainsKey(slotString) == false)
                    stringToSkinIndex.Add(slotString, j);
            }
		}
        InitSlot();
    }
    

    void InitSlot()
	{
        foreach (var item in skeleton.Skeleton.Slots)
        {
            if (item.ToString().IndexOf("effect") != -1 || item.ToString().IndexOf("r1_obj") != -1 || item.ToString().IndexOf("wolf_shadow") != -1)
            {
                slotTypes.Add(item.ToString(), SpineSlotType.effect);
                item.SetCustomColor(new Color(0, 1, 0, 0));
                continue;
            }
            SpineSlotType type = SpineSlotType.other;
            if (item.ToString().IndexOf("body") != -1)
            {
                type = SpineSlotType.skin;
            }
            if (item.ToString().IndexOf("hair") != -1)
            {
                Debug.Log(item.ToString() + "hair");
                type = SpineSlotType.hair;
            }
            if (item.ToString().IndexOf("PC_F_head") != -1)
            {
                type = SpineSlotType.skin;
            }

            if (item.ToString().IndexOf("arm") != -1)
            {
                type = SpineSlotType.skin;
            }
            if (item.ToString().IndexOf("leg") != -1)
            {
                type = SpineSlotType.skin;
            }
            if (item.ToString().IndexOf("costume") != -1)
            {
                type = SpineSlotType.costume;
            }
            if (item.ToString().IndexOf("hair_acc") != -1)
            {
                type = SpineSlotType.costume;
            }
            slotTypes.Add(item.ToString(), type);
        }
    }

}
