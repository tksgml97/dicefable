using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum eItemEffectType
{
    Item_SpeedUp,
	Item_Shield,
    Count,
}

[Serializable]
public struct itemEffectData
{
    public eItemEffectType type;
    public float duration;
}


public class PlayerItemController : MonoBehaviour
{

    [SerializeField] itemEffectData[] datas;
    float[] durationDatas = new float[(int)eItemEffectType.Count];


    float[] itemDurations = new float[(int)eItemEffectType.Count];




    public void AddItemEffect(eItemEffectType item)
	{
        int itemIndex = (int)item;
        if(itemDurations[itemIndex] <= 0)
		{
            switch(item)
			{
                case eItemEffectType.Item_SpeedUp:

                    GameManager.playerController.eventSpeed = 1.5f;
                    break;
            }
		}
        itemDurations[itemIndex] = durationDatas[itemIndex];
	}

	// Start is called before the first frame update
	void Start()
    {
        for (int i = 0; i < itemDurations.Length; i++)
        {
            itemDurations[i] = 0;
        }

		for (int i = 0; i < datas.Length; i++)
		{
            int index = (int)datas[i].type;
            durationDatas[index] = datas[i].duration;
		}
    }

    // Update is called once per frame
    void Update()
    {
		for (int i = 0; i < itemDurations.Length; i++)
        {
            if (itemDurations[i] <= 0) continue;

            itemDurations[i] -= Time.deltaTime;
            if(itemDurations[i] <= 0)
            {
                switch ((eItemEffectType)i)
                {
                    case eItemEffectType.Item_SpeedUp:

                        GameManager.playerController.eventSpeed = 1f;
                        break;
                }
            }

        }
    }
}
