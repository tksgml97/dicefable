using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;



public class CinematicDirector_Round3Result : MonoBehaviour
{
    [SerializeField]
    private UIGameResultView resultView;

    [ShowInInspector]
    [ReadOnly]
    private int losePlayerIndex = 0;

    [ShowInInspector]
    [ReadOnly]
    private List<Transform> playerSlotList = new List<Transform>();

    [Header("첫번째 위치는 킬 되는 처형 대상자 위치로 고정입니다.")]
    [SerializeField]
    private List<Transform> killStandPointList;

    [ShowInInspector]
    [ReadOnly]
    private List<UISpineAnimator> uiSpineAnimatorList = new List<UISpineAnimator>();

    [SerializeField]
    private UISpineAnimator santaSpineAnimator;

    [ShowInInspector]
    [ReadOnly]
    private float currentFadeTime;

    [SerializeField]
    private Image fadeImage;

    [SerializeField]
    private float fadeInTime;

    public UnityEvent startFadeInEvent;
    public UnityEvent endFadeInEvent;

    [SerializeField]
    private float fadeOutTime;

    public UnityEvent startFadeOutEvent;
    public UnityEvent endFadeOutEvent;

    [SerializeField]
    private Color fadeColor;

    [SerializeField]
    private float fadeCinematicEndTime;
    [SerializeField]
    private Color fadeCinematicEndColor;

    public void GetLosePlayerIndex(int index)
    {
        playerSlotList.Clear();

        losePlayerIndex = index;
        for (var i = 0; i < resultView.matchPlayers.Length; ++i)
        {
            var matchPlayer = resultView.matchPlayers[i];
            playerSlotList.Add(matchPlayer.transform);
            uiSpineAnimatorList.Add(matchPlayer.GetComponent<UISpineAnimator>());
        }
    }

    public void RandomLookAt()
    {
        foreach (var playerTransform in playerSlotList)
        {
            var scale = playerTransform.localScale;

            scale.x = Random.Range(0f, 1f) > 0.5f ? -1f : 1f;

            playerTransform.localScale = scale;
        }
    }

    public void LookAt(int lookAt)
    {
        foreach (var playerTransform in playerSlotList)
        {
            var scale = playerTransform.localScale;

            scale.x = lookAt;

            playerTransform.localScale = scale;
        }
    }

    public void ChangeSantaAnimation(string clipName) {
        santaSpineAnimator.PlayAnimation(clipName);
    }


    public void ChangeKillPlayerAnimation(string clipName)
    {
        uiSpineAnimatorList[losePlayerIndex].PlayAnimation(clipName);
    }

    public void ChangeAllPlayerAnimation(string clipName)
    {
        foreach (var spineAnimator in uiSpineAnimatorList)
        {
            spineAnimator.PlayAnimation(clipName);
        }
    }

    public void ChangeStandPointForKill()
    {
        var positionIndex = 1;

        for (var i = 0; i < playerSlotList.Count; ++i)
        {
            var playerSlot = playerSlotList[i];
            if (i == losePlayerIndex)
            {
                playerSlot.SetParent(killStandPointList[0]);
            }
            else
            {
                playerSlot.SetParent(killStandPointList[positionIndex]);
                ++positionIndex;
            }
            playerSlot.localPosition = Vector3.zero;
        }
    }

    public void FadeIn()
    {
        StartCoroutine(CoFadeIn());
    }

    public void FadeOut()
    {
        StartCoroutine(CoFadeOut());
    }

    public void FadeCinematicEnd() {
        StartCoroutine(CoFadeCinematicEnd());
    }

    IEnumerator CoFadeIn()
    {
        currentFadeTime = fadeInTime;
        startFadeInEvent?.Invoke();

        do
        {
            currentFadeTime -= Time.deltaTime;
            fadeColor.a = Mathf.Lerp(0f, 1f, 1 - currentFadeTime / fadeInTime);
            fadeImage.color = fadeColor;
            yield return null;
        } while (fadeColor.a < 0.9f);

        fadeColor.a = 1f;
        fadeImage.color = fadeColor;
        endFadeInEvent?.Invoke();
    }

    IEnumerator CoFadeOut()
    {
        currentFadeTime = fadeOutTime;
        startFadeOutEvent?.Invoke();

        do
        {
            currentFadeTime -= Time.deltaTime;
            fadeColor.a = Mathf.Lerp(1f, 0f, 1 - currentFadeTime / fadeOutTime);
            fadeImage.color = fadeColor;
            yield return null;
        } while (fadeColor.a > 0.1f);

        fadeColor.a = 0f;
        fadeImage.color = fadeColor;
        endFadeOutEvent?.Invoke();
    }

    IEnumerator CoFadeCinematicEnd()
    {
        currentFadeTime = fadeCinematicEndTime;

        fadeColor = fadeCinematicEndColor;

        do
        {
            currentFadeTime -= Time.deltaTime;
            fadeColor.a = Mathf.Lerp(0f, 1f, 1 - currentFadeTime / fadeCinematicEndTime);
            fadeImage.color = fadeColor;
            yield return null;
        } while (fadeColor.a < 0.9f);

        fadeColor.a = 1f;
        fadeImage.color = fadeColor;
    }

}
