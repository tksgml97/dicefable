using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Spine;
using Spine.Unity;
using Sirenix.OdinInspector;
using System;
using UnityEngine.UI;

public class CinematicDirector_Round4Stair : MonoBehaviour
{
    public UnityEvent startCinematicEvnet;
    public UnityEvent endCinematicEvnet;

    [SerializeField]
    private int nextReadyCount = 2;

    [SerializeField]
    private int readyCount = 0;

    [SerializeField]
    private SkeletonAnimation[] fakeCharacterRenders;
    [SerializeField]
    private GameObject[] bezierFollowers;

    public UnityEvent allReadyEvent;

    [SerializeField]
    private List<Transform> localPlayers;
    [SerializeField]
    private Transform[] spawnPoints;

    [SerializeField]
    private float moveTime;

    [SerializeField]
    private Transform[] destinationPoints;

    public UnityEvent destinationEvent;

    
    public void StartCinematic()
    {
        SpawnPlayers();
        ActiveBezier();
        startCinematicEvnet?.Invoke();
    }
    List<GameObject> networkPlayers;
    public void SpawnPlayers()
    {
        //직접 이동이 필요한 연출에 사용되는 Controller 오브젝트를 위치 시킵니다.
        networkPlayers = new List<GameObject>();
        networkPlayers.AddRange(NetworkGameManager.instance.networkPlayer);
        Debug.Log(NetworkGameManager.memberNumber);
        networkPlayers[NetworkGameManager.memberNumber] = GameManager.playerController.gameObject;
        GameManager.playerController.gameObject.layer = 3;
        int kill = GameManager.instance.GetKillPlayerIndex();
        Debug.Log("[Alive User] dead user : " + kill);
        for (var i = 0; i < networkPlayers.Count; ++i)
        {
            if (networkPlayers[i] == null)
                continue;

            if (i == kill) { 
                continue;
            }

            if (networkPlayers[i].GetComponent<RemotePlayer>().isDie)
                continue;

            localPlayers.Add(networkPlayers[i].transform);

            //Debug.Log("[Alive User] user update : " + GameManager.instance.GetNameFromPlayerIndex(i));
        }
        //Debug.Log($"[Cienematic] spawnPoints : {spawnPoints.Length} , localPlayers : {localPlayers.Count}");
        for (var i = 0; i < spawnPoints.Length; ++i)
        {
            localPlayers[i].position = spawnPoints[i].position;
            localPlayers[i].GetComponent<PlayerController>().state = PlayerState.Idle;
        }

        for (var i = 0; i < fakeCharacterRenders.Length; ++i)
        {
            var skinData = localPlayers[i].GetComponent<PlayerAnimationController>().currentSkin;
            CreateFakeCharacter(i, skinData);
        }
    }

    public void CreateFakeCharacter(int index, PlayerSpineSkinData skin)
    {
        //강제 이동 간 필요한 Fake 용 캐릭터를 생성합니다.
        Skin SKIN = new Skin(skin.ToString());

        var skeleton = fakeCharacterRenders[index].skeleton;

        foreach (var item in skin.slots)
        {
            if (item.Value == "") continue;
            Skin addSkin = skeleton.Data.FindSkin(item.Value);
            if (addSkin == null)
            {
                Debug.LogError(item.Value);
            }
            SKIN.AddSkin(addSkin);
        }

        skeleton.SetSkin(SKIN);
        skeleton.SetSlotsToSetupPose();
    }

    [Button("ActiveBezier")]
    public void ActiveBezier()
    {
        for (var i = 0; i < bezierFollowers.Length; ++i)
        {
            bezierFollowers[i].SetActive(true);
        }
    }

    private float time_start;
    public void AllowMoveInput() {
        GameManager.instance.isStart = true;
        TimerOn = true;
        time_start = (float)DateTime.Now.TimeOfDay.TotalSeconds;
    }

    public void ReadyForNextRound()
    {
        ++readyCount;
        Debug.Log("[Cienematic] ReadyCount :}" + readyCount);
        if (readyCount >= nextReadyCount)
        {
            //TODO ::  연출 시작
            GameManager.instance.isStart = false;
            allReadyEvent?.Invoke();
        }
    }

    bool TimerOn = false;
    bool isEventActive = false;
    public void MoveBookStand()
    {
        if (isEventActive) return;
        isEventActive = true;
        Debug.Log("[Cienematic] MoveBookStand");
        for (var i = 0; i < localPlayers.Count; ++i)
            PlayerRunAni(i);
        StartCoroutine(CoMoveBookStand());
    }

    IEnumerator CoMoveBookStand()
    {
        var time = 0f;

        var startPoints = new Vector3[2];
        for (var i = 0; i < localPlayers.Count; ++i)
        {
            startPoints[i] = localPlayers[i].position;
        }

        while (time <= moveTime)
        {
            time += Time.deltaTime;
            for (var i = 0; i < localPlayers.Count; ++i)
            {
                localPlayers[i].position = Vector3.Lerp(startPoints[i], destinationPoints[i].position, time / moveTime);
            }

            yield return null;
        }
        destinationEvent?.Invoke();
    }

    public void EndCinematic()
    {
        Debug.Log("[Cienematic] End Cinematic");
        //TODO :: 여기서 4라운드로 넘기거나, 영상 재생등등.. 하시면됩니다.
        for (int i = 0; i < localPlayers.Count; i++) localPlayers[i].GetComponent<PlayerMoveSound>().StopMoveSound();
        endCinematicEvnet?.Invoke();
        StartCoroutine(SceneLoader.instance.LoadScene(eSceneType.Round4, NetworkConnect.instance.GetGameInitData));
    }

    // 오류로 연출이 진행되지 않을 때 timeLimit 이후에 강제 진행
    [SerializeField]
    private int timeLimit = 10; 
    //[SerializeField]
    //private float automoveSpeed = 2.4f;
    [SerializeField]
    Text timeLimit_text;
    private void Update()
    {
        if(TimerOn && !isEventActive)
        {
            float timer = timeLimit - ((float)System.DateTime.Now.TimeOfDay.TotalSeconds - time_start);
            if (timer < 6 && timeLimit_text != null) timeLimit_text.text = ((int)timer).ToString();
            Debug.Log(timer);
            if (timer < 0)
            {
                //MoveBookStand();
                Debug.Log("[Cienematic] AutoNextRound");
                TimerOn = false;
                isEventActive = true;

                GameManager.instance.isStart = false;
                EndCinematic();
                //for (var i = 0; i < localPlayers.Count; ++i)
                //    PlayerRunAni(i);
                //StartCoroutine(CoAutoMove());

            }  
        }
    }
    
    void PlayerRunAni(int i)
    {
        if(localPlayers[i].position.z < destinationPoints[i].position.z)
            localPlayers[i].GetComponent<PlayerAutoAnimation>().SetAnimation(eAutoAnimation.Run_B);
        else
            localPlayers[i].GetComponent<PlayerAutoAnimation>().SetAnimation(eAutoAnimation.Run_F);
    }

    bool idleAni = true;
    void PlayerIdleAni(int i)
    {
        if(idleAni)
        {
            localPlayers[i].GetComponent<PlayerAutoAnimation>().SetAnimation(eAutoAnimation.Idle_B);
            idleAni = false;
        }
    }
    
    IEnumerator CoAutoMove()
    {
        var startPoints = new Vector3[2];
        for (var i = 0; i < localPlayers.Count; ++i)
        {
            startPoints[i] = localPlayers[i].position;
        }
        int cnt = 0;
        while (cnt < localPlayers.Count)
        {
            cnt = 0;

            for (var i = 0; i < localPlayers.Count; ++i)
            {
                if ((localPlayers[i].position - destinationPoints[i].position).magnitude < 0.1f)
                {
                    PlayerIdleAni(i);
                    cnt++;
                }
                    
                //else
                    //localPlayers[i].position = Vector3.MoveTowards(localPlayers[i].position, destinationPoints[i].position, automoveSpeed * Time.deltaTime);

            }

            yield return null;
        }
        destinationEvent?.Invoke();
    }
}
