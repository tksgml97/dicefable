using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KoreaGameManager : MonoBehaviour
{
    
    public static KoreaGameManager instance;

    public static bool isFeverTime = false;
    public static int feverStartSecond = 120;

    public UnityEvent<int,int> scoreUpdateEvent;
    private void Awake()
    {
        Debug.Assert(instance == null);
        instance = this;
    }

    List<KoreaGameTable> tables = new List<KoreaGameTable>();
    [SerializeField] GameObject itemPrefab;
    [SerializeField] GameObject feverUI;
    [SerializeField] Animator feverAni;

    public void SpawnItem(Vector3 position, Vector3 velocity, eItemType type, int id)
	{
        Debug.Log("Spawn" + id);
        GameObject go = Instantiate(itemPrefab);
        go.transform.position = position;

        go.GetComponent<KoreaItemObject>().SetVelocity(velocity);
        go.GetComponent<KoreaItemObject>().SetType(type);
        ItemManager.instance.AddItem(go.GetComponent<NetworkObject>(), id);
	}
    public void AddTable(KoreaGameTable table)
    {
        table.id = tables.Count;
        tables.Add(table);
    }

    public void SetTable(int tableID, int itemID)
	{
        tables[tableID].SetTable((eItemType)itemID);
        UpdateScore();
    }

    public void TakeTable(int tableID, int itemID)
    {
        tables[tableID].TakeItem((eItemType)itemID);
        UpdateScore();
    }

    // Start is called before the first frame update
    void Start()
    {
        isFeverTime = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.currentGame != eGameRound.Round2)
            return;

        if(isFeverTime == false && GameManager.instance.GetCurrenetTime() < feverStartSecond)
		{
            if (GameManager.instance.currentGame == eGameRound.Round2)
            {
                Round2_Tutorial.instance.FeverTime();
            }
            isFeverTime = true;
            feverUI.SetActive(true);
            feverAni.SetTrigger("Fever");
        }
    }

    void UpdateScore()
	{
        int r = 0, b = 0;

        r = tables[0].GetScore();
        b = tables[1].GetScore();
        scoreUpdateEvent?.Invoke(r, b);
    }
}
