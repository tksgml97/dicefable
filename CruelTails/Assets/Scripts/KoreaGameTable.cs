using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KoreaGameTable : MonoBehaviour
{
    [SerializeField] GameObject[] items;
    [SerializeField] GameObject pivot;

    [SerializeField] int[] needItems;
    [SerializeField] int[] currentItems;

    public static int[] itemScore =
    {
        1,
        1,
        1,
        2,
        2,
        3,
        4,
        5,
        5,
        6,
        7,
        7,
        8,
        8,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
        10,
    };

    List<GameObject>[] itemObjects;

    public int team = 0;

    public int id;

    public int GetScore()
	{
        int score = 0;
        for (int i = 0; i < (int)eItemType.None; i++)
        {
            score += currentItems[i] * itemScore[i];
        }
        return score;
	}
    public bool TrySetItem(eItemType type)
    {
        if (currentItems[(int)type] >= needItems[(int)type])
            return false;

        Debug.Log(type);
        //NetworkManager.instance.SendSetTable((int)type, id);
        NetworkConnect.instance.SendSetTable((int)type, id);
        
        return true;
        
    }

    public void SetTable(eItemType type)
	{
        if (currentItems[(int)type] >= needItems[(int)type])
            return;
        UpdateItemActive(itemObjects[(int)type][currentItems[(int)type]], true);
        currentItems[(int)type]++;
    }

    private void Awake()
	{
        needItems = new int[(int)eItemType.None];
        currentItems = new int[(int)eItemType.None];
        itemObjects = new List<GameObject>[(int)eItemType.None];
    }

    
	// Start is called before the first frame update
	void Start()
    {
        GameManager.playerController.spacebarEvent += SetTableEvent;
    }

    bool init = false;

    // Update is called once per frame

    float cool = 0;
    void Update()
    {
        if(!init)
		{
            init = true;
            InitTableItem();
        }
        cool -= Time.deltaTime;
    }

    void SetTableEvent()
	{
        if (GameManager.playerController.state == PlayerState.Hit) return;
        if (GameManager.instance.myTeam == eTeamType.None) return;
        Vector3 vec = pivot.transform.position - GameManager.playerController.transform.position;
        vec.y = 0;
        if (vec.magnitude < 1.3f)
        {

            if (team == (int)GameManager.instance.myTeam)
            {
                if (GameManager.playerController.currentItem != eItemType.None)
                {
                    GameManager.playerController.canDrop = false;
                    //������

                    Debug.Log("SetItem");

                    Debug.Log("SetItem" + GameManager.playerController.currentItem.ToString());
                    TrySetItem(GameManager.playerController.currentItem);
                    GameManager.playerController.GetComponent<PlayerMoveSound>().PlayTableSound(true);

                    if (GameManager.instance.currentGame == eGameRound.Round2)
                    {

                        Round2_Tutorial.instance.PutItem(true);
                    }

                }
            }
            else
            {
                if (GameManager.playerController.currentItem == eItemType.None && KoreaGameManager.isFeverTime)
                {
                    GameManager.playerController.canDrop = false;
                    //�ٸ���
                    NetworkConnect.instance.TakeTable(team);
                }

            }

        }
    }
    public void TakeItem(eItemType type)
	{
        Debug.Log((int)type);
        if (currentItems[(int)type] == 0)
            return;
        UpdateItemActive(itemObjects[(int)type][currentItems[(int)type] - 1], false);
        currentItems[(int)type]--;
        Vector3 vec = transform.position - GameManager.playerController.transform.position;
        vec.y = 0;
        if (vec.magnitude < 3f && cool <= 0)
        {
            GameManager.playerController.GetComponent<PlayerMoveSound>().PlayTableSound(false);

            Round2_Tutorial.instance.GetEnemyItem();
            cool = 0.5f;
        }
    }

    public void UpdateItemActive(GameObject go, bool active)
	{
        go.SetActive(active);


        if(active)
		{
            ParticleManager.instance.playEffect(go.transform.position, Quaternion.identity, eParticleType.twinkletable, false);
		}
        /*{
            if (active)
            {
                go.GetComponent<SpriteRenderer>().color = Color.white;

            }
            else
            {

                go.GetComponent<SpriteRenderer>().color = Color.black;
            }
        }*/
	}

    

    void InitTableItem()
    {
        for (int i = 0; i < (int)eItemType.None; i++)
        {
            needItems[i] = 0;
            currentItems[i] = 0;
            itemObjects[i] = new List<GameObject>();
        }
        for (int i = 0; i < items.Length; i++)
        {
            KoreaItemObject kio = items[i].GetComponent<KoreaItemObject>();
            kio.Init();
            kio.canGet = false;
               // Debug.Log(kio.type.ToString());
            needItems[(int)kio.type]++;
            itemObjects[(int)kio.type].Add(items[i]);
            //Collider c= items[i].GetComponent<Collider>();
            //if (c) c.enabled = false;
            UpdateItemActive(items[i], false);
        }

		for (int i = 0; i < (int)eItemType.None; i++)
		{
            
            //Debug.Log(i + "  need : "+needItems[i]);
		}
    }
    
}
