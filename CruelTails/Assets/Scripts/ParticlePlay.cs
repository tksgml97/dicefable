using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePlay : MonoBehaviour
{

    [SerializeField] float particleTime = 1;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitRemoveParticle(particleTime));
    }
    IEnumerator WaitRemoveParticle(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);

        Destroy(gameObject);

    }
}
