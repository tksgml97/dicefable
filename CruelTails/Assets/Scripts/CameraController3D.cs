using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController3D : MonoBehaviour
{
    public static CameraController3D instance;

    [SerializeField] float lerpSpeed = 10f;
    [SerializeField] float cameraDistance = 40f;
    [SerializeField] float cameraAngle = 60f;
    [SerializeField] float camSize = 12;
    [SerializeField] float camSizeSpeed = 3f;

    [SerializeField] bool followTarget = true;
    public float shakeTIme = 0;
    [SerializeField] Vector3 shakeDirection = Vector3.right;
    [SerializeField] bool randomShakeDirection = false;
    public float shakePower = 1;

    public Transform target;
    public bool cameraClamp = true;

    public Vector3 minTargetArea, maxTargetArea;
    private Camera m_cam;
    private bool m_shakeReverse = false;

    [SerializeField] AnimationCurve shakeCurve;
    float verticalHeightSeen;
    float verticalWidthSeen;

    float currentCamSize ;

    public float sizeUp;
    public bool isSub = false;
    // Update is called once per frame
    private void Awake()
	{
        if (isSub == false)
        {
            instance = this;
            if (target == null)
            {
                target = GameObject.Find("LocalPlayer").transform;
            }
        }
        m_cam = transform.GetComponentInChildren<Camera>();
        Debug.Assert(m_cam);
        currentCamSize = camSize;

    }
	void LateUpdate()
    {
        Vector3 tPos = target.position;


        if (cameraClamp)
        {
            verticalHeightSeen = m_cam.orthographicSize * 2.0f;
            verticalWidthSeen = verticalHeightSeen * Camera.main.aspect;
            tPos.x = Mathf.Clamp(tPos.x, minTargetArea.x + verticalWidthSeen * 0.5f, maxTargetArea.x - verticalWidthSeen * 0.5f);
            tPos.z = Mathf.Clamp(tPos.z, minTargetArea.z + verticalHeightSeen * 0.5f, maxTargetArea.z - verticalHeightSeen * 0.5f);
        }

        if (followTarget)
        {
            transform.position = Vector3.Lerp(transform.position, tPos, Mathf.Clamp01(lerpSpeed * Time.deltaTime));
        }
        m_cam.transform.localPosition = Vector3.back * cameraDistance;
        transform.rotation = Quaternion.Euler(cameraAngle, 0, 0);
        currentCamSize = Mathf.MoveTowards(currentCamSize, camSize + sizeUp, Time.deltaTime * camSizeSpeed);
        m_cam.orthographicSize = currentCamSize;

		#region update camera shake
		m_shakeReverse = !m_shakeReverse;
        shakeTIme = Mathf.Max(0, shakeTIme - Time.deltaTime);

        Vector3 direction;
        if(randomShakeDirection)
		{
            float x = Random.Range(-1f, 1f);
            float y = Random.Range(-1f, 1f);
            float z = Random.Range(-1f, 1f);
            direction = new Vector3(x, y, z);
            direction.Normalize();
        }
		else
		{
            direction = shakeDirection.normalized;

        }
        Vector3 shakePosition = direction * shakeCurve.Evaluate(shakeTIme);
        shakePosition *= m_shakeReverse ? -1 : 1;
        shakePosition *= shakePower;
        m_cam.transform.position = m_cam.transform.position + shakePosition;
		#endregion  
	}

	public void MoveToTarget()
    {
        transform.position = target.position;
    }

    public void Shake(Vector3 direction)
	{
        shakeDirection = direction;
    }
    private void OnDrawGizmosSelected()
    {
        float verticalHeightSeen = Camera.main.orthographicSize * 2.0f;
        float verticalWidthSeen = verticalHeightSeen * Camera.main.aspect;



        //타겟 이동 허용 범위
        Gizmos.color = Color.red - new Color(0, 0, 0, 0.5f);
        Gizmos.DrawCube((maxTargetArea + minTargetArea) * 0.5f, maxTargetArea - minTargetArea);

    }
}
