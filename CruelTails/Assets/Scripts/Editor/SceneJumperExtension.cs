#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
public class SceneJumperExtension : EditorWindow
{

	[MenuItem("Jumper/3And4R/4RStair", priority = 100)]
	private static void ChangeTo_Jumper_3And4R_4RStair()
	{
		SceneJumper.ChangeScene("Assets/Scenes/3And4R/4RStair.unity");
	}

	[MenuItem("Jumper/BuildScenes/Lobby_BuildScene", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_Lobby_BuildScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/Lobby_BuildScene.unity");
	}

	[MenuItem("Jumper/BuildScenes/R1 Test", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_R1_Test()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/R1 Test.unity");
	}

	[MenuItem("Jumper/BuildScenes/Round1_BuildScene", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_Round1_BuildScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/Round1_BuildScene.unity");
	}

	[MenuItem("Jumper/BuildScenes/Round2_BuildScene", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_Round2_BuildScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/Round2_BuildScene.unity");
	}

	[MenuItem("Jumper/BuildScenes/Round3_BuildScene", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_Round3_BuildScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/Round3_BuildScene.unity");
	}

	[MenuItem("Jumper/BuildScenes/Round4_BuildScene", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_Round4_BuildScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/Round4_BuildScene.unity");
	}

	[MenuItem("Jumper/BuildScenes/StartScene", priority = 100)]
	private static void ChangeTo_Jumper_BuildScenes_StartScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/BuildScenes/StartScene.unity");
	}

	[MenuItem("Jumper/ResouceUI/New Scene", priority = 100)]
	private static void ChangeTo_Jumper_ResouceUI_New_Scene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/ResouceUI/New Scene.unity");
	}

	[MenuItem("Jumper/ResouceUI/R1_UIResource", priority = 100)]
	private static void ChangeTo_Jumper_ResouceUI_R1_UIResource()
	{
		SceneJumper.ChangeScene("Assets/Scenes/ResouceUI/R1_UIResource.unity");
	}

	[MenuItem("Jumper/ResouceUI/Tutorial_Round2", priority = 100)]
	private static void ChangeTo_Jumper_ResouceUI_Tutorial_Round2()
	{
		SceneJumper.ChangeScene("Assets/Scenes/ResouceUI/Tutorial_Round2.unity");
	}

	[MenuItem("Jumper/ResouceUI/UI Scene", priority = 100)]
	private static void ChangeTo_Jumper_ResouceUI_UI_Scene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/ResouceUI/UI Scene.unity");
	}

	[MenuItem("Jumper/SanDev/GameScene 1", priority = 100)]
	private static void ChangeTo_Jumper_SanDev_GameScene_1()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SanDev/GameScene 1.unity");
	}

	[MenuItem("Jumper/SanDev/GameScene", priority = 100)]
	private static void ChangeTo_Jumper_SanDev_GameScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SanDev/GameScene.unity");
	}

	[MenuItem("Jumper/SanDev/GameScene_2D", priority = 100)]
	private static void ChangeTo_Jumper_SanDev_GameScene_2D()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SanDev/GameScene_2D.unity");
	}

	[MenuItem("Jumper/SanDev/Lobby", priority = 100)]
	private static void ChangeTo_Jumper_SanDev_Lobby()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SanDev/Lobby.unity");
	}

	[MenuItem("Jumper/SanDev/NetworkTest", priority = 100)]
	private static void ChangeTo_Jumper_SanDev_NetworkTest()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SanDev/NetworkTest.unity");
	}

	[MenuItem("Jumper/SanDev/New Scene", priority = 100)]
	private static void ChangeTo_Jumper_SanDev_New_Scene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SanDev/New Scene.unity");
	}

	[MenuItem("Jumper/SoundScene/R1_FootSoundScene", priority = 100)]
	private static void ChangeTo_Jumper_SoundScene_R1_FootSoundScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SoundScene/R1_FootSoundScene.unity");
	}

	[MenuItem("Jumper/SoundScene/R2_FootSoundScene", priority = 100)]
	private static void ChangeTo_Jumper_SoundScene_R2_FootSoundScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/SoundScene/R2_FootSoundScene.unity");
	}

	[MenuItem("Jumper/UIDev/GameScene_UIDev", priority = 100)]
	private static void ChangeTo_Jumper_UIDev_GameScene_UIDev()
	{
		SceneJumper.ChangeScene("Assets/Scenes/UIDev/GameScene_UIDev.unity");
	}

	[MenuItem("Jumper/UIDev/Lobby_UIDev", priority = 100)]
	private static void ChangeTo_Jumper_UIDev_Lobby_UIDev()
	{
		SceneJumper.ChangeScene("Assets/Scenes/UIDev/Lobby_UIDev.unity");
	}

	[MenuItem("Jumper/YCDev/Round4_BuildScene 1", priority = 100)]
	private static void ChangeTo_Jumper_YCDev_Round4_BuildScene_1()
	{
		SceneJumper.ChangeScene("Assets/Scenes/YCDev/Round4_BuildScene 1.unity");
	}

	[MenuItem("Jumper/YNDev/Round3_BuildScene", priority = 100)]
	private static void ChangeTo_Jumper_YNDev_Round3_BuildScene()
	{
		SceneJumper.ChangeScene("Assets/Scenes/YNDev/Round3_BuildScene.unity");
	}

	[MenuItem("Jumper/CopyScene/Round1/Round1_lighting", priority = 100)]
	private static void ChangeTo_Jumper_CopyScene_Round1_Round1_lighting()
	{
		SceneJumper.ChangeScene("Assets/Scenes/CopyScene/Round1/Round1_lighting.unity");
	}

	[MenuItem("Jumper/CopyScene/Round3/Round3Copy", priority = 100)]
	private static void ChangeTo_Jumper_CopyScene_Round3_Round3Copy()
	{
		SceneJumper.ChangeScene("Assets/Scenes/CopyScene/Round3/Round3Copy.unity");
	}

	[MenuItem("Jumper/CopyScene/Round3/Resource_Scenes/patterntest", priority = 100)]
	private static void ChangeTo_Jumper_CopyScene_Round3_Resource_Scenes_patterntest()
	{
		SceneJumper.ChangeScene("Assets/Scenes/CopyScene/Round3/Resource_Scenes/patterntest.unity");
	}

}

#endif