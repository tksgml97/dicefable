﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Unity.IO.LowLevel.Unsafe;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using Utils;
using Utils.Data;

[InitializeOnLoad]
public class SceneJumper : EditorWindow
{
	private static readonly string mSceneExtension = ".unity";
	private static readonly string mSceneReletivePath = "Assets/Scenes";

	private static string mExtensionFilePath => Path.Combine
	(
		GetThisCodeFileDirectory(),
		$"{GetthisCodeFileNameWithoutExtension()}{"Extension.cs"}"
	);

	private static string mSceneAbsolutePath => Application.dataPath + "/Scenes";

	public class ScenePathData
	{
		public string SceneAbsolutePath;
		public string SceneEditorPath;
		public string QuickSlotPath;
		public string FunctionSignature;

		public ScenePathData(string sceneAbsolutePath)
		{
			string relativePath = Directory.GetParent(Application.dataPath).ToString();
			SceneAbsolutePath = sceneAbsolutePath.Replace("\\", "/");

			SceneEditorPath = Path.GetRelativePath(relativePath, SceneAbsolutePath).Replace("\\", "/"); ;
			QuickSlotPath = SceneEditorPath.Replace(mSceneReletivePath, "Jumper");
			QuickSlotPath = QuickSlotPath[..^mSceneExtension.Length];
			FunctionSignature = "ChangeTo_" + QuickSlotPath.Replace("/", "_");
			FunctionSignature = FunctionSignature.Replace(" ", "_");
		}

		public string GetFunctionDeclaration(int priority = 0)
		{
			return $"\n\t[MenuItem(\"{QuickSlotPath}\", priority = {priority.ToString()})]\n" +
				$"\tprivate static void {FunctionSignature}()\n" +
				"\t{\n" +
				$"\t\tSceneJumper.ChangeScene(\"{SceneEditorPath}\");\n" +
				"\t}\n";
		}
	}

	[MenuItem("Jumper/Play Game")]
	public static void PlaySceneMenu()
	{
		PlayScene("Assets/Scenes/BuildScenes/Lobby_BuildScene.unity");
	}

	[MenuItem("Jumper/Setup scene quick menu", priority = 200)]
	public static void SetupSceneQuickMenu()
	{
		// Extrack scene path and relative data
		List<ScenePathData> scenes = new();
		Queue<string> sceneDirectories = new(Directory.GetDirectories(mSceneAbsolutePath));

		while (sceneDirectories.Count != 0)
		{
			string curDirectory = sceneDirectories.Dequeue();
			foreach (var d in Directory.GetDirectories(curDirectory))
			{
				sceneDirectories.Enqueue(d);
			}

			var files = Directory.GetFiles(curDirectory);

			foreach (var filePath in files)
			{
				string curExtension = Path.GetExtension(filePath);

				if (mSceneExtension == curExtension)
				{
					scenes.Add(new ScenePathData(filePath));
				}
			}
		}

		// Generate scene jump code
		string header = @"#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
public class SceneJumperExtension : EditorWindow
{
";

		string footer = @"
}

#endif";

		string data = header;

		foreach (var sceneData in scenes)
		{
			data += sceneData.GetFunctionDeclaration(100);
		}
		data += footer;

		if (FileHandler.TrySaveToFile(mExtensionFilePath, data))
		{
			Debug.Log($"Save file success at {mExtensionFilePath}");
		}
		else
		{
			Debug.Log($"Saveing failed!");
		}

		AssetDatabase.Refresh();
	}

	#region Scene Management Functions

	public static void PlayScene(string scenePath)
	{
		if (EditorApplication.isPlaying)
		{
			EditorApplication.ExitPlaymode();
		}

		ChangeScene(scenePath);
		EditorApplication.isPlaying = true;
	}

	public static void ChangeScene(string scenePath)
	{
		if (EditorApplication.isPlaying)
		{
			EditorApplication.ExitPlaymode();
		}

		// If the scene has been modified, Editor ask to you want to save it.
		if (EditorSceneManager.GetActiveScene().isDirty)
		{
			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
		}

		EditorSceneManager.OpenScene(scenePath);
	}

	#endregion

	public static string GetThisCodeFilePath([CallerFilePath] string path = null)
	{
		return path;
	}

	public static string GetThisCodeFileDirectory()
	{
		return Path.GetDirectoryName(GetThisCodeFilePath());
	}

	public static string GetthisCodeFileNameWithoutExtension()
	{
		return Path.GetFileNameWithoutExtension(GetThisCodeFilePath());
	}
}

#endif