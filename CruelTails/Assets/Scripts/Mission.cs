using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MissionState
{
	Disable,
	Active,
	Play,
	RemotePlay,
	Claer
}

public class Mission : MonoBehaviour
{
	[SerializeField] GameObject waitObject;
	[SerializeField] GameObject clearObject;
	[SerializeField] int missionID;
	[HideInInspector] MissionData m_missionData;
	MissionSound missionSound = null;
	float m_currentGage = 0;
	float m_delay = 0;
	float m_maxGage = 6f;
	MissionState m_missionState = MissionState.Disable;
	public MissionState missionState
	{
		get { return m_missionState; }
	}

	public MissionData data
	{
		get { return m_missionData; }
	}

	public void SetMissionActive()
	{
		m_missionState = MissionState.Active;
		FranceGameManager.instance.UpdateMissionList();
		waitObject.SetActive(true);
	}

	void Start()
	{
		missionSound = GetComponent<MissionSound>();
		waitObject.SetActive(false);
		m_missionData = GameDataLoader.instance.GetMission(missionID);
		if(m_missionData.m_missionTriggerID == 0)
		{
			m_missionState = MissionState.Active;
			waitObject.SetActive(true);
		}
		GameManager.playerController.spaceHoldEvent += MissionEvent;
		FranceGameManager.instance.AddMission(this);
	}
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.G))
		{


			MessageUIManager.instance.CreateMessageFromWorldPosition(transform.position, "미션완료");
		}
		m_delay -= Time.deltaTime;
		if (m_currentGage > 0 && m_delay <= 0)
		{
			m_currentGage = 0;
			GameManager.playerController.SetMissionPlay(false, data);
			FranceGameManager.instance.missionUpdateEvent?.Invoke(0, m_missionData);
		}
	}

	void MissionEvent()
	{
		if (GameManager.playerController.state == PlayerState.Hit || GameManager.playerController._AnimState == AnimState.PC_wolf_rotation || GameManager.playerController._AnimState == AnimState.PC_wolf_passout) return;

		if (m_missionState != MissionState.Active && m_missionState != MissionState.Play) return;
		if (GameManager.instance.myTeam == eTeamType.Wolf) return;
		Vector3 vec = transform.position - GameManager.playerController.transform.position;
		vec.y = 0;
		if (vec.magnitude < 1.2f)
		{
			m_delay = 0.3f;
			m_currentGage += Time.deltaTime;
			GameManager.playerController.SetMissionPlay(true, data);

			FranceGameManager.instance.missionUpdateEvent?.Invoke(m_currentGage / m_maxGage, m_missionData);
			if(m_currentGage > m_maxGage)
			{
				Clear();
			}
		}
	}

	void Clear()
	{
		if (missionID == 6004) WolfCamController.Instance.OnWolfCam(10f);
		MessageUIManager.instance.CreateMessageFromWorldPosition(transform.position + Vector3.up, "미션완료");
		m_missionState = MissionState.Claer;
		FranceGameManager.instance.Clear(this);
		FranceGameManager.instance.missionUpdateEvent?.Invoke(0, m_missionData);
		waitObject.SetActive(false);
		clearObject.SetActive(true);

		missionSound?.PlayMissionSound(m_missionState);
	}


}
