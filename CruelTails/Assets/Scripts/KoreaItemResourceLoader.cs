using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum eItemType
{
	Sell_product_Miso,
	Sell_product_medicinal,
	R2_Item_1,
	R2_Item_2_1,
	R2_Item_2_2,
	R2_Item_3,
	R2_Item_4,
	R2_Item_5_1,
	R2_Item_5_2,
	R2_Item_6,
	R2_Item_7_1,
	R2_Item_7_2,
	R2_Item_8_1,
	R2_Item_8_2,
	R2_Item_10,
	SpeedUp,
	Sheild,
	None
}


public class KoreaItemResourceLoader : MonoBehaviour
{
	public static KoreaItemResourceLoader Instance;

	Sprite[] mResources;
	string mResourcePath = "KoreaItemSprite/R2_Item_테두리강조/";

	public Sprite GetSprite(eItemType eType)
	{
		return mResources[(int)eType];
	}
	void Awake()
	{
		Instance = this;
		LoadParticleResources();

	}


	void LoadParticleResources()
	{
		string[] particleResourcesNames = Enum.GetNames(typeof(eItemType));
		mResources = new Sprite[particleResourcesNames.Length];

		for (int i = 0; i < particleResourcesNames.Length-1; i++)
		{
			string path = mResourcePath + particleResourcesNames[i];
			mResources[i] = Resources.Load(path, typeof(Sprite)) as Sprite;

			Debug.Assert(mResources[i] != null, "아이템 리소스를 찾을 수 없음" + particleResourcesNames[i]);
		}
	}

}
