using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public enum R3MissionType
{
	CupCake, Generator, Battery, GiftBox
}

public class ChristmasJob : MonoBehaviour
{
	[SerializeField] GameObject waitObject;
	[SerializeField] GameObject clearObject;
	[SerializeField] int missionID;
	[HideInInspector] WorkData m_workData;
	MissionSound missionSound = null;
	float m_currentGage = 0;
	float m_delay = 0;
	float m_maxGage = 5f;

	public int networkID = -1;

	public float coolCount = 0;


	MissionState m_missionState = MissionState.Disable;

	public MissionState missionState
	{
		get { return m_missionState; }
	}

	public WorkData data
	{
		get { return m_workData; }
	}

	[SerializeField] SkeletonAnimation ani;
	[SerializeField] R3MissionType missionType;

	public void SetMissionActive()
	{
		m_missionState = MissionState.Active;
		waitObject.SetActive(true);
		clearObject.SetActive(false);

		if (ani != null)
			ChangeAnim();
	}

	public void SetStateFromRemote(MissionState state)
	{

		m_missionState = state;

		if(state == MissionState.Claer)
		{
			waitObject.SetActive(false);
			clearObject.SetActive(true);
		}

		if(state == MissionState.Active)
		{
			waitObject.SetActive(true);
			clearObject.SetActive(false);
		}
		if (ani != null)
			ChangeAnim();
	}

	void ChangeAnim()
    {
		string anim = "";
		switch (missionType)
		{
			case R3MissionType.CupCake:
				anim = (m_missionState == MissionState.Active)? "R3_ReceivingGoods_error" : "R3_ReceivingGoods";
				break;

			case R3MissionType.Generator:
					anim = (m_missionState == MissionState.Active) ? "":"";
				break;
		}

		if(anim !="")
			ani.state.SetAnimation(0, anim, true);
	}

	void Start()
	{
		missionSound = GetComponent<MissionSound>();
		clearObject.SetActive(false);
		m_workData = GameDataLoader.instance.GetWork(missionID);
		m_missionState = MissionState.Active;
		GameManager.playerController.spaceHoldEvent += MissionEvent;
		//GameManager.playerController.spacebarEvent += MissionSoundEvent;
		//GameManager.playerController.spaceUpEvent += MissionSoundEndEvent;
		ChristmasGameManager.Instance.AddMission(this);
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.G))
		{

			MessageUIManager.instance.CreateMessageFromWorldPosition(transform.position, "미션완료");
		}
		m_delay -= Time.deltaTime;
		if (m_currentGage > 0 && m_delay <= 0)
		{
			m_currentGage = 0;
			ChristmasGameManager.Instance.missionUpdateEvent?.Invoke(0, m_workData);
			GameManager.playerController.SetJobPlay(false, data);
			if (m_missionState == MissionState.Play) // missionCancle
			{
				if(NetworkConnect.instance)
					NetworkConnect.instance.SendMissionStart(MissionState.Active, networkID);
				m_missionState = MissionState.Active;
			}
		}
		if(m_missionState == MissionState.Claer)
		{
			coolCount += Time.deltaTime;

			if (coolCount > 10)
			{
				coolCount = 0;
				SetMissionActive();
			}
		}
	}
	//void MissionSoundEvent()
 //   {
	//	if (GameManager.instance.currentGame != eGameRound.Round3) return;
	//	if (isSpaceHold) return;
	//	isSpaceHold = true;
	//	if (GameManager.playerController.state == PlayerState.Hit) return;
	//	if (m_missionState != MissionState.Active && m_missionState != MissionState.Play) return;
	//	if (GameManager.instance.myTeam == eTeamType.Wolf) return;

	//	missionSound.PlayMissionSound(MissionState.Play);

	//}
	//void MissionSoundEndEvent()
 //   {
	//	if (GameManager.instance.currentGame != eGameRound.Round3) return;
	//	isSpaceHold = false;
	//	missionSound.PlayMissionSound(MissionState.Active);
	//}

	//bool isSpaceHold = false;

	void MissionEvent()
	{
		if (GameManager.playerController.state == PlayerState.Hit)
		{
			//MissionSoundEndEvent();
			return;
		}
		if (m_missionState != MissionState.Active && m_missionState != MissionState.Play) return;
		if (GameManager.instance.myTeam == eTeamType.Wolf) return;
		Vector3 vec = waitObject.transform.position - GameManager.playerController.transform.position;
		vec.y = 0;
		if (vec.magnitude < 1f)
		{
			if (m_missionState == MissionState.Active) // missionStart
			{
				if(NetworkConnect.instance)
					NetworkConnect.instance.SendMissionStart(MissionState.Play, networkID);
			}
			m_missionState = MissionState.Play;
			m_delay = 0.1f;
			m_currentGage += Time.deltaTime;

			GameManager.playerController.SetJobPlay(true, data);
			ChristmasGameManager.Instance.missionUpdateEvent?.Invoke(m_currentGage / m_maxGage, m_workData);
			if (m_currentGage > m_maxGage) //missionClear
			{
				if(NetworkConnect.instance)
					NetworkConnect.instance.SendMissionStart(MissionState.Claer, networkID);
				MissionClear();
			}
		}
	}

	public void MissionClear(bool isLocal = true)
	{
		if (isLocal)
		{
			MessageUIManager.instance.CreateMessageFromWorldPosition(transform.position + Vector3.up, "작업완료");
		}
		m_missionState = MissionState.Claer;
		//isSpaceHold = false;
		missionSound?.PlayMissionSound(m_missionState);
		ChristmasGameManager.Instance.Clear(this);
		waitObject.SetActive(false);
		clearObject.SetActive(true);
		if (ani != null)
			ChangeAnim();
	}

}
