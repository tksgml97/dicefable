﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworkController : MonoBehaviour {

	PlayerController localPlayer;

	float NetPosSendDelay = 0.5f;
	float NetPosSendCount = 0;

	public void SendPosition(Vector3 direction, float speed)
	{
		//Debug.Log("[MoveLog]" + direction + " , " + speed);
		if (NetworkConnect.instance != null)
			NetworkConnect.instance.SendPos(gameObject.transform.position, direction,  localPlayer.scaleObj.localScale.x, speed);
	}


	public void SendMyAni(int aniNumber)
	{
			if (NetworkConnect.instance != null)
			NetworkConnect.instance.SendAni((int)aniNumber);
	}

	// Use this for initialization
	void Start () {
		localPlayer = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		//NetPosSendCount += Time.deltaTime;
		/*
		if (NetPosSendCount >= NetPosSendDelay)
		{
			NetPosSendCount = 0;
			localPlayer.SendNetworkPosition();
		}
		*/
	}
}
