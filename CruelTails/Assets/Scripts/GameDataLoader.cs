using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class ScriptData
{
    public int m_scriptID { get; }
    public string m_scriptResource { get; }

    public ScriptData(int id, string resource)
	{
        m_scriptID = id;
        m_scriptResource = resource;
    }
}

public class WorkData
{
    public int m_id { get; }
    public string m_objName{ get; }
    public int m_time{ get; }
    public int m_value{ get; }
    public int m_amount{ get; }
    public int m_cool{ get; }
    public int m_ap { get; }
    public int m_alarm { get; }
    public int m_script { get; }

    public WorkData(int id, string name, int time, int value, int amount, int cool, int ap, int alarm, int script)
    {
        m_id = id;
        m_objName = name;
        m_time = time;
        m_value = value;
        m_amount = amount;
        m_cool = cool;
        m_ap = ap;
        m_alarm = alarm;
        m_script = script;

    }
}
public class MissionData
{
    public int m_missionID { get; }
    public string m_missionName { get; }
    public string m_missionNameClear { get; }
    public int m_missionTriggerID { get; }
    public int m_missionProgScriptID { get; }
    public int m_missionTextScriptID { get; }

    public MissionData(int id, string name, string nameClaer, int triggerID, int progScriptID, int textScriptID)
    {
        m_missionID = id;
        m_missionName = name;
        m_missionNameClear = nameClaer;
        m_missionTriggerID = triggerID;
        m_missionProgScriptID = progScriptID;
        m_missionTextScriptID = textScriptID;
    }
}

public class LoadingRule
{
    public int m_ruleID { get; }
    public string m_roundTItle { get; }
    public int m_roundNumber { get; }
    public int m_pageNumber { get; }
    public string m_fileName { get; }

    public string m_ruleScript { get; }
    
    public LoadingRule(int id, string tile, int roundNum, int pageNum,string ruleScript,  string fileName)
    {
        m_ruleID = id;
        m_roundTItle = tile;
        m_roundNumber = roundNum;
        m_pageNumber = pageNum;
        m_fileName = fileName;
        m_ruleScript = ruleScript;
    }
}


public class GameDataLoader
{
    static GameDataLoader m_instance;
    public static GameDataLoader instance
    {
        get {
            if (m_instance == null) m_instance = new GameDataLoader();
            return m_instance;
        }
    }

    static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
    static char[] TRIM_CHARS = { '\"' };
    Dictionary<int, MissionData> r1_mission = new Dictionary<int, MissionData>();
    Dictionary<int, ScriptData> script = new Dictionary<int, ScriptData>();
    Dictionary<int, LoadingRule> loadingRule = new Dictionary<int, LoadingRule>();
    Dictionary<int, WorkData> r3_work = new Dictionary<int, WorkData>();
    Dictionary<int, DiceData> r4_dice = new Dictionary<int, DiceData>();
    public r4_show_time_t r4_show_times = new r4_show_time_t();
    public r4_show_time_t r4_target_score = new r4_show_time_t();


    public (int, int)[] bind_to_show_times = new (int, int)[3];
    public (int, int)[] bind_to_target_scores = new (int, int)[3];


    public (float, float)[]TentacleAttack_Mid = new (float, float)[4];
    public (float, float)[]TentacleAttack_Big = new (float, float)[4];

    public (float, float)[] follwerData = new (float, float)[8];

    public DiceData GetDiceData(int num) {
        return r4_dice[num];
    }
    public int GetDiceListSize() {
        return r4_dice.Count;
    }

    public MissionData GetMission(int missionNum)
    {
        return r1_mission[missionNum];
    }
    public ScriptData GetScript(int scriptNum)
	{
        return script[scriptNum];
    }
    public LoadingRule GetLoadingRule(int ruleNum)
    {
        return loadingRule[ruleNum];
    }

    public WorkData GetWork(int workNum)
    {
        return r3_work[workNum];
    }
    GameDataLoader()
    {
        //
        List<Dictionary<string, object>> r1_missionCSV = Read("r1_mission");
        for (int i = 0; i < r1_missionCSV.Count; i++)
        {
            int id = (int)r1_missionCSV[i]["mission_id"];
            MissionData data = new MissionData(
                id,
                r1_missionCSV[i]["mission_obj_name"].ToString(),
                r1_missionCSV[i]["mission_obj_name_clear"].ToString(),
                (int)r1_missionCSV[i]["mission_active_id"],
                (int)r1_missionCSV[i]["mission_progressbar_script_id"],
                (int)r1_missionCSV[i]["mission_textUI_script_id"]
                );

            r1_mission.Add(id, data);
        }

        //
        List<Dictionary<string, object>> scriptCSV = Read("Script");
		for (int i = 1; i < scriptCSV.Count; i++)
		{
            int id = (int)scriptCSV[i]["script_id"];
            ScriptData data = new ScriptData(
                id,
                scriptCSV[i]["script_resource"].ToString()
                );

            script.Add(id, data);
        }


        try
        {
            
            List<Dictionary<string, object>> loadingRuleCSV = Read("Loading_Rule");
            for (int i = 0; i < loadingRuleCSV.Count; i++)
            {
                int id = (int)loadingRuleCSV[i]["Rule_Id"];
                LoadingRule data = new LoadingRule(
                    id,
                    loadingRuleCSV[i]["Round_Title"].ToString(),
                    (int)loadingRuleCSV[i]["Round_Number"],
                    (int)loadingRuleCSV[i]["Page_Number"],
                    loadingRuleCSV[i]["Rule_Script"].ToString(),
                    loadingRuleCSV[i]["File_Name"].ToString()
                    );

                loadingRule.Add(id, data);
            }
        }
        catch
        {
            Debug.Log("err");
        }

        

        // work
        List<Dictionary<string, object>> workCSV = Read("r3_work");
        for (int i = 0; i < workCSV.Count; i++)
        {
            int id = (int)workCSV[i]["work_id"];
            WorkData data = new WorkData(
                id,
                workCSV[i]["work_obj_name"].ToString(),
                (int)workCSV[i]["work_time"],
                (int)workCSV[i]["work_value"],
                (int)workCSV[i]["work_amount"],
                (int)workCSV[i]["work_cooltime"],
                (int)workCSV[i]["work_activePer"],
                (int)workCSV[i]["work_alarm"],
                (int)workCSV[i]["work_script_id"]
                );

            r3_work.Add(id, data);
        }


        // dice_data load
        List<Dictionary<string, object>> Dice = Read("r4_item");
        for (int i = 0; i < Dice.Count; i++) {
            r4_dice[i] = new DiceData();
            r4_dice[i].item_key = (int)Dice[i]["Item_Key"];
            r4_dice[i].name = (string)Dice[i]["Item_Name"];
            r4_dice[i].Min = (int)Dice[i]["Min"];
            r4_dice[i].Max = (int)Dice[i]["Max"];
            r4_dice[i].Gen = (int)Dice[i]["Gen"];
            r4_dice[i].ReGen = (int)Dice[i]["ReGen"];
            r4_dice[i].LowHigh = (int)Dice[i]["LowHigh"];
        }


        List<Dictionary<string, object>> R4ShowTime = Read("r4_MapData");

        int cnt = 3;
        for (int i = 0; i < 3; i++)
        {
            bind_to_target_scores[i] = ((int)R4ShowTime[i]["Value_Min"],
                                        (int)R4ShowTime[i]["Value_Max"]);
        }
        for (int i = 0; i < 3; i++)
        {
            bind_to_show_times[i] = ((int)R4ShowTime[(i + cnt)]["Value_Min"],
                                     (int)R4ShowTime[(i + cnt)]["Value_Max"]);
        }

        r4_show_times.___1 = bind_to_show_times[0];
        r4_show_times.__10 = bind_to_show_times[1];
        r4_show_times._100 = bind_to_show_times[2];

        r4_target_score.___1 = bind_to_target_scores[0];
        r4_target_score.__10 = bind_to_target_scores[1];
        r4_target_score._100 = bind_to_target_scores[2];

        float f_min, f_max;
        int v_min, v_max;

        // tentacle
        cnt = 8;
        for (int i = 0; i < 4; i++)
        {
            if (float.TryParse(R4ShowTime[(i + cnt)]["Value_Min"].ToString(), out f_min) == true)
                //float value
                TentacleAttack_Mid[i].Item1 = f_min;
            else if (Int32.TryParse(R4ShowTime[(i + cnt)]["Value_Min"].ToString(), out v_min) == true)
                //int value
                TentacleAttack_Mid[i].Item1 = v_min;
            if (float.TryParse(R4ShowTime[(i + cnt)]["Value_Max"].ToString(), out f_max) == true)
                //float value
                TentacleAttack_Mid[i].Item2 = f_max;
            else if (Int32.TryParse(R4ShowTime[(i + cnt)]["Value_Max"].ToString(), out v_max) == true)
                //int value
                TentacleAttack_Mid[i].Item2 = v_max;
            else Debug.Log("���� �߸���");
        }

        cnt = 12;
        for (int i = 0; i < 4; i++)
        {
            if (float.TryParse(R4ShowTime[(i + cnt)]["Value_Min"].ToString(), out f_min) == true)
                TentacleAttack_Big[i].Item1 = f_min;
            else if (Int32.TryParse(R4ShowTime[(i + cnt)]["Value_Min"].ToString(), out v_min) == true)
                TentacleAttack_Big[i].Item1 = v_min;

            if (float.TryParse(R4ShowTime[(i + cnt)]["Value_Max"].ToString(), out f_max) == true)
                TentacleAttack_Big[i].Item2 = f_max;
            else if (Int32.TryParse(R4ShowTime[(i + cnt)]["Value_Max"].ToString(), out v_max) == true)
                TentacleAttack_Big[i].Item2 = v_max;

            else Debug.Log("���� �߸���");
        }

        // follower monster
        cnt = 16;
        for (int i = 0; i < 8; i++)
        {
            if(float.TryParse(R4ShowTime[(i + cnt)]["Value_Min"].ToString(), out f_min) == true)
                //float value
                follwerData[i].Item1 = f_min;

            else if(Int32.TryParse(R4ShowTime[(i + cnt)]["Value_Min"].ToString(), out v_min) ==true)
                //int value
                follwerData[i].Item1 = v_min;

            if (float.TryParse(R4ShowTime[(i + cnt)]["Value_Max"].ToString(), out f_max) == true)
                //float value
                follwerData[i].Item2 = f_max;

            else if (Int32.TryParse(R4ShowTime[(i + cnt)]["Value_Max"].ToString(), out v_max) == true)
                //int value
                follwerData[i].Item2 = v_max;

            else Debug.Log("���� �߸���");
        }

    }

    public static List<Dictionary<string, object>> Read(string file)
    {
        var list = new List<Dictionary<string, object>>();
        TextAsset data = Resources.Load(file) as TextAsset;

        var lines = Regex.Split(data.text, LINE_SPLIT_RE);

        if (lines.Length <= 1) return list;

        var header = Regex.Split(lines[0], SPLIT_RE);
        for (var i = 1; i < lines.Length; i++)
        {

            var values = Regex.Split(lines[i], SPLIT_RE);
            if (values.Length == 0 || values[0] == "") continue;

            var entry = new Dictionary<string, object>();
            for (var j = 0; j < header.Length && j < values.Length; j++)
            {
                string value = values[j];
                value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "");
                object finalvalue = value;
                int n;
                float f;
                if (int.TryParse(value, out n))
                {
                    finalvalue = n;
                }
                else if (float.TryParse(value, out f))
                {
                    finalvalue = f;
                }
                entry[header[j]] = finalvalue;
            }
            list.Add(entry);
        }
        return list;
    }
}
