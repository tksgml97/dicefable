using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonActive : MonoBehaviour
{
    // One Select
    [SerializeField] UIButtonSelectManager manager; // 버튼 중 하나만 켜야하면 포함

    // Image
    [SerializeField] Image rend;
    [SerializeField] Sprite spr_selected, spr_unselected;

    // Text
    [SerializeField] Text text;
    [SerializeField] Color txt_c_selected, txt_c_unselected;

    // Awake
    public bool isSelected; // 초기에 선택되었는지

    private void Awake()
    {
        if (isSelected)
            ChangeButton(true);
    }


    public void ChangeButton(bool _input)
    {
        if(manager)
            manager.InitButtons();

        if (rend)
            rend.sprite =(_input)? spr_selected : spr_unselected;

        if (text)
        {
            text.color = (_input) ? txt_c_selected : txt_c_unselected;
        }

    }
}
