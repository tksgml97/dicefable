using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;


public class FadeController : Singleton<FadeController>
{
    public DOTweenAnimation fadeInAnimation;
    public UnityEvent fadeInEndEvent;
    public DOTweenAnimation fadeOutAnimation;
    public UnityEvent fadeOutEndEvent;
    public void FadeIn()
    {
        fadeInAnimation.DORestartById("FadeIn");
        fadeInAnimation.onComplete.AddListener(() => fadeInEndEvent?.Invoke());
    }

    public void FadeIn(UnityAction fadeEndAction = null) {
        fadeInAnimation.DORestartById("FadeIn");
        fadeInAnimation.onComplete.AddListener(fadeEndAction);
        fadeInAnimation.onComplete.AddListener(()=>fadeInEndEvent?.Invoke());
    }
    public void FadeOut()
    {
        fadeInAnimation.DORestartById("FadeOut");
        fadeOutAnimation.onComplete.AddListener(() => fadeOutEndEvent?.Invoke());
    }

    public void FadeOut(UnityAction fadeEndAction = null)
    {
        fadeInAnimation.DORestartById("FadeOut");
        fadeOutAnimation.onComplete.AddListener(fadeEndAction);
        fadeOutAnimation.onComplete.AddListener(() => fadeOutEndEvent?.Invoke());
    }

}
