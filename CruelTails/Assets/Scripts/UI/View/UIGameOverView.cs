using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameOverView : MonoBehaviour
{
    [SerializeField] GameObject view;
    [SerializeField] UIButtonActive button_gotoLobby;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetActiveGameOverView(bool state)
	{
        view.SetActive(state);
        button_gotoLobby.ChangeButton(false);
    }

    public void GotoGame()
	{
        view.SetActive(false);
    }
    public void GotoMain()
    {
        NetworkGameManager.instance.InitData();
        NetworkConnect.instance.ExitGame();
        Debug.Log("Go Main");
        StartCoroutine( SceneLoader.instance.LoadScene(eSceneType.Lobby, null));
        view.SetActive(false);
    }
}
