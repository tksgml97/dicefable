using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainHUD : MonoBehaviour
{
    [SerializeField]
    private UITimer timer;

    [SerializeField]
    private UIMinimapView uiMinimapView;

    public bool isViewMinimap = false;

    public void OnMinimap()
    {
        isViewMinimap = !isViewMinimap;
        if (isViewMinimap)
        {
            uiMinimapView.Open();
        }
        else
        {
            uiMinimapView.Close();
        }
    }

}
