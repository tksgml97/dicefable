using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using Spine.Unity;
using Spine;
using Sirenix.OdinInspector;

public class UIGameResultView : MonoBehaviour
{
    [SerializeField] GameObject bookScene;
    [SerializeField] GameObject bookCamera;
    [SerializeField] GameObject gameEndMeesageObj;
    [SerializeField] Animator gameEndMessageAni;
    [SerializeField] GameObject resultScene;
    [SerializeField] DoorScreenController doorScreenController;

    public RulletAnimation rulletAnimation;
    public PlayerSpineSkinUpdator[] matchPlayers;
    public KillSceneController killSceneControllers;
    public int losePlayer = 1;
    public float playAnimationTime = 5f;

    public UnityEvent endMessageCloseEvent;

    public UnityEvent startRulletEvent;
    public UnityEvent endRulletEvent;
    public UnityEvent<int> updateLosePlayerIndexEvent;

    [Button("TestProduction")]
    public void TESTPRODUCTION()
    {
        losePlayer = 1;
        updateLosePlayerIndexEvent?.Invoke(losePlayer);

        //rulletAnimation.UpdateRulletSlots();
        startRulletEvent?.Invoke();
        rulletAnimation.PlayRullet(losePlayer);
    }

    // Update is called once per frame
    public void GameEndEvnet(int losePlayerNumber)
    {
        if (ScrollMapManager.Instance && ChristmasGameManager.Instance.state == Round3State.secondGame)
        {
            return;
        }
        losePlayer = losePlayerNumber;
        //Debug.Log("[End] Game End Event" + losePlayer);
        updateLosePlayerIndexEvent?.Invoke(losePlayerNumber);
        PlayResult();
    }


    [Button("Play")]
    public void PlayResult()
    {
        gameEndMeesageObj?.SetActive(true);
        gameEndMessageAni?.SetTrigger("Play");
        StartCoroutine(WaitForEndMessage());
    }

    IEnumerator WaitForEndMessage()
    {
        yield return new WaitForSeconds(2f);
        endMessageCloseEvent?.Invoke();
    }

    public void ShowBook()
    {
        GameManager.instance.SetActiveUI(false);

        if (Tutorial.parentInstance)
            Tutorial.parentInstance.gameObject.SetActive(false);

        bookScene?.SetActive(true);
        bookCamera?.SetActive(true);
        SoundInstance.Instance.Round2SoundEnd();
        resultScene.SetActive(true);
        rulletAnimation.UpdateRulletSlots();

        UpdateMatchPlayers();
        //TEST
        //rulletAnimation.UpdateRulletSlots();
        if (GameManager.instance.currentGame == eGameRound.Round3)
        {
            ChristmasGameManager.Instance.EndFirstGame();
        }
    }

    public void PlayRullet()
    {
        // kill player ????
        losePlayer = GameManager.instance.GetKillPlayerIndex();
        //Debug.Log("[End Scene] Game End Event" + GameManager.instance.GetNameFromPlayerIndex(losePlayer));

        Debug.Log("Game End Event");
        startRulletEvent?.Invoke();
        rulletAnimation.PlayRullet(losePlayer);
    }


    IEnumerator WaitPlayKillScene(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        killSceneControllers.PlayKill(losePlayer);
    }
    public void rulletEndEvnet()
    {
        if (GameManager.instance.currentGame != eGameRound.Round3)
        {
            PlaySpineAnimation();
        }
        endRulletEvent?.Invoke();
    }

    public void UpdateMatchPlayers()
    {
        if (NetworkConnect.instance == null) return;
        for (int i = 0; i < matchPlayers.Length; i++)
        {
            PlayerSpineSkinData skin = GameManager.instance.GetSkinFromPlayerIndex(i);
            if (skin == null)
            {
                matchPlayers[i].gameObject.SetActive(false);
                continue;
            }

            matchPlayers[i].UpdateSpineSkin(skin);
        }

    }

    public void PlaySpineAnimation()
    {

        Dictionary<int, List<AnimState>> result_anim = new Dictionary<int, List<AnimState>>();

        result_anim[0] = new List<AnimState> { AnimState.PC_live_win, AnimState.PC_live_victory, AnimState.PC_live_lose };
        result_anim[1] = new List<AnimState> { AnimState.PC_calm_win, AnimState.PC_calm_victory, AnimState.PC_calm_lose };
        result_anim[2] = new List<AnimState> { AnimState.PC_timid_win, AnimState.PC_timid_victory, AnimState.PC_timid_lose };


        // TODO : ???? ???? ?????? ??????? ???
        // 
        // 
        for (int i = 0; i < matchPlayers.Length; i++)
        {
            List<AnimState> anim = null;
            if (NetworkConnect.instance.personalitys.Count > i)
            {
                anim = result_anim[NetworkConnect.instance.personalitys[i]];
            }

            if (i == losePlayer)
            {
                matchPlayers[i].PlayAnimation(anim is not null ? anim[2] : AnimState.PC_live_lose);
            }
            else
            {
                matchPlayers[i].PlayAnimation(anim is not null ? anim[0] : AnimState.PC_live_win);
            }
        }
        StartCoroutine(WaitPlayKillScene(playAnimationTime));
    }


    public void OnAniEvent_PlayerSurprise()
    {

        matchPlayers[0].PlayAnimation(AnimState.PC_calm_lose);
        for (int i = 1; i < matchPlayers.Length; i++)
        {
            matchPlayers[i].PlayAnimation(AnimState.PC_live_lose);
        }

    }

    public void OnAniEvent_SantaAttack()
    {

        matchPlayers[0].PlayAnimation(AnimState.PC_wolf_passout);
    }
    public void OnAniEvent_PlayerRun()
    {
        for (int i = 0; i < matchPlayers.Length; i++)
        {
            matchPlayers[i].PlayAnimation(AnimState.PC_F_L_run, true);
            Vector3 scale = matchPlayers[i].gameObject.transform.localScale;
            scale.x = -scale.x;
            matchPlayers[i].gameObject.transform.localScale = scale;
        }
    }

    public void OnAniEvent_RulletClose()
    {
        Debug.Log("RulletClose");
        ChristmasGameManager.Instance.InitSecondGame();
        ChristmasGameManager.Instance.ReadySecondGame();

        bookScene?.SetActive(false);
        resultScene.SetActive(false);
        killSceneControllers.Close();
    }

}
