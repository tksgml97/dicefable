using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;


public class UIMinimapView : UIBaseView
{
    [ReadOnly]
    [ShowInInspector]
    protected List<UIMinimapData> minimapEntityDataList = new List<UIMinimapData>();

    [SerializeField]
    protected Vector2 scaling;
    [SerializeField]
    protected Vector3 offset;
    [SerializeField]
    protected GameObject playerIconPrefab;
    [SerializeField]
    protected Transform playerIconParent;

    [SerializeField]
    private Sprite[] iconSpirtes;

    public override void Init(UIData uiData)
    {

    }

    void Update()
    {
        var entityCount = minimapEntityDataList.Count;
        for (int i = 0; i < entityCount; ++i)
        {
            UIMinimapData med = minimapEntityDataList[i];
            if (med.target == null)
            {
                minimapEntityDataList.RemoveAt(i);
                Destroy(med.minimapObject);
                --i;
                continue;
            }
            if (med.isInside == false)
            {
                med.UpdatePosition(scaling, offset);
            }
        }
    }

    public void AddPlayer(eTeamType type, Transform playerTransform, int id)
    {
        UIMinimapData med = new UIMinimapData();
        med.target = playerTransform;
        med.id = id;
        med.minimapObject = Instantiate(playerIconPrefab, playerIconParent);
        
        Sprite sprite = iconSpirtes[(int)type];

        med.ChangeSprite(sprite);
        minimapEntityDataList.Add(med);
    }

    public void SetInside(int playerId, bool isInside, Vector3 pos)
	{
        minimapEntityDataList[playerId].isInside = isInside;
        minimapEntityDataList[playerId].SetPosition(pos, scaling, offset);
        Debug.Log($"{playerId} teleport inside{isInside}");
    }

}
