using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILobbyView : UIBaseView
{
    public override void Init(UIData uiData)
    {

    }

    public void OnGameExit()
    {
        UIController.Instance.OpenPopup(new UIConfirmPopupData()
        {
            title = "Game Out"
        ,
            descrition = "GameOutAnswerDesc"
        ,
            confirmAction = () => { Debug.Log("Confirm Action"); }
        });
    }

}
