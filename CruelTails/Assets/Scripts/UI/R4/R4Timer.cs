using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class R4Timer : MonoBehaviour
{
    [SerializeField] Text ___1;
    [SerializeField] Text __10;
    [SerializeField] Text _100;

    [SerializeField] Text up___1;
    [SerializeField] Text up__10;
    [SerializeField] Text up_100;

    [SerializeField] ShowPopupUI showPopupUI;

    private void Start() {
        GameManager.instance.gameInitEvent += Init;

    }

    void Init() {
        ___1.text = "?";
        __10.text = "?";
        _100.text = "?";

        up___1.text = "";
        up__10.text = "";
        up_100.text = "";
    }

    int start_time = -1;

    bool show_popup = false;
    bool show_1 = true;
    bool show_10 = true;
    bool show_100 = true;
    int change_idx = 1;
    const int max_idx = 3;

    public void UpdateTimer(int time) {
        if (start_time == -1) start_time = time;

        int dt = (start_time - time);
        up___1.text = (start_time - R4GameManager.show_timer[0] - dt).ToString();
        up__10.text = (start_time - R4GameManager.show_timer[1] - dt).ToString();
        up_100.text = (start_time - R4GameManager.show_timer[2] - dt).ToString();

        if (show_1 && start_time - R4GameManager.show_timer[0] - dt <= 0) {
            ___1.text = (R4GameManager.target_score % 10).ToString();
            up___1.text = "";

            show_1 = false;
            show_popup = true; 
        }
        if (show_10 && start_time - R4GameManager.show_timer[1] - dt <= 0) {
            __10.text = (R4GameManager.target_score / 10 % 10).ToString();
            up__10.text = "";

            show_10 = false;
            show_popup = true;
        }
        if (show_100 && start_time - R4GameManager.show_timer[2] - dt <= 0) {
            _100.text = (R4GameManager.target_score / 100).ToString();
            up_100.text = "";

            show_100 = false;
            show_popup = true;
        }

        if (show_popup)
        {
            show_popup = false;
            if(showPopupUI != null) showPopupUI.ShowPopup(change_idx);
            TentacleManager.Instance.ChangeInterval(change_idx);
            if(change_idx < max_idx) change_idx++;
        }
            
    }
    

}
