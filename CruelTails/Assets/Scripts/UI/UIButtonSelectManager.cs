using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonSelectManager : MonoBehaviour
{
    [SerializeField] UIButtonActive[] buttons;

    public void InitButtons()
    {
        foreach(var b in buttons)
        {
            b.ChangeButton(false);
        }
    }
}
