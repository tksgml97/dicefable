using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UIMinimapData
{
    [SerializeField]
    public Transform target;
    public GameObject minimapObject;
    public int id;
    public bool isInside = false;

    public void ChangeSprite(Sprite sprite) {
        minimapObject.GetComponent<Image>().sprite = sprite;
    }

    public void UpdatePosition(Vector2 scaling, Vector3 offset) {
        if (target != null) {
            Vector3 pos = target.position;
            pos.y = pos.z;
            pos.z = 0;

            pos.x *= scaling.x;
            pos.y *= scaling.y;
            minimapObject.transform.localPosition = pos + offset;
        }
    }

    public void SetPosition(Vector3 pos, Vector2 scaling, Vector3 offset)
	{
        if (target != null)
        {
            pos.y = pos.z;
            pos.z = 0;
            pos.x *= scaling.x;
            pos.y *= scaling.y;
            minimapObject.transform.localPosition = pos + offset;
        }
    }

}
