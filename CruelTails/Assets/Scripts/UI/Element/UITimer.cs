using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour
{
    [SerializeField] Image timerImage;


    public UIBaseText timeTextC;
    public UIBaseText timeTextS;
    public UIBaseText timeTextM;

    public void Start()
	{
        GameManager.instance.gameInitEvent += Init;
    }

    void Init()
	{
        timeTextS.gameObject.SetActive(true);
        timeTextM.gameObject.SetActive(true);
        timeTextC.gameObject.SetActive(true);
    }

	public void UpdateTimer(int time) {
        if (time > 0)
        {
            //if (GameManager.instance.currentGame == eGameRound.Round1 && time > 30)
            //{
            //    timeTextS.SetText("??");

            //    timeTextM.SetText("??");
            //}
            //else
            //{
                int s = time % 60;
                int m = time / 60;

                if (s >= 10)
                {
                    timeTextS.SetText($"{s}");
                    timeTextM.SetText($"0{m}");
                    timeTextC.SetText(":");
                }
				else
				{
                    timeTextS.SetText($"0{s}");
                    timeTextM.SetText($"0{m}");
                    timeTextC.SetText(":");
                }
            //}
        }
        else
        {
            timeTextS.SetText("");
            timeTextM.SetText("");
            timeTextC.SetText("Game End");
        }
    }
}
