using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class RankType
{
    public RankType(int i, int r) { Name = GameManager.instance.GetNameFromPlayerIndex(i); number = i; rank = r; last_score = 0;  insentive_hp = 0; }

    public string Name;
    public int number;
    public int rank;
    public int last_score;
    public int insentive_hp;
};

public class RankManager : Singleton<RankManager>
{
    [SerializeField] int[] rank_Insetive_hps;
    [SerializeField] Text scoreText;
    public Text[] scoreTexts;
    public Text[] nameTexts;

    public List<RankType> rankUsers { get; set; } = new List<RankType>(); // ��ũǥ���� ���� ����
    int[] rankScores; // ��ũ�� ���ھ� (UI�� ǥ��)
    int[] userRanks; // ������ ��ũ����

    public GameObject ChristmasRankUI;
    [SerializeField] int playerMaxNum = 3;
    public void RankInit()
    {
        int rank = 0;
        if (GameManager.instance.currentGame < eGameRound.Round3)
            return;
        // isDie�� �����Ǵ� �� ���� �� �����Ƿ� ȣ��
        for (int i = 0; i < GameManager.instance.GetPlayerSize(); i++)
        {
            if (i == NetworkGameManager.memberNumber && GameManager.instance.isDie) // local die
                continue;
            if (GameManager.instance.GetPlayer(i) == null) continue; // exit client
            if (GameManager.instance.GetPlayer(i).GetComponent<RemotePlayer>().isDie) // client die
                continue;

            //Debug.Assert(false, "��ũ ����: " + i + " " + rank);
            if (rankUsers.Count >= playerMaxNum) continue;
            rankUsers.Add(new RankType(i, rank++));
            if(HPManager.instance)
                HPManager.instance.InitHpUser(i);

            if (GameManager.instance.currentGame == eGameRound.Round3) // ������Ʈ �� ���� Ȯ��
            {
                Debug.Log("User Update " + GameManager.instance.GetNameFromPlayerIndex(i));
            }
            
            

            if (GameManager.instance.currentGame == eGameRound.Round4 &&
                R4GameManager.instance.playerIdx.Count < 2)
            {
                R4GameManager.instance.playerIdx.Add(i);
            }
        }
        if(GameManager.instance.currentGame == eGameRound.Round4 && NetworkConnect.instance)
        {
            if (R4GameManager.instance.monsterManager != null)
            {
                R4GameManager.instance.monsterManager.playerIdx[0] = rankUsers[0].number;
                R4GameManager.instance.monsterManager.playerIdx[1] = rankUsers[1].number;

                Debug.Log("User Update " + GameManager.instance.GetNameFromPlayerIndex(rankUsers[0].number) + " "
                          + GameManager.instance.GetNameFromPlayerIndex(rankUsers[1].number));

            
                R4GameManager.instance.monsterManager.Init();
            }
        }
        GameManager.instance.playerStateUpdateEvent += UpdatePlayerScore;
        UpdatePlayerScore();
    }

    void ChangeRank(int i, int j)
    {
        int temp = rankUsers[j].rank;
        rankUsers[j].rank = rankUsers[i].rank;
        rankUsers[i].rank = temp;
    }

    void CalcRank()
    {
        
        for(int i = 0; i < rankUsers.Count; i++)
        {
            for(int j = 0; j < rankUsers.Count; j++)
            {
                if (i == j) continue;
                if (GameManager.instance.users[rankUsers[i].number].score > GameManager.instance.users[rankUsers[j].number].score
                    && rankUsers[i].rank > rankUsers[j].rank)
                    ChangeRank(i, j);
            }
        }
    }

    public void UpdatePlayerScore()
    {
        CalcRank();
        for (int i = 0; i < rankUsers.Count; i++)
        {
            
            //scoreTexts[rankUsers[i].rank].text = rankUsers[i].Name + ": " + GameManager.instance.users[rankUsers[i].number].score.ToString();
            scoreTexts[rankUsers[i].rank].text = GameManager.instance.users[rankUsers[i].number].score.ToString();
            if (nameTexts[rankUsers[i].rank] != null) nameTexts[rankUsers[i].rank].text = rankUsers[i].Name;

        }
        //StringBuilder sb = new StringBuilder();

        // legacy
        //for (int i = 0; i < GameManager.instance.GetPlayerSize(); i++)
        //{
        //    string name = GameManager.instance.GetNameFromPlayerIndex(i);

        //    if (name == "\0") continue;
        //    sb.Append(name);
        //    sb.Append(" : ");
        //    sb.AppendLine(GameManager.instance.users[i].score.ToString());
        //}
        //scoreText.text = sb.ToString();

    }

    
    public void setHP() // ������ ���� HP ���ϱ�
    {
        //Debug.Log(rankUsers.Count);
        for (int i = 0; i < rankUsers.Count; i++)
        {
            rankUsers[i].last_score = GameManager.instance.users[rankUsers[i].number].score;
            Debug.Log("����: " + rankUsers[i].last_score);
        }

        if (rankUsers.Count == 3 && 
            rankUsers[0].last_score == rankUsers[1].last_score &&
            rankUsers[1].last_score == rankUsers[2].last_score)
        {
            Debug.Log("1�� 3��");
            for (int i = 0; i < rankUsers.Count; i++)
            {
                rankUsers[i].rank = 0;
            }

        }
        else
        {
            for (int i = 0; i < rankUsers.Count; i++)
            {
                for (int j = 0; j < rankUsers.Count; j++)
                {
                    if (i == j) continue;
                    if (rankUsers[i].last_score == rankUsers[j].last_score)
                    {
                        Debug.Log("���� ��� 2��");
                        if (rankUsers[i].rank > rankUsers[j].rank)
                            rankUsers[i].rank = rankUsers[j].rank;
                        else if (rankUsers[i].rank < rankUsers[j].rank)
                            rankUsers[j].rank = rankUsers[i].rank;
                    }
                }
            }
        }
        for(int i = 0; i < rankUsers.Count; i++) // ��ũ�� ���� ü�� �߰� ���� ����
        {
            rankUsers[i].insentive_hp = rank_Insetive_hps[rankUsers[i].rank];
        }
    }
}
