using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonExit : MonoBehaviour
{
    public void OnClick_ApplicationQuit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
