using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum Round3State { firstGame, secondGame }
public class ChristmasGameManager : Singleton<ChristmasGameManager>
{
    public GameObject[] firstMapUIs = null;
    public CameraController3D mainCam;
    public GameObject missionUI;
    public GameObject firstGame;
    public GameObject seconGame;
    int idCount = 0;
    Dictionary<int, ChristmasJob> missions = new Dictionary<int, ChristmasJob>();

    public UnityEvent<int> workPlayEvent;
    public UnityEvent<float, WorkData> missionUpdateEvent;

    public Round3State state = Round3State.firstGame;

    bool isAllClear = false;
    protected override void Awake()
    {
        IsDontDestroy = false;
        base.Awake();
    }

    private void Start()
    {
    }

    private void Update()
    {
    }

    public void MissionClear(int missionID, int memberNum)
	{
        missions[missionID].MissionClear();
    }

    public void SetMissionState(MissionState state,int missionID, int memberNumber)
	{
        if(memberNumber == NetworkGameManager.memberNumber) return;

		switch (state)
		{
            case MissionState.Active:
                missions[missionID].SetStateFromRemote(MissionState.Active);
                break;
            case MissionState.Play:
                missions[missionID].SetStateFromRemote(MissionState.RemotePlay);
                break;
            case MissionState.Claer:
                missions[missionID].MissionClear();
                break;

        }

        int missionPlayCount = 0;

        foreach (var mission in missions)
		{
            ChristmasJob job = mission.Value;
            if(job.missionState == MissionState.RemotePlay)
			{
                missionPlayCount++;
            }
		}

        workPlayEvent?.Invoke(missionPlayCount);

    }

    public void AddMission(ChristmasJob mission)
    {
        int netID = idCount++;
        mission.networkID = netID;
        missions.Add(netID, mission);
    }

    public void Clear(ChristmasJob mission)
    {

    }


    public void EndFirstGame()
	{
        foreach(var f in firstMapUIs)
        {
            f.SetActive(false);// ui ��Ȱ��ȭ
        }
        //purposeUI.SetActive(false); 

        missionUI.SetActive(false);
        firstGame.SetActive(false);
        GameManager.instance.isStart = false;

        RankManager.Instance.setHP();
    }

    public void InitSecondGame()
    {
        Debug.Log("SecondGameInit");
        state = Round3State.firstGame;
        seconGame.SetActive(true);
        GameManager.playerController.InitState();
        GameManager.playerController.transform.position = new Vector3(0, 0, 0);
        GameManager.instance.SetActiveUI(true);
        //GameObject centor = new GameObject("centor");
        //centor.transform.position = Vector3.zero;
        //mainCam.target = centor.transform;
        state = Round3State.secondGame;

        //StartSecondGame();
    }

    public void ReadySecondGame()
    {
        NetworkConnect.instance.SendReady();
    }

    public void StartSecondGame()
    {
        HPManager.instance.InitMaxHP();
        ScrollMapManager.Instance.ScrollMapStart();
        //state = Round3State.secondGame;
        Debug.Log("SecondGameStart");
        GameManager.instance.isStart = true;
    }
    public void EndSecondGame()
    {
        ScrollMapManager.Instance.ScrollMapEnd();
    }


}
