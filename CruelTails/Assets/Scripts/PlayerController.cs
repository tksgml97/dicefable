using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public enum PlayerState
{
    Idle, Attack, Hit, GameEnd, Mission
}

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject nameUI;
    [SerializeField] Text nameText;
    [SerializeField] AkAmbient playerAkAmbient;
    [SerializeField] ParticleSystem moveDust;
    public Transform scaleObj;
    bool DebugAIMode = false;//true;
    float itemDropVelocity = 1.5f;

    [HideInInspector] public float currentCooldown = 0;
    public float cooldown = 1;
    public WolfDirectionArrow wolfArrow;
    public PlayerNetworkController networkController;
    public PlayerItemController playerItemController;
    private CharacterController rig;
    private PlayerAnimationController animationController;
    public PlayerMoveSound moveSound;
    float h, v;
    float speed;


    string playerName;


    bool isMoveBack;
    bool isAttack = false;

    float currentAttackTime = 0;
    float currentHitTime = 0;
    bool wolfSturn = false;


    [HideInInspector] public AnimState _AnimState;
    public PlayerState state;
    [HideInInspector] public eItemType currentItem = eItemType.None;

    [Header("Speed")]
    public float eventSpeed = 1;
    public float normalSpeed;
    public float runSpeed;
    public float wolfNormalSpeed;
    public float wolfRunSpeed;
    float hungryWolfSpeed = 1;
    public void SetHungryWolfSpeed(float n)
	{
        hungryWolfSpeed = n;
	}

    public void SetMissionPlay(bool s, MissionData mission)
	{
        if(s)
		{

            if (state == PlayerState.Hit || _AnimState == AnimState.PC_wolf_rotation || _AnimState == AnimState.PC_wolf_passout) return;
            state = PlayerState.Mission;
            SetZeroVelocity();


			switch (mission.m_missionID)
			{
                case 6001:
                    _AnimState = AnimState.r1_mission_r1_mission_cleaning;
                    break;

                case 6002:
                    _AnimState = AnimState.r1_mission_r1_mission_fire1;
                    break;

                case 6003:
                    _AnimState = AnimState.r1_mission_r1_mission_pickflower;
                    break;

                case 6004:
                    _AnimState = AnimState.r1_mission_r1_mission_flowerToSkel2;
                    break;

                case 6005:
                    _AnimState = AnimState.r1_mission_r1_mission_restaurantToTable1;
                    break;

                case 6006:
                    _AnimState = AnimState.r1_mission_r1_mission_restaurantToTable2;
                    break;
                case 6007:
                    _AnimState = AnimState.r1_mission_r1_mission_cleaning;
                    break;
            }



        }
		else
		{
            state = PlayerState.Idle;
        }
	}

    public void SetJobPlay(bool s, WorkData mission)
    {
        if (s)
        {
            state = PlayerState.Mission;
            SetZeroVelocity();


            switch (mission.m_id)
            {
                case 7001:
                    _AnimState = AnimState.round3_PC_work_hammering;
                    break;

                case 7002:
                    _AnimState = AnimState.round3_PC_work_packing;
                    break;

                case 7003:
                    _AnimState = AnimState.round3_PC_work_hammering_welding;
                    break;
                case 7004:
                    _AnimState = AnimState.round3_PC_work_hammering;
                    break;
                default:
                    _AnimState = AnimState.round3_PC_work_hammering;
                    break;
            }
        }
        else
        {
            if (state == PlayerState.Mission)
            {
                state = PlayerState.Idle;
            }
        }
    }

    public void SetName(string _name)
    {
        nameUI.SetActive(true);
        playerName = _name;
        Debug.Log(playerName);
        nameText.GetComponent<Text>().text = _name;
    }
    public void PlayGameEndMotion(bool isWin)
	{
        state = PlayerState.GameEnd;
        // 3라운드 후반부
        if (GameManager.instance.currentGame == eGameRound.Round3 && ChristmasGameManager.Instance.state == Round3State.secondGame)
            return;
        _AnimState = AnimState.PC_B_L_idle;
    }

    public void SetSkin(PlayerSpineSkinData skin)
	{
        animationController.SetSkin(skin);
    }
    public void SetColor(Color hairColor, Color headColor)
    {
        animationController.SetColor(hairColor, headColor);
    }

    public void SetTeam(int team, bool isInit = false)
	{
        #region tutorial_video_play

        if (GameManager.instance.currentGame == eGameRound.Round1)
        {

            if (isInit)
            {
                Round1_Tutorial.instance.SetRole((eTeamType)team);
            }
            else
            {
                Round1_Tutorial.instance.ChangeRole((eTeamType)team);
            }

        }
		#endregion


		animationController.SetTeam(team);
        nameText.color = GameManager.instance.GetTeamColor((eTeamType)team);
	}

    public void WolfFight()
	{
        SetZeroVelocity();
        AnimState playKnockAni = AnimState.PC_wolf_rotation;


        var hitAni = SpineAniLoader.instance.GetAni(playKnockAni);
        currentHitTime = hitAni.Animation.Duration;
        Debug.Log("Hit" + currentHitTime);
        _AnimState = playKnockAni;
        state = PlayerState.Hit;
    }
    public void Hit(Vector3 direction)
    {
        if (state == PlayerState.GameEnd) return;
        if (state == PlayerState.Hit) return;
        SetZeroVelocity();
        AnimState playKnockAni = AnimState.PC_F_pushed3;


        CameraController3D.instance.shakeTIme += 0.35f;
        Vector3 referenceForward = Vector3.up;
        Vector3 referenceRight = Vector3.Cross(Vector3.up, referenceForward);
        Vector3 newDirection = direction;

        float angle = Vector3.Angle(newDirection, referenceForward);
        float sign = Mathf.Sign(Vector3.Dot(newDirection, referenceRight));
        float finalAngle = sign * angle;
        Debug.Log("hit angle" + finalAngle);

        scaleObj.localScale = new Vector3(direction.x > 0 ? 1 : -1, 1, 1);
        if (finalAngle < 0)
        {
            finalAngle = -finalAngle;
        }
        else
        {
        }

        if (finalAngle < 35)
        {
            playKnockAni = AnimState.PC_F_pushed1;
        }
        if (finalAngle >= 35 && finalAngle < 70)
        {
            playKnockAni = AnimState.PC_F_pushed2;
        }
        if (finalAngle >= 70 && finalAngle < 110)
        {
            playKnockAni = AnimState.PC_F_pushed3;
        }
        if (finalAngle >= 110 && finalAngle < 145)
        {
            playKnockAni = AnimState.PC_F_pushed4;
        }
        if (finalAngle >= 145)
        {
            playKnockAni = AnimState.PC_F_pushed5;
        }
        if (pushed)
            playKnockAni = AnimState.round3_PC_attacked;

        var hitAni = SpineAniLoader.instance.GetAni(playKnockAni);
        currentHitTime = hitAni.Animation.Duration;
        Debug.Log("Hit" + currentHitTime);
        _AnimState = playKnockAni;
        knockDirection = direction;
        state = PlayerState.Hit;
        knockbackTime = 0;
        animationController.hitColorTime = 0.2f;
        moveSound.PlayAtkedSound();
        //ParticleManager.instance.playEffect(transform.position + Vector3.up, Quaternion.identity, eParticleType.HitStar, true);
        DropItem();
        RemoveItem();

    }

    public void InitState()
	{
        state = PlayerState.Idle;
        _AnimState = AnimState.PC_B_L_idle;
    }

    private void Awake()
    {
        state = PlayerState.Idle;
        rig = GetComponent<CharacterController>();
        animationController = GetComponent<PlayerAnimationController>();

        //moveSound = GetComponent<PlayerMoveSound>();

            playerScrollMap = GetComponentInChildren<PlayerScrollMap>();
        if(playerScrollMap)    
            scrollSpeed = playerScrollMap.scrollMoveSpeed;
        
    }

	private void Start()
	{
        _AnimState = AnimState.PC_F_L_idle;

        if(NetworkConnect.instance == null)
		{
            SetName("Test");
		}
		else
        {
            SetName(NetworkConnect.myName);
        }

        if (GameManager.instance)
        {
            GameManager.instance.uiActiveEvent += SetActiveUI;
        }
        //moveSound = GetComponent<PlayerMoveSound>();

    }

    void SetActiveUI(bool state)
    {
        nameUI.SetActive(state);
    }

    Vector3 m_currentDirection = Vector3.zero;
    float m_velocity = 0;
    float m_prevVelocity = 0;
    Vector3 m_prevScale = Vector3.one;

    public void SetDebugAIMode()
	{
        DebugAIMode = !DebugAIMode;
    }

    public void SetZeroVelocity()
	{
        m_currentDirection = Vector3.zero;
        m_velocity = 0;
    }
    public delegate void spcaebarEvent();
    public spcaebarEvent spacebarEvent;
    public spcaebarEvent spaceHoldEvent;
    public spcaebarEvent spaceUpEvent;

    [HideInInspector] public bool canDrop = true;


    float packetTestCount = 0;

    public void CheckChangeState()
	{
#if PACKET_TEST
        if(PacketTest.Instance.toggle.isOn)
		{
            packetTestCount += Time.deltaTime * PacketTest.Instance.slider.value;
			while (packetTestCount > 1)
			{
                packetTestCount -= 1;
                SendNetworkPosition();
			}

        }
#endif
        if (m_prevDir != m_currentDirection || m_prevVelocity != m_velocity || m_prevScale != scaleObj.localScale)
        {
            m_prevDir = m_currentDirection;
            m_prevVelocity = m_velocity;
            m_prevScale = scaleObj.localScale;

            SendNetworkPosition();
        }
    }
    private void Update()
    {

        nameText.transform.position = Camera.main.WorldToScreenPoint(transform.position + Vector3.up * 2f);
        if (transform.position.y != 0)
        {
            rig.Move(Vector3.down * transform.position.y);
		}


        moveDust.enableEmission = animationController.currentState == AnimState.PC_B_L_run || animationController.currentState == AnimState.PC_F_L_run;



        if (state == PlayerState.GameEnd || GameManager.instance.isStart == false)
        {
            if(GameManager.instance.currentGame==eGameRound.Round3 &&
                ChristmasGameManager.Instance.state == Round3State.secondGame)
            {
                if (state == PlayerState.GameEnd && GameManager.instance.isStart == false)
                {
                    m_velocity = 0;
                    UpdateAnimation(); CheckChangeState();
                }
                return;
            }
            m_velocity = 0;
            UpdateAnimation(); CheckChangeState();
            return;
        }


        if (Input.GetKeyDown(KeyCode.P))
        {
            //SetDebugAIMode();

        }
        if(Input.GetKeyDown(KeyCode.L))
		{
            //NetworkConnect.instance.SendSpawnItem(Random.RandomRange(0,10), GameManager.playerController.transform.position + Vector3.up * 0.1f, true, Vector3.right * scaleObj.localScale.x * itemDropVelocity);
        }
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.L))
        {
            //NetworkConnect.instance.SendSpawnItem(Random.RandomRange(0, 10), GameManager.playerController.transform.position + Vector3.up * 0.1f, true, Vector3.right * scaleObj.localScale.x * itemDropVelocity);
        }

        if (Input.GetKeyDown(KeyCode.Space))
		{
            if(spacebarEvent != null)
                spacebarEvent();
            if (canDrop)
            {
                if (DropItem())
                {
                    RemoveItem();
                }
            }
            canDrop = true;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            if (spaceHoldEvent != null)
                spaceHoldEvent();
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            if (spaceUpEvent != null)
                spaceUpEvent();
        }

        currentCooldown -= Time.deltaTime;
        if (GameManager.instance.isDie == false)
        {
            UpdateKnockback();
            UpdateHit();
            UpdateAttack();
        }

        if (state == PlayerState.Idle)
        {
            InputMove();
		}
        CheckChangeState();

            rig.Move(m_currentDirection * m_velocity * Time.deltaTime);
        UpdateAnimation();
    }

    public void SendNetworkPosition()
	{

        networkController.SendPosition(m_currentDirection, m_velocity);
    }

    float knockbackTime = 0;
    Vector3 knockDirection;
    void UpdateKnockback()
	{
        if (state == PlayerState.Hit)
        {
            SetZeroVelocity();
            knockbackTime += Time.deltaTime;
            float n = GameManager.instance.knockbackCurve.Evaluate(knockbackTime);
            Vector3 deltaVector = knockDirection.normalized * n * Time.deltaTime * GameManager.instance.knockbackScale;
            rig.Move(deltaVector);
        } else
		{
            knockbackTime = 0;
		}
	}
    
    void PlayWolfSturn()
	{
        SetZeroVelocity();
        AnimState sturnAni = AnimState.PC_wolf_passout;


        var hitAni = SpineAniLoader.instance.GetAni(sturnAni);
        currentHitTime = hitAni.Animation.Duration;
        Debug.Log("Hit" + currentHitTime);
        _AnimState = sturnAni;
        state = PlayerState.Hit;
        wolfSturn = true;

        //ParticleManager.instance.playEffect(transform.position + Vector3.up, Quaternion.identity, eParticleType.HitStar, true);
    }
    public void PlayGetItemSound()
	{

        moveSound.PlayItemSound(true);
    }
    public void GetItem(int item)
	{
        if(GameManager.instance.currentGame == eGameRound.Round2)
		{
            Round2_Tutorial.instance.GetItem();
        }


        animationController.SetItem(item);
        currentItem = (eItemType)item;
    }


    public bool DropItem()
	{
        if (currentItem == eItemType.None) return false;


        Vector3 dropVel = Vector3.right * scaleObj.localScale.x * itemDropVelocity;
        Vector3 startPos = GameManager.playerController.transform.position + Vector3.up * 0.1f;

        if (NetworkItem.GetPositionItemDrop(startPos, dropVel) == startPos) return false;

        GetComponent<PlayerMoveSound>().PlayItemSound(false);
        NetworkConnect.instance. SendSpawnItem((int)currentItem, startPos, true, dropVel);

        if (GameManager.instance.currentGame == eGameRound.Round2)
        {

            Round2_Tutorial.instance.PutItem(false);
        }
        return true;
            
        
    }

    public void SetItem(int itemID)
	{
        animationController.SetItem(itemID);
        
    }

    public void RemoveItem()
	{
        NetworkConnect.instance.SetPlayerItem(-1);
		
    }

    void UpdateHit()
	{
        if (state != PlayerState.Hit) return;
        
        
        

        if (currentHitTime > 0)
        {
            currentHitTime -= Time.deltaTime;
        }
        else
        {
            if (state == PlayerState.Hit)
            {
                if(GameManager.instance.myTeam == eTeamType.Wolf && wolfSturn == false)
				{
                    wolfSturn = true;
                    PlayWolfSturn();
                    return;
				}
                state = PlayerState.Idle;
                wolfSturn = false;
            }
        }
    }

    public void Remove()
	{
        transform.position = new Vector3(-100, 0, 0);
        state = PlayerState.GameEnd;
        SendNetworkPosition();
	}
    [SerializeField] float wolfAttackVelocity = 2.5f;

    void UpdateAttack()
	{
        if(PlayerState.Attack == state)
		{
            m_velocity = animationController.attackCurve.Evaluate(animationController.currentAnimatonTime);
            if(GameManager.instance.myTeam == eTeamType.Wolf)
			{
                m_velocity = animationController.wolfAttackCurve.Evaluate(animationController.currentAnimatonTime)*(wolfRunSpeed * wolfAttackVelocity);
            }
        }

        bool attackInput = Input.GetMouseButtonDown(0) &&
        !EventSystem.current.IsPointerOverGameObject();

        if (attackInput && state == PlayerState.Idle && currentCooldown <= 0)
        {
            
            Debug.Log("Attack");
            isAttack = true;
            var attackAni = SpineAniLoader.instance.GetAni(AnimState.PC_wolf_attack_side);
            currentAttackTime = attackAni.Animation.Duration;
            Vector3 playerScreenPos = Camera.main.WorldToScreenPoint(transform.position);
            animationController.attackDirection = Input.mousePosition - playerScreenPos;
            animationController.attackDirection.z = animationController.attackDirection.y;
            animationController.attackDirection.y = 0;
            if (Input.GetKey(KeyCode.LeftControl))
            {
                Hit(animationController.attackDirection);
            }
            else
            {
                scaleObj.localScale = new Vector3(animationController.attackDirection.x > 0 ? -1 : 1, 1, 1);
                _AnimState = AnimState.PC_F_L_push;
                if(GameManager.instance.myTeam == eTeamType.Wolf)
				{

                    Vector3 referenceForward = Vector3.forward;
                    Vector3 referenceRight = Vector3.Cross(Vector3.forward, referenceForward);
                    Vector3 newDirection = animationController.attackDirection;

                    float angle = Vector3.Angle(newDirection, referenceForward);
                    float sign = Mathf.Sign(Vector3.Dot(newDirection, referenceRight));
                    float finalAngle = sign * angle;

                    if(finalAngle < 30)
					{

                        _AnimState = AnimState.PC_wolf_attack_top;
                    }
					else
					{

                        _AnimState = AnimState.PC_wolf_attack_side;
                    }
                    Debug.Log(finalAngle);

				}

                state = PlayerState.Attack;
                SetZeroVelocity();
                m_currentDirection = animationController.attackDirection.normalized;
                //velocity = animationController.attackCurve.Evaluate(animationController.currentAnimatonTime);
                m_velocity = animationController.attackCurve.Evaluate(0);
                if (GameManager.instance.myTeam == eTeamType.Wolf)
				{
                    m_velocity = wolfRunSpeed  + wolfAttackVelocity;

                }
            }


            currentCooldown = cooldown;
		}
		else if(Input.GetMouseButtonDown(0))
		{


            Debug.Log($"{Input.GetMouseButtonDown(0)}, { !EventSystem.current.IsPointerOverGameObject()}, {currentCooldown}");
        }

        if (currentAttackTime > 0)
        {
            currentAttackTime -= Time.deltaTime;
        }
        else
        {
            isAttack = false;
            if (state == PlayerState.Attack)
            {
                state = PlayerState.Idle;
            }
        }
    }
    Vector3 m_prevDir = Vector3.zero;

    float debugAiUpdateTime = 0;
    int debugH = 0; int debugV = 0;
    bool debugRun = false;


    //bool isMoveSound = true;
    void InputMove()
	{
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");
        
        bool isRun = !Input.GetKey(KeyCode.LeftShift);
        if (DebugAIMode)
		{
            debugAiUpdateTime -= Time.deltaTime;
            if(debugAiUpdateTime < 0)
			{
                debugAiUpdateTime = Random.Range(1, 3);
                debugH = Random.Range(-1, 2);
                debugV = Random.Range(-1, 2);
                debugRun = Random.Range(0, 2) == 0;

            }

            h = debugH;
            v = debugV;
            isRun = true;

        }

        //위아래 이동이 0일 때 이전 이동중이던 정보를 가져옴
        if (v != 0)
        {
            if (v > 0)
                isMoveBack = true;
            else
                isMoveBack = false;
        }

        if (h == 0f && v == 0f)
        {
            if (isMoveBack)
                _AnimState = AnimState.PC_B_L_idle;
            else
                _AnimState = AnimState.PC_F_L_idle;
        }
        else
        {
            //달리기
            if (isRun)
            {

                playerAkAmbient.enabled = true;
                speed = GameManager.instance.myTeam == eTeamType.Wolf ? wolfRunSpeed : runSpeed;
                if (isMoveBack)
                    _AnimState = AnimState.PC_B_L_run;
                else
                    _AnimState = AnimState.PC_F_L_run;
            }
            //걷기
            else
            {
                speed = GameManager.instance.myTeam == eTeamType.Wolf ? wolfNormalSpeed : normalSpeed;
                if (isMoveBack)
                    _AnimState = AnimState.PC_B_L_walk;
                else
                    _AnimState = AnimState.PC_F_L_walk;
            }

            if (h != 0f) scaleObj.localScale = new Vector3(h * (-1), 1, 1);

        }
        m_currentDirection = (new Vector3(h, 0, v)).normalized;

        speed *= GameManager.instance.myTeam == eTeamType.Wolf ? hungryWolfSpeed : 1;
        m_velocity = (h == 0 && v == 0) ? 0 : speed * eventSpeed;
        

        // [3라운드 후반부]
        if (ScrollMapManager.Instance != null && ScrollMapManager.Instance.is_start)
        {
            //if(pushed && _AnimState != AnimState.round3_PC_attacked)
            //{
                //_AnimState = AnimState.round3_PC_attacked;
            //}
            //else
            //{
                _AnimState = AnimState.PC_F_L_run;
            //}
            rig.Move(new Vector3(-1 * scrollSpeed, 0, m_currentDirection.z) * Time.deltaTime);
            scaleObj.localScale = new Vector3(-1, 1, 1);
            
        }
    }

    public bool pushed { get; set; } = false;
    public void Pushed(float time)
    {
        pushed = true;
        Invoke(nameof(PushEnd), time);
    }
    void PushEnd()
    {
        pushed = false;
    }

    public float scrollSpeed { get; set; } = 0;
    [SerializeField] PlayerScrollMap playerScrollMap = null;
    void UpdateAnimation()
	{
        //애니메이션 적용
        bool changed = animationController.SetCurrentAnimation(_AnimState);
        if (changed)
		{
            networkController.SendMyAni((int)_AnimState);
		}

        if (changed && _AnimState == AnimState.PC_wolf_rotation)
        {
            if (GameManager.instance.myTeam == eTeamType.RedHat)
            {
                var ani = SpineAniLoader.instance.GetAni(AnimState.PC_wolf_passout);
                float hideTime = ani.Animation.Duration;
                animationController.SetHide(hideTime);
            }
		}

        
    }


}
