using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using Spine.Unity;
using Spine;

public enum AnimState
{
	PC_F_L_idle,
	PC_F_L_walk, PC_B_L_walk,
	PC_F_L_run, PC_B_L_run,
	PC_B_L_idle,
	PC_wolf_attack_side, PC_wolf_attack_top,
	PC_F_L_push,
	PC_F_pushed1, PC_F_pushed2, PC_F_pushed3, PC_F_pushed4, PC_F_pushed5,
	PC_live_win,
	PC_live_lose,
	PC_wolf_rotation,
	PC_wolf_passout,
	r1_mission_r1_mission_cleaning,
	r1_mission_r1_mission_fire1,
	r1_mission_r1_mission_flowerToSkel2,
	r1_mission_r1_mission_pickflower,
	r1_mission_r1_mission_restaurantToTable1,
	r1_mission_r1_mission_restaurantToTable2,
	PC_calm_lose,
	round3_PC_runaway,
	round3_PC_sidestepA,
	round3_PC_sidestepB,
	round3_PC_work_hammering,
	round3_PC_work_hammering_welding,
	round3_PC_work_packing,
    PC_calm_win,
    PC_timid_win,
    PC_live_victory,
    PC_calm_victory,
    PC_timid_victory,
    PC_timid_lose,
	round3_PC_attacked,
	round3_PC_exhaustion,
	round3_PC_exhausted,
	round3_PC_r3_blood,

}

public class SpineAniLoader : MonoBehaviour
{
	public static SpineAniLoader instance;

	AnimationReferenceAsset[] mAnimations;
	string mResourcePath = "SpineAnimations/";

	void Awake()
	{
		instance = this;
		LoadParticleResources();
	}

	public AnimationReferenceAsset GetAni(AnimState animState)
	{
		return mAnimations[(int)animState];
	}

	void LoadParticleResources()
	{
		string[] particleResourcesNames = Enum.GetNames(typeof(AnimState));
		mAnimations = new AnimationReferenceAsset[particleResourcesNames.Length];

		for (int i = 0; i < particleResourcesNames.Length; i++)
		{
			string path = mResourcePath + particleResourcesNames[i];
			mAnimations[i] = Resources.Load(path, typeof(AnimationReferenceAsset)) as AnimationReferenceAsset;

			//eParticleType 열거형의 멤버와 동일한 이름의 프리팹이 ParticlePrefab폴더에 있어야 합니다.
			Debug.Assert(mAnimations[i] != null, "파티클 리소스를 찾을 수 없습니다" + particleResourcesNames[i]);	
		}
	}
}
