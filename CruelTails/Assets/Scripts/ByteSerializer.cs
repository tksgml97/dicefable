﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using System.Diagnostics;

	public class ByteSerializer : IDisposable
	{
		List<byte> m_buff;
		byte[] m_readBuff;
		int m_readpos;
		bool m_buffUpdate = false;

		public ByteSerializer()
		{
			m_buff = new List<byte>();
			m_readpos = 0;
		}

		public int GetReadPos()
		{
			return m_readpos;
		}

		public byte[] ToArray()
		{
			return m_buff.ToArray();
		}

		public int Count()
		{
			return m_buff.Count;
		}

		public int Length()
		{
			return Count() - m_readpos;
		}

		public void Clear()
		{
			m_buff.Clear();
			m_readpos = 0;
		}

		public void AddReadPos(int pos)
		{
			Debug.Assert(m_buff.Count > m_readpos);
			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			if (m_buff.Count > m_readpos + pos)
			{
				m_readpos += pos;
			}
		}

		#region"Write Data"
		public void WriteVector3(Vector3 vec)
		{
			WriteFloat(vec.X);
			WriteFloat(vec.Y);
			WriteFloat(vec.Z);
		}

		public void WriteQuaternion(Quaternion rot)
		{
			WriteFloat(rot.X);
			WriteFloat(rot.Y);
			WriteFloat(rot.Z);
			WriteFloat(rot.W);
		}

		public void WriteByte(byte Inputs)
		{
			m_buff.Add(Inputs);
			m_buffUpdate = true;
		}

		public void WriteBytes(byte[] Input)
		{
			m_buff.AddRange(Input);
			m_buffUpdate = true;
		}

		public void WriteShort(short Input)
		{
			m_buff.AddRange(BitConverter.GetBytes(Input));
			m_buffUpdate = true;
		}

		public void WriteInteger(int Input)
		{
			m_buff.AddRange(BitConverter.GetBytes(Input));
			m_buffUpdate = true;
		}

		public void WriteLong(long Input)
		{
			m_buff.AddRange(BitConverter.GetBytes(Input));
			m_buffUpdate = true;
		}

		public void WriteFloat(float Input)
		{
			m_buff.AddRange(BitConverter.GetBytes(Input));
			m_buffUpdate = true;
		}

		public void WriteBool(bool Input)
		{
			m_buff.AddRange(BitConverter.GetBytes(Input));
			m_buffUpdate = true;
		}

		public void WriteString(string Input)
		{
			if (Input == null) Input = "";
			m_buff.AddRange(BitConverter.GetBytes(Input.Length * 2));
			if (Input.Length > 0)
			{
				m_buff.AddRange(Encoding.Unicode.GetBytes(Input));
			}
			m_buffUpdate = true;
		}
		#endregion

		#region "Read Data"

		public Vector3 ReadVector3()
		{
			return new Vector3(ReadFloat(), ReadFloat(), ReadFloat());
		}
		public Quaternion ReadQuaternion()
		{
			return new Quaternion(ReadFloat(), ReadFloat(), ReadFloat(), ReadFloat());
		}

		public string ReadString(bool Peek = true)
		{
			int len = ReadInteger(true);
			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			string ret = Encoding.Unicode.GetString(m_readBuff, m_readpos, len);
			if (Peek & m_buff.Count > m_readpos)
			{
				if (ret.Length > 0)
				{
					m_readpos += len;
				}
			}
			return ret;
		}

		public byte ReadByte(bool Peek = true)
		{
			Debug.Assert(m_buff.Count > m_readpos);


			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			byte ret = m_readBuff[m_readpos];
			if (Peek & m_buff.Count > m_readpos)
			{
				m_readpos += 1;
			}
			return ret;
		}

		public byte[] ReadBytes(int Length, bool Peek = true)
		{
			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			byte[] ret = m_buff.GetRange(m_readpos, Length).ToArray();
			if (Peek)
			{
				m_readpos += Length;
			}
			return ret;
		}

		public bool ReadBool(bool Peek = true)
		{
			Debug.Assert(m_buff.Count > m_readpos);

			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			bool ret = BitConverter.ToBoolean(m_readBuff, m_readpos);
			if (Peek & m_buff.Count > m_readpos)
			{
				m_readpos += 1;
			}
			return ret;

		}

		public float ReadFloat(bool Peek = true)
		{
			Debug.Assert(m_buff.Count > m_readpos);

			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			float ret = BitConverter.ToSingle(m_readBuff, m_readpos);
			if (Peek & m_buff.Count > m_readpos)
			{
				m_readpos += 4;
			}
			return ret;

		}
		public long ReadLong(bool Peek = true)
		{
			Debug.Assert(m_buff.Count > m_readpos);

			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			long ret = BitConverter.ToInt64(m_readBuff, m_readpos);
			if (Peek & m_buff.Count > m_readpos)
			{
				m_readpos += 4;
			}
			return ret;

		}
		public int ReadInteger(bool Peek = true)
		{
			Debug.Assert(m_buff.Count > m_readpos);

			if (m_buffUpdate)
			{
				m_readBuff = m_buff.ToArray();
				m_buffUpdate = false;
			}

			int ret = BitConverter.ToInt32(m_readBuff, m_readpos);
			if (Peek & m_buff.Count > m_readpos)
			{
				m_readpos += 4;
			}
			return ret;

		}
		#endregion



		#region dispose
		private bool disposedValue = false;
		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					m_buff.Clear();
				}

				m_readpos = 0;
				disposedValue = true;
			}

		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
