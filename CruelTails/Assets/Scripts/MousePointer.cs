using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MousePointer : MonoBehaviour
{
    [SerializeField] Text coolText;
    [SerializeField] Image cursor;
    [SerializeField] Sprite idle;

    [SerializeField] Sprite cool;
    public bool isPlayGameScene = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayGameScene)
        {
            if (GameManager.playerController.currentCooldown > 0)
            {
                coolText.text = Mathf.CeilToInt(GameManager.playerController.currentCooldown).ToString();
                cursor.sprite = cool;
            }
            else
            {
                coolText.text = "";
                cursor.sprite = idle;
            }
        }

		transform.position = Input.mousePosition;

        Cursor.visible = false;
    }
}
