using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface ISerializable
{
	void Serialize(ByteSerializer serializer);
	void Deserialize(ByteSerializer serializer);
}