using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionFollow : MonoBehaviour
{
    [SerializeField] Transform target;

    // Update is called once per frame
    void Start()
    {
        transform.SetParent(target);
        transform.localPosition = Vector3.zero;

    }
}
