using Sirenix.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

using Random = UnityEngine.Random;

public struct r4_show_time_t {
    public (int, int) ___1;
    public (int, int) __10;
    public (int, int) _100;
}

public struct r4_target_score_t {
    public int ___1;
    public int __10;
    public int _100;
}


public class R4GameManager : MonoBehaviour {
    public static R4GameManager instance;
    [SerializeField] Transform spawn_pr;
    [SerializeField] List<Transform> ranges_of_spawn = new List<Transform>();
    [SerializeField] List<GameObject> dice_prefabs = new List<GameObject>();
    public List<DiceAction> dice_obj_datas = new List<DiceAction>();

    List<float> dice_regen_cool_times = new List<float>();

    [SerializeField] float test_regen_speed = 1f;
    [SerializeField] Text[] score_texts = new Text[2];

    public static int[] show_timer = new int[3];
    public static int target_score = -1;
    public Stack<DiceAction>[] scores = new Stack<DiceAction>[2];

    /// <summary>
    /// LocalPlayer : 0, RemotePlayer : 1 
    /// </summary>
    /// <param name="player_idx"></param>
    /// <returns></returns>
    private int dice_sum_score(int player_idx)
    {
        return scores[player_idx].Sum(x => x.score);
    }

    public Stack<int> dice_stack = new Stack<int>();

    public List<int> playerIdx;
    [SerializeField] int playerMaxNum = 2;

    System.Random rnd = null;
    
    public static int seed = 1;
    public float diceYoffset = 3.0f;

    int game_time = 284;
    public MonsterManager monsterManager;
    [SerializeField] TentacleManager tentacleManager;

    public bool IsLocalPlay => NetworkConnect.instance == null;
    public bool IsGameStart;
    [SerializeField] GameObject followerPrefab;
    [SerializeField] Transform followerPrefabPrint;
    [SerializeField] Transform[] spawnRanges = new Transform[2];
    [SerializeField] List<Vector3> spawnPoints;
    
    List<R4MobFllower> mobs = new List<R4MobFllower>();
    float FRandomRange(float min, float max) {
        return rnd.Next((int)min, (int)max) + (float)rnd.NextDouble();
    }

    int IRandomRange(int min, int max) {
        return rnd.Next(min, max);
    }

    public void OnGameStart()
    {
        init();
        IsGameStart = true;
    }
    
    public void init()
    {
        rnd = new System.Random(seed);

        scores[0] = new Stack<DiceAction>();
        scores[1] = new Stack<DiceAction>();

        var show_time_datas = GameDataLoader.instance.r4_show_times;
        var target_score_datas = GameDataLoader.instance.r4_target_score;

        target_score =  IRandomRange(target_score_datas.___1.Item1, target_score_datas.___1.Item2);
        target_score += IRandomRange(target_score_datas.__10.Item1, target_score_datas.__10.Item2) * 10;
        target_score += IRandomRange(target_score_datas._100.Item1, target_score_datas._100.Item2) * 100;

        Debug.Log($"target_score : {target_score}");

        show_timer[0] = IRandomRange(show_time_datas.___1.Item1, show_time_datas.___1.Item2);
        show_timer[1] = IRandomRange(show_time_datas.__10.Item1, show_time_datas.__10.Item2);
        show_timer[2] = IRandomRange(show_time_datas._100.Item1, show_time_datas._100.Item2);


        for (int i = 0; i < GameDataLoader.instance.GetDiceListSize(); i++)
        {
            dice_regen_cool_times.Add(GameDataLoader.instance.GetDiceData(i).ReGen * test_regen_speed);
        }

        // seed �� ����Ͽ� ����ȭ �մϴ�.
        start_dice_spawn();
        StartSpawnMob();
        //monsterManager.Init(seed);
        tentacleManager.Init(seed);

        HitPlayerEvent.OnPlayerHit["R4"] = (HitPlayerData data) => {
            if (data.type == HitPlayerData.EHitType.player_hit) {
                NetworkConnect.instance.SendDropDice(data.pc.transform.position);
                if (scores[0].Count > 0) scores[0].Pop();
                UpdateScore();
            } else if (data.type == HitPlayerData.EHitType.player_atk) { 
                if (scores[1].Count > 0) scores[1].Pop();
                UpdateScore();
            } else if (data.type == HitPlayerData.EHitType.player_self) {
                const int dropDiceOfAI = 3;
                int hitPlayerNumber = NetworkGameManager.memberNumber == data.target_player_member_number ? 0 : 1;
                for (int i = 0; i < dropDiceOfAI; ++i) {
                    NetworkConnect.instance.SendDropDice(data.pc.transform.position);
                    if (scores[hitPlayerNumber].Count > 0) scores[hitPlayerNumber].Pop();
                }
                UpdateScore();
            }
        };
    }

    private void Awake() {
        Debug.Assert(instance == null);
        instance = this;

        if(IsLocalPlay) init();
    }

    public void SpawnDice(Vector3 pos, int DiceNumber, int DicePrefabNumber, int DiceScore) {
        var dice = Instantiate(dice_prefabs[DicePrefabNumber], spawn_pr).GetComponent<DiceAction>();
        dice.transform.position = pos;
        dice.score = DiceScore;
        dice.dice_number = dice_obj_datas.Count;
        dice_obj_datas.Add(dice);
    }

    public void start_dice_spawn() {
        var game_data = GameDataLoader.instance;
        int dice_prefab_size = game_data.GetDiceListSize();
        
        for (int i = 0; i < dice_prefab_size; i++) {
            var dice_data = game_data.GetDiceData(i);
            for (int j = 0; j < dice_data.Gen; j++) {
                var list_of_ranges = ranges_of_spawn[IRandomRange(0, ranges_of_spawn.Count)].GetComponentsInChildren<Transform>();
                var min = list_of_ranges.First(x => x.name == "min").position;
                var max = list_of_ranges.First(x => x.name == "max").position;

                var x = FRandomRange(min.x, max.x);
                var z = FRandomRange(max.z, min.z);

                var obj = Instantiate(dice_prefabs[i], spawn_pr).GetComponent<DiceAction>();
                obj.transform.position = new Vector3((float)x, 0, (float)z);
                obj.score = IRandomRange(dice_data.Min, dice_data.Max + 1);
                obj.dice_number = dice_obj_datas.Count;
                dice_obj_datas.Add(obj);
            }
        }
        
        // ��� �ֻ����� �̸� ���� �س���.
        for (int i = 0; i < dice_prefab_size; i++) {
            int re_spawn_cnt = (int)(game_time / dice_regen_cool_times[i]);
            var dice_data = GameDataLoader.instance.GetDiceData(i);
            for (int j = 0; j < re_spawn_cnt; j++) {
                var list_of_ranges = ranges_of_spawn[IRandomRange(0, ranges_of_spawn.Count)].GetComponentsInChildren<Transform>();
                var min = list_of_ranges.First(x => x.name == "min").position;
                var max = list_of_ranges.First(x => x.name == "max").position;
                var x = FRandomRange(min.x, max.x);
                var z = FRandomRange(max.z, min.z);
        
                var obj = Instantiate(dice_prefabs[i], spawn_pr).GetComponent<DiceAction>();
                obj.transform.position = new Vector3((float)x, 0, (float)z);
                obj.score = IRandomRange(dice_data.Min, dice_data.Max + 1);
                obj.dice_number = dice_obj_datas.Count;
                obj.state = DiceActionState.Disable;
                obj.gameObject.SetActive(false);
                obj.prefab_number = i;
                StartCoroutine(OnTimeOver((j + 1) * dice_regen_cool_times[i], () => {
                    obj.state = DiceActionState.Active;
                    obj.gameObject.SetActive(true);
                }));
                dice_obj_datas.Add(obj);
            }
        }

        Debug.Log("Dice Count : " + dice_obj_datas.Count);
    }

    public void StartSpawnMob()
    {
        Vector3 min = spawnRanges[0].position;
        Vector3 max = spawnRanges[1].position;
        for (int i = 0; i < 10; i++)
        {
            spawnPoints.Add(new Vector3(FRandomRange(min.x, max.x), 0f, FRandomRange(max.z, min.z)));
        }

        var spawn_times_data = new float[]
        {
            GameDataLoader.instance.follwerData[4].Item1,
            GameDataLoader.instance.follwerData[5].Item1,
            GameDataLoader.instance.follwerData[6].Item1,
            GameDataLoader.instance.follwerData[7].Item1
        };

        int spawn_time_idx = 0;
        for (float t = 0; t < game_time; t += spawn_times_data[spawn_time_idx])
        {
            if(game_time - show_timer[0] <= t)  spawn_time_idx = 1;
            if(game_time - show_timer[1] <= t)  spawn_time_idx = 2;
            if(game_time - show_timer[2] <= t)  spawn_time_idx = 3;

            var obj = Instantiate(followerPrefab, followerPrefabPrint).GetComponent<R4MobFllower>();
            mobs.Add(obj);
            obj.transform.position = spawnPoints[IRandomRange(0,10)];
            obj.gameObject.SetActive(false);
            obj.lookVector = (IRandomRange(0,2) > 0) ? Vector3.forward : Vector3.back;
            obj.GM = this;
            obj.mobNumber = mobs.Count - 1;
            StartCoroutine(OnTimeOver(t, () => {
                obj.gameObject.SetActive(true);
                obj.StartMob();
            }));
        }
    }
    
    IEnumerator OnTimeOver(float time, Action act) {
        yield return new WaitForSeconds(time);
        act();
    }

    public void UpdateScore()
    {
        if (IsLocalPlay) // local
        {
            score_texts[0].text = dice_sum_score(0).ToString();
            return;
        } else {
            for (int i = 0; i < 2; ++i) {
                var score_ = dice_sum_score(i);
                GameManager.instance.users[playerIdx[i]].score = score_;
                score_texts[i].text = score_.ToString();
            }
        }
        RankManager.Instance.UpdatePlayerScore();
    }

    public void TakeDice(int playerNum, int dice_num) {
        UpdateScore();
        bool is_local_player = NetworkGameManager.instance.GetPlayerObject(playerNum) == GameManager.playerController.gameObject;
        dice_obj_datas[dice_num].StartAct(NetworkGameManager.instance.GetPlayerObject(playerNum).transform, is_local_player);
    }

    public void SendMobTarget(int mobNumber, int targetPlayerMemberNumber, Vector3 mobPos, long timestamp)
    {
        if (IsLocalPlay) return;

        ByteBuffer sendBuffer = new ByteBuffer();
        sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
        sendBuffer.WriteInteger((int)ClientGamePackets.CMobTarget);
        sendBuffer.WriteInteger(mobNumber);
        sendBuffer.WriteInteger(targetPlayerMemberNumber);
        sendBuffer.WriteVector3(mobPos);
        sendBuffer.WriteLong(timestamp);
        NetworkConnect.instance.SendDataToServer(sendBuffer.ToArray());
    }

    public void SendAttackStart(int mobNumber, int targetPlayerMemberNumber, Vector3 mobPos, long timestamp)
    {
        if (IsLocalPlay) return;
        
        ByteBuffer sendBuffer = new ByteBuffer();
        sendBuffer.WriteInteger((int)ClientPackets.CPlayGameData);
        sendBuffer.WriteInteger((int)ClientGamePackets.CMobAttack);
        sendBuffer.WriteInteger(mobNumber);
        sendBuffer.WriteInteger(targetPlayerMemberNumber);
        sendBuffer.WriteVector3(mobPos);
        sendBuffer.WriteLong(timestamp);
        NetworkConnect.instance.SendDataToServer(sendBuffer.ToArray());
    }
    
    public void SetMobTarget(int mobNumber, int targetPlayerMemberNumber, Vector3 mobPos, long timestamp)
    {
        if (IsLocalPlay) return;
        
        try
        {
            mobs[mobNumber].SetTarget(targetPlayerMemberNumber);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
    
    public void SetMobAttack(int mobNumber, int targetPlayerMemberNumber, Vector3 mobPos, long timestamp)
    {
        if (IsLocalPlay) return;
        mobs[mobNumber].StartJumpTo(targetPlayerMemberNumber);
    }
    
    int myNum = 0;
    int otherNum = 0;
    public void DropDiceByAI(bool gimic, int _num, Vector3 hitPos)
    {
        int idx = 0;
        if (gimic)
        {
            if (_num == 3) // local
                idx = 0;
            else if (_num == 6) idx = 1;
            else return;
        }
        else
        {
            for (int i = 0; i < playerIdx.Count; i++)
            {
                if (GameManager.instance.GetPlayer(_num) == GameManager.playerController.gameObject)
                {
                    Debug.Log("�� ����");
                    idx = 0;
                }
                else { idx = 1; }
            }
        }
        if (IsLocalPlay)
        {
            for (int i = 0; i < 3; i++)
            {
                if (scores[idx].Count < 1) { Debug.Log(" ���� �������"); continue; }
                var poped_score = scores[idx].Pop();
            
                var dice_data = dice_obj_datas[dice_stack.Pop()];
                SpawnDice(hitPos + (Vector3)Random.insideUnitCircle, -1, dice_data.prefab_number, dice_data.score);
            }
        }
        UpdateScore();
    }

    public bool CalcTrueEnd()
    {
        if (playerIdx.Count < 2) return false; // 1�� �÷���

        int val1 = Mathf.Abs(dice_sum_score(0)  - target_score);
        int val2 = Mathf.Abs(dice_sum_score(1) - target_score);
        
        Debug.Log(GameManager.instance.GetNameFromPlayerIndex(playerIdx[0])+ " " + val1);
        Debug.Log(GameManager.instance.GetNameFromPlayerIndex(playerIdx[1])+ " " + val2);

        if(val1 == 0 && val2 == 0)
        {
            Debug.Log("�����ϴ�");
            return true;
        }
            
        if (val1 > val2) // ��ǥ�������� ������ �� ū ���� �й���
            Debug.Log(GameManager.instance.GetNameFromPlayerIndex(playerIdx[0]) + " �й�");
        else
            Debug.Log(GameManager.instance.GetNameFromPlayerIndex(playerIdx[1]) + " �й�");
        return false;
    }
}
