using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MessageUIManager : MonoBehaviour
{
    public static MessageUIManager instance;
	[SerializeField] Transform screenMessageView;
	[SerializeField] GameObject messagePrefab;
	private void Awake()
	{
        instance = this;
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }
	

    public void CreateMessageFromWorldPosition(Vector3 worldPosition, string message)
	{
		GameObject newGameObject = Instantiate(messagePrefab, transform);
		newGameObject.transform.position = worldPosition;
		//newGameObject.transform.position = Camera.main.WorldToScreenPoint(worldPosition);
		newGameObject.GetComponent<Text>().text = message;
	}

	public void CreateMessageFromScreenPosition(Vector3 screenPosition, string message)
	{
		GameObject newGameObject = Instantiate(messagePrefab, screenMessageView);
		newGameObject.transform.localScale = Vector3.one;
		newGameObject.transform.position = screenPosition;
		newGameObject.GetComponent<Text>().text = message;
	}
}
