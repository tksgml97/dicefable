using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum eParticleType
{
	Hit,
	HitStar,
	twinkletable
}

public class ParticleManager : MonoBehaviour
{
	public static ParticleManager instance;

	GameObject[] mParticlePrefabs;
	string mResourcePath = "ParticlePrefab/";

	public GameObject playEffect(Vector3 pos, Quaternion rot, eParticleType type, bool isNetworkSync = false)
	{
		GameObject prefab = mParticlePrefabs[(int)type];

		Vector3 p = pos;
		GameObject ret = Instantiate(prefab, p, rot);

		if (NetworkConnect.instance != null && isNetworkSync)
		{
			NetworkConnect.instance.SendParticlePlay(type, pos, rot);
		}
		return ret;

	}

	public void playEffect(Vector3 pos, Quaternion rot, eParticleType type, Transform targetTransform)
	{
		GameObject t = mParticlePrefabs[(int)type];
		Vector3 p = pos;
		Instantiate(t, p, rot, targetTransform);
	}

	void Awake()
	{
		instance = this;
		LoadParticleResources();
	}
	


	void LoadParticleResources()
	{
		Debug.Log("LoadParticles");
		string[] particleResourcesNames = Enum.GetNames(typeof(eParticleType));
		mParticlePrefabs = new GameObject[particleResourcesNames.Length];

		for (int i = 0; i < particleResourcesNames.Length; i++)
		{
			string path = mResourcePath + particleResourcesNames[i];
			mParticlePrefabs[i] = Resources.Load(path, typeof(GameObject)) as GameObject;

			//eParticleType 열거형의 멤버와 동일한 이름의 프리팹이 ParticlePrefab폴더에 있어야 합니다.
			Debug.Assert(mParticlePrefabs[i] != null, "파티클 리소스를 찾을 수 없습니다");
		}
	}
}
