using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
public class InsideController : MonoBehaviour
{
    [SerializeField] GameObject[] insideObjects;
    [SerializeField] int playerSetlayer;
    int tCount = 0;
    Dictionary<GameObject, int> rtCounts = new Dictionary<GameObject, int>();
    List<GameObject> targets = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        SetActiveObjects(false);
    }

    void SetActiveObjects(bool active)
	{
		for (int i = 0; i < insideObjects.Length; i++)
		{
            Vector3 pos = insideObjects[i].transform.position;
            // insideObjects[i].SetActive(active);
            if (active)
			{
                pos.z = 0;
            }
			else
			{
                pos.z = -1000;
            }
            insideObjects[i].transform.position = pos;
        }

    }

    int AddCount(GameObject obj, int n)
    {
        if(rtCounts.ContainsKey(obj) == false)
		{
            rtCounts.Add(obj, 0);
            targets.Add(obj);

        }
        rtCounts[obj] += n;
        return rtCounts[obj];
    }

    void UpdateRemotePlayerLayer(bool active)
	{
        
		for (int i = 0; i < targets.Count; i++)
		{
            if (targets[i] == null) continue;
            int setlayer = 1;
            if (rtCounts[targets[i]] > 0)
			{

                setlayer = active ? playerSetlayer : 1;
            } else
			{
                setlayer = 1;
            }
            SortingGroup sg = targets[i].GetComponent<SortingGroup>();
            if (sg)
            {
                sg.sortingOrder = setlayer;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            tCount++;
            collision.gameObject.GetComponent<SortingGroup>().sortingOrder = playerSetlayer;
            UpdateRemotePlayerLayer(true);
            SetActiveObjects(true);
        }
        if (collision.gameObject.layer == LayerMask.NameToLayer("RemotePlayer") || collision.gameObject.layer == LayerMask.NameToLayer("Item"))
        {
            AddCount(collision.gameObject, 1);
            if(tCount > 0)
			{
                collision.gameObject.GetComponent<SortingGroup>().sortingOrder = playerSetlayer;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            tCount--;


            if (tCount == 0)
            {
                collision.gameObject.GetComponent<SortingGroup>().sortingOrder = 1;
                UpdateRemotePlayerLayer(false);
                SetActiveObjects(false);
            }

        }
        if (collision.gameObject.layer == LayerMask.NameToLayer("RemotePlayer") || collision.gameObject.layer == LayerMask.NameToLayer("Item"))
        {
            int count = AddCount(collision.gameObject, -1);
            if (count == 0)
            {
               
                collision.gameObject.GetComponent<SortingGroup>().sortingOrder = 1;
            }

        }
    }
}
