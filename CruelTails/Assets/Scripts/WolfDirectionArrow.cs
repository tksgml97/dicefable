using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfDirectionArrow : MonoBehaviour
{
	struct Arrow
	{
        public Transform target;
        public GameObject arrowObj;
    }

    [SerializeField] GameObject arrwoPrefab;
    [SerializeField] Transform ui;

    List<Arrow> arrows = new List<Arrow>();

    float height = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        //AddTarget(GameManager.instance.transform);
    }

    public void AddTarget(Transform target)
	{
        Arrow newArrow;
        newArrow.arrowObj = Instantiate(arrwoPrefab, ui);
        newArrow.arrowObj.transform.localPosition = Vector3.zero;
        newArrow.target = target;
        arrows.Add(newArrow);
	}

    // Update is called once per frame
    void LateUpdate()
    {
        ui.gameObject.SetActive(GameManager.instance.myTeam == eTeamType.Wolf);
        ui.position = Camera.main.WorldToScreenPoint(GameManager.playerController.transform.position);
		for (int i = 0; i < arrows.Count; i++)
		{
            if(arrows[i].target == null)
			{
                Destroy(arrows[i].arrowObj);
                arrows.RemoveAt(i);
                break;
            }
            Vector3 p = GameManager.playerController.transform.position;
            p.y = p.z;
            p.z = 0;
            Vector3 t = arrows[i].target.position;
            //Vector3 t = Vector3.zero;
            t.y = t.z;
            t.z = 0;
            Vector3 direction = p - t;
            float distance = (direction).magnitude;

            float viewDistance = 2f;

            arrows[i].arrowObj.SetActive(distance > viewDistance);

            arrows[i].arrowObj.transform.rotation = Quaternion.LookRotation(Vector3.forward, (t - p));
			


        }
    }
}
