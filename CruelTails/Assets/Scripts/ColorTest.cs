using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Spine.Unity;
using Sirenix.OdinInspector;
using Spine;
public class ColorTest : MonoBehaviour
{
    [SerializeField] SkeletonMecanim skeletonMecanim;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        setcolor();
        //skeletonMecanim.skeleton.SetColor(Color.blue);
    }
    [Button("setcolor")]
    public void setcolor()
	{

        foreach (var item in skeletonMecanim.skeleton.Slots)
        {
            if (item.ToString().IndexOf("hair") != -1)
            {
                Debug.Log(item.ToString());
                item.SetColor(Color.blue);
            }
            if (item.ToString().IndexOf("PC_F_head") != -1)
            {
                Debug.Log(item.ToString());
                item.SetColor(Color.blue);
            }
        }
    }
}
