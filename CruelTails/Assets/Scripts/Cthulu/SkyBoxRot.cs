using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxRot : MonoBehaviour
{
    float degree;
    [SerializeField] float speed = 3f;

    void Start()
    {
        degree = 0;
    }

    void Update()
    {
        degree += Time.deltaTime * speed;
        if (degree >= 360)
            degree = 0;

        RenderSettings.skybox.SetFloat("_Rotation", degree);
    }
}
