﻿using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace Utils.Data
{
	public static class FileHandler
	{
		#region Saving

		public static bool TrySaveToFile(string filePath, string data)
		{
			return TrySaveToFile(new Uri(filePath), data);
		}

		public static bool TrySaveToFile(Uri fileUri, string data)
		{
			try
			{
				string fileDirectory = Path.GetDirectoryName(fileUri.OriginalString);
				if (!Directory.Exists(fileDirectory))
				{
					Directory.CreateDirectory(fileDirectory);
				}

				using (FileStream fs = new FileStream(fileUri.OriginalString, FileMode.Create))
				using (StreamWriter sw = new StreamWriter(fs))
				{
					sw.Write(data);
				}

				return true;
			}
			catch (Exception e)
			{
				Debug.LogWarning($"[FileHandler] {e}");
				return false;
			}
		}

		public static async Task<bool> TrySaveToFileAsync(Uri fileUri, string data)
		{
			try
			{
				string fileDirectory = Path.GetDirectoryName(fileUri.OriginalString);
				if (!Directory.Exists(fileDirectory))
				{
					Directory.CreateDirectory(fileDirectory);
				}

				using (FileStream fs = new FileStream(fileUri.OriginalString, FileMode.Create))
				using (StreamWriter sw = new StreamWriter(fs))
				{
					await sw.WriteAsync(data);
				}

				return true;
			}
			catch (Exception e)
			{
				Debug.LogWarning($"[FileHandler] {e}");
				return false;
			}
		}

		#endregion

		#region Loading

		public static bool TryLoadTextFromFile(string filePath, out string data)
		{
			return TryLoadTextFromFile(new Uri(filePath), out data);
		}

		public static bool TryLoadTextFromFile(Uri fileUri, out string data)
		{
			try
			{
				if (!File.Exists(fileUri.OriginalString))
				{
					Debug.LogWarning($"[FileHandler] There is no such file exist! file : {fileUri}");
					data = string.Empty;
					return false;
				}

				using (FileStream fs = new FileStream(fileUri.OriginalString, FileMode.Open))
				using (StreamReader sr = new StreamReader(fs))
				{
					data = sr.ReadToEnd();
				}

				return true;
			}
			catch (Exception e)
			{
				Debug.LogWarning($"[FileHandler] {e}");
				data = string.Empty;
				return false;
			}
		}

		public static async Task<string> TryLoadTextFromFileAsync(Uri fileUri)
		{
			try
			{
				if (!File.Exists(fileUri.OriginalString))
				{
					Debug.LogWarning($"[FileHandler] There is no such file exist! file : {fileUri}");
					return string.Empty;
				}

				using (FileStream fs = new FileStream(fileUri.OriginalString, FileMode.Open))
				using (StreamReader sr = new StreamReader(fs))
				{
					return await sr.ReadToEndAsync();
				}
			}
			catch (Exception e)
			{
				Debug.LogWarning($"[FileHandler] {e}");
				return string.Empty;
			}
		}

		#endregion
	}
}
