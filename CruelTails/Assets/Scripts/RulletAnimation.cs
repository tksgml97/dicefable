using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Spine.Unity;
using Spine;
using Sirenix.OdinInspector;
using UnityEngine.Events;


public class RulletAnimation : MonoBehaviour
{
    [SerializeField] RulletSlot[] rulletSlots;
    [SerializeField] Transform arrow;

    private const float slotDegree = 360f / 5f;

    public bool isPlay = false;

    public float currentSpinTime = 0f;
    public float spinTime = 4f;
    public float spinCount = 5f;

    public float totalSpinDegree;

    public AnimationCurve spinCurve;

    public int killTargetIndex = 1;

    public UIGameResultView uiGameResultView;

    // Update is called once per frame
    void Update()
    {
        if(!isPlay)
            return;


        currentSpinTime += Time.deltaTime;
        var lerpTime = spinCurve.Evaluate(currentSpinTime / spinTime);
        arrow.localRotation = Quaternion.Euler(0,0, Mathf.Lerp(0, totalSpinDegree, lerpTime));


        if(lerpTime >= 1f) {
            uiGameResultView.rulletEndEvnet();
            isPlay = false;
        }

    }

    public void UpdateRulletSlots()
	{
        for (int i = 0; i < rulletSlots.Length; i++)
        {
            //PlayerSpineSkinData skin = GameManager.instance.GetSkinFromPlayerIndex(i);
            NetworkPlayerInfo info = NetworkGameManager.instance.GetPlayerInfo(i);
            PlayerSpineSkinData skin = info.skinData;
            if (skin == null)
            {
                continue;
            }

            string username = " ";

            // username = GameManager.instance.GetNameFromPlayerIndex(i);

            username = info.playerName;
            bool isDie = info.isDIe;
            bool currentDie = false;
            if(GameManager.instance.currentKillPlayer == i)
			{
                isDie = false;
                currentDie = true;
            }

            rulletSlots[i].SetSlot(skin, username, isDie, currentDie);
        }
    }

    [Button("Play")]
    public void PlayRullet(int rulletTarget)
	{
        if(isPlay)
            return;

        killTargetIndex = rulletTarget;
        isPlay = true;

        totalSpinDegree = spinCount * -360f + slotDegree * killTargetIndex;

    }

}
