using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class BackGroundAni : MonoBehaviour
{
    [SerializeField] string aniName;
    SkeletonGraphic skeleton;
    private void Awake()
    {
        skeleton = GetComponent<SkeletonGraphic>();
        skeleton.AnimationState.SetAnimation(1, aniName, true);
    }
}
