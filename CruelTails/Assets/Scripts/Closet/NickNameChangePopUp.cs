using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NickNameChangePopUp : MonoBehaviour
{
    [SerializeField] NickNameInit nickNameInit;
    [SerializeField] InputField nameField;


    public void NameSave()
    {
        nickNameInit.playerNameCard.text = nameField.text;
        LobbyUIManager.Instance.savedName = nameField.text;
    }
    
    public void NameCancel()
    {
        nameField.text = LobbyUIManager.Instance.savedName;
    }
}
