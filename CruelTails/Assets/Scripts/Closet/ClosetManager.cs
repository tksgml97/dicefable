using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetManager : MonoBehaviour
{
    [SerializeField] PlayerCloset playerCloset;

    [SerializeField] GameObject basicPanel;
    GameObject curPanel, nextPanel;

    public GameObject closetUI;
    public GameObject cancelPopUp;

    private static ClosetManager instance;
    public static ClosetManager Instance { get { if (null != instance) return instance; return null; } }


    // 사용 예시: ClosetManager.Instance.getClosetSkin();
    public PlayerSpineSkinData getClosetSkin()
    {
        if (playerCloset.confirmedSkin != null)
            return playerCloset.confirmedSkin;
        else // 저장된 스킨이 없으면 기본 스킨
            return playerCloset.basicSkin;
    }

    private void Awake()
    {
        if (null == instance)
        {
            instance = this;
        }

        // 맨 처음 뜨는 창: 캐릭터 설정 창
        curPanel = basicPanel;
    }

    public void ResetPanel()
    {
        // 캐릭터 설정 창이 띄워지도록 리셋
        if (curPanel != basicPanel)
            ChangePanel(basicPanel);
    }

    public void ChangePanel(GameObject panel)
    {
        if(curPanel != panel)
        {
            Debug.Log("Down: " + curPanel);
            nextPanel = panel;
            curPanel.SetActive(false);
            nextPanel.SetActive(true);
            curPanel = panel;
            Debug.Log("On: " + nextPanel);
        }
        else
        {
            Debug.Log("same panel");
        }
    }

    public void ClosetOut()
    {
        LobbyAnimation.Instance.GoToState(LobbyAnimationState.Main);
    }

    public void ClosetIn()
    {
        LobbyAnimation.Instance.GoToState(LobbyAnimationState.Closet);
    }
}
