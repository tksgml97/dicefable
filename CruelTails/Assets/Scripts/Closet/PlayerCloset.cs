using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using Spine;

public class PlayerCloset : MonoBehaviour
{
    [SerializeField] SkeletonGraphic skeletonGraphic;
    [SerializeField] ColorPicker colorPicker;
    [SerializeField] SkinColorSelector skinColorSelector;
    [SerializeField] PlayerSpineSkinUpdator updator;

    public PlayerSpineSkinData basicSkin = null;
    public PlayerSpineSkinData confirmedSkin = null;

    public PlayerSpineSkinData currentSkin = null;
    public int i=0;

    private void Start()
    {
        SetBasicSkin();
        ResetSkin();
    }
    
    public void SetSkin(int i)
    {
        SpineSkinSlot s;
        
        if (i == 0) return;
        switch (i / 100)
        {
            case 1: s = SpineSkinSlot.eye; break;
            case 2: s = SpineSkinSlot.eyebrow; break;
            case 3: s = SpineSkinSlot.mouth; break;
            case 4: s = SpineSkinSlot.hair; break;
            case 5: s = SpineSkinSlot.top; break;
            case 6: s = SpineSkinSlot.bott; break;
            case 7: s = SpineSkinSlot.onepiece; break;
            case 8: s = SpineSkinSlot.hat; break;
            case 9: s = SpineSkinSlot.shoe; break;
            case 10: s = SpineSkinSlot.glassed; break;
            case 11: s = SpineSkinSlot.bag; break;
            case 12: s = SpineSkinSlot.tail; break;
            case 13: s = SpineSkinSlot.helmet; break;
            default: s = SpineSkinSlot.MAX; break;
        }
        SetParts(s, i % 100);
    }

    public void NoneSkin(int i)
    {
        switch(i)
        {
            case 7: currentSkin.slots[SpineSkinSlot.onepiece] = "";
                currentSkin.slots[SpineSkinSlot.top] = basicSkin.slots[SpineSkinSlot.top];
                currentSkin.slots[SpineSkinSlot.bott] = basicSkin.slots[SpineSkinSlot.bott];
                break;
            case 8: currentSkin.slots[SpineSkinSlot.hat] = "";
                currentSkin.slots[SpineSkinSlot.helmet] = "";
                currentSkin.slots[SpineSkinSlot.cliping] = "";
                break;
            case 10: currentSkin.slots[SpineSkinSlot.glassed] = ""; break;
            case 11: currentSkin.slots[SpineSkinSlot.bag] = ""; break;
            case 12: currentSkin.slots[SpineSkinSlot.tail] = ""; break;
            default: Debug.Log("제거할 수 없는 스킨"); break;
        }
        showSkin(currentSkin);
    }

    public void setHairColor()
    {
        currentSkin.hair = colorPicker.GetColor();
        showSkin(currentSkin);
    }
    public void setSkinColor()
    {
        currentSkin.skin = skinColorSelector.GetColor();
        updator.UpdateSpineSkin(currentSkin);
    }

    public void SetParts(SpineSkinSlot skinSlot, int i)
    {
        if (skinSlot == SpineSkinSlot.hat)
        {
            string s = SpineSkinManager.instance.GetSkin(SpineSkinSlot.cliping, 0);
            currentSkin.slots[SpineSkinSlot.cliping] = s;
        }
        if (skinSlot == SpineSkinSlot.helmet)
        {
            if(i < 2)
            {
                string s = SpineSkinManager.instance.GetSkin(SpineSkinSlot.cliping, 1);
                currentSkin.slots[SpineSkinSlot.cliping] = s;
            }
            else
            {
                currentSkin.slots[SpineSkinSlot.cliping] = "";
            }
                

        }
        
        string skin = SpineSkinManager.instance.GetSkin(skinSlot, i);

        OverLapSkin(skinSlot);

        if (currentSkin.slots[skinSlot] != skin) currentSkin.slots[skinSlot] = skin;
        showSkin(currentSkin);
    }

    // 중복 착용 불가 스킨
    public void OverLapSkin(SpineSkinSlot skinSlot)
    {
        switch(skinSlot)
        {
            case SpineSkinSlot.onepiece:
                currentSkin.slots[SpineSkinSlot.top] = "";
                currentSkin.slots[SpineSkinSlot.bott] = "";
                break;

            case SpineSkinSlot.top:
            case SpineSkinSlot.bott:
                if(currentSkin.slots[SpineSkinSlot.onepiece]!="")
                    currentSkin.slots[SpineSkinSlot.onepiece] = "";

                if(currentSkin.slots[SpineSkinSlot.top]=="")
                    currentSkin.slots[SpineSkinSlot.top] = basicSkin.slots[SpineSkinSlot.top];

                if (currentSkin.slots[SpineSkinSlot.bott] == "")
                    currentSkin.slots[SpineSkinSlot.bott] = basicSkin.slots[SpineSkinSlot.bott];
                break;

            case SpineSkinSlot.helmet:
                currentSkin.slots[SpineSkinSlot.hat] = "";
                break;
            case SpineSkinSlot.hat:
                currentSkin.slots[SpineSkinSlot.helmet] = "";
                break;
        }
    }

    public void CompareSkin()
    {
        if (currentSkin.hair != confirmedSkin.hair || currentSkin.skin != confirmedSkin.skin)
        {
            ClosetManager.Instance.cancelPopUp.SetActive(true);
            return;
        }

        for(int i = 0; i <(int)SpineSkinSlot.MAX; i++)
        {
            if(currentSkin.slots[(SpineSkinSlot)i]!=confirmedSkin.slots[(SpineSkinSlot)i])
            {
                //Debug.Log("diffrent");
                ClosetManager.Instance.cancelPopUp.SetActive(true);
                return;
            }
        }
        ClosetManager.Instance.ClosetOut();
        ClosetManager.Instance.ResetPanel();
    }

    public void ResetSkin()
    {
        if(currentSkin.skin != basicSkin.skin)
        {
            currentSkin.skin = basicSkin.skin;
        }
        if (currentSkin.hair != basicSkin.hair)
        {
            currentSkin.hair = basicSkin.hair;
        }
        
        // 스킨 초기화
        for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
        {
            
                //if (basicSkin.slots[(SpineSkinSlot)i] != "")
                currentSkin.slots[(SpineSkinSlot)i] = basicSkin.slots[(SpineSkinSlot)i];
        }
        //updator.UpdateSpineSkin(currentSkin);
        showSkin(currentSkin);
    }

    public void showSkin(PlayerSpineSkinData skinData)
    {
        Skeleton skeleton = skeletonGraphic.Skeleton;
        Skin SKIN = new Skin(skinData.ToString());

        foreach (var item in skinData.slots)
        {
            if (item.Value == "") continue;
            Skin addSkin = skeleton.Data.FindSkin(item.Value);
            if (addSkin == null)
            {
                Debug.LogError(item.Value);
            }
            SKIN.AddSkin(addSkin);

        }
        skeleton.SetSkin(SKIN);
        skeleton.SetSlotsToSetupPose();
        skeletonGraphic.AnimationState.Apply(skeleton);

        Color hair = skinData.hair;
        Color skin = skinData.skin;
        hair.a = 1;
        skin.a = 1;

        foreach (var item in skeletonGraphic.Skeleton.Slots)
        {
            string slotName = item.ToString();
            if (SpineSkinManager.instance.slotTypes.ContainsKey(slotName) == false) continue;
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.hair)
            {
                item.SetColor(hair);
            }
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.skin)
            {
                item.SetColor(skin);
            }
        }
    }
    public void ConfirmSkin()
    {
        confirmedSkin.hair = currentSkin.hair;
        confirmedSkin.skin = currentSkin.skin;
        Debug.Log(confirmedSkin.hair);

        for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
        {
            
            //if (currentSkin.slots[(SpineSkinSlot)i] != "")
            //{
                confirmedSkin.slots[(SpineSkinSlot)i] = currentSkin.slots[(SpineSkinSlot)i];
            //}
        }
    }


    // 메인 화면에서 옷장 버튼 클릭 시 호출
    public void RollBackSkin()
    {
        
        if (confirmedSkin != null)
        {
            if(currentSkin.skin != confirmedSkin.skin)
            {
                currentSkin.skin = confirmedSkin.skin;
            }
            if (currentSkin.hair != confirmedSkin.hair)
            {
                currentSkin.hair = confirmedSkin.hair;
            }
            
            for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
            {
                //if (confirmedSkin.slots[(SpineSkinSlot)i] != "")
                    currentSkin.slots[(SpineSkinSlot)i] = confirmedSkin.slots[(SpineSkinSlot)i];
            }
            showSkin(currentSkin);
        }
        // 이전 스킨 기록이 없으면 기본 스킨으로 초기화
        else ResetSkin();

    }

    void SetBasicSkin()
    {
        basicSkin = new PlayerSpineSkinData();
        currentSkin = new PlayerSpineSkinData();
        confirmedSkin = new PlayerSpineSkinData();

        for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
        {
            basicSkin.slots[(SpineSkinSlot)i] = "";
            currentSkin.slots[(SpineSkinSlot)i] = "";
            confirmedSkin.slots[(SpineSkinSlot)i] = "";
        }
        basicSkin.hair = new Color(1, 1, 1, 1);
        basicSkin.skin = new Color(1, 1, 1, 1);
        basicSkin.slots[SpineSkinSlot.eye] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.eye, 0);
        basicSkin.slots[SpineSkinSlot.eyebrow] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.eyebrow, 1);
        basicSkin.slots[SpineSkinSlot.mouth] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.mouth, 1);
        basicSkin.slots[SpineSkinSlot.hair] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.hair, 1);
        basicSkin.slots[SpineSkinSlot.top] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.top, 0);
        basicSkin.slots[SpineSkinSlot.bott] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.bott, 0);
        basicSkin.slots[SpineSkinSlot.shoe] = SpineSkinManager.instance.GetSkin(SpineSkinSlot.shoe, 0);

        confirmedSkin.hair = new Color(1, 1, 1, 1);
        confirmedSkin.skin = new Color(1, 1, 1, 1);
        for (int i = 0; i < (int)SpineSkinSlot.MAX; i++)
        {
            confirmedSkin.slots[(SpineSkinSlot)i] = basicSkin.slots[(SpineSkinSlot)i];
        }
    }

    public void SetRandomSkin()
    {
        //currentSkin = 
        SpineSkinManager.instance.SetRandomSkin();
        //showSkin(currentSkin);
    }
}
