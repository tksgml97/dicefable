using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ClosetName : MonoBehaviour
{
    InputField inputField;
    private void Awake()
    {
        inputField = GetComponent<InputField>();
        inputField.characterLimit = 6;
        inputField.onValueChanged.AddListener((word) => inputField.text = Regex.Replace(word, @"[^0-9a-zA-Z��-�R��-����-��]", ""));
    }
}
