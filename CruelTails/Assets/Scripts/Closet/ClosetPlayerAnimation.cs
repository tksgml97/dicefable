using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class ClosetPlayerAnimation : MonoBehaviour
{
    SkeletonDataAsset skeletonAsset;
    [SerializeField] SkeletonGraphic skeletonGraphic;
    [SerializeField] PlayerSpineSkinUpdator updator;
    [SerializeField] PlayerCloset playerCloset = null;

    float animTime = 0f;
    List<string> ch_live_anim, ch_calm_anim, ch_timid_anim;
    

    private void Awake()
    {
        skeletonAsset = skeletonGraphic.SkeletonDataAsset;

        ch_live_anim = new List<string>();
        ch_calm_anim = new List<string>();
        ch_timid_anim = new List<string>();
        

        ch_live_anim.Add("PC_live_win"); ch_live_anim.Add   ("PC_live_victory"); ch_live_anim.Add("PC_live_lose");
        ch_calm_anim.Add("PC_calm_win"); ch_calm_anim.Add   ("PC_calm_victory"); ch_calm_anim.Add("PC_calm_lose");
        ch_timid_anim.Add("PC_timid_win"); ch_timid_anim.Add("PC_timid_victory"); ch_timid_anim.Add("PC_timid_lose");
    }
    public void Animation(int i)
    {
        // LobbyUIManager 에 성격정보 저장.
        LobbyUIManager.Instance.personality = i;

        
        skeletonGraphic.AnimationState.TimeScale = 0;
        skeletonGraphic.AnimationState.TimeScale = 1.0f;
        int randNum = Random.Range(0, 3);
        //Debug.Log(randNum);
        string anim = ch_live_anim[0];
        float fix = 0f;
        switch (i)
        {
            case 0:
                anim = ch_live_anim[randNum];
                skeletonGraphic.AnimationState.SetAnimation(0, anim, false);
                
                break;
            case 1:
                anim = ch_calm_anim[randNum];
                //skeletonGraphic.AnimationState.SetAnimation(0, "PC_calm_win", false);
                skeletonGraphic.AnimationState.SetAnimation(0, anim, false);
                if(randNum == 0)
                fix = 0.2f;
                //animTime = skeletonAsset.GetSkeletonData(true).FindAnimation("PC_calm_win").Duration - 0.2f;
                break;

            case 2:
                anim = ch_timid_anim[randNum];
                //skeletonGraphic.AnimationState.SetAnimation(0, "PC_timid_win", false);
                skeletonGraphic.AnimationState.SetAnimation(0, anim, false);
                //animTime = skeletonAsset.GetSkeletonData(true).FindAnimation("PC_timid_win").Duration;
                break;
        }
        
        animTime = skeletonAsset.GetSkeletonData(true).FindAnimation(anim).Duration - fix;

        skeletonGraphic.AnimationState.AddAnimation(0, "PC_F_L_idle", true, animTime);
    }
}
