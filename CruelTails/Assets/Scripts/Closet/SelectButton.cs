using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectButton : MonoBehaviour
{
    [SerializeField] SelectManager selectManager;

    public Sprite unselected,selected;
    Image image;
    [SerializeField] Text text;
    

    public bool isSelected = false;


    private void Awake()
    {
        selectManager.SelectButtons.Add(this);
        image = GetComponent<Image>();
        image.sprite =(isSelected)? selected :unselected;
        if (text != null && isSelected)
            text.color = selectManager.selectTextColors[0];
    }

    public void onClick()
    {
        selectManager.buttonInit();

        isSelected = true;
        image.sprite = selected;
        if(text != null)
            text.color = selectManager.selectTextColors[0];
    }

    public void setButton(bool _input) // true: 선택, false: 선택 해제
    {
        image.sprite = (_input) ? selected : unselected;
        if (text != null)
            text.color = (_input) ? selectManager.selectTextColors[0] : selectManager.selectTextColors[1];
    }
    public void pointerOutButton()
    {
        if(isSelected == false)
        {
            image.sprite = unselected;
            if (text != null)
                text.color = selectManager.selectTextColors[1];
        }
    }

}
