using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Round3LobbyAni : MonoBehaviour
{
    [SerializeField] string aniName;
    SkeletonAnimation skeleton;
    private void Awake()
    {
        skeleton = GetComponent<SkeletonAnimation>();
        skeleton.AnimationState.SetAnimation(1, aniName, true);
    }
}
