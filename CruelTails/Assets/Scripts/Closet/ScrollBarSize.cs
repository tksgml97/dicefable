using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollBarSize : MonoBehaviour
{
    private void OnEnable()
    {
        Invoke(nameof(size), 0.1f);
    }
    void size()
    {
        transform.GetComponent<Scrollbar>().size = 0.01f;
    }
}
