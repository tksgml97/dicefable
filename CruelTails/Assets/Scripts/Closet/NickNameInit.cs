using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class NickNameInit : MonoBehaviour
{
    [HideInInspector] public Text playerNameCard;
    private void Awake()
    {
        
    }
    void Start()
    {
        playerNameCard = GetComponent<Text>();
        LobbyUIManager.Instance.nameInputField.characterLimit = 6;
        LobbyUIManager.Instance.nameInputField.onValueChanged.
            AddListener((word) => LobbyUIManager.Instance.nameInputField.text
            = Regex.Replace(word, @"[^0-9a-zA-Z��-�R��-����-��]", ""));

        string text = "����" + Random.RandomRange(0, 100).ToString();
        playerNameCard.text = text;
        LobbyUIManager.Instance.savedName = text;
        LobbyUIManager.Instance.nameInputField.text = text;
    }

}
