using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectManager : MonoBehaviour
{
    public Color[] selectTextColors;
    public List<SelectButton> SelectButtons;

    public void buttonInit()
    {
        
        for(int i = 0; i < SelectButtons.Count; i++)
        {
            SelectButtons[i].isSelected = false;
            SelectButtons[i].setButton(false);
        }
    }
}
