using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Spine.Unity;
using Spine;
public class KillSceneController : MonoBehaviour
{
    [SerializeField] Animator bookCloseAni;
    [SerializeField] Animator bookOpenAni;
    [SerializeField] GameObject[] KillScene;
    int LocalizationType = 0; // default : 0, teenager : 1

    [SerializeField] int bookCloseTime = 20;
    [SerializeField] int killSceneTime = 20;
    //[SpineAnimation] public string AniName = "Death_Kill";

    public SkeletonGraphic[] skeletonAnimationScene;
    public SkeletonGraphic[] skeletonAnimation;

    private void Awake()
    {
        
    }
    public void Close()
	{
        KillScene[LocalizationType].SetActive(false);
	}
    public void PlayKill(int losePlayer)
	{
        LocalizationType = (GameManager.instance.LocalizationType == global::LocalizationType.Teenager) ? 1 : 0;

        //int killPlayer = GameManager.instance.GetKillPlayerIndex();
        bookOpenAni?.SetTrigger("Open");
        KillScene[LocalizationType].SetActive(true);
        if (NetworkConnect.instance != null)
        {
            PlayerSpineSkinData skin = GameManager.instance.GetSkinFromPlayerIndex(losePlayer);
            UpdateSpineSkin(skin);
        }
        skeletonAnimationScene[LocalizationType].timeScale = 1;
        skeletonAnimation[LocalizationType].timeScale = 1;
        StartCoroutine(WaitPlayBookCloseAnimation(bookCloseTime));

        //if (GameManager.instance.currentGame != eGameRound.Round3 && skeletonAnimationScene)
            //killSceneTime = (int)(skeletonAnimationScene.skeletonDataAsset.GetSkeletonData(true).FindAnimation(AniName).Duration + 2);
        StartCoroutine(WaitPlayNextGame(killSceneTime));
    }
    IEnumerator WaitPlayBookCloseAnimation(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        bookCloseAni.SetTrigger("Close");

    }
    IEnumerator WaitPlayNextGame(float killAnimationTime)
    {
        yield return new WaitForSeconds(killAnimationTime);

        eSceneType nextScene = eSceneType.Round1;
        switch (GameManager.instance.currentGame)
		{
            case eGameRound.Round1:
                nextScene = eSceneType.Round2;
                break;
            case eGameRound.Round2:
                nextScene = eSceneType.Round3;
                break;
            case eGameRound.Round3:
                nextScene = eSceneType.Round4;
                break;
            case eGameRound.Round4:
                nextScene = eSceneType.Lobby;
                break;
            default:
                Debug.LogError("Scene Load Error" + GameManager.instance.currentGame);
                break;
        }
        if (nextScene != eSceneType.Lobby)
            StartCoroutine(SceneLoader.instance.LoadScene(nextScene, NetworkConnect.instance.GetGameInitData));
        else
        {
            if(NetworkGameManager.instance.isDie())
            {
                NetworkGameManager.instance.gameOverUI.SetActiveGameOverView(true);
            }
            NetworkGameManager.instance.InitData();
                Debug.Log("Load Lobby");
                SceneManager.LoadScene(0);
            
        }
            

    }

    // Start is called before the first frame update
    void Start()
    {
        //KillScene.SetActive(false);
        //PlayKill();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void UpdateSpineSkin(PlayerSpineSkinData skin)
    {
        if (skin == null) return;
        Skeleton skeleton = skeletonAnimation[LocalizationType].Skeleton;
        Skin SKIN = new Skin(skin.ToString());
        foreach (var item in skin.slots)
        {
            if (item.Value == "") continue;
            //if (item.Key == SpineSkinSlot.helmet) continue;
            //if (item.Key == SpineSkinSlot.hat) continue;
            //if (item.Key == SpineSkinSlot.glassed) continue;
            if (item.Key == SpineSkinSlot.team && GameManager.instance.currentGame != eGameRound.Round1) continue;
            Skin addSkin = skeleton.Data.FindSkin(item.Value);
            if (addSkin == null)
            {
                Debug.LogError(item.Value);
            }
            SKIN.AddSkin(addSkin);
        }
        skeleton.SetSkin(SKIN);
        skeleton.SetSlotsToSetupPose();
        skeletonAnimation[LocalizationType].AnimationState.Apply(skeleton);

        Color hair = skin.hair;
        Color head = skin.skin;
        hair.a = 1;
        head.a = 1;

        foreach (var item in skeletonAnimation[LocalizationType].Skeleton.Slots)
        {
            string slotName = item.ToString();
            if (SpineSkinManager.instance.slotTypes.ContainsKey(slotName) == false) continue;
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.hair)
            {
                item.SetColor(hair);
            }
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.skin)
            {
                item.SetColor(head);
            }
        }
    }
}
