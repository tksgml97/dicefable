using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpItem : NetworkItem
{
    // Start is called before the first frame update
    void Awake()
    {
        type = eItemType.SpeedUp;
    }
    public override void Eat(GameObject player)
    {
        if (GameManager.playerController.gameObject != player) return;

        PlayerItemController pItemController = GameManager.playerController.playerItemController;
        pItemController.AddItemEffect(eItemEffectType.Item_SpeedUp);

    }
}
