using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomPostprocess : MonoBehaviour
{
	[SerializeField] Material zoomBlurMaterial;

	public float blurSize = 5f;
	public float _Intensity = 5f;
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		zoomBlurMaterial.SetFloat("_MaskScale", blurSize);
		zoomBlurMaterial.SetFloat("_Intensity", _Intensity);
		Graphics.Blit(source, destination, zoomBlurMaterial);
	}

}
