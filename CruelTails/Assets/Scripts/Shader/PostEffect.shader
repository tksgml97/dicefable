﻿﻿Shader "MyoMyo/Post"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _MagnetMask("Mask", 2D) = "white" {}
        _MaskScale("Scale", float) = 1
        _Intensity("Intensity", float) = 0.1
    }
        SubShader
        {
            // No culling or depth
            Cull Off ZWrite Off ZTest Always

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };

                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = v.uv;
                    return o;
                }

                sampler2D _MainTex;
                half4 _MainTex_TexelSize;
                sampler2D _MagnetMask;
                float _MaskScale;
                half2 _CenterPos;
                float _Intensity;

                fixed4 frag(v2f i) : SV_Target
                {
                    half2 movedTexcoord = i.uv - _CenterPos;
                    fixed4 col = tex2D(_MainTex, i.uv);
                    float x = (movedTexcoord * _MaskScale + _CenterPos).x;
                    x = clamp(x, 0, 1);
                    float y = (movedTexcoord * _MaskScale + _CenterPos).y;
                    y = clamp(y, 0, 1);

                    half2 m = half2(x, y);
                    fixed4 mag = tex2D(_MagnetMask, m);


                    half scale = 1 - mag.r * _Intensity;


                    // just invert the colors
                    col.rgb = tex2D(_MainTex, movedTexcoord * scale + _CenterPos);

                    float2 v = float2(0.5 - i.uv.x, 0.5 - i.uv.y);
                    v = v * _ScreenParams;

                    if (sqrt(v.x * v.x + v.y * v.y) < 2.5)
                    {
                        col = lerp(col, 1 - col, 0.5);
                    }

                    return float4(1,1,1,1);
                }
                ENDCG
            }
        }
}
