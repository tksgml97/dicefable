using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using Spine;
public class PlayerSpineSkinUpdator : MonoBehaviour
{
    public SkeletonGraphic skeletonGraphic;

    public GameObject[] scrollbars;

    private void Start()
    {
        //foreach(var s in scrollbars)
        //{
        //    if (s == true)
        //        Debug.Log(s.name+" true");
        //    else
        //        Debug.Log(s.name+" false");
        //}
        
    }

    public void PlayAnimation(AnimState state, bool loop = false)
	{

        AnimationReferenceAsset ani = SpineAniLoader.instance.GetAni(state);
        PlayAnimation(ani, loop, 1);
	}

    void PlayAnimation(AnimationReferenceAsset animClip, bool loop, float timeScale)
    {
        skeletonGraphic.AnimationState.SetAnimation(0, animClip, loop).TimeScale = timeScale;
    }
    public void UpdateSpineSkin(PlayerSpineSkinData skin)
    {
        Skeleton skeleton = skeletonGraphic.Skeleton;
        Skin SKIN = new Skin(skin.ToString());
        foreach (var item in skin.slots)
        {
            if (item.Value == "") continue;
            if (item.Key == SpineSkinSlot.team) continue;
            Skin addSkin = skeleton.Data.FindSkin(item.Value);
            if (addSkin == null)
            {
                Debug.LogError(item.Value);
            }
            SKIN.AddSkin(addSkin);
        }
        skeleton.SetSkin(SKIN);
        skeleton.SetSlotsToSetupPose();
        skeletonGraphic.AnimationState.Apply(skeleton);

        Color hair = skin.hair;
        Color head = skin.skin;
        hair.a = 1;
        head.a = 1;

        Debug.Log(hair);
        Debug.Log(head);

        foreach (var item in skeletonGraphic.Skeleton.Slots)
        {
            string slotName = item.ToString();
            if (SpineSkinManager.instance.slotTypes.ContainsKey(slotName) == false) continue;
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.hair)
            {
                item.SetColor(hair);
            }
            if (SpineSkinManager.instance.slotTypes[slotName] == SpineSlotType.skin)
            {
                item.SetColor(head);
            }
        }
    }
}
