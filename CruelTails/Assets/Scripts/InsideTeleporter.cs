using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideTeleporter : MonoBehaviour
{
    [SerializeField] bool isInside = false;
    [SerializeField] Transform positionA;

    [SerializeField] Transform positionB;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.layer != LayerMask.NameToLayer("Player"))
		{
            return;
		}

        Vector3 pointToPlayer = GameManager.playerController.transform.position - positionA.position;


        GameManager.playerController.transform.position = positionB.position + pointToPlayer;
        GameManager.playerController.SendNetworkPosition();
        CameraController3D.instance.cameraClamp = isInside;
        CameraController3D.instance.MoveToTarget();
        Vector3 pos = isInside ? positionB.position: positionA.position;
        if(NetworkConnect.instance)
            NetworkConnect.instance.SendInsideTeleport(!isInside, pos);

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawCube(positionA.position, Vector3.one * 0.5f);
    }
}
