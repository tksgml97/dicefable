using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial_RoundPurpose : MonoBehaviour
{
    [SerializeField] bool Test = false;
    [SerializeField] eTeamType Team = eTeamType.RedHat;

    [SerializeField] int missionID;

    [SerializeField] Text missionScript;
    string RedhatScript, WolfScript;
    string KoreaScript;

    [SerializeField] int[] cutNumber = new int[3]; // String.SubString

    private void Start()
    {
        if (missionID < 1000) return;

        if(GameManager.instance.currentGame == eGameRound.Round1)
        {
            RedhatScript = GameDataLoader.instance.GetScript(missionID).m_scriptResource;
            WolfScript = GameDataLoader.instance.GetScript(missionID + 1).m_scriptResource;
        }
        if(Test || GameManager.instance.currentGame == eGameRound.Round3)
            SetPurpose(Team);
        if(GameManager.instance.currentGame == eGameRound.Round4)
            SetPurpose(Team);
    }

    // 1라운드 목표
    public void SetPurpose(eTeamType team)
    {
        string s;
        switch (GameManager.instance.currentGame)
        {
            case eGameRound.Round1:
                if (team == eTeamType.RedHat)
                {
                    missionScript.text = RedhatScript.Replace("<br>", "\n");
                }
                    
                else if (team == eTeamType.Wolf)
                {
                    //s = WolfScript;
                    //missionScript.text = s.Substring(0, cutNumber[1]) + "\n" + s.Substring(cutNumber[1]);
                    missionScript.text = WolfScript.Replace("<br>", "\n");
                }
                    
                break;
            case eGameRound.Round2:
                s = GameDataLoader.instance.GetScript(missionID).m_scriptResource;
                s = s.Replace("<br>", "\n");
                missionScript.text = s;
                //missionScript.text = s.Substring(0, cutNumber[2]) + "\n" + s.Substring(cutNumber[2]);
                break;
            case eGameRound.Round3:
                if(ChristmasGameManager.Instance.state==Round3State.firstGame)
                    missionScript.text = GameDataLoader.instance.GetScript(missionID).m_scriptResource.Replace("<br>", "\n");
                else
                    missionScript.text = GameDataLoader.instance.GetScript(missionID+1).m_scriptResource.Replace("<br>", "\n");
                break;
            case eGameRound.Round4:
                s = GameDataLoader.instance.GetScript(missionID).m_scriptResource;
                s = s.Replace("<br>", "\n");
                missionScript.text = s;

                break;

        }
        
    }
}
