using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial_Attack : MonoBehaviour
{
    [SerializeField] Sprite Key_ML_BtUp;
    //[SerializeField] Sprite Key_ML_BtDown;
    [SerializeField] Vector3 Key_offset = Vector2.zero;
    Image image;

    bool canInput = true;
    int tutorialCount = 3;
    int curCount = 0;

    bool isActivate = true;

    void Awake()
    {
        image = GetComponent<Image>();
    }

    void Update()
    {
        transform.position = Input.mousePosition + Key_offset;
        if (isActivate)
        {
            if (GameManager.playerController.currentCooldown < 0)
                image.sprite = Key_ML_BtUp;
            //else
                //image.sprite = Key_ML_BtDown;

            if (curCount < tutorialCount)
            {
                
                if (Input.GetMouseButtonDown(0) && canInput)
                {
                    curCount++;
                    canInput = false;
                    Invoke(nameof(CoolTime), 1.0f);
                }
            }
            else
            {
                isActivate = false;
                Invoke(nameof(TutorialEnd), 1.0f);
            }
                
        }
        
        
    }

    void CoolTime()
    {
        canInput = true;
    }

    void TutorialEnd()
    {
        Destroy(gameObject);
    }
}
