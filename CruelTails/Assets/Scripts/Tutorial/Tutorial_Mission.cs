using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_Mission : MonoBehaviour
{
    [SerializeField] Mission mission;
    [SerializeField] GameObject key_space;

    bool isActivate = true;

    void Update()
    {
        Mission_Tutorial();
    }

    void Mission_Tutorial()
    {
        if(isActivate)
        {
            
                
            if (mission.missionState == MissionState.Disable)
                key_space.SetActive(false);

            else if (mission.missionState == MissionState.Claer)
            {
                Debug.Log("tutorial complete");
                isActivate = false;
                key_space.SetActive(false);
            }
            else if (mission.missionState == MissionState.Active)
            {
                key_space.SetActive(true);
            }
        }
    }
}
