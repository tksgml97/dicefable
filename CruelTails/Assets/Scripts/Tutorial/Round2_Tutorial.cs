using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Round2_Tutorial : Tutorial
{
    static public Round2_Tutorial instance = null;
    [SerializeField] GameObject Key_Space;
    [SerializeField] Vector3 Key_offset = Vector2.zero;
    [SerializeField] GameObject[] items_position;
    List<GameObject> Key_Item_Spaces;

    [SerializeField] GameObject Key_RedTable_Space, Key_BlueTable_Space;

    [SerializeField] Tutorial_RoundPurpose Purpose;

    int itemMaxCount = 3;
    int curCount = 0;

    bool GetItemActivate = true;
    bool firstItem = true;
    
    bool FeverTimeActivate = false;

    bool playAgain = false;
    bool isInput = false;

    bool isActivate = true;

    [SerializeField] GameObject item, fever;
    //VideoPlayer item_player, fever_player;
    [SerializeField] VideoClip[] item_video, fever_video;

    double getItemTime, putItemTime, feverTime;
    double timer = 0;
    double MaxTime;


    public void TutorialActive(bool _input)
    {
        gameObject.SetActive(_input);
    }

    public void StartTutorial() // 게임 시작 -> 아이템 줍기 영상
    {
        item.SetActive(true);
        //item_player.Play();
    }

    // 아이템 줍기
    public void GetItem() 
    {
        if(firstItem) isActivate = true;
        if (playAgain || firstItem) // 2라운드 최초 획득 아이템 or 아이템 두기 튜토리얼 중 떨어뜨림
        {
            PutTutorial(true);

            if(firstItem)
                firstItem = false;
        }
        PutItemUI(true);
        if (GetItemActivate)
        {
            GetItemTutorial(false);
            if (++curCount == itemMaxCount)
            {
                // 아이템 줍기 튜토리얼 완료
                GetItemActivate = false;
                //Debug.Log("tutorial end");
            }
        }
    }

    // 아이템이 없어짐
    // 잔칫상에 두다 : true
    // (스스로 OR 맞아서)떨어뜨렸다 : false
    public void PutItem(bool input) 
    {
        if(input)
        {
            // 튜토리얼 완료
            playAgain = false;
            //item_player.Stop();
            item.SetActive(false);
            //PutTutorial(false);
        }
        else
        {
            if(isActivate)
                playAgain = true;
            //item_player.Stop();
            item.SetActive(false);
            timer = 0;
        }
        PutItemUI(false);
        if(GetItemActivate)
            GetItemTutorial(true);
    }

    eTeamType team;

    public void FeverTime()
    {
        FeverTimeTutorial();
        team = GameManager.instance.myTeam;
        FeverTimeActivate = true;
    }

    public void GetEnemyItem() // 아이템 강탈
    {
        fever.SetActive(false);
        //fever_player.Stop();
    }


    void PutTutorial(bool input) // 잔칫상 올리기 영상
    {
        MaxTime = putItemTime;
        timer = 0;

        item.SetActive(input);
        //item_player.clip = item_video[1];
        //if (input)
        //    item_player.Play();
        //else
        //    item_player.Stop();
    }

    void FeverTimeTutorial() // 피버타임 영상
    {
        timer = 0;
        MaxTime = feverTime;
        //item_player.Stop();
        item.SetActive(false);

        fever.SetActive(true);
        //fever_player.Play();
    }


    void Start()
    {
        Key_Item_Spaces = new List<GameObject>();
        if (instance == null)
        {
            parentInstance = this;
            instance = this;
        }
        //item_player = item.GetComponent<VideoPlayer>();
        //fever_player = fever.GetComponent<VideoPlayer>();

        for (int i = 0; i < items_position.Length; i++)
        {
            Key_Item_Spaces.Add(Instantiate(Key_Space));
            Key_Item_Spaces[i].transform.position = items_position[i].transform.position + Key_offset;
        }

        //item_player.clip = item_video[0];
        //fever_player.clip = fever_video[0];

        getItemTime = item_video[0].length * 5; // 5회 반복
        putItemTime = item_video[1].length * 5;
        feverTime = fever_video[0].length * 5;
        MaxTime = getItemTime;

        Purpose.SetPurpose(eTeamType.None);
        //StartTutorial(); // debug

        //FeverTime();
    }

    

    void GetItemTutorial(bool input)
    {
        for (int i = 0; i < items_position.Length; i++)
        {
            Key_Item_Spaces[i].SetActive(input);
        }
    }
    void PutItemUI(bool input)
    {
        eTeamType team = GameManager.instance.myTeam;
        if (team == eTeamType.Red)
            Key_RedTable_Space.SetActive(input);
        else if (team == eTeamType.Blue)
            Key_BlueTable_Space.SetActive(input);
    }

    void Update()
    {
        if (timer < MaxTime)
        {
            timer += Time.deltaTime;
        }
        else
        {
            isActivate = false;
            //item_player.Stop();
            item.SetActive(false);
        }
        //if (Input.GetKeyDown(KeyCode.Space)) // debug
        //{
        //    if (!isInput)
        //    {
        //        Debug.Log("get" + curCount);
        //        isInput = true;
        //        GetItem();
        //    }
        //    else
        //    {
        //        Debug.Log("put");
        //        isInput = false;
        //        PutItem(false);
        //    }
        //}
        if (FeverTimeActivate)
        {
            if(GameManager.playerController.currentItem == eItemType.None)
            {
                switch (team)
                {
                    case eTeamType.Red:
                        Key_BlueTable_Space.SetActive(true);
                        break;
                    case eTeamType.Blue:
                        Key_RedTable_Space.SetActive(true);
                        break;
                }
            }
            else
            {
                switch (team)
                {
                    case eTeamType.Red:
                        Key_BlueTable_Space.SetActive(false);
                        break;
                    case eTeamType.Blue:
                        Key_RedTable_Space.SetActive(false);
                        break;
                }
            }
        }
    }
}
