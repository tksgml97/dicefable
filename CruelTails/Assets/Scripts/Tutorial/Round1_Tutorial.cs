using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Round1_Tutorial : Tutorial
{
    public static Round1_Tutorial instance;
    [SerializeField] GameObject redhat, wolf;
    //VideoPlayer redhat_player, wolf_player;
    //[SerializeField] VideoClip redhat_video, wolf_video;
    bool isActivate = false;
    bool isRedhat = false;
    bool isWolf = false;


    double redhatTime, wolfTime;
    double timer = 0;
    double MaxTime;


    [SerializeField] Tutorial_RoundPurpose Purpose;
    void Start()
    {
        if (instance == null)
            instance = this;

        //redhat_player = redhat.GetComponent<VideoPlayer>();
        //wolf_player = wolf.GetComponent<VideoPlayer>();

        //redhatTime = redhat_video.length * 5; // 5회 반복
        //wolfTime = wolf_video.length * 5;
        MaxTime = redhatTime;

        //redhat_player.clip = redhat_video;
        //wolf_player.clip = wolf_video;

        //SetRole(eTeamType.Wolf);
        //Debug.Log("늑대");
        //Invoke(nameof(d), 5.0f);
    }

    private void Update()
    {
        if(isActivate)
        {
            if (timer < MaxTime)
            {
                timer += Time.deltaTime;
            }
            else
            {
                //wolf_player.Stop();
                //redhat_player.Stop();

                wolf.SetActive(false);
                redhat.SetActive(false);
            }
            
        }
    }

    public void ChangeRole(eTeamType team)
    {
        Purpose.SetPurpose(team);
        // 두 역할 다 해봣을 때 튜토리얼 종료
        if (isRedhat && isWolf)
        {
            isActivate = false;

            //wolf_player.Stop();
            //redhat_player.Stop();

            wolf.SetActive(false);
            redhat.SetActive(false);
        }
        else
        {
            // 한가지 역할만 해봤을 때
            SetRole(team);
            timer = 0;
        }
    }

    public void SetRole(eTeamType team)
    {
        Purpose.SetPurpose(team);
        isActivate = true;

        if (team == eTeamType.RedHat)
        {
            isRedhat = true;
            wolf.SetActive(false);
            redhat.SetActive(true);

            MaxTime = redhatTime;
            //redhat_player.Play();
        }
        else if(team == eTeamType.Wolf)
        {
            isWolf = true;
            redhat.SetActive(false);
            wolf.SetActive(true);
            
            MaxTime = wolfTime;
            //wolf_player.Play();
        }
    }

    public void EndTutorial()
    {
        gameObject.SetActive(false);
    }

    void d()
    {
        ChangeRole(eTeamType.RedHat);
        Debug.Log("빨모");
        //Invoke(nameof(dd), 5.0f);
    }
    //void dd()
    //{
    //    Debug.Log("늑대");
    //    Invoke(nameof(ddd), 5.0f);
    //    ChangeRole(eTeamType.Wolf);
    //}
    //void ddd()
    //{
    //    Debug.Log("빨모");
    //    ChangeRole(eTeamType.RedHat);
    //}
}
