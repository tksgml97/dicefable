using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial_Move : MonoBehaviour
{
    // wasd
    //[SerializeField] Vector3 key_offset;
    [SerializeField] GameObject key_wasd;
    bool isActivate = true;

    // mission


    // portal

    void Update()
    {
        WASD_Tutorial();
    }


    void WASD_Tutorial()
    {
        key_wasd.transform.position = Camera.main.WorldToScreenPoint(GameManager.playerController.transform.position); //+ key_offset);

        if (GameManager.instance.isStart && isActivate)
        {
            if (Input.GetKeyDown(KeyCode.W) ||
            Input.GetKeyDown(KeyCode.A) ||
            Input.GetKeyDown(KeyCode.S) ||
            Input.GetKeyDown(KeyCode.D))
            {
                isActivate = false;
                Invoke(nameof(WASDOff), 3.0f);
            }
        }
    }

    void WASDOff()
    {
        key_wasd.SetActive(false);
    }
}
