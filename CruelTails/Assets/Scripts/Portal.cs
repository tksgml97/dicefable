using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class Portal : MonoBehaviour
{
    [SerializeField] string portalName;
    [SerializeField] GameObject buttonPrefab;
    [SerializeField] Transform buttonCanvas;
    [SerializeField] Portal link;

    List<GameObject> buttons = new List<GameObject>();

    static float currentPortalCooldown = 0;
    static float portalCooldown = 0.2f;
    static float portalCooldownRound1 = 5;
    [SerializeField] float portalActiveRange = 3f;
    [SerializeField] GameObject portalUI;
    [SerializeField] GameObject textUI;
    [SerializeField] SkeletonAnimation skeletonAnimation;
    [SerializeField] Text coolText;
    [SerializeField] bool isInside = false;

    bool usedPortal = false;
    // Start is called before the first frame update
    void Start()
    {
        textUI.SetActive(true);

        GameObject newButton = Instantiate(buttonPrefab, portalUI.transform);
        newButton.transform.localPosition = Vector3.zero;
        buttons.Add(newButton);


        Vector3 v = (link.transform.position - transform.position);
        v.y = v.z;
        v.z = 0;

        newButton.transform.rotation = Quaternion.LookRotation(Vector3.forward, v);

        
        if (GameManager.instance)
        {
            GameManager.instance.uiActiveEvent += SetActiveUI;
        }
        GameManager.playerController.spacebarEvent += OnInterect;
    }

    bool uiOn = true;

    void SetActiveUI(bool state)
	{
        uiOn = state;
        portalUI.SetActive(state);
    }
    void OnInterect()
    {
      


        Vector3 portalToPlayer = (GameManager.playerController.transform.position - transform.position);
        portalToPlayer.y = 0;

        if (portalToPlayer.magnitude < portalActiveRange)
        {
            GameManager.playerController.canDrop = false;
        }

        if (portalToPlayer.magnitude < portalActiveRange && currentPortalCooldown <= 0)
        {

            if (isInside || link.isInside)
            {
                link.teleportInside();
            }
            else
            {
                link.teleport();
            }

        }
    }
    // Update is called once per frame
    void LateUpdate()
    {

        if(usedPortal && currentPortalCooldown > 0)
		{
            currentPortalCooldown -= Time.deltaTime;
		}
        coolText.gameObject.transform.position = Camera.main.WorldToScreenPoint( transform.position);
        if (currentPortalCooldown > 0)
		{
            //sprite change
            skeletonAnimation.skeleton.SetColor(Color.red);
            coolText.text = Mathf.CeilToInt(currentPortalCooldown).ToString();

        } else
        {

            //sprite change
            skeletonAnimation.skeleton.SetColor(Color.white);
            coolText.text = "";
            usedPortal = false;
        }

        Vector3 portalToPlayer = (GameManager.playerController.transform.position - transform.position);
        portalToPlayer.y = 0;
        if (portalToPlayer.magnitude < portalActiveRange && currentPortalCooldown <= 0)
        {
            portalUI.SetActive(true && uiOn);
            foreach (var item in buttons)
            {

                item.transform.position = Camera.main.WorldToScreenPoint(transform.position);
            }



        }
        else
        {
            portalUI.SetActive(false);

        }


    }
    public void teleportInside()
    {
        if (GameManager.playerController.state == PlayerState.Hit) return;
        GameManager.playerController.moveSound.PlayPortalSound();
        CameraController3D.instance.cameraClamp = !isInside;
        if(GameManager.instance.currentGame == eGameRound.Round1)
            currentPortalCooldown = portalCooldownRound1;
        else
            currentPortalCooldown = portalCooldown;
        usedPortal = true;
        GameManager.playerController.transform.position = transform.position;
        CameraController3D.instance.MoveToTarget();
        GameManager.playerController.SendNetworkPosition();
    }
    public void teleport()
    {
        if (GameManager.playerController.state == PlayerState.Hit) return;
        GameManager.playerController.moveSound.PlayPortalSound();
        CameraController3D.instance.cameraClamp = !isInside;

        if (GameManager.instance.currentGame == eGameRound.Round1)
            currentPortalCooldown = portalCooldownRound1;
        else
            currentPortalCooldown = portalCooldown;
        usedPortal = true;
        GameManager.playerController.transform.position = transform.position;
        GameManager.playerController.SendNetworkPosition();
    }
}
