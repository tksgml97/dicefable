using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundInstance : MonoBehaviour
{
    private static SoundInstance soundInstance = null;
    public static SoundInstance Instance { get { if (null != soundInstance) return soundInstance; return null; } }

    // SFXs
    public AK.Wwise.Event pc_atk_sound;
    public AK.Wwise.Event pc_wolf_atk_sound;

    public AK.Wwise.Event pc_wolf_change_sound;
    public AK.Wwise.Event pc_wolf_faint_sound;

    public AK.Wwise.Event pc_portal_sound;

    [SerializeField] AK.Wwise.Event round1_roulette_sound;
    [SerializeField] AK.Wwise.Event round2_roulette_sound;

    public AK.Wwise.Event round1_redhoodKill_sfx;
    public AK.Wwise.Event round2_dowokKill_sfx;
    public AK.Wwise.Event round4_nitoKill_sfx;

    // AMBs
    [SerializeField] AK.Wwise.Event sound_amb_koreaStage_bird;
    public AK.Wwise.RTPC rtpc_inBush;
    public AK.Wwise.Event AMB_korea;

    [SerializeField] AK.Wwise.Event sound_ui_game_countdown;

    // BGMs
    [SerializeField] AK.Wwise.Event round1_bgm;
    [SerializeField] AK.Wwise.Event round1_bgm_end;
    public AK.Wwise.RTPC rtpc_Place = null;
    public AK.Wwise.RTPC rtpc_Role;

    [SerializeField] AK.Wwise.Event round2_bgm;
    [SerializeField] AK.Wwise.Event round2_bgm_end;
    public AK.Wwise.RTPC rtpc_speed; // 피버타임 BGM 속도

    AK.Wwise.Event round3_bgm;
    [SerializeField] AK.Wwise.Event round3_bgm_1;
    [SerializeField] AK.Wwise.Event round3_bgm_1_end;
    [SerializeField] AK.Wwise.Event round3_bgm_2;
    [SerializeField] AK.Wwise.Event round3_bgm_2_end;

    [SerializeField] AK.Wwise.Event round4_bgm;

    eGameRound curRound;
    //[Header("BGM 켜기/끄기")]
    bool BGM = true;

    private void Awake()
    {
        if(null==soundInstance)
        {
            soundInstance = this;
        }
    }
    private void Start()
    {
        curRound = GameManager.instance.currentGame;
        //AfterCountDown();
    }

    // BGM 재생
    public void AfterCountDown()
    {
        switch (curRound)
        {
            case eGameRound.Round1:
                if(GameManager.instance.myTeam==eTeamType.RedHat)
                    rtpc_Role?.SetValue(gameObject, 1);
                else
                    rtpc_Role?.SetValue(gameObject, 0);

                round1_bgm?.Post(gameObject);
                
                break;

            case eGameRound.Round2:
                //bgm
                round2_bgm?.Post(gameObject);
                //amb
                sound_amb_koreaStage_bird?.Post(gameObject);
                break;
            case eGameRound.Round3:
                if (ChristmasGameManager.Instance.state == Round3State.firstGame)
                    round3_bgm = round3_bgm_1;
                else
                    round3_bgm = round3_bgm_2;

                round3_bgm?.Post(gameObject);
                break;
            case eGameRound.Round4:
                round4_bgm?.Post(gameObject);
                break;
            default: break;
        }

    }

    // 카운트 다운 효과음
    public void CountDown()
    {
        sound_ui_game_countdown?.Post(gameObject);
        Invoke(nameof(AfterCountDown), 3.0f);
    }

    // BGM 정지
    // SoundInstance.Instance.Round2SoundStart();
    public void Round2SoundEnd()
    {
        switch (curRound)
        {
            case eGameRound.Round1:
                Round1_Tutorial.instance.EndTutorial();
                round1_bgm_end?.Post(gameObject);
                break;
            case eGameRound.Round2:
                sound_amb_koreaStage_bird?.Stop(gameObject);
                round2_bgm_end?.Post(gameObject); break;
            case eGameRound.Round3:
                if (ChristmasGameManager.Instance.state == Round3State.firstGame)
                    round3_bgm_1_end?.Post(gameObject);
                else
                    round3_bgm_2_end?.Post(gameObject);
                break;
            case eGameRound.Round4:
                round4_bgm?.Stop(gameObject);
                break;
            default: break;
        }
    }

    // 룰렛 효과음
    public void PlayRouletteSound()
    {
        //룰렛 효과음 임시 제외
        //Invoke(nameof(RouletteSFX), 2.0f); // 룰렛 씬에서 1초 후 재생
    }
    void RouletteSFX()
    {
        switch (curRound)
        {
            case eGameRound.Round1:
                //round1_roulette_sound?.Post(gameObject);
                break;
            case eGameRound.Round2:
                    //round2_roulette_sound?.Post(gameObject); break;
            case eGameRound.Round3: break;
            case eGameRound.Round4: break;
            default: break;
        }
    }

    // 1라운드

    public void ChangeRoleSound(eTeamType team)
    {
        Debug.Log("BGM : " + team);
        if(team==eTeamType.RedHat)
        {

            StopCoroutine(SoundFadeIn());
            StartCoroutine(SoundFadeOut());
        }
        else if(team==eTeamType.Wolf)
        {

            StopCoroutine(SoundFadeOut());
            StartCoroutine(SoundFadeIn());
        }
    }
    

    // 2라운드

    // 피버 타임 BGM 속도 증가
    public void PlayFeverTimeSound()
    {
        rtpc_speed?.SetValue(gameObject, 1);
    }



    // fade in out
    IEnumerator SoundFadeIn() // 늑대
    {
        for (float f = 1f; f > 0; f -= 0.1f)
        {
            rtpc_Role?.SetValue(gameObject, f);
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    IEnumerator SoundFadeOut() // 빨간모자
    {
        for (float f = 0f; f < 1f; f += 0.1f)
        {
            rtpc_Role?.SetValue(gameObject, f);
            yield return new WaitForSeconds(0.05f);
        }
        yield return null;
    }

}
