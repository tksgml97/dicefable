using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillSceneSound : MonoBehaviour
{
    // ó�� �� SFXs
    AK.Wwise.Event round1_redhoodKill_sfx;
    AK.Wwise.Event round2_dowokKill_sfx;
    AK.Wwise.Event round3_santaKill_sfx;
    AK.Wwise.Event round4_cthulhuKill_sfx;


    void Start()
    {
        round1_redhoodKill_sfx = SoundInstance.Instance.round1_redhoodKill_sfx;
        round2_dowokKill_sfx = SoundInstance.Instance.round2_dowokKill_sfx;
        round4_cthulhuKill_sfx = SoundInstance.Instance.round4_nitoKill_sfx;

        switch (GameManager.instance.currentGame)
        {
            case eGameRound.Round1: round1_redhoodKill_sfx?.Post(gameObject); break;
            case eGameRound.Round2: round2_dowokKill_sfx?.Post(gameObject); break;
            case eGameRound.Round3: round3_santaKill_sfx?.Post(gameObject); break;
            case eGameRound.Round4: round4_cthulhuKill_sfx?.Post(gameObject); break;
            default: break;
        }
    }

    void Update()
    {
        
    }
}
