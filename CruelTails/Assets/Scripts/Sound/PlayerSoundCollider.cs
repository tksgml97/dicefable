using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //player 
        if(other.gameObject.layer == 3)
        {
            other.GetComponent<PlayerMoveSound>().soundPlay = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 3)
        {
            other.GetComponent<PlayerMoveSound>().soundPlay = false;
        }
    }
}
