using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 발소리 타입(재질)
public enum FootType
{
    // foot
    DIRT, WOOD, STONE,
}

public class FootMatType : MonoBehaviour
{
    [Header("발소리 선택")]
    [Header("1. 흙길")]
    [Header("2. 목재")]
    [Header("3. 석재")]
    [SerializeField] FootType material;

    private void OnTriggerEnter(Collider other)
    {
        
        // player layer
        if(other.gameObject.layer == 3)
        other.GetComponent<PlayerMoveSound>().SwitchMaterial(material);
    }
}
