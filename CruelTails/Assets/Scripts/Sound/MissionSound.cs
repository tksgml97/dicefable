using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionSound : MonoBehaviour
{
    [SerializeField] AK.Wwise.Event sound_missionGuage;
    [SerializeField] AK.Wwise.Event sound_missionComplete;
    // Start is called before the first frame update
    
    public void PlayMissionSound(MissionState m)
    {
        switch(m)
        {
            case MissionState.Active:
                sound_missionGuage?.Stop(gameObject);
                break;
            case MissionState.Play:
                sound_missionGuage?.Post(gameObject);
                break;
            case MissionState.Claer:
                sound_missionComplete?.Post(gameObject);
                break;
        }    
    }
}
