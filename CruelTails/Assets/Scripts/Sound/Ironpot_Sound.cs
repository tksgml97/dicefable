using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ironpot_Sound : MonoBehaviour
{
    [SerializeField]
    private AK.Wwise.Switch ironpot;
    private void Awake()
    {
        ironpot.SetValue(this.gameObject);
    }
}
