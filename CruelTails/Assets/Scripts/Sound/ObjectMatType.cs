using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ObjectType
{
    BUSH, IRONPOT, FLOWINGWATER, NONE
}

public class ObjectMatType : MonoBehaviour
{
    [Header("오브젝트 선택")]
    [Header("1. 수풀 벌레 소리")]
    [Header("2. 가마솥 소리")]
    [Header("3. 흐르는 물 소리")]
    [Header("3. 지정되지않음")]
    
    [SerializeField] ObjectType material;

    [SerializeField] AK.Wwise.Switch _switch;
    [SerializeField] AK.Wwise.RTPC rtpc;

    float distanceX, distanceZ, V = 0;

    private void Start()
    {
        _switch.SetValue(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        // player layer
        if (other.gameObject.layer == 3)
        {
            distanceX = other.gameObject.transform.position.x - transform.position.x;
            if (distanceX < 0) distanceX *= -1;
            distanceZ = other.gameObject.transform.position.z - transform.position.z;
            if (distanceZ < 0) distanceZ *= -1;

            V = (distanceX < distanceZ) ? distanceZ : distanceX;

            // 새소리 볼륨 다운
            StopCoroutine(SoundFadeOut());
            StartCoroutine(SoundFadeIn());

            // 풀벌레, 물가, 가마솥 환경음 재생
            SoundInstance.Instance.AMB_korea.Post(gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 3)
        {
            distanceX = other.gameObject.transform.position.x - transform.position.x;
            if (distanceX < 0) distanceX *= -1;
            distanceZ = other.gameObject.transform.position.z - transform.position.z;
            if (distanceZ < 0) distanceZ *= -1;
            //Debug.Log(100 - (((distanceX < distanceZ) ? distanceZ : distanceX) * (100 / V)));
            rtpc.SetValue(gameObject, 100 - (((distanceX < distanceZ) ? distanceZ : distanceX)*(100/V)));
        }
            
    }
    private void OnTriggerExit(Collider other)
    {
        // player layer
        if (other.gameObject.layer == 3)
        {
            StopCoroutine(SoundFadeIn());
            StartCoroutine(SoundFadeOut());
        }
    }

    // sound fade in/out
    IEnumerator SoundFadeIn()
    {
        switch (material)
        {
            case ObjectType.BUSH:
                float f2 = 0f;
                for (float f = 0f; f < 100f; f += 1f)
                {
                    //f2 += 1f;
                    //rtpc.SetValue(gameObject, f2);
                    SoundInstance.Instance.rtpc_inBush.SetValue(SoundInstance.Instance.gameObject, f);
                    yield return new WaitForSeconds(0.01f);
                }
                
                break;
                
            default:
                
                for (float f = 0; f < 100f; f += 1f)
                {
                    //rtpc.SetValue(gameObject, f);
                    yield return new WaitForSeconds(0.01f);
                }
                break;
        }
        yield return null;
    }


    IEnumerator SoundFadeOut()
    {
        switch (material)
        {
            case ObjectType.BUSH:
                float f2 = 100f;
                for (float f = 100f; f > -1; f -= 1f)
                {
                    //f2 -= 1f;
                    //rtpc.SetValue(gameObject, f2);
                    SoundInstance.Instance.rtpc_inBush.SetValue(SoundInstance.Instance.gameObject, f);

                    yield return new WaitForSeconds(0.01f);
                }

                break;
            default:
                for (float f = 100f; f > 0; f -= 1f)
                {
                    //rtpc.SetValue(gameObject, f);
                    yield return new WaitForSeconds(0.01f);
                }
                break;
        }
        SoundInstance.Instance.AMB_korea.Stop(gameObject);
        yield return null;
    }
}
