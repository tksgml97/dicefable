using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbySoundInstance : MonoBehaviour
{
    [SerializeField] AK.Wwise.Event lobby_bgm;

    private void Start()
    {
        Invoke(nameof(LobbySoundStart), 1.0f);
    }
    public void LobbySoundStart()
    {
        if(lobby_bgm != null)
        lobby_bgm.Post(gameObject);
    }
    public void LobbySoundEnd()
    {
        if (lobby_bgm != null)
            lobby_bgm.Stop(gameObject);
    }
}
