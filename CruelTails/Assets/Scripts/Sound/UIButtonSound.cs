using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonSound : MonoBehaviour
{
    [SerializeField] AK.Wwise.Event hover;
    [SerializeField]AK.Wwise.Event selection;

    bool hovered = false;
    public void onClick()
    {
        selection?.Post(gameObject);
    }
    public void onMouse()
    {
        //if (hovered)
        //if(!hovered)
            hover?.Post(gameObject);
        //hovered = true;
    }

    public void OutMouse()
    {
        //hovered = false;
    }

    public void Hovered()
    {
        //Debug.Log(gameObject.name + "hoverd");
        //hovered = (hovered) ? false : true;
    }
}
