using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMoveSound : MonoBehaviour
{
    // foot Sounds
    [SerializeField] AK.Wwise.Event sound_pc_dirt_walk;
    [SerializeField] AK.Wwise.Event sound_pc_dirt_run;
    [SerializeField] AK.Wwise.Event sound_pc_wood_walk;
    [SerializeField] AK.Wwise.Event sound_pc_wood_run;
    [SerializeField] AK.Wwise.Event sound_pc_stone_walk;
    [SerializeField] AK.Wwise.Event sound_pc_stone_run;

    AK.Wwise.Event sound_pc_walk;
    AK.Wwise.Event sound_pc_run;

    // interactions
    [SerializeField] AK.Wwise.Event sound_pc_interaction_itemOnPlayer;
    [SerializeField] AK.Wwise.Event sound_pc_interaction_itemOffPlayer;
    [SerializeField] AK.Wwise.Event sound_pc_interaction_itemOnTable;
    [SerializeField] AK.Wwise.Event sound_pc_interaction_itemOffTable;

    // atks
    AK.Wwise.Event pc_atk_sound;
    AK.Wwise.Event pc_wolf_atk_sound;
    AK.Wwise.Event pc_wolf_change_sound;
    AK.Wwise.Event pc_wolf_faint_sound;
    AK.Wwise.Event pc_portal_sound;

    public FootType curFootMaterial;

    // atked Sound
    [SerializeField] AK.Wwise.Event sound_pc_basic_atked;

    bool Walkpressed = false;
    bool Runpressed = false;

    [field: SerializeField]
    public bool soundPlay { get; set; } = false;

    PlayerController playerController;


    public bool isCopy = false;
    private void Awake()
    {
        playerController = GetComponent<PlayerController>();

        sound_pc_walk = sound_pc_dirt_walk;
        sound_pc_run = sound_pc_dirt_run;
    }
    private void Start()
    {
        pc_atk_sound = SoundInstance.Instance.pc_atk_sound;
        pc_wolf_atk_sound = SoundInstance.Instance.pc_wolf_atk_sound;
        pc_wolf_change_sound = SoundInstance.Instance.pc_wolf_change_sound;
        pc_wolf_faint_sound = SoundInstance.Instance.pc_wolf_faint_sound;
        pc_portal_sound = SoundInstance.Instance.pc_portal_sound;
    }

    //3라운드 후반부용 렌더러 언체크
    [SerializeField] GameObject playerSpine;
    public void DeletePlayer()
    {
        Debug.Log("Delete Player");

        // -> 빌드 올릴 때 주석 해제, 1인 테스트일때만 주석처리
        this.gameObject.SetActive(false);
        //ScrollMapManager.Instance.Hpbars[0].gameObject.SetActive(false);
    }

    // 바닥 재질 변경
    public void SwitchMaterial(FootType t)
    {
        if(curFootMaterial != t)
        {
            
            sound_pc_walk.Stop(gameObject);
            sound_pc_run.Stop(gameObject);
            curFootMaterial = t;
            switch (t)
            {
                 
                case FootType.DIRT:
                    sound_pc_walk = sound_pc_dirt_walk;
                    sound_pc_run = sound_pc_dirt_run;
                    break;
                case FootType.WOOD:
                    sound_pc_walk = sound_pc_wood_walk;
                    sound_pc_run = sound_pc_wood_run;
                    break;
                case FootType.STONE:
                    sound_pc_walk = sound_pc_stone_walk;
                    sound_pc_run = sound_pc_stone_run;
                    break;
            }
            if (Walkpressed)
                sound_pc_walk.Post(gameObject);
            else if (Runpressed)
                sound_pc_run.Post(gameObject);
        }
    }

    // 이동 키를 눌렀을 때
    public void PlayMoveSound()
    {
        // 플레이어 상태에 따른 재생을 해봅시다
        if(playerController.state == PlayerState.Idle)
        {
            // 걷기 사운드
            if (Input.GetKey(KeyCode.LeftShift))
            {
                sound_pc_run.Stop(gameObject);
                if (!Walkpressed)
                    sound_pc_walk.Post(gameObject);
                Runpressed = false;
                Walkpressed = true;
                
            }
            // 뛰기 사운드
            else
            {
                sound_pc_walk.Stop(gameObject);
                if (!Runpressed)
                {
                    sound_pc_run.Post(gameObject);
                }
                Runpressed = true;
                Walkpressed = false;
            }
        }
        else
        {
            StopMoveSound();
        }
        
    }

    // 이동 키에서 손을 뗐을 때
    public void StopMoveSound()
    {
        Runpressed = false;
        Walkpressed = false;
        if(sound_pc_walk != null)
            sound_pc_walk.Stop(gameObject);
        if (sound_pc_run != null)
            sound_pc_run.Stop(gameObject);
    }



    // 포탈
    public void PlayPortalSound()
    {
        pc_portal_sound.Post(gameObject);
    }

    // 피격
    public void PlayAtkedSound()
    {
        sound_pc_walk.Stop(gameObject);
        sound_pc_run.Stop(gameObject);
        sound_pc_basic_atked.Post(gameObject);
        soundPlay = false;
    }


    // 공격    
    public void PlayAtkSound()
    {
        if (GetComponent<RemotePlayer>().team == eTeamType.Wolf)
            pc_wolf_atk_sound.Post(gameObject);
        else
            pc_atk_sound.Post(gameObject);
    }

    // 1 라운드

    // 늑대 교체
    public void WolfChange()
    {
        pc_wolf_change_sound.Post(gameObject);
        
    }

    // 늑대 기절
    public void WolfFaint()
    {
        pc_wolf_faint_sound.Post(gameObject);
    }


    // 2 라운드

    // 아이템
    public void PlayItemSound(bool _input) // 아이템 획득: true
                                           // 아이템 떨구기: false
    {

        if (_input)
            sound_pc_interaction_itemOnPlayer.Post(gameObject);
        else
            sound_pc_interaction_itemOffPlayer.Post(gameObject);
    }
    // 상차림
    public void PlayTableSound(bool _input) // 상차림 올리기: true
                                            // 상차림 빼기: false
    {
        if (_input)
            sound_pc_interaction_itemOnTable.Post(gameObject);
        else
            sound_pc_interaction_itemOffTable.Post(gameObject);
    }
    private void Update()
    {
        if(isCopy)
        {
            if (Input.GetKeyDown(KeyCode.A))
                PlayItemSound(true);
            if (Input.GetKeyDown(KeyCode.S))
                PlayItemSound(false);
            if (Input.GetKeyDown(KeyCode.D))
                PlayTableSound(true);
            if (Input.GetKeyDown(KeyCode.F))
                PlayTableSound(false);
        }
        
    }
}
