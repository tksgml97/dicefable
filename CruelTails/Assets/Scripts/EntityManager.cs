using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Sirenix.OdinInspector;


public struct EntityData
{
	public GameObject obj;
	public NetworkItem entity;
	public eItemType type;
	public int number;
}

public class EntityManager : MonoBehaviour
{

	public static EntityManager instance;
	public GameObject[] entityPref;
	public LinkedList<EntityData> entityList;
	Transform entityHolder;

	[SerializeField] GameObject[] defaultEntitysRound2;

	[SerializeField] GameObject[] defaultEntitysRuond1;
	[SerializeField] bool developMode = true;

	// Use this for initialization
	void Start()
	{
		entityHolder = new GameObject("Entitys").transform;
		entityHolder.parent = gameObject.transform;
		instance = this;
		entityList = new LinkedList<EntityData>();
		
	}


	public void InitDefaultEntitys()
	{
		if (GameManager.instance.currentGame == eGameRound.Round1) InitRound1Entity(developMode);
		if (GameManager.instance.currentGame == eGameRound.Round2) InitRound2Entity(developMode);
		
	}

	[Button("Build ItemDatas")]
	public void BuildItemData()
	{
		InitRound1Entity(true);
		InitRound2Entity(true);
	}


	void InitRound1Entity(bool buildMode)
	{

		ByteBuffer byteSerializer = new ByteBuffer();

		byteSerializer.WriteInteger(defaultEntitysRuond1.Length);
		for (int i = 0; i < defaultEntitysRuond1.Length; i++)
		{
			NetworkItem item = defaultEntitysRuond1[i].GetComponent<NetworkItem>();
			if (item == null)
			{
				Debug.LogWarning("item not found <NetworkItem>");
			}

			EntityData entity = new EntityData();
			entity.obj = item.gameObject;
			entity.number = i;
			entity.type = item.type;
			entity.entity = item;
			entity.entity.id = i;

			entityList.AddFirst(entity);


			byteSerializer.WriteInteger((int)entity.type);
			byteSerializer.WriteVector3(item.transform.position);
		}

		if (buildMode)
		{
			try
			{
				Debug.Log("create itemdata1 file");
				FileStream stream = File.Open("itemData1.data", FileMode.OpenOrCreate);
				int len = byteSerializer.ToArray().Length;

				ByteBuffer fileWriter = new ByteBuffer();
				fileWriter.WriteInteger(len);
				fileWriter.WriteBytes(byteSerializer.ToArray());
				stream.Write(fileWriter.ToArray(), 0, fileWriter.ToArray().Length);
				stream.Close();
			} catch(Exception e)
			{
				Debug.LogWarning(e.Message);
			}

		}
	}
	void InitRound2Entity(bool buildMode)
	{
		ByteBuffer byteSerializer = new ByteBuffer();

		byteSerializer.WriteInteger(defaultEntitysRound2.Length);
		for (int i = 0; i < defaultEntitysRound2.Length; i++)
		{
			NetworkItem item = defaultEntitysRound2[i].GetComponent<NetworkItem>();
			if (item == null)
			{
				Debug.LogWarning("item not found <NetworkItem>");
			}

			EntityData entity = new EntityData();
			entity.obj = item.gameObject;
			entity.number = i;
			entity.type = item.type;
			entity.entity = item;
			entity.entity.id = i;

			entityList.AddFirst(entity);

			Debug.Log(entity.type.ToString());
			byteSerializer.WriteInteger((int)entity.type);
			byteSerializer.WriteVector3(item.transform.position);
		}

		if (buildMode)
		{
			Debug.Log("create itemdata2 file");
			FileStream stream = File.Open("itemData2.data", FileMode.OpenOrCreate);
			int len = byteSerializer.ToArray().Length;

			ByteBuffer fileWriter = new ByteBuffer();
			fileWriter.WriteInteger(len);
			fileWriter.WriteBytes(byteSerializer.ToArray());
			stream.Write(fileWriter.ToArray(), 0, fileWriter.ToArray().Length);

			stream.Close();
		}
	}


	public void SpawnEntity(int num, Vector3 pos, eItemType _type, int roomNum, bool playAni, Vector3 velocity)
	{
		EntityData entity = new EntityData(); ;
		Debug.Log(pos);

		GameObject prefab = entityPref[0];
		if(_type == eItemType.SpeedUp)
		{
			prefab = entityPref[1];
		}
		if (_type == eItemType.Sheild)
		{
			prefab = entityPref[2];
		}
		entity.obj = Instantiate(prefab, pos, Quaternion.identity, entityHolder) as GameObject;
		entity.number = num;
		entity.type = _type;
		entity.entity = entity.obj.GetComponent<NetworkItem>();
		entity.entity.getCool = 0f;
		entity.entity.SetType((eItemType)_type);
		entity.entity.id = num;
		entity.entity.SetVelocity(velocity);
		if (playAni)
		{
			entity.entity.PlayAni();
			entity.entity.getCool = 1f;
		}

		entityList.AddFirst(entity);
	}

	public void Eat(int num, int playerID)
	{
		foreach (EntityData entity in entityList)
		{
			if (entity.number == num)
			{
				GameObject eatPlayer = NetworkGameManager.instance.GetPlayerObject(playerID);
				if (playerID == NetworkGameManager.memberNumber)
				{
					entity.entity.myEat = true;
				}
				entity.entity.Eat(eatPlayer);
				StartCoroutine(removeEntity(entity));
				break;
			}
		}
	}
	IEnumerator removeEntity(EntityData entity, bool DestObject = true)
	{
		yield return new WaitForEndOfFrame();
		if (DestObject)
			if (entity.obj != null) Destroy(entity.obj);
		entityList.Remove(entity);
	}

	public EntityData FindEntity(int id)
	{
		foreach (EntityData entity in entityList)
		{
			if (entity.number == id)
			{
				return entity;
			}
		}
		Debug.LogError("Not find");
		return new EntityData();
	}
	// Update is called once per frame

	bool init = false;
	void Update()
	{
		if (GameManager.instance.isDie) return;
		if (!init && GameManager.instance.isInit)
		{
			init = true;
			InitDefaultEntitys();
		}
		if (GameManager.playerController.state == PlayerState.Hit) return;
		foreach (EntityData entity in entityList)
		{
			if (entity.entity.getCool > 0) return;
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Vector3 d = (entity.obj.transform.position - GameManager.playerController.transform.position);
				d.y = 0;
				if (entity.entity.canGet == true && d.magnitude < 0.5f && GameManager.playerController.currentItem == eItemType.None)
				{
					GameManager.playerController.PlayGetItemSound();
					NetworkConnect.instance.SendGetEntity(entity.number, false);
					break;

				}
			}
		}
	}
}
