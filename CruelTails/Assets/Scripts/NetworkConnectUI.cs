using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NetworkConnectUI : MonoBehaviour
{
	[SerializeField] InputField inputIP;

	public int port = 51235;
	public string CloudServerIP = "115.85.180.36";

	private void Start()
	{
		//NetworkConnect.instance.network.ConnectionSuccess += ConnectionSuccessEvent;
	}
	public void ConnectCloudServer()
	{
		Connect(CloudServerIP);
	}

	public void ConnectLocalServer()
	{
		Connect("127.0.0.1");
	}

	public void ConnevctIPServer()
	{
		Connect(inputIP.text.Trim());
	}

	void Connect(string ip)
	{
		bool success = NetworkConnect.instance.Connect(ip, port);
		if (success) ConnectionSuccessEvent();
		else ConnectionFailEvent();
	}

	void ConnectionSuccessEvent()
	{
		CloseConnectUI();
	}

	void ConnectionFailEvent()
	{
		MessageBoxManager.Instance.ShowMessageBox("���� ����");
	}
	public void CloseConnectUI()
	{
		gameObject.SetActive(false);
	}
}
