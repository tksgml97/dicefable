using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceSpeed : MonoBehaviour
{
    [SerializeField] float speed;
    Vector3 startPosition;
	// Start is called before the first frame update

	private void Awake()
	{
        startPosition = transform.position;
	}
	void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 playerPos = GameManager.playerController.transform.position;
        playerPos.y = 0;
        transform.position = startPosition - playerPos * speed;
    }
}
