using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkItem : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    [SerializeField] float drag = 0.8f;
    [SerializeField] Animator ani;
    Vector3 velocity = Vector3.zero;
    public bool canGet = true;

    public int id;
    public bool myEat = false;

    public float getCool = 0;

     public eItemType type;
    // Start is called before the first frame update

    // Update is called once per frame

    public void PlayAni()
	{
        ani.SetTrigger("Drop");
	}
    void Update()
    {
        if (getCool > 0) getCool -= Time.deltaTime;

        if(velocity != Vector3.zero)
        transform.position = Vector3.Lerp(transform.position, velocity, Mathf.Clamp01( Time.deltaTime * 3));
    }

    public static Vector3 GetPositionItemDrop(Vector3 start, Vector3 velocity)
	{
        List<Vector3> dir8 = new List<Vector3>();
		for (int i = 0; i < 8; i++)
		{
            dir8.Add(Quaternion.Euler(0, i * (360/8), 0) * velocity.normalized * 0.8f);
        }
        /*
        for (int i = 0; i < 8; i++)
        {
            dir8.Add(dir8[i]*0.5f);
        }*/

        Vector3 result = start;
        RaycastHit hit;
        bool isHit;

        foreach (var dir in dir8)
		{
            Vector3 dirVel = dir * velocity.magnitude;

            isHit = Physics.CheckSphere(start+dirVel, 0.3f, LayerMask.GetMask("Map", "Collider", "PortalCollider", "Item"));

            if (isHit)
            {
                Debug.Log($"hit{1}");
                continue;
            }
			
            isHit = Physics.Raycast(new Ray(start, dirVel), out hit, velocity.magnitude, LayerMask.GetMask("Map", "Collider"));

            if(isHit)
            {
                Debug.Log($"hit{2}");
                continue;
			}

            result = start + dirVel;
            break;
            
        }

        return result;

	}

    public void SetVelocity(Vector3 velocity)
	{
        if (velocity == Vector3.zero)
        {
            this.velocity = transform.position;
        }
        RaycastHit hit;


        this.velocity = GetPositionItemDrop(transform.position, velocity);
        //bool isHit = Physics.SphereCast(transform.position, 0.5f, velocity, out hit, velocity.magnitude, LayerMask.GetMask("Default", "Map", "Collider"));
        /*
        bool isHit =  Physics.Raycast(new Ray(transform.position, velocity), out hit , velocity.magnitude, LayerMask.GetMask("Default", "Map", "Collider"));
        
        if(isHit)
		{
            Debug.Log("ItemHit"+hit.point);
            this.velocity = Vector3.Lerp(transform.position,  hit.point, 0.8f);
        }
		else
		{
            this.velocity = transform.position + velocity;
        }
        bool isPortalHit = false;
        do
        {
            isPortalHit = Physics.CheckSphere(this.velocity, 0.5f, LayerMask.GetMask("PortalCollider"));
            if (isPortalHit)
            {
                this.velocity.z += 0.5f;
            }
        } while (isPortalHit);
        */

    }
    public virtual void SetType(eItemType type)
    {
        this.type = type;
        spriteRenderer.sprite = KoreaItemResourceLoader.Instance.GetSprite(type);
    }
    public virtual void Eat(GameObject player)
    {

    }
}
