using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class ReadyViewUI : MonoBehaviour
{
    [SerializeField] Animator countAnimator;
    [SerializeField] GameObject count;


    // Start is called before the first frame update
    public void Start()
    {
        GameManager.instance.gameInitEvent += Init;
    }
    void Init()
    {

    }
    // Update is called once per frame
    void Update()
    {
        
    }

    [Button("Play")]
    public void PlayAnimation()
	{
        count.SetActive(true);
        countAnimator.SetTrigger("Count");

    }
}
