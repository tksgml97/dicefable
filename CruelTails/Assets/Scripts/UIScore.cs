using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIScore : MonoBehaviour
{
    [SerializeField] Text red;
    [SerializeField] Text blue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void UpdateScore(int r, int b)
	{
        red.text = r.ToString();
        blue.text = b.ToString();
    }
}
