using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
public class MissionUI : MonoBehaviour
{
    [SerializeField] Color activeColor;
    [SerializeField] Color deactiveColor;
    [SerializeField] Color claerColor;

    [SerializeField] Text missionScript;
    [SerializeField] GameObject missionListView;
    [SerializeField] GameObject missionGageView;
    [SerializeField] Image progressBar;
    [SerializeField] GameObject missionTextPrefab;
    [SerializeField] Transform textHolder;

    [SerializeField] float textHeight = 10f;


    StringBuilder sb = new StringBuilder();

    List<MissionTextUI> missionTexts = new List<MissionTextUI>();
	// Start is called before the first frame update

	private void Awake()
	{

    }
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateMissionList(List<Mission> missions)
	{
		for (int i = 0; i < missionTexts.Count; i++)
		{
            Destroy(missionTexts[i].gameObject);
        }
        missionTexts.Clear();
		for (int i = 0; i < missions.Count; i++)
		{
            if (missions[i].missionState == MissionState.Disable) continue;
            GameObject go = Instantiate(missionTextPrefab, textHolder);
            go.transform.localPosition = new Vector3(0, textHeight * missionTexts.Count, 0);
            MissionTextUI ui = go.GetComponent<MissionTextUI>();
            ui.missionInst = missions[i];
            missionTexts.Add(ui);

        }


        missionListView.SetActive(missions.Count > 0);
		for (int i = 0; i < missionTexts.Count; i++)
        {
            //if (missions[i].missionState == MissionState.Disable) continue;
            bool isClear = false;
            switch (missionTexts[i].missionInst.missionState)
			{
                case MissionState.Disable:
                    missionTexts[i].SetColor(deactiveColor);
                    break;
                case MissionState.Claer:
                    missionTexts[i].SetColor(claerColor);
                    isClear = true;
                    break;
                case MissionState.Active:
                    missionTexts[i].SetColor(activeColor);
                    break;
                default:
                    break;

            }
            missionTexts[i].SetClear(isClear);
            string script =  GameDataLoader.instance.GetScript(missionTexts[i].missionInst.data.m_missionTextScriptID).m_scriptResource;
            missionTexts[i].SetText(script);

        }


    }
    public void UpdateUIJob(float gage, WorkData data)
    {
        missionGageView.SetActive(gage > 0);
        progressBar.fillAmount = gage;
        missionScript.text = GameDataLoader.instance.GetScript(data.m_script).m_scriptResource;
    }
    public void UpdateUI(float gage, MissionData data)
	{
        missionGageView.SetActive(gage > 0);
        progressBar.fillAmount = gage;
        missionScript.text = GameDataLoader.instance.GetScript(data.m_missionProgScriptID).m_scriptResource;
    }
}
